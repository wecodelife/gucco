import 'dart:async';

import 'package:guoco_priv_app/src/models/attach_preference_response_model.dart';
import 'package:guoco_priv_app/src/models/birthday_request_model.dart';
import 'package:guoco_priv_app/src/models/birthday_response_model.dart';
import 'package:guoco_priv_app/src/models/check_password_strength_response_model.dart';
import 'package:guoco_priv_app/src/models/create_password_request.dart';
import 'package:guoco_priv_app/src/models/create_password_response.dart';
import 'package:guoco_priv_app/src/models/detach_preference_response_model.dart';
import 'package:guoco_priv_app/src/models/forgot_password_response_model.dart';
import 'package:guoco_priv_app/src/models/get_preference_response_model.dart';
import 'package:guoco_priv_app/src/models/get_reference_code_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_appointments_request.dart';
import 'package:guoco_priv_app/src/models/key_collection_appointments_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_booking_request_model.dart';
import 'package:guoco_priv_app/src/models/key_collection_booking_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_confirm_request.dart';
import 'package:guoco_priv_app/src/models/key_collection_confirm_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_self_request_model.dart';
import 'package:guoco_priv_app/src/models/key_collection_self_response.dart';
import 'package:guoco_priv_app/src/models/login_request_model.dart';
import 'package:guoco_priv_app/src/models/login_response_model.dart';
import 'package:guoco_priv_app/src/models/privilege_club_t_and_c_accept_1_response.dart';
import 'package:guoco_priv_app/src/models/privilege_club_t_and_c_decline_1_response.dart';
import 'package:guoco_priv_app/src/models/privilege_club_t_and_c_decline_2_response.dart';
import 'package:guoco_priv_app/src/models/privlege_club_t_and_c_accept_2_response.dart';
import 'package:guoco_priv_app/src/models/reference_code_check_response_model.dart';
import 'package:guoco_priv_app/src/models/register_privilege_club_otp_response.dart';
import 'package:guoco_priv_app/src/models/register_privilege_otp_generate.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step1_request.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step1_response.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step2_request.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step2_response.dart';
import 'package:guoco_priv_app/src/models/state.dart';
import 'package:guoco_priv_app/src/models/user_profile_response.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/validators.dart';

import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class AuthBloc extends Object with Validators implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<LoginResponse> _login =
      new StreamController<LoginResponse>.broadcast();
  StreamController<ForgotPasswordResponseModel> _forgotPassword =
      new StreamController<ForgotPasswordResponseModel>.broadcast();

  StreamController<AttachPreferenceResponse> _attachPreference =
      new StreamController<AttachPreferenceResponse>.broadcast();

  StreamController<GetPreferenceResponse> _getPreference =
      new StreamController<GetPreferenceResponse>.broadcast();
  StreamController<CheckPasswordStrengthResponseModel> _checkpasswordStrength =
      new StreamController<CheckPasswordStrengthResponseModel>.broadcast();

  StreamController<DetachPreferenceResponse> _detachPreference =
      new StreamController<DetachPreferenceResponse>.broadcast();

  StreamController<GetReferenceCodeResponse> _getReferenceCode =
      new StreamController<GetReferenceCodeResponse>.broadcast();

  StreamController<ReferenceCodeCheckResponse> _referenceCodeCheck =
      new StreamController<ReferenceCodeCheckResponse>.broadcast();

  StreamController<RegisterUserDetailsStep1Response> _registerUserDetailsStep1 =
      new StreamController<RegisterUserDetailsStep1Response>.broadcast();

  StreamController<RegisterUserDetailsStep2Response> _registerUserDetailsStep2 =
      new StreamController<RegisterUserDetailsStep2Response>.broadcast();

  StreamController<PrivilegeClubTAndCAccept1Response>
      _privilegeClubTAndCAccept1 =
      new StreamController<PrivilegeClubTAndCAccept1Response>.broadcast();

  StreamController<PrivilegeClubTAndCDecline1Response>
      _privilegeClubTAndCDecline1 =
      new StreamController<PrivilegeClubTAndCDecline1Response>.broadcast();

  StreamController<PrivilegeClubTAndCAccept2Response>
      _privilegeClubTAndCAccept2 =
      new StreamController<PrivilegeClubTAndCAccept2Response>.broadcast();

  StreamController<PrivilegeClubTAndCDecline2Response>
      _privilegeClubTAndCDecline2 =
      new StreamController<PrivilegeClubTAndCDecline2Response>.broadcast();

  StreamController<RegisterPrivilegeClubOtpResponse> _registerprivilegeClubOtp =
      new StreamController<RegisterPrivilegeClubOtpResponse>.broadcast();

  StreamController<BirthdayResponse> _birthday =
      new StreamController<BirthdayResponse>.broadcast();

  StreamController<RegisterPrivilegeClubOtpGenerateResponse>
      _registerPrivilegeClubOtpGenerate = new StreamController<
          RegisterPrivilegeClubOtpGenerateResponse>.broadcast();

  StreamController<CreatePasswordResponse> _createPasswordController =
      new StreamController<CreatePasswordResponse>.broadcast();

  StreamController<ProfileResponseModel> _getProfile =
      new StreamController<ProfileResponseModel>.broadcast();

  StreamController<KeyCollectionAppointmentsResponse>
      _keyCollectionAppointments =
      new StreamController<KeyCollectionAppointmentsResponse>.broadcast();

  StreamController<KeyCollectionBookingResponse> _keyCollectionBooking =
      new StreamController<KeyCollectionBookingResponse>.broadcast();

  StreamController<KeyCollectionConfirmResponse> _keyCollectionConfirm =
      new StreamController<KeyCollectionConfirmResponse>.broadcast();

  StreamController<KeyCollectionSelfResponse> _keyCollectionSelf =
      new StreamController<KeyCollectionSelfResponse>.broadcast();

  // ignore: close_sinks

  // stream controller is broadcasting the  details

  Stream<LoginResponse> get loginResponse => _login.stream;
  Stream<ForgotPasswordResponseModel> get forgotPasswordResponse =>
      _forgotPassword.stream;
  Stream<AttachPreferenceResponse> get attachPreferenceResponse =>
      _attachPreference.stream;
  Stream<GetPreferenceResponse> get getPreferenceResponse =>
      _getPreference.stream;
  Stream<DetachPreferenceResponse> get detachPreferenceResponse =>
      _detachPreference.stream;
  Stream<GetReferenceCodeResponse> get getReferenceCodeResponse =>
      _getReferenceCode.stream;
  Stream<ReferenceCodeCheckResponse> get referenceCodeCheckResponse =>
      _referenceCodeCheck.stream;
  Stream<CheckPasswordStrengthResponseModel>
      get checkPasswordStrengthResponse => _checkpasswordStrength.stream;

  Stream<RegisterUserDetailsStep1Response>
      get registerUserDetailsStep1Response => _registerUserDetailsStep1.stream;

  Stream<RegisterUserDetailsStep2Response>
      get registerUserDetailsStep2Response => _registerUserDetailsStep2.stream;
  Stream<PrivilegeClubTAndCAccept1Response>
      get privilegeClubTAndCAccept1Response =>
          _privilegeClubTAndCAccept1.stream;

  Stream<PrivilegeClubTAndCDecline1Response>
      get privilegeClubTAndCDecline1Response =>
          _privilegeClubTAndCDecline1.stream;

  Stream<PrivilegeClubTAndCAccept2Response>
      get privilegeClubTAndCAccept2Response =>
          _privilegeClubTAndCAccept2.stream;

  Stream<PrivilegeClubTAndCDecline2Response>
      get privilegeClubTAndCDecline2Response =>
          _privilegeClubTAndCDecline2.stream;

  Stream<RegisterPrivilegeClubOtpResponse>
      get registerPrivilegeClubOtpResponse => _registerprivilegeClubOtp.stream;

  Stream<BirthdayResponse> get birthdayResponse => _birthday.stream;

  Stream<RegisterPrivilegeClubOtpGenerateResponse>
      get registerPrivilegeClubOtpGenerate =>
          _registerPrivilegeClubOtpGenerate.stream;

  Stream<CreatePasswordResponse> get createPasswordStreamResponse =>
      _createPasswordController.stream;

  Stream<ProfileResponseModel> get getProfile => _getProfile.stream;

  Stream<KeyCollectionAppointmentsResponse>
      get keyCollectionAppointmentsResponse =>
          _keyCollectionAppointments.stream;

  Stream<KeyCollectionBookingResponse> get keyCollectionBookingResponse =>
      _keyCollectionBooking.stream;

  Stream<KeyCollectionConfirmResponse> get keyCollectionConfirmResponse =>
      _keyCollectionConfirm.stream;

  Stream<KeyCollectionSelfResponse> get keyCollectionSelfResponse =>
      _keyCollectionSelf.stream;

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  StreamSink<bool> get loadingSink => _loading.sink;

  login({LoginRequest loginRequest}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.login(
        loginRequest: LoginRequest(
            username: loginRequest.username, password: loginRequest.password));

    if (state is SuccessState) {
      loadingSink.add(false);
      _login.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _login.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  forgotPassword({String email}) async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.forgotPassword(email: email);

    if (state is SuccessState) {
      loadingSink.add(false);
      _forgotPassword.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _forgotPassword.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  attachPreference({String preferenceId, String userId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .attachPreference(preferenceId: preferenceId, userId: userId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _attachPreference.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _attachPreference.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getPreference() async {
    loadingSink.add(true);

    State state = await ObjectFactory().repository.getPreference();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getPreference.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getPreference.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  checkPasswordStrength({String password}) async {
    loadingSink.add(true);

    State state = await ObjectFactory()
        .repository
        .checkPasswordStrength(password: password);

    if (state is SuccessState) {
      loadingSink.add(false);
      _checkpasswordStrength.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _checkpasswordStrength.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  detachPreference({String preferenceId, String userId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .detachPreference(preferenceId: preferenceId, userId: userId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _detachPreference.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _detachPreference.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  referenceCodeCheck({String referenceCode}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .loyaltymanagement_referencecode_validate(referenceCode: referenceCode);

    if (state is SuccessState) {
      loadingSink.add(false);
      _referenceCodeCheck.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _referenceCodeCheck.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  registerUserDetailsStep1(
      {RegisterUserDetailsStep1Request registerUserDetailsStep1Request}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.registerUserDetailsStep1(
        registerUserDetailsStep1Request: RegisterUserDetailsStep1Request(
            salutation: registerUserDetailsStep1Request.salutation,
            firstname: registerUserDetailsStep1Request.firstname,
            lastname: registerUserDetailsStep1Request.lastname,
            contactno: registerUserDetailsStep1Request.contactno,
            email: registerUserDetailsStep1Request.email));

    if (state is SuccessState) {
      loadingSink.add(false);
      _registerUserDetailsStep1.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _registerUserDetailsStep1.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  registerUserDetailsStep2(
      {RegisterUserDetailsStep2Request registerUserDetailsStep2Request,
      String userId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.registerUserDetailsStep2(
        registerUserDetailsStep2Request: RegisterUserDetailsStep2Request(
          block: registerUserDetailsStep2Request.block,
          postalcode: registerUserDetailsStep2Request.postalcode,
          unit: registerUserDetailsStep2Request.unit,
          zone: registerUserDetailsStep2Request.zone,
        ),
        userId: userId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _registerUserDetailsStep2.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _registerUserDetailsStep2.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  privilegeClubTAndCAccept1({String userId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .privilegeClubTAndCAccept1(userId: userId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _privilegeClubTAndCAccept1.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _privilegeClubTAndCAccept1.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  privilegeClubTAndCDecline1({String userId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .privilegeClubTAndCDecline1(userId: userId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _privilegeClubTAndCDecline1.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _privilegeClubTAndCDecline1.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  privilegeClubTAndCAccept2({String userId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .privilegeClubTAndCAccept2(userId: userId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _privilegeClubTAndCAccept2.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _privilegeClubTAndCAccept2.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  privilegeClubTAndCDecline2({String userId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .privilegeClubTAndCDecline2(userId: userId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _privilegeClubTAndCDecline2.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _privilegeClubTAndCDecline2.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  registerPrivilegeClubOtp({String userId, String otpCode}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .registerPrivilegeClubOtp(otpCode: otpCode, userId: userId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _registerprivilegeClubOtp.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _registerprivilegeClubOtp.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  birthday({BirthdayRequest birthdayRequest, String userId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .birthday(birthdayRequest: birthdayRequest, userId: userId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _birthday.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _birthday.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  registerPrivilegeClubGenerateOtp() async {
    loadingSink.add(true);
    State state =
        await ObjectFactory().repository.registerPrivilegeClubOtpGenerate();

    if (state is SuccessState) {
      loadingSink.add(false);
      _registerPrivilegeClubOtpGenerate.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _registerPrivilegeClubOtpGenerate.sink
          .addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  createPassword({CreatePasswordRequest createPasswordRequest}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .createPassword(createPasswordRequest: createPasswordRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _createPasswordController.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _createPasswordController.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getUserProfile() async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.getProfile();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getProfile.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getProfile.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  keyCollectionAppointments(
      {KeyCollectionAppointmentsRequest
          keyCollectionAppointmentsRequest}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.keyCollectionAppointments(
        keyCollectionAppointmentsRequest: keyCollectionAppointmentsRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _keyCollectionAppointments.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _keyCollectionAppointments.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  keyCollectionBooking(
      {KeyCollectionBookingRequest keyCollectionBookingRequest}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.keyCollectionBooking(
        keyCollectionBookingRequest: keyCollectionBookingRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _keyCollectionBooking.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _keyCollectionBooking.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  keyCollectionConfirm(
      {KeyCollectionConfirmRequest keyCollectionConfirmRequest}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.keyCollectionConfirm(
        keyCollectionConfirmRequest: keyCollectionConfirmRequest);

    if (state is SuccessState) {
      loadingSink.add(false);
      _keyCollectionConfirm.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _keyCollectionConfirm.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  keyCollectionSelf(
      {KeyCollectionSelfRequest keyCollectionSelfRequest,
      String unitId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.keyCollectionSelf(
        keyCollectionSelfRequest: keyCollectionSelfRequest, unitId: unitId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _keyCollectionSelf.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _keyCollectionSelf.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _login?.close();
    _attachPreference.close();
    _getPreference.close();
    _detachPreference.close();
    _getReferenceCode.close();
    _referenceCodeCheck.close();
    _registerUserDetailsStep1.close();
    _registerUserDetailsStep2.close();
    _privilegeClubTAndCAccept1.close();
    _privilegeClubTAndCDecline1.close();
    _privilegeClubTAndCAccept2.close();
    _privilegeClubTAndCDecline2.close();
    _registerprivilegeClubOtp.close();
    _birthday.close();
    _registerPrivilegeClubOtpGenerate.close();
    _createPasswordController.close();
    _getProfile.close();
    _keyCollectionAppointments.close();
    _keyCollectionBooking.close();
    _keyCollectionConfirm.close();
    _keyCollectionSelf.close();
    _forgotPassword.close();
    _checkpasswordStrength.close();
  }
}

AuthBloc authBlocSingle = AuthBloc();
