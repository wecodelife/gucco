import 'dart:async';

import 'package:guoco_priv_app/src/models/add_unit_login_martin_modern_request.dart';
import 'package:guoco_priv_app/src/models/add_unit_login_martin_modern_response.dart';
import 'package:guoco_priv_app/src/models/book_a_facility_request_model.dart';
import 'package:guoco_priv_app/src/models/book_a_facility_response_model.dart';
import 'package:guoco_priv_app/src/models/cancelAFacilityBookingResponse.dart';
import 'package:guoco_priv_app/src/models/change_password_for_user_request_model.dart';
import 'package:guoco_priv_app/src/models/change_password_for_user_response_model.dart';
import 'package:guoco_priv_app/src/models/condo_info_response.dart';
import 'package:guoco_priv_app/src/models/earliest_slot_key_collection_request.dart';
import 'package:guoco_priv_app/src/models/earliest_slot_key_collection_response.dart';
import 'package:guoco_priv_app/src/models/getAllFacilitiesInACondoMiniumResponseModel.dart';
import 'package:guoco_priv_app/src/models/get_blockid_response.dart';
import 'package:guoco_priv_app/src/models/get_current_booking_for_facility_response_model.dart';
import 'package:guoco_priv_app/src/models/get_floor_id_response.dart';
import 'package:guoco_priv_app/src/models/get_key_collection_appointments_response_model.dart';
import 'package:guoco_priv_app/src/models/get_past_bookings_for_a_facility_response.dart';
import 'package:guoco_priv_app/src/models/image_upload_request_model.dart';
import 'package:guoco_priv_app/src/models/image_upload_response_model.dart';
import 'package:guoco_priv_app/src/models/key_collection_appointments_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_cancel_appointment_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_time_slot_response.dart';
import 'package:guoco_priv_app/src/models/payment_status_rseponse.dart';
import 'package:guoco_priv_app/src/models/share_text_response_model.dart';
import 'package:guoco_priv_app/src/models/state.dart';
import 'package:guoco_priv_app/src/models/timeSlotsForFacilityRequest.dart';
import 'package:guoco_priv_app/src/models/time_slots_for_facilities_resposne.dart';
import 'package:guoco_priv_app/src/models/unit_users_response.dart';
import 'package:guoco_priv_app/src/models/user_profile_response.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/validators.dart';

import 'base_bloc.dart';

/// api response of login is managed by AuthBloc
/// stream data is handled by StreamControllers

class EstateBloc extends Object with Validators implements BaseBloc {
  StreamController<bool> _loading = new StreamController<bool>.broadcast();

  StreamController<CondoInfoResponse> _getCondoInfo =
      new StreamController<CondoInfoResponse>.broadcast();

  StreamController<UnitUsersResponse> _getUnitUsers =
      new StreamController<UnitUsersResponse>.broadcast();

  StreamController<AddUnitLoginMartinModernResponse> _addUnitLoginMartinModern =
      new StreamController<AddUnitLoginMartinModernResponse>.broadcast();

  StreamController<GetBlockIdResponse> _getBlockId =
      new StreamController<GetBlockIdResponse>.broadcast();

  StreamController<GetFloorIdResponse> _getFloorId =
      new StreamController<GetFloorIdResponse>.broadcast();

  StreamController<KeyCollectionTimeSlotsResponse> _getKeyCollectionTimeSlots =
      new StreamController<KeyCollectionTimeSlotsResponse>.broadcast();

  StreamController<KeyCollectionAppointmentsResponse>
      _getKeyCollectionAppointments =
      new StreamController<KeyCollectionAppointmentsResponse>.broadcast();

  StreamController<KeyCollectionCancelAppointment>
      _cancelKeyCollectionAppointments =
      new StreamController<KeyCollectionCancelAppointment>.broadcast();

  StreamController<ProfileResponseModel> _getMartinModernProfile =
      new StreamController<ProfileResponseModel>.broadcast();

  StreamController<UploadProfileImageResponseModel> _uploadProfileImage =
      new StreamController<UploadProfileImageResponseModel>.broadcast();

  StreamController<PaymentStatusResponse> _checkPaymentStatus =
      new StreamController<PaymentStatusResponse>.broadcast();

  StreamController<ChangePasswordForUserResponseModel> _changePasswordForUser =
      new StreamController<ChangePasswordForUserResponseModel>.broadcast();

  StreamController<ShareTextResponse> _shareTextKeyCollection =
      new StreamController<ShareTextResponse>.broadcast();

  StreamController<GetKeyCollectionAppointmentResponse>
      _getKeyCollectionAppointment =
      new StreamController<GetKeyCollectionAppointmentResponse>.broadcast();
  StreamController<EarliestSlotKeyCollectionResponse>
      _getEarliestSlotKeyCollection =
      new StreamController<EarliestSlotKeyCollectionResponse>.broadcast();
  StreamController<GetAllFacilitiesInACondoMiniumResponse>
      _getAllFacilitiesInACondominium =
      new StreamController<GetAllFacilitiesInACondoMiniumResponse>.broadcast();

  StreamController<TimeSlotForFacilitiesResponse> _timeSlotsForFacility =
      new StreamController<TimeSlotForFacilitiesResponse>.broadcast();
  StreamController<BookAFacilityResponse> _bookAFacility =
      new StreamController<BookAFacilityResponse>.broadcast();

  StreamController<GetCurrentBookingsForFacilityResponse>
      _getCurrentBookingsForFacility =
      new StreamController<GetCurrentBookingsForFacilityResponse>.broadcast();
  StreamController<GetPastBookingsForFacilityResponse>
      _getPastBookingsForFacility =
      new StreamController<GetPastBookingsForFacilityResponse>.broadcast();
  StreamController<CancelAFacilityBookingResponse> _cancelAFacilityBooking =
      new StreamController<CancelAFacilityBookingResponse>.broadcast();

  // ignore: close_sinks

  // stream controller is broadcasting the  details

  Stream<CondoInfoResponse> get getCondoInfo => _getCondoInfo.stream;

  Stream<UnitUsersResponse> get getUnitUser => _getUnitUsers.stream;

  Stream<AddUnitLoginMartinModernResponse>
      get addUnitLoginMartinModernResponse => _addUnitLoginMartinModern.stream;

  Stream<GetBlockIdResponse> get getBlockIdResponse => _getBlockId.stream;

  Stream<GetFloorIdResponse> get getFloorIdResponse => _getFloorId.stream;

  Stream<KeyCollectionTimeSlotsResponse> get getKeyCollectionTimeSlotResponse =>
      _getKeyCollectionTimeSlots.stream;

  Stream<KeyCollectionAppointmentsResponse>
      get getKeyCollectionAppointmentsResponse =>
          _getKeyCollectionAppointments.stream;

  Stream<KeyCollectionCancelAppointment>
      get cancelKeyCollectionAppointmentsResponse =>
          _cancelKeyCollectionAppointments.stream;

  Stream<ProfileResponseModel> get getMartinModernProfileResponse =>
      _getMartinModernProfile.stream;

  Stream<UploadProfileImageResponseModel> get uploadProfileImageResponse =>
      _uploadProfileImage.stream;

  Stream<PaymentStatusResponse> get checkPaymentStatusResponse =>
      _checkPaymentStatus.stream;

  Stream<ChangePasswordForUserResponseModel>
      get changePasswordForUserResponse => _changePasswordForUser.stream;

  Stream<ShareTextResponse> get shareTextKeyCollectionResponse =>
      _shareTextKeyCollection.stream;

  Stream<GetKeyCollectionAppointmentResponse>
      get getKeyCollectionAppointmentResponse =>
          _getKeyCollectionAppointment.stream;

  Stream<EarliestSlotKeyCollectionResponse>
      get getEarliestSlotKeyCollectionResponse =>
          _getEarliestSlotKeyCollection.stream;

  Stream<GetAllFacilitiesInACondoMiniumResponse>
      get getAllFacilitiesInACondominiumResponse =>
          _getAllFacilitiesInACondominium.stream;
  Stream<TimeSlotForFacilitiesResponse> get timeSlotsForFacilitiesResponse =>
      _timeSlotsForFacility.stream;

  Stream<BookAFacilityResponse> get bookAFacilityResponse =>
      _bookAFacility.stream;

  Stream<GetCurrentBookingsForFacilityResponse>
      get getCurrentBookingsForFacilityResponse =>
          _getCurrentBookingsForFacility.stream;

  Stream<GetPastBookingsForFacilityResponse>
      get getPastBookingsForFacilityResponse =>
          _getPastBookingsForFacility.stream;
  Stream<CancelAFacilityBookingResponse> get cancelAFacilityBookingResponse =>
      _cancelAFacilityBooking.stream;

  /// stream for progress bar
  Stream<bool> get loadingListener => _loading.stream;

  StreamSink<bool> get loadingSink => _loading.sink;

  getCondosInfo() async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.getCondoInfo();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getCondoInfo.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getCondoInfo.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getUnitUsers() async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.getUnitUsers();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getUnitUsers.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getUnitUsers.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  addUnitLoginMartinModern(
      {AddUnitLoginMartinModernRequest addUnitLoginMartinModernRequest}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.addUnitLoginMartinModern(
        addUnitLoginMartinModernRequest: AddUnitLoginMartinModernRequest(
            unitpassword: addUnitLoginMartinModernRequest.unitpassword,
            unitusername: addUnitLoginMartinModernRequest.unitusername,
            owner: addUnitLoginMartinModernRequest.owner));

    if (state is SuccessState) {
      loadingSink.add(false);
      _addUnitLoginMartinModern.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _addUnitLoginMartinModern.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  // getBlockId() async {
  //   loadingSink.add(true);
  //   State state = await ObjectFactory().repository.getBlockId();
  //
  //   if (state is SuccessState) {
  //     loadingSink.add(false);
  //     _getBlockId.sink.add(state.value);
  //   } else if (state is ErrorState) {
  //     loadingSink.add(false);
  //     _getBlockId.sink.addError(Constants.SOME_ERROR_OCCURRED);
  //   }
  // }

  // getFloorId() async {
  //   loadingSink.add(true);
  //   State state = await ObjectFactory().repository.getFloorId();
  //
  //   if (state is SuccessState) {
  //     loadingSink.add(false);
  //     _getFloorId.sink.add(state.value);
  //   } else if (state is ErrorState) {
  //     loadingSink.add(false);
  //     _getFloorId.sink.addError(Constants.SOME_ERROR_OCCURRED);
  //   }
  // }

  getKeyCollectionTimeSlots(String date, String unitId) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .getKeyCollectionTimeSlots(date, unitId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getKeyCollectionTimeSlots.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getKeyCollectionTimeSlots.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getKeyCollectionAppointments(String unitId) async {
    loadingSink.add(true);
    State state =
        await ObjectFactory().repository.getKeyCollectionAppointments(unitId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getKeyCollectionAppointments.sink.add(state.value);
      print(state.value.toString());
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getKeyCollectionAppointments.sink
          .addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  cancelKeyCollectionAppointments(String unitId, String keyId) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .cancelKeyCollectionAppointments(unitId, keyId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _cancelKeyCollectionAppointments.sink.add(state.value);
      print(state.value.toString());
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _cancelKeyCollectionAppointments.sink
          .addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getMartinModernProfile() async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.getProfile();

    if (state is SuccessState) {
      loadingSink.add(false);
      _getMartinModernProfile.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getMartinModernProfile.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  uploadProfileImage({ImageUploadRequestModel imageUploadRequestModel}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.uploadProfileImage(
        imageUploadRequestModel: ImageUploadRequestModel(
            category: imageUploadRequestModel.category,
            file: imageUploadRequestModel.file));

    if (state is SuccessState) {
      loadingSink.add(false);
      _uploadProfileImage.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _uploadProfileImage.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  checkPaymentStatus(
      {String condoInfoId,
      String floorId,
      String blockId,
      String unitId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .checkPaymentStatus(condoInfoId, floorId, blockId, unitId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _checkPaymentStatus.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _checkPaymentStatus.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  changePasswordForUser(
      {ChangePasswordForUserRequestModel
          changePasswordForUserRequestModel}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.changePasswordForUser(
        changePasswordForUserRequestModel: ChangePasswordForUserRequestModel(
            newpassword: changePasswordForUserRequestModel.newpassword,
            oldpassword: changePasswordForUserRequestModel.oldpassword));

    if (state is SuccessState) {
      loadingSink.add(false);
      _changePasswordForUser.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _changePasswordForUser.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  shareTextKeyCollection(String keyCollectionAppointmentId) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .shareTextKeyCollectionAppointment(
            keyCollectionAppointmentId: keyCollectionAppointmentId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _shareTextKeyCollection.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _shareTextKeyCollection.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getKeyCollectionAppointment({String keyCollectionAppointmentId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.getKeyCollectionAppointment(
        keyCollectionAppointmentId: keyCollectionAppointmentId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getKeyCollectionAppointment.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getKeyCollectionAppointment.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  earliestSlotKeyCollection(
      {EarliestSlotKeyCollectionRequest earliestSlotKeyCollectionRequest,
      String unitInfoId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.earliestSlotKeyCollection(
        unitInfoId: unitInfoId,
        earliestSlotKeyCollectionRequest: EarliestSlotKeyCollectionRequest(
            appointmentfromtime:
                earliestSlotKeyCollectionRequest.appointmentfromtime,
            appointmentondate:
                earliestSlotKeyCollectionRequest.appointmentondate));

    if (state is SuccessState) {
      loadingSink.add(false);
      _getEarliestSlotKeyCollection.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getEarliestSlotKeyCollection.sink
          .addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getAllFacilitiesInACondominium({String condoInfoId}) async {
    loadingSink.add(true);
    State state =
        await ObjectFactory().repository.getAllFacilitiesInACondominium(
              condoInfoId: condoInfoId,
            );

    if (state is SuccessState) {
      loadingSink.add(false);
      _getAllFacilitiesInACondominium.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getAllFacilitiesInACondominium.sink
          .addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  timeSlotsForFacilities(
      {String facilitiesInfoId,
      String onDate,
      TimeSlotsForFacilitiesRequest timeSlotsForFacilitiesRequest}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.timeSlotsForFacilities(
        onDate: onDate,
        facilitiesInfoId: facilitiesInfoId,
        timeSlotsForFacilitiesRequest: TimeSlotsForFacilitiesRequest(
            unitinfoId: timeSlotsForFacilitiesRequest.unitinfoId,
            userId: timeSlotsForFacilitiesRequest.userId));

    if (state is SuccessState) {
      loadingSink.add(false);

      _timeSlotsForFacility.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _timeSlotsForFacility.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  bookAFacility(
      {BookAFacilityRequest bookAFacilityRequest,
      String facilityInfoId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory().repository.bookAFacility(
        bookAFacilityRequest: BookAFacilityRequest(
          additionalnotes: bookAFacilityRequest.additionalnotes,
          starttime: bookAFacilityRequest.starttime,
          endtime: bookAFacilityRequest.endtime,
          facilityslotid: bookAFacilityRequest.facilityslotid,
          ondate: bookAFacilityRequest.ondate,
          unitinfoid: bookAFacilityRequest.unitinfoid,
        ),
        facilityInfoId: facilityInfoId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _bookAFacility.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _bookAFacility.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getCurrentBookingsForFacility({String unitInfoId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .getCurrentBookingsForFacility(unitInfoId: unitInfoId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getCurrentBookingsForFacility.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getCurrentBookingsForFacility.sink
          .addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  getPastBookingsForFacility({String unitInfoId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .getPastBookingsForFacility(unitInfoId: unitInfoId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _getPastBookingsForFacility.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _getPastBookingsForFacility.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  cancelAFacilityBooking({String facilityBookingId}) async {
    loadingSink.add(true);
    State state = await ObjectFactory()
        .repository
        .cancelABookingFacility(facilityBookingId: facilityBookingId);

    if (state is SuccessState) {
      loadingSink.add(false);
      _cancelAFacilityBooking.sink.add(state.value);
    } else if (state is ErrorState) {
      loadingSink.add(false);
      _cancelAFacilityBooking.sink.addError(Constants.SOME_ERROR_OCCURRED);
    }
  }

  ///disposing the stream if it is not using
  @override
  void dispose() {
    _loading?.close();
    _getCondoInfo.close();
    _getUnitUsers.close();
    _addUnitLoginMartinModern.close();
    _getBlockId.close();
    _getFloorId.close();
    _getKeyCollectionTimeSlots.close();
    _getKeyCollectionAppointments.close();
    _cancelKeyCollectionAppointments.close();
    _getMartinModernProfile.close();
    _uploadProfileImage.close();
    _checkPaymentStatus.close();
    _changePasswordForUser.close();
    _shareTextKeyCollection.close();
    _getKeyCollectionAppointment.close();
    _getEarliestSlotKeyCollection.close();
    _getAllFacilitiesInACondominium.close();
    _timeSlotsForFacility.close();
    _bookAFacility.close();
    _getCurrentBookingsForFacility.close();
    _getPastBookingsForFacility.close();
    _cancelAFacilityBooking.close();
  }
}

EstateBloc estateBlocSingle = EstateBloc();
