import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_feild_address_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/grid_view.dart';
import 'package:guoco_priv_app/src/ui/widgets/home_tile.dart';
import 'package:guoco_priv_app/src/ui/widgets/search_box.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FavouritesPage extends StatefulWidget {
  @override
  _FavouritesPageState createState() => _FavouritesPageState();
}

class _FavouritesPageState extends State<FavouritesPage> {
  TextEditingController searchTextEditingController=new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: screenWidth(context,dividedBy: 1),
        height: screenHeight(context,dividedBy: 1),
        decoration: BoxDecoration(
          image:DecorationImage(
            image: AssetImage("assets/images/favourite_page_bg.png"),
            fit: BoxFit.cover
          ),



        ),

        child:Column(
          children: [
            SizedBox(
              height: screenHeight(context,dividedBy: 30),
            ),
            AppBarGuocoLand(pageTitle: "FAVOURITES",boxWidth:3.8,icon: true,),

            SizedBox(
              height: screenHeight(context,dividedBy: 15),
            ),

            Row(
              children: [
                SizedBox(
                  width: screenWidth(context,dividedBy: 20),
                ),
                Container(
                  height: screenHeight(context,dividedBy: 30),
                    width: screenWidth(context,dividedBy: 1.6),
                    child: SearchBox(hintText: "Search favourites",textEditingController:searchTextEditingController ,)),
                
                Image(image:AssetImage("assets/icons/search.png"),width: screenWidth(context,dividedBy:10 ),color: Colors.white, ),
                
                Text("SEARCH",style: TextStyle(color: Colors.white,fontFamily:'MuliSemiBold',fontSize: 20),),

              ],
            ),

         Container(
            height: screenHeight(context,dividedBy: 2),
             width: screenWidth(context,dividedBy: 1),
              child: CustomGridView(
                tile: HomeTile(width: 2.8,height: 10,
                  likeBar: true,likes: 2,
                  itemDescription:"Workspace Espresso Bar (Guoco Tower)",
                  itemImage: "assets/images/card_image.png",
                  boxHeight: 10,
                  borderRadius: 6
                ,),aspectoRatio: 2/2,
            
            


              ),
         ),

           ],
        ),


      ),
    );
  }
}
