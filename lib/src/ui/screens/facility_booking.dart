import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/timeSlotsForFacilityRequest.dart';
import 'package:guoco_priv_app/src/models/time_slots_for_facilities_resposne.dart';
import 'package:guoco_priv_app/src/ui/screens/facilities_details.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/booking_calender_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/time_slot_tile.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

class FacilitiesBooking extends StatefulWidget {
  final String facilitiesInfoId;
  final String blockName;
  final String floorAndUnitId;
  final String facilityName;
  FacilitiesBooking(
      {this.facilitiesInfoId,
      this.facilityName,
      this.blockName,
      this.floorAndUnitId});
  @override
  _FacilitiesBookingState createState() => _FacilitiesBookingState();
}

class _FacilitiesBookingState extends State<FacilitiesBooking> {
  EstateBloc estateBloc = new EstateBloc();
  bool timeSlotAvailability = false;
  int itemNumber;
  bool selected;
  String rule;
  DateTime dateSelected;
  String startTimeSelected;
  String startTimeEarliest;
  String endTimeSelected;
  String endTimeEarliest;
  String facilityName;
  bool isLoading;
  bool loadingTimeSlots;

  String facilitySlotId;
  bool firstLoading = true;
  @override
  void initState() {
    print("unitInfoId" + widget.floorAndUnitId);

    estateBloc.timeSlotsForFacilities(
        timeSlotsForFacilitiesRequest: TimeSlotsForFacilitiesRequest(
            unitinfoId: ObjectFactory().appHive.getUnitInfoId(),
            userId: ObjectFactory().appHive.getUserId()),
        onDate: new DateFormat('dd-MM-yyyy').format(DateTime.now()),
        facilitiesInfoId: widget.facilitiesInfoId);

    estateBloc.timeSlotsForFacilitiesResponse.listen((event) {
      if (event.statuscode == 200) {
        if (event.data.length > 0) {
          setState(() {
            timeSlotAvailability = true;
          });
        } else {
          setState(() {
            timeSlotAvailability = false;
          });
        }
      } else {
        showToast("NetworkError");
      }
    });

    itemNumber = 100;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Container(),
        backgroundColor: Constants.kitGradients[7],
        actions: [
          AppBarBookingPage(
            pageTitle: "FACILITIES",
            rightIcon: "assets/icons/estate_icon.png",
            leftIcon: "assets/icons/arrow_back.png",
            onTapLeftIcon: () {
              pop(context);
            },
            onTapRightIcon: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => EstatePage()),
                  (route) => true);
            },
          ),
        ],
      ),
      body: ListView(
        children: [
          BookingCalenderPage(
            towerName: widget.blockName,
            towerSubName: widget.floorAndUnitId,
            heading: "New Booking",
            icon: "assets/icons/keyCollectionIcon.png",
            rightIcon: false,
            valueChanged: (date) {
              setState(() {
                firstLoading = false;
              });
              dateSelected = date;
              estateBloc.timeSlotsForFacilities(
                timeSlotsForFacilitiesRequest: TimeSlotsForFacilitiesRequest(
                    userId: ObjectFactory().appHive.getUserId(),
                    unitinfoId: ObjectFactory().appHive.getUnitInfoId()),
                facilitiesInfoId: widget.facilitiesInfoId,
                onDate: new DateFormat('dd-MM-yyyy').format(dateSelected),
              );
            },
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20)),
            child: Row(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                Text(
                  timeSlotAvailability == true
                      ? "Available Time Slots"
                      : "No Time Slots Available",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16,
                      fontFamily: "sfProSemiBold"),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 30)),
            child: StreamBuilder<TimeSlotForFacilitiesResponse>(
                stream: estateBloc.timeSlotsForFacilitiesResponse,
                builder: (context, snapshot) {
                  return snapshot.hasData
                      ? firstLoading == false
                          ? timeSlotAvailability == true
                              ? GridView.builder(
                                  itemCount: snapshot.data.data.length,
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                          childAspectRatio: 2 / 1,
                                          crossAxisCount: 4),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    itemNumber == index
                                        ? selected = true
                                        : selected = false;

                                    return GestureDetector(
                                        onTap: () {
                                          itemNumber = index;
                                          setState(() {
                                            startTimeSelected = snapshot
                                                .data.data[index].starttime;
                                            startTimeEarliest = snapshot
                                                .data.data[index].starttime;
                                            endTimeSelected = snapshot
                                                .data.data[index].endtime;
                                            endTimeEarliest = snapshot
                                                .data.data[index].endtime;
                                            facilitySlotId =
                                                snapshot.data.data[index].id;
                                            print("faclitySlotId" +
                                                facilitySlotId);
                                          });
                                        },
                                        child: TimeSlotTile(
                                            selected: selected,
                                            deselected: false,
                                            time: time(
                                              snapshot
                                                  .data.data[index].starttime,
                                            )));
                                  },
                                )
                              : Container()
                          : Container()
                      : Shimmer.fromColors(
                          highlightColor: Constants.kitGradients[0],
                          baseColor: Constants.kitGradients[1],
                          child: Container(
                            height: screenHeight(context, dividedBy: 3.1),
                            width: screenWidth(context, dividedBy: 1),
                            decoration: BoxDecoration(color: Colors.black54),
                          ),
                        );
                }),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Row(
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 10),
              ),
              BuildButton(
                title: "Next",
                arrowIcon: false,
                isLoading: isLoading,
                onPressed: () {
                  if (startTimeSelected != null) {
                    push(
                        context,
                        FacilitiesDetails(
                            floorAndUnitId: widget.floorAndUnitId,
                            blockName: widget.blockName,
                            startTime: startTimeSelected,
                            endTime: endTimeSelected,
                            additionalNotes: "",
                            facilityInfoId: widget.facilitiesInfoId,
                            facilitySlotId: facilitySlotId,
                            onDate: dateSelected,
                            facilityName: widget.facilityName));
                  } else {
                    showToast("Please Select a time slot");
                  }
                },
                faultReport: true,
                disabled: timeSlotAvailability,
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
        ],
      ),
    );
  }
}
