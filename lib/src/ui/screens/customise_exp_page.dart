import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/create_password_page.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/preference_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class CustomiseExpPage extends StatefulWidget {
  @override
  _CustomiseExpPageState createState() => _CustomiseExpPageState();
}

class _CustomiseExpPageState extends State<CustomiseExpPage> {
  Future<bool> _willPopCallback() async {
    push(context, CreatePassword());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
          body: Stack(
        children: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 1),
            child: SvgPicture.asset("assets/images/login_bg_image.svg",
                fit: BoxFit.cover,
                color: Colors.black.withOpacity(0.60),
                colorBlendMode: BlendMode.darken),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 10),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 18)),
                    child: Container(
                      child: Text(
                        "Guoco Club Privileges App enhances your experience by featuring privileges that align with your interests.",
                        style: TextStyle(
                          height: 1.5,
                          color: Constants.kitGradients[0],
                          fontFamily: 'Muli',
                          fontSize: screenWidth(context, dividedBy: 16),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 10),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 18)),
                    child: Container(
                      child: Text(
                        "The next few steps will help us to curate content specifically for you.",
                        style: TextStyle(
                          height: 1.5,
                          color: Constants.kitGradients[0],
                          fontFamily: 'Muli',
                          fontSize: screenWidth(context, dividedBy: 16),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  BuildButton(
                    title: "CONTINUE",
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => PreferencePage()));
                    },
                    disabled: true,
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                ],
              )
            ],
          ),
        ],
      )),
    );
  }
}
