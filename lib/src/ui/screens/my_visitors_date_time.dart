import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/invite_visitors__details.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/home_key_collection_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/booking_calender_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/drop_down_label.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class MyVistorsDateTime extends StatefulWidget {
  @override
  _MyVistorsDateTimeState createState() => _MyVistorsDateTimeState();
}

class _MyVistorsDateTimeState extends State<MyVistorsDateTime> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: Container(),
        backgroundColor: Constants.kitGradients[7],
        actions: [
          AppBarBookingPage(
            pageTitle: "MY VISITORS",
            rightIcon: "assets/icons/estate_icon.png",
            leftIcon: "assets/icons/arrow_back.png",
            onTapLeftIcon: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => HomeKeyCollection()),
                  (route) => true);
            },
            onTapRightIcon: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
      body: ListView(
        children: [
          BookingCalenderPage(
            towerName: "Tower 01",
            towerSubName: "04-01",
            heading: "Event: Invite Visitor",
          ),
          DropDownLabel(
            label: "From Time",
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 100),
          ),
          DropDownLabel(
            label: "To Time",
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 11.5),
          ),
          Row(
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 10),
              ),
              BuildButtonIcon(
                title: "Next",
                arrowIcon: true,
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => VisitorDetails()));
                },
                faultReport: true,
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          BottomBar()
        ],
      ),
    );
  }
}
