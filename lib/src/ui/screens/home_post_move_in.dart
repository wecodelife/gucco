import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/screens/new_home_screen.dart';
import 'package:guoco_priv_app/src/ui/screens/profile_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/curved_text_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/drawer_guoco.dart';
import 'package:guoco_priv_app/src/ui/widgets/estate_bottom_navigation_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/estate_list_view.dart';
import 'package:guoco_priv_app/src/ui/widgets/smart_features.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/urls.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HomePostMoveIn extends StatefulWidget {
  final String page;
  final String condInfoId;
  final String blockName;
  final String floorAndUnitId;
  final List<String> imgList;
  HomePostMoveIn(
      {this.page,
      this.condInfoId,
      this.floorAndUnitId,
      this.blockName,
      this.imgList});
  @override
  _HomePostMoveInState createState() => _HomePostMoveInState();
}

class _HomePostMoveInState extends State<HomePostMoveIn> {
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();

  String tabValue;
  int _current = 0;
  double position;
  String title;
  bool data = false;
  EstateBloc estateBloc = new EstateBloc();
  List<String> imgList = [];
  List<String> happeningsImages = [
    "assets/images/estate_page_image.svg",
    "assets/images/key_collection_happenings.svg",
    "assets/images/falutreport_happenings.svg"
  ];
  List<String> broadcastImages = [
    "assets/images/key_collection_broadcast.svg",
    "assets/images/smart_home_broadcasts.svg"
  ];

  List<String> happeningsText1 = [
    "EXCITING OFFERS",
    "SMART HOME",
    "FAULT REPORTING"
  ];
  List<String> happeningsText2 = ["COMING SOON", "CONVENIENCE", "UPON MOVE-IN"];

  List<String> broadcastsText1 = [
    "KEY COLLECTION",
    "BOOK FACILITIES",
  ];
  List<String> broadcastsText2 = [
    "AVAILABLE NOW",
    "AVAILABLE NOW",
  ];

  @override
  void initState() {
    tabValue = "happenings";
    imgList.add(Urls.baseUrl +
        "/application/api/v1/filemanagement/" +
        ObjectFactory().appHive.getCondoInfoId() +
        "/files/view/" +
        ObjectFactory().appHive.getPostMoveInImage1());
    imgList.add(Urls.baseUrl +
        "/application/api/v1/filemanagement/" +
        ObjectFactory().appHive.getCondoInfoId() +
        "/files/view/" +
        ObjectFactory().appHive.getPostMoveInImage2());
    imgList.add(Urls.baseUrl +
        "/application/api/v1/filemanagement/" +
        ObjectFactory().appHive.getCondoInfoId() +
        "/files/view/" +
        ObjectFactory().appHive.getPostMoveInImage3());

    print(imgList);

    // userBloc.getCondoInfo.listen((event) {
    //   if(event.statuscode==200)
    //     {
    //      setState(() {
    //        title=event.data[0].propertyname;
    //      });
    //     }
    // });
    super.initState();
  }

  void dotPosition() {
    setState(() {});
  }

  Future<bool> _willPopCallback() async {
    push(context, EstatePage());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    DotsDecorator(
        activeColor: Colors.red.withOpacity(.50),
        color: Colors.white.withOpacity(.50),
        shape: CircleBorder(),
        size: Size(10, 10));
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Constants.kitGradients[17],
          key: _globalKey,
          drawer: DrawerGuoco(
            count: 0,
          ),
          appBar: PreferredSize(
            preferredSize:
                Size.fromHeight(screenHeight(context, dividedBy: 10)),
            child: Container(
              color: Constants.kitGradients[17],
              height: screenHeight(context, dividedBy: 10),
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        GestureDetector(
                          onTap: () {
                            _globalKey.currentState.openDrawer();
                          },
                          child: Container(
                            height: screenHeight(context, dividedBy: 17),
                            width: screenWidth(context, dividedBy: 17),
                            child: SvgPicture.asset(
                                "assets/icons/estate_drawer.svg"),
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 70),
                        )
                      ],
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 10),
                    ),
                    Container(
                        width: screenWidth(context, dividedBy: 3),
                        child: Image.asset("assets/images/estate_logo.png")),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 70),
                    ),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            width: screenWidth(context, dividedBy: 18),
                            height: screenHeight(context, dividedBy: 23),
                            child: SvgPicture.asset(
                                "assets/icons/notification.svg",
                                fit: BoxFit.cover),
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 30),
                          ),
                          GestureDetector(
                              onTap: () {
                                push(context, ProfilePage());
                              },
                              child: ObjectFactory().appHive.getUserImage() ==
                                      null
                                  ? Container(
                                      width:
                                          screenWidth(context, dividedBy: 14),
                                      height:
                                          screenWidth(context, dividedBy: 12),
                                      child: SvgPicture.asset(
                                        "assets/icons/person_icon_home.svg",
                                        color: Colors.white,
                                        fit: BoxFit.cover,
                                      ),
                                    )
                                  : Container(
                                      width:
                                          screenWidth(context, dividedBy: 14),
                                      height:
                                          screenHeight(context, dividedBy: 12),
                                      decoration: BoxDecoration(
                                          color: Colors.red,
                                          shape: BoxShape.circle),
                                      child: Image(
                                        image: NetworkImage(ObjectFactory()
                                            .appHive
                                            .getUserImage()),
                                      ))),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          body: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CarouselSlider(
                      options: CarouselOptions(
                          aspectRatio: screenWidth(context, dividedBy: 1) /
                              screenHeight(context, dividedBy: 3.2),
                          viewportFraction: 1,
                          onPageChanged: (index, reason) {
                            setState(() {
                              _current = index;
                              print(_current);
                            });
                          }),
                      items: imgList.map((i) {
                        return Builder(
                          builder: (BuildContext context) {
                            return Stack(
                              children: [
                                Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  height: screenHeight(context, dividedBy: 3.2),
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                    image: CachedNetworkImageProvider(
                                      i,
                                    ),
                                    fit: BoxFit.fill,
                                    colorFilter: new ColorFilter.mode(
                                        Colors.black.withOpacity(0.40),
                                        BlendMode.darken),
                                  )),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            CurvedTextBar(
                                              label: widget.blockName,
                                              blockName: widget.floorAndUnitId,
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Container(
                                            child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "WELCOME HOME,",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color:
                                                      Constants.kitGradients[0],
                                                  fontFamily:
                                                      "Montserrat-Light",
                                                  fontWeight: FontWeight.w300),
                                            ),
                                            Text(
                                                ObjectFactory()
                                                    .appHive
                                                    .getName(),
                                                style: TextStyle(
                                                    fontSize: 24,
                                                    color: Constants
                                                        .kitGradients[0],
                                                    fontFamily:
                                                        "Montserrat-Light",
                                                    fontWeight:
                                                        FontWeight.w600)),
                                          ],
                                        )),
                                      )
                                    ],
                                  ),
                                ),
                                Positioned(
                                  top: screenHeight(context, dividedBy: 4.8),
                                  left: screenWidth(context, dividedBy: 2.5),
                                  child: Row(
                                    children: [
                                      DotsIndicator(
                                        dotsCount: 3,
                                        position: _current.toDouble(),
                                        axis: Axis.horizontal,
                                        decorator: DotsDecorator(
                                            activeColor:
                                                Colors.red.withOpacity(.40),
                                            color:
                                                Colors.white.withOpacity(.40),
                                            shape: CircleBorder(),
                                            size: Size(10, 10)),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            );
                          },
                        );
                      }).toList(),
                    ),
                    SmartFeatures(
                      blockName: widget.blockName,
                      floorAndUnitId: widget.floorAndUnitId,
                      boxTitle: "SMART FEATURES",
                      onTap: () {},
                    ),
                    Container(
                      color: Constants.kitGradients[17],
                      width: screenWidth(context, dividedBy: 1),
                      height: screenHeight(context, dividedBy: 15),
                      child: Row(
                        children: [
                          GestureDetector(
                            child: Container(
                              color: Colors.transparent,
                              width: screenWidth(context, dividedBy: 2),
                              height: screenHeight(context, dividedBy: 15),
                              child: Center(
                                  child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "HAPPENINGS",
                                    style: TextStyle(
                                        fontSize: 13,
                                        color: Constants.kitGradients[0],
                                        fontFamily: "SFProText-Medium",
                                        fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 100),
                                  ),
                                  tabValue == "happenings"
                                      ? Container(
                                          color: Constants.kitGradients[11],
                                          height: screenHeight(context,
                                              dividedBy: 300),
                                          width: screenWidth(context,
                                              dividedBy: 8),
                                        )
                                      : Container(),
                                ],
                              )),
                            ),
                            onTap: () {
                              setState(() {
                                tabValue = "happenings";
                              });
                            },
                          ),
                          GestureDetector(
                            child: Container(
                              width: screenWidth(context, dividedBy: 2),
                              height: screenHeight(context, dividedBy: 15),
                              color: Colors.transparent,
                              child: Center(
                                  child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "BROADCASTS",
                                    style: TextStyle(
                                        fontSize: 13,
                                        color: Constants.kitGradients[0],
                                        fontFamily: "SFProText-Medium",
                                        fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(
                                    height:
                                        screenHeight(context, dividedBy: 100),
                                  ),
                                  tabValue == "broadcasts"
                                      ? Container(
                                          color: Constants.kitGradients[11],
                                          height: screenHeight(context,
                                              dividedBy: 300),
                                          width: screenWidth(context,
                                              dividedBy: 8),
                                        )
                                      : Container(),
                                ],
                              )),
                            ),
                            onTap: () {
                              setState(() {
                                tabValue = "broadcasts";
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    if (tabValue == "happenings")
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 5),
                        child: ListView.builder(
                            itemCount: happeningsImages.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) {
                              return EstateListView(
                                image: happeningsImages[index],
                                headText: happeningsText1[index],
                                subText: happeningsText2[index],
                              );
                            }),
                      ),
                    if (tabValue == "broadcasts")
                      Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 5),
                        child: ListView.builder(
                            itemCount: broadcastImages.length,
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (BuildContext context, int index) {
                              return EstateListView(
                                image: broadcastImages[index],
                                headText: broadcastsText1[index],
                                subText: broadcastsText2[index],
                              );
                            }),
                      ),
                    Container(
                      color: Constants.kitGradients[17],
                      width: screenWidth(context, dividedBy: 1),
                      height: screenHeight(context, dividedBy: 13),
                    ),
                  ],
                ),
              ),
              // Positioned(
              //   top: screenHeight(context, dividedBy: 3.2),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     children: imgList.map((image) {
              //       int index = imgList.indexOf(image);
              //       return Container(
              //         width: 8.0,
              //         height: 8.0,
              //         margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
              //         decoration: BoxDecoration(
              //           shape: BoxShape.circle,
              //           color: _current == index ? Colors.blue : Colors.red,),
              //       );
              //     }).toList(),
              //   ),
              // ),
              Positioned(
                bottom: screenHeight(context, dividedBy: 13),
                child: Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 20),
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                        Colors.black.withOpacity(0),
                        Colors.black.withOpacity(.05),
                        Colors.black.withOpacity(.10),
                        Colors.black.withOpacity(.20),
                        Colors.black.withOpacity(.30),
                        Colors.black.withOpacity(.40),
                        Colors.black.withOpacity(.50),
                        Colors.black.withOpacity(.60),
                        Colors.black.withOpacity(.70)
                      ])),
                ),
              ),
              Positioned(
                  bottom: 0,
                  child: EstateBottomBar(
                    iconLeft: "assets/images/privilege_logo.png",
                    iconRight: "assets/icons/estate_icon.svg",
                    titleLeft: "",
                    pageNavigationLeftIcon: () {
                      push(context, NewHomeScreen());
                    },
                    pageNavigationRightIcon: () {
                      push(context, EstatePage());
                    },
                    titleRight: "My Estate",
                    pageValue: "postmovein",
                    logoTrue: true,
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
