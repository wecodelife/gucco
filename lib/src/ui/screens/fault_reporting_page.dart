import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/home_post_move_in.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FaultReport extends StatefulWidget {
  @override
  _FaultReportState createState() => _FaultReportState();
}

class _FaultReportState extends State<FaultReport> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        backgroundColor: Constants.kitGradients[7],
        actions: [
          AppBarBookingPage(
            pageTitle: "Fault Reporting",
            leftIcon: "assets/icons/arrow_back.png",
            rightIcon: "assets/icons/estate_icon.png",
            onTapLeftIcon: () {
              Navigator.pop(context);
            },
            onTapRightIcon: () {
              push(
                  context,
                  HomePostMoveIn());
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TowerName(
              towerName: "TOWER 1",
              towerSubName: "01-01",
            ),
            AppBarRightIcon(
              pageTitle: "Report Details",
              iconOrText: "text",
              boxWidth: 7,
              rightIcon: true,
              bookedListTap: (){
                print("print 1");
                actionAlertBox(
                    questionAlert: "Are you sure you want to cancel?",cancelBoxTitle: "",msg: " Cancel Report",
                    text1: "After cancellation you will not be able to recover this report",cancelBox: true,contentNum: 1,text2: "",context: context,cancelAlert: 3);
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: screenHeight(context, dividedBy: 40),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Basic Details",
                            style: TextStyle(
                                fontSize: 18,
                                fontFamily: "SFProText-Regular",
                                fontWeight: FontWeight.w600),
                          ),
                          SizedBox(
                            height: screenHeight(context, dividedBy: 30),
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 1.1),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Reporting About:",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: "SFProText-Regular"),
                                    ),
                                    Text(
                                      "Cleanliness",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: "SFProText-Regular"),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 50),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Location:",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: "SFProText-Regular"),
                                    ),
                                    Text(
                                      "BBQ Pit A",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: "SFProText-Regular"),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 50),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Status:",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: "SFProText-Regular"),
                                    ),
                                    Text(
                                      "In Progress",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: "SFProText-Regular",
                                          color: Colors.amberAccent),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 50),
                                ),
                                Container(
                                  width: screenWidth(context, dividedBy: 1.1),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Comments:",
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontFamily: "SFProText-Regular"),
                                      ),
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 60),
                                      ),
                                      Text(
                                        "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus sollicitudin, elit laoreet ultricies ullamcorper, tortor neque laoreet ipsum, ut aliquam ligula magna eget erat.",
                                        style: TextStyle(
                                          fontSize: 15,
                                          fontFamily: "SFProText-Regular",
                                        ),
                                      ),
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 20),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Uploaded Photo:",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontFamily:
                                                    "SFProText-Regular"),
                                          ),
                                          Text(
                                            "IMG01229.jpg",
                                            style: TextStyle(
                                              fontSize: 15,
                                              fontFamily: "SFProText-Regular",
                                              decoration:
                                                  TextDecoration.underline,
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 20),
                                      ),
                                      Container(
                                        width:
                                            screenWidth(context, dividedBy: 1),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              "This report was created on ",
                                              style: TextStyle(
                                                  fontSize: 13,
                                                  fontFamily:
                                                      "SFProText-Regular"),
                                            ),
                                            Text(
                                              "10-Apr-2020, 10:01AM.",
                                              style: TextStyle(
                                                fontSize: 13,
                                                fontFamily: "SFProText-Regular",
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 50),
                                      ),
                                      Divider(
                                        thickness: screenHeight(context,
                                            dividedBy: 800),
                                        color: Colors.black,
                                      ),
                                      Text(
                                        "Management Comments",
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontFamily: "SFProText-Regular",
                                            fontWeight: FontWeight.w600,
                                            color: Colors.grey),
                                      ),
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 50),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Resolved By:",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontFamily: "SFProText-Regular",
                                                color: Colors.grey),
                                          ),
                                          Text(
                                            "--",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontFamily: "SFProText-Regular",
                                                color: Colors.grey),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 50),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Management Comments:",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontFamily: "SFProText-Regular",
                                                color: Colors.grey),
                                          ),
                                          Text(
                                            "--",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontFamily: "SFProText-Regular",
                                                color: Colors.grey),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: screenHeight(context,
                                            dividedBy: 50),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            "Attachments:",
                                            style: TextStyle(
                                                fontSize: 15,
                                                fontFamily: "SFProText-Regular",
                                                color: Colors.grey),
                                          ),
                                          Text(
                                            "--",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontFamily: "SFProText-Regular",
                                                color: Colors.grey),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),
                BottomBar()
              ],
            ),
          ],
        ),
      ),
    );
  }
}
