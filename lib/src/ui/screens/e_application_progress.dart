import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/heading_content.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class EApplicationsProgress extends StatefulWidget {
  @override
  _EApplicationsProgressState createState() => _EApplicationsProgressState();
}

class _EApplicationsProgressState extends State<EApplicationsProgress> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        leading: Container(),
        actions: [
          AppBarBookingPage(
            pageTitle: "E-Applications",
            rightIcon: "assets/icons/estate_icon.png",
            leftIcon: "assets/icons/arrow_back.png",
            onTapRightIcon: () {
              push(context, EstatePage());
            },
            onTapLeftIcon: () {
              pop(context);
            },
          )
        ],
      ),
      body: Column(
        children: [
          TowerName(
            towerName: "Tower 01",
            towerSubName: "01-01",
          ),
          AppBarRightIcon(
            pageTitle: "Application Details",
            rightIcon: true,
            iconOrText: "text",
            boxWidth: 7,
            bookedListTap: () {
              actionAlertBox(
                  msg: "Cancel E-Application",
                  context: context,
                  text1:
                      "After Cancellation you will not be able to recover this Application.",
                  cancelBox: true,
                  contentNum: 1,
                  text2: "",
                  cancelBoxTitle: "",
                  questionAlert: "Are you sure you want to cancel?",
                  cancelAlert: 4);
            },
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 20),
            ),
            Text(
              "Application Details",
              style: TextStyle(
                  color: Constants.kitGradients[11],
                  fontFamily: 'SFProText-SemiBold',
                  fontSize: 20),
            ),
          ]),
          HeadingContent(
            heading: "Application Type",
            content: "Second Car Registrtaion",
          ),
          HeadingContent(
            heading: "Comments",
            content: "-",
          ),
          HeadingContent(
            heading: "Status",
            content: "InProgress",
            fontBold: true,
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            Text(
              "This Application was submitted on",
              style: TextStyle(
                color: Colors.black,
                fontFamily: 'SFProText-SemiBold',
                fontSize: screenWidth(context, dividedBy: 26),
              ),
            ),
          ]),
          Text(
            "10-Apr-2020, 10:01AM.",
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'SFProText-SemiBold',
                fontSize: screenWidth(context, dividedBy: 26),
                fontWeight: FontWeight.w600),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          Divider(
            thickness: 2.0,
            color: Colors.grey,
          ),
          Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 20),
            ),
            Text(
              "File Uploaded",
              style: TextStyle(
                  color: Constants.kitGradients[11],
                  fontFamily: 'SFProText-SemiBold',
                  fontSize: 20),
            ),
          ]),
          SizedBox(
            height: screenHeight(context, dividedBy: 50),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 20),
              ),
              Text(
                "Second-Car-Registration-Form-2020.pdf",
                style: TextStyle(
                    fontSize: 16,
                    fontFamily: "SFProText-Regular",
                    color: Colors.black,
                    decoration: TextDecoration.underline),
              ),
            ],
          )
        ],
      ),
    );
  }
}
