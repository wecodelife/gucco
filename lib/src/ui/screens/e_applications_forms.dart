import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/coment_box.dart';
import 'package:guoco_priv_app/src/ui/widgets/drop_down%20form%20feild.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/ui/widgets/upload_files_e_app_form.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class EApplicationsForm extends StatefulWidget {
  @override
  _EApplicationsFormState createState() => _EApplicationsFormState();
}

class _EApplicationsFormState extends State<EApplicationsForm> {
  List<String>relatedTopics=["Internet","value","work"];
  String relatedTopicSelectedValue;
  TextEditingController commentTextEditingController =new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          leading: Container(),
          actions: [
            AppBarBookingPage(
              pageTitle: "E-Applications",
              rightIcon: "assets/icons/estate_icon.png",
              leftIcon: "assets/icons/arrow_back.png",
              onTapRightIcon: () {
                push(
                    context,
                    EstatePage(
                    ));
              },
              onTapLeftIcon: () {
                pop(context);
              },
            ),
          ],
        ),

        body: ListView(
          padding: EdgeInsets.only(),
            children: [

             TowerName(towerName: "TOWER 1",towerSubName: "01-01",),

             AppBarRightIcon(pageTitle: "Submit Application",boxWidth: 7,),

             SizedBox(
               height: screenHeight(context,dividedBy: 40),
             ),

             Column(
               children: [
                 Padding(
                   padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
                   child: Column(

                     children: [
                       Row(
                         children: [

                           Text("Application Details",style: TextStyle(fontSize: 20,color:Constants.kitGradients[15],
                             fontFamily: "SFProText-SemiBold",fontWeight: FontWeight.w600
                           ),
                           ),
                         ],
                       ),


                      SizedBox(
                        height: screenHeight(context,dividedBy: 40),
                      ),
                      Row(
                        children: [
                          RichText(text: TextSpan(text: "Application Type",style:TextStyle(color: Colors.black,fontSize: 16,fontFamily: "SFProText-Regular",


                          ),
                            children: [
                              TextSpan(text: " *",style: TextStyle(color:Constants.kitGradients[15])),
                            ]
                          ),
                          ),
                        ],
                      ),

                      SizedBox(
                        height: screenHeight(context,dividedBy: 50),
                      ),

                      DropDownFormField(boxWidth: 1.26,boxHeight: 20, underline: true,onClicked: (value){

                        setState(() {
                          relatedTopicSelectedValue=value;
                        });
                      },
                        dropDownList:relatedTopics ,dropDownValue: relatedTopicSelectedValue,),

                       SizedBox(
                         height: screenHeight(context,dividedBy: 90),
                       ),

                       CommentBox(commentTextEditingController: commentTextEditingController,boxWidth: 1.14,boxHeight: 6,height: 40,),

                     SizedBox(
                       height: screenHeight(context,dividedBy: 50),
                     ),

                     UploadFilesEAppForm(),

                     SizedBox(
                       height: screenHeight(context,dividedBy: 7.5),
                     ),

                     BuildButtonIcon(
                       title: "Submit",
                       arrowIcon: true,
                       faultReport: true,
                     ),
                     ],
                   ),

                 ),

                 SizedBox(
                   height: screenHeight(context,dividedBy: 45),
                 ),

                 BottomBar()


               ],
             ),







            ],



        ),
      ),
    );
  }
}
