import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/book_a_facility_request_model.dart';
import 'package:guoco_priv_app/src/ui/screens/facilities_list_bookings.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/check_box_facilities.dart';
import 'package:guoco_priv_app/src/ui/widgets/facilities_body.dart';
import 'package:guoco_priv_app/src/ui/widgets/facilities_body_head.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:intl/intl.dart';

import 'martin_modern/estate_page.dart';

class FacilitiesDetails extends StatefulWidget {
  final String startTime;
  final String additionalNotes;
  final String endTime;
  final String facilitySlotId;
  final String blockName;
  final String floorAndUnitId;
  final DateTime onDate;
  final String facilityName;
  final String facilityInfoId;
  final String rule;

  FacilitiesDetails(
      {this.floorAndUnitId,
      this.blockName,
      this.facilityInfoId,
      this.facilityName,
      this.endTime,
      this.startTime,
      this.onDate,
      this.rule,
      this.additionalNotes,
      this.facilitySlotId});

  @override
  _FacilitiesDetailsState createState() => _FacilitiesDetailsState();
}

class _FacilitiesDetailsState extends State<FacilitiesDetails> {
  String rule1 =
      "I hereby confirm that I have read and will fully abide by the rules and regulations governing the facilities.";
  String rule2 =
      "A reminder will be sent to you 24 hours before the start of your booking time.";
  ScrollController _scrollController = ScrollController();
  bool checkValue = false;
  EstateBloc estateBloc = new EstateBloc();
  bool isLoading;
  @override
  void initState() {
    estateBloc.bookAFacilityResponse.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200) {
        showToast("Booking Successful");

        push(
            context,
            FacilitiesListBookings(
              blockName: widget.blockName,
              floorAndUnitId: widget.floorAndUnitId,
            ));
      } else {
        showToast("NetworkError");
      }
    });

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[7],
        leading: Container(),
        actions: [
          AppBarBookingPage(
            pageTitle: 'FACILITIES',
            rightIcon: "assets/icons/estate_icon.png",
            leftIcon: "assets/icons/arrow_back.png",
            onTapLeftIcon: () {
              Navigator.pop(context);
            },
            onTapRightIcon: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => EstatePage()),
                  (route) => false);
            },
          ),
        ],
      ),
      backgroundColor: Constants.kitGradients[0],
      body: SingleChildScrollView(
        controller: _scrollController,
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TowerName(
              towerName: widget.blockName,
              towerSubName: widget.floorAndUnitId,
            ),
            AppBarRightIcon(
              boxWidth: screenWidth(context, dividedBy: 1),
              pageTitle: 'New booking',
            ),
            FacilitiesBodyHead(
              title: 'Facility:',
              value: widget.facilityName,
            ),
            FacilitiesBodyHead(
              title: 'Date:',
              value: new DateFormat('dd-MMM-yyyy').format(DateTime.parse(
                  widget.onDate != null
                      ? widget.onDate.toString()
                      : DateTime.now().toString())),
            ),
            FacilitiesBodyHead(
                title: 'Time:', value: widget.startTime + "-" + widget.endTime),
            SizedBox(
              height: screenHeight(context, dividedBy: 75),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 30),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                          'assets/images/key_collection_background.png'),
                      fit: BoxFit.fill)),
            ),
            FacilitiesBody(
              title: "Rules",
              description: ObjectFactory().appHive.getTermsAndCondition(),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 25),
                  vertical: screenHeight(context, dividedBy: 60)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CheckBoxFacilities(
                    value: checkValue,
                    onChangedCheckValue: (value) {
                      checkValue = value;
                    },
                  ),
                  Container(
                    width: screenWidth(context, dividedBy: 1.25),
                    child: Text(
                      rule1,
                      style: TextStyle(
                          fontSize: 15,
                          color: Constants.kitGradients[7],
                          fontFamily: "SFProText-Regular"),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 30),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                          'assets/images/key_collection_background.png'),
                      fit: BoxFit.fill)),
            ),
            FacilitiesBody(
              title: "Reminder",
              description: rule2,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 10),
            ),
            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 13),
                ),
                BuildButtonIcon(
                  title: 'Submit Booking',
                  arrowIcon: false,
                  faultReport: true,
                  isLoading: isLoading,
                  onPressed: () {
                    if (checkValue == true) {
                      setState(() {
                        isLoading = true;
                      });
                      estateBloc.bookAFacility(
                          bookAFacilityRequest: BookAFacilityRequest(
                              unitinfoid:
                                  ObjectFactory().appHive.getUnitInfoId(),
                              ondate: new DateFormat('dd-MM-yyyy')
                                  .format(widget.onDate),
                              facilityslotid: widget.facilitySlotId,
                              endtime: widget.endTime,
                              starttime: widget.startTime,
                              additionalnotes: ""),
                          facilityInfoId: widget.facilityInfoId);
                    } else {
                      showToast("Please accept the Rules to continue");
                    }
                    //do something here
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
