import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class PropertyDetails extends StatefulWidget {
  @override
  _PropertyDetailsState createState() => _PropertyDetailsState();
}

class _PropertyDetailsState extends State<PropertyDetails> {
  String description =
      "A lifestyle, creative culture and sustainable living journal that explores the stories of passionate people, alluring landscapes and intriguing ideas.";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1),
          height: screenHeight(context, dividedBy: 1),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(
                      'assets/images/property_details_background.png'),
                  fit: BoxFit.fill)),
        ),
        Positioned(
            top: 0.0,
            left: 0.0,
            child: Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 3),
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                          'assets/images/property_details_image.png'),
                      fit: BoxFit.fill)),
            )),
        Positioned(
            top: screenHeight(context, dividedBy: 30),
            left: screenWidth(context, dividedBy: 65),
            child: IconButton(
                iconSize: 40,
                icon: Icon(Icons.arrow_back),
                color: Constants.kitGradients[7],
                onPressed: () {
                  //do something here
                })),
        Positioned(
          top: screenHeight(context, dividedBy: 3),
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  height: 3.0,
                  width: 25.0,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2.0),
                    color: Constants.kitGradients[4],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Text(
                  "Q1 2021",
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontSize: 14,
                      fontFamily: "Muli"),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 70),
                ),
                Text(
                  "MARTIN MODERN",
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontSize: 22,
                      fontFamily: "MuliBold"),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 70),
                ),
                Text(
                  "Residential",
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontSize: 18,
                      fontFamily: "Muli"),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                Container(
                  height: screenHeight(context, dividedBy: 6.5),
                  width: screenWidth(context, dividedBy: 1.2),
                  child: Text(
                    description,
                    style: TextStyle(
                        color: Constants.kitGradients[0],
                        fontSize: 14,
                        fontFamily: "Muli"),
                  ),
                ),
                Container(
                  height: 0.5,
                  width: screenWidth(context, dividedBy: 1),
                  color: Constants.kitGradients[1],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 40),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1.2),
                  height: screenHeight(context, dividedBy: 5.5),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SvgPicture.asset(
                              'assets/icons/property_details_address.svg'),
                          Container(
                            width: screenWidth(context, dividedBy: 1.9),
                            child: Text(
                              '8 Martin Place, Singapore 237992',
                              overflow: TextOverflow.visible,
                              maxLines: 3,
                              style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontSize: 14,
                                  fontFamily: "Muli"),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SvgPicture.asset(
                              'assets/icons/property_details_phone.svg'),
                          Container(
                            width: screenWidth(context, dividedBy: 1.3),
                            child: Text(
                              '+65 6225 9000',
                              style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontSize: 14,
                                  fontFamily: "Muli"),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SvgPicture.asset(
                              'assets/icons/property_details_email.svg'),
                          Container(
                            width: screenWidth(context, dividedBy: 1.3),
                            child: Text(
                              'sales_enquiry@guocoland.com',
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  color: Constants.kitGradients[0],
                                  fontSize: 14,
                                  fontFamily: "Muli"),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 90),
                ),
                BuildButton(
                  title: "MORE DETAILS",
                  onPressed: () {
                    //do something here
                  },
                )
              ],
            ),
          ),
        )
      ],
    ));
  }
}
