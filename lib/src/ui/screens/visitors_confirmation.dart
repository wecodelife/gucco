import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/copy_past_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/text_feild_visitor_confirmation.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/ui/widgets/visitors_confirmation_body1.dart';
import 'package:guoco_priv_app/src/ui/widgets/visitors_confirmation_body3.dart';
import 'package:guoco_priv_app/src/ui/widgets/visitors_confirmationbody4.dart';
import 'package:guoco_priv_app/src/ui/widgets/vvisitors_confirmation_body2.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class VisitorsConfirmation extends StatefulWidget {
  @override
  _VisitorsConfirmationState createState() => _VisitorsConfirmationState();
}

class _VisitorsConfirmationState extends State<VisitorsConfirmation> {
  int count;
  String clipBoard;
  TextEditingController emailTexEditingController = new TextEditingController();
  String pastValue;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: Container(),
          actions: [
            AppBarBookingPage(
              pageTitle: "MY VISITORS",
              leftIcon: "assets/icons/arrow_back.png",
              rightIcon: "assets/icons/estate_icon.png",
              transparent: false,
              onTapLeftIcon: () {
                pop(context);
              },
              onTapRightIcon: () {
                push(context, EstatePage());
              },
            ),
          ],
        ),
        body: ListView(
          children: [
            TowerName(
              towerName: "TOWER 1",
              towerSubName: "01-01",
            ),

            AppBarRightIcon(
              pageTitle: "Visitor Invite Confirmed!",
              boxWidth: 10,
            ),

            VisitorsConfirmationBody1(
              content1: "15-Apr-2020",
              content2: "10:00 AM",
              onValueChanged: (value) {
                setState(() {
                  count = value;
                });
              },
              counter: count,
            ),

            VisitorsConfirmationBody2(
              content1: "Neil Green",
              content2: "98765432",
              content3: "neilgreen@hotmail.com",
              content4: "Friend",
              content5: "-",
              content6: "3",
              content7: "Visiting",
              content8: "Yes",
              onChanged: (value) {
                setState(() {
                  count = value;
                });
              },
              counter: count,
            ),

            VisitorsConfirmationBody3(
              content1: "-",
              counter: count,
              onChanged: (value) {
                setState(() {
                  count = value;
                });
              },
            ),

            VisitorsConfirmationBody4(
              counter: count,
              onChanged: (value) {
                setState(() {
                  count = value;
                });
              },
            ),

            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),

            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                Text(
                  "Send Invite",
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'SFProText-SemiBold',
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),

            SizedBox(
              height: screenHeight(context, dividedBy: 60),
            ),

            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                Text(
                  "Copy and Send the link",
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'SFProText-SemiBold',
                    fontSize: 16,
                  ),
                ),
              ],
            ),

            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 18),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1.52),
                  child: Text(
                    "https://matrinmodren.com/",
                    style: TextStyle(
                        color: Constants.kitGradients[13],
                        fontFamily: 'SFProText-SemiBold',
                        fontWeight: FontWeight.w600,
                        fontSize: 16,
                        decoration: TextDecoration.underline),
                  ),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 150),
                ),
                BorderColorGoldButton(
                  heading: "Copy",
                  onTap: () {
                    Clipboard.setData(
                        new ClipboardData(text: "https://matrinmodren.com/"));
                    cancelAlertBox(
                        contentPadding: 30,
                        context: context,
                        insetPadding: 2.7,
                        msg: "Copied!",
                        text1: "The link to this invite has been copied",
                        text2: "",
                        titlePadding: 100,
                        onPressed: () {
                          push(context, VisitorsConfirmation());
                        });
                  },
                  borderTrue: true,
                  boxWidth: 16,
                )
              ],
            ),

            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),

            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1.52),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Send this invite to:",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'SFProText-SemiBold',
                          fontSize: 16,
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      Text(
                        "neilgreen@hotmail.com",
                        style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 150),
                ),
                BorderColorGoldButton(
                  heading: "Send",
                  onTap: () {
                    cancelAlertBox(
                        contentPadding: 30,
                        context: context,
                        insetPadding: 2.7,
                        msg: "Invitation Sent!",
                        text1: "A Copy of this Invitation ha sent to",
                        text2: "neilgreen@hotmail.com",
                        titlePadding: 100,
                        onPressed: () {
                          push(context, VisitorsConfirmation());
                        });
                  },
                  borderTrue: true,
                  boxWidth: 16,
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            // Text("Send this invite to another email address",style: TextStyle(fontSize: 15),),
            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                Text(
                  "Send this invite to another email address",
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'SFProText-SemiBold',
                    fontSize: 15,
                  ),
                ),

                // SizedBox(
                //   width: screenWidth(context,dividedBy: 30),
                // ),
              ],
            ),
            TextFieldVisitorConfirmation(
              hintText: "Email Address",
              textEditingController: emailTexEditingController,
            ),

            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),

            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                BorderColorGoldButton(
                  heading: "Send",
                  onTap: () {},
                  boxWidth: 16,
                ),
              ],
            ),

            SizedBox(
              height: screenHeight(context, dividedBy: 30),
            ),

            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                Text(
                  "Should you wish to edit this invite, you can do so via",
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'SFProText-SemiBold',
                    fontSize: screenWidth(context, dividedBy: 26),
                  ),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 30),
                ),
              ],
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  "My Visitors > Edit.",
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'SFProText-SemiBold',
                      fontSize: screenWidth(context, dividedBy: 26),
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),

            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),

            BottomBar()
          ],
        ));
  }
}
