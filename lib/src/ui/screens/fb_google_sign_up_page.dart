import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/ref_code.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/select_login_or_signup.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

import 'auth/login_page.dart';
//import 'package:guoco_priv_app/src/external_app/external_app_launcher.dart';

class FbGoogleSignUp extends StatefulWidget {
  @override
  _FbGoogleSignUpState createState() => _FbGoogleSignUpState();
}

class _FbGoogleSignUpState extends State<FbGoogleSignUp> {
  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, SelectLoginOrSignUp(), false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: SvgPicture.asset("assets/images/login_bg_image.svg",
                  fit: BoxFit.cover,
                  color: Colors.black.withOpacity(0.60),
                  colorBlendMode: BlendMode.darken),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 9)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 10),
                  ),
                  Container(
                    height: screenHeight(context, dividedBy: 3),
                    width: screenWidth(context, dividedBy: 1.4),
                    child: SvgPicture.asset(
                      "assets/images/Guoco_logo.svg",
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 8),
                  ),
                  // AuthButton(
                  //   mediaName: "SignUp with Facebook",
                  //   mediaSymbol: "assets/icons/facebook.svg",
                  //   buttonColor: Constants.kitGradients[2],
                  //   boxWidth: 1.4,
                  //   onPressed: () {},
                  // ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                  // AuthButton(
                  //   mediaName: "SignUp with Google",
                  //   mediaSymbol: "assets/icons/google.svg",
                  //   buttonColor: Constants.kitGradients[3],
                  //   boxWidth: 1.4,
                  //   onPressed: () {
                  //     Auth().signInWithGoogle();
                  //   },
                  // ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),
                  BuildButton(
                    title: "SIGN UP",
                    disabled: true,
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => RefCode()));
                    },
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  RichText(
                    text: TextSpan(
                        text: "Already have an account? ",
                        style: TextStyle(
                            fontFamily: "Muli",
                            fontSize: 16,
                            fontWeight: FontWeight.w400,
                            color: Colors.white),
                        children: [
                          TextSpan(
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  push(context, Login());
                                },
                              text: "Login here",
                              style: TextStyle(
                                  decoration: TextDecoration.underline)),
                        ]),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
