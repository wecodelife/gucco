import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/models/user_profile_response.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/select_login_or_signup.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_homescreen.dart';
import 'package:guoco_priv_app/src/ui/widgets/estate_bottom_navigation_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/home_drawer.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';

class NewHomeScreen extends StatefulWidget {
  @override
  _NewHomeScreenState createState() => _NewHomeScreenState();
}

class _NewHomeScreenState extends State<NewHomeScreen> {
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();
  AuthBloc userBloc = new AuthBloc();
  bool isLoading = false;
  DateTime currentBackPressTime;
  int statusCode = 0;

  @override
  void initState() {
    userBloc.getUserProfile();
    userBloc.getProfile.listen((event) {
      if (event.errorcode >= 20000) {
        ObjectFactory().appHive.hiveClear(key: "token");
        pushAndRemoveUntil(context, SelectLoginOrSignUp(), false);
      }
    });
    // TODO: implement initState
    super.initState();
  }

  Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (currentBackPressTime == null ||
        now.difference(currentBackPressTime) > Duration(seconds: 2)) {
      currentBackPressTime = now;
      showToast("Press back again to close");
      return Future.value(false);
    }
    return Future.value(true);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Scaffold(
        key: _globalKey,
        drawer: HomeDrawer(
          count: 1,
        ),
        appBar: AppBar(
          leading: Container(),
          backgroundColor: Colors.black,
          toolbarHeight: screenHeight(context, dividedBy: 7),
          actions: [
            StreamBuilder<ProfileResponseModel>(
                stream: userBloc.getProfile,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    // exitToLogin(snapshot.data.errorcode, context);
                    ObjectFactory().appHive.putPasswordLastChangesOn(
                        passwordlastchangedon:
                            snapshot.data.data.passwordlastchangedon);
                    ObjectFactory()
                        .appHive
                        .putName(name: snapshot.data.data.firstname);
                    ObjectFactory()
                        .appHive
                        .putLastName(snapshot.data.data.lastname);
                    ObjectFactory().appHive.putEmail(snapshot.data.data.email);
                    ObjectFactory()
                        .appHive
                        .putContactNo(snapshot.data.data.mobileno);
                    ObjectFactory()
                        .appHive
                        .putUserImage(snapshot.data.data.profileimage);
                    ObjectFactory().appHive.putRefId(snapshot.data.data.id);
                  }
                  return ObjectFactory().appHive.getName() != null
                      ? AppBarHomeScreen(
                          name: ObjectFactory().appHive.getName(),
                          isLoading: false,
                          onPressingDrawer: () {
                            _globalKey.currentState.openDrawer();
                          },
                        )
                      : snapshot.hasData
                          ? AppBarHomeScreen(
                              name: snapshot.data.data.firstname,
                              isLoading: snapshot.hasData ? false : true,
                              onPressingDrawer: () {
                                _globalKey.currentState.openDrawer();
                              },
                            )
                          : Shimmer.fromColors(
                              highlightColor: Colors.grey,
                              baseColor: Colors.black38,
                              child: Container(
                                height: screenHeight(context, dividedBy: 20),
                                width: screenWidth(context, dividedBy: 1),
                                decoration:
                                    BoxDecoration(color: Colors.black54),
                              ),
                            );
                })
          ],
        ),
        bottomNavigationBar: EstateBottomBar(
          pageValue: "key",
          titleLeft: "Home",
          iconLeft: "assets/icons/homeselected.png",
          iconRight: "assets/icons/estate_icon.svg",
          titleRight: "My Estate",
          bottomBarBlack: false,
          divider: false,
          bottomBarGold: true,
          pageNavigationRightIcon: () {
            push(context, EstatePage());
          },
          pageNavigationLeftIcon: () {
            print("HomeScreen");
          },
        ),
        body: Container(
          height: screenHeight(context, dividedBy: 1.2),
          width: screenWidth(context, dividedBy: 1),
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/bg_blackgradient.png"),
                  fit: BoxFit.cover)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset("assets/images/estate_logo.png"),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.2),
                child: Text(
                  "Residents may register property via My Estate to access Key Collection and Post-Move In features",
                  style: TextStyle(
                      color: Colors.white, fontSize: 17, fontFamily: 'Muli'),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 50),
              ),
              Text(
                "More features coming soon........",
                style: TextStyle(
                    color: Color(0XFF707070), fontSize: 15, fontFamily: 'Muli'),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 7),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
