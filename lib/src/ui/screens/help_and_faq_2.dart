import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/help_and_faq_appbar.dart';
import 'package:guoco_priv_app/src/ui/widgets/help_and_faq_categories_1.dart';
import 'package:guoco_priv_app/src/ui/widgets/help_and_faq_search_bar.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

import 'help_and_Faq.dart';

class HelpAndFaq2 extends StatefulWidget {
  @override
  _HelpAndFaq2State createState() => _HelpAndFaq2State();
}

class _HelpAndFaq2State extends State<HelpAndFaq2> {
  TextEditingController textEditingController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 1),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/help_and_faq_background.png'),
                fit: BoxFit.fill)),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 40)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 20),
              ),
              HelpAndFaqAppbar(
                title: 'HELP & FAQ',
                icon: Image.asset('assets/icons/arrow_back.png'),
                onTap: () {
                 Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>HelpAndFAQ()), (route) => false);
                },
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 20),
              ),
              HelpAndFAQSearchBAR(hintText: 'How can we help?', textEditingController: textEditingController, onTap: (){
                //do something
              },),
              SizedBox(
                height: screenHeight(context, dividedBy: 25),
              ),
              FAQCategories1(),
            ],
          ),
        ),
      ),
    );
  }
}
