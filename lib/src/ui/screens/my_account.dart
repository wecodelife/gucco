import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/my_account_widget_1.dart';
import 'package:guoco_priv_app/src/ui/widgets/my_account_widget_2.dart';
import 'package:guoco_priv_app/src/ui/widgets/my_account_widget_3.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class MyAccount extends StatefulWidget {
  @override
  _MyAccountState createState() => _MyAccountState();
}

class _MyAccountState extends State<MyAccount> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[7],
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[7],
        actions: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  child: Container(
                    child: SvgPicture.asset('assets/icons/menu_icon.svg'),
                    width: screenWidth(context, dividedBy: 6),
                  ),
                  onTap: () {
                    //do something here
                  },
                ),
                Text(
                  'MY ACCOUNT',
                  style: TextStyle(
                      fontFamily: 'MuliBold',
                      fontSize: 16,
                      color: Constants.kitGradients[0]),
                ),
                GestureDetector(
                  child: Container(
                    child: SvgPicture.asset('assets/icons/bell_icon.svg'),
                    width: screenWidth(context, dividedBy: 6),
                  ),
                  onTap: () {
                    //do something here
                  },
                ),
              ],
            ),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 35),
            vertical: screenHeight(context, dividedBy: 55)),
        child: Column(
          children: [
            MyAccountWidget1(
              name: 'JOHN SMITH',
              membership: 'VIPMEMBER',
            ),
            Container(
              width: screenHeight(context, dividedBy: 1),
              height: 0.5,
              color: Constants.kitGradients[6],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  child: MyAccountWidget2(
                    title: 'My profile',
                    icon: 'assets/icons/my_profile_icon.svg',
                  ),
                  onTap: (){
                    //do something here
                  },
                ),
                GestureDetector(
                  child: MyAccountWidget2(
                    title: 'My family',
                    icon: 'assets/icons/my_family_icon.svg',
                  ),
                  onTap: (){
                    //do something here
                  },
                ),
                GestureDetector(
                  child: MyAccountWidget2(
                    title: 'My favourites',
                    icon: 'assets/icons/favourites_icon.svg',
                  ),
                  onTap: (){
                    //do something here
                  },
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            Container(
              width: screenHeight(context, dividedBy: 1),
              height: 0.5,
              color: Constants.kitGradients[6],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 85),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'MY ACTIVITIES',
                  style: TextStyle(
                      fontFamily: 'MuliBold',
                      fontSize: 16,
                      color: Constants.kitGradients[0]),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 85),
            ),
            MyAccountWidget3(
              title1: 'Redeemed on 28 Nov 2020, 12:34PM',
              title2: '1-FOR-1 LATTE',
              title3: 'Workspace Espresso Bar  |  Guoco Tower',
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 85),
            ),
            MyAccountWidget3(
              title1: 'Redeemed on 15 Nov 2020, 07:42PM',
              title2: 'UP TO 30% OFF TENNIS GEAR',
              title3: 'Royal Sporting House  |  ION Orchard',
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 85),
            ),
            MyAccountWidget3(
              title1: 'Redeemed on 12 Nov 2020, 04:23PM',
              title2: '\$49.90 TEA TIME SET (U.P. \$70.00)',
              title3: 'TWG Tea  |  Republic Plaza',
            ),
          ],
        ),
      ),
    );
  }
}
