import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/login_page.dart';
import 'package:guoco_priv_app/src/ui/screens/forget_password_sent.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/description_text.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ForgetPassword extends StatefulWidget {
  @override
  _ForgetPasswordState createState() => _ForgetPasswordState();
}

class _ForgetPasswordState extends State<ForgetPassword> {
  TextEditingController emailTextEditingController =
      new TextEditingController();
  AuthBloc userBloc = AuthBloc();

  @override
  void initState() {
    print("AAAAA");
    userBloc.forgotPasswordResponse.listen((event) {
      print(event);
      if (event.statuscode == 200 && event.errorcode == 0) {
        push(
            context,
            ForgetPasswordSent(
              title: event.data,
            ));
      }
      // showToast(event.data);
    });

    super.initState();
  }

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, Login(), false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: _willPopCallback,
        child: Scaffold(
            resizeToAvoidBottomPadding: false,
            body: Stack(
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 1),
                  width: screenWidth(context, dividedBy: 1),
                  child: SvgPicture.asset("assets/images/login_bg_image.svg",
                      color: Colors.black.withOpacity(0.80),
                      fit: BoxFit.cover,
                      colorBlendMode: BlendMode.darken),
                ),
                Column(
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 40),
                    ),
                    AppBarGuocoLand(
                      pageTitle: "RESET PASSWORD",
                      icon: true,
                      boxWidth: 4.5,
                      pageNavigation: () {
                        pushAndRemoveUntil(context, Login(), false);
                      },
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 4),
                    ),
                    DescriptionText(
                      descriptionText:
                          "Please key in the email address you registered with",
                      textSize: 19,
                      horizontalPadding: screenWidth(context, dividedBy: 11),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 10),
                    ),
                    FormFeildUserDetails(
                      hintText: "Email Address",
                      textEditingController: emailTextEditingController,
                      onPressed: () {},
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 3.2),
                    ),
                    BuildButton(
                      title: "RESET PASSWORD",
                      disabled: true,
                      onPressed: () {
                        final bool isValid = EmailValidator.validate(
                            emailTextEditingController.text);
                        print(isValid);
                        if (isValid) {
                          print("called");
                          userBloc.forgotPassword(
                              email: emailTextEditingController.text);
                        } else {
                          print("wrong email");
                          showToast("Invalid Email ID");
                        }
                      },
                    ),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
