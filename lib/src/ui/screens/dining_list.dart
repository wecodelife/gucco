import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_navigation_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/grid_view.dart';
import 'package:guoco_priv_app/src/ui/widgets/home_tile.dart';
import 'package:guoco_priv_app/src/ui/widgets/search_bar.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
class DiningList extends StatefulWidget {
  @override
  _DiningListState createState() => _DiningListState();
}

class _DiningListState extends State<DiningList> {
  List<int> likes = [1, 2, 3, 4, 5];
  int index;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(

        body: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/bg_blackgradient.png"),
                    fit: BoxFit.cover),
              ),
              child: ListView(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 11),
                  ),
                  Container(
                      decoration: BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.topCenter,
                              end: Alignment.bottomCenter,
                              colors: [
                                Colors.black26,
                                Colors.black26,
                                Colors.transparent
                              ])),
                      child: SearchBar(
                        searchIcon: "assets/icons/search_icon.svg",
                        hintText: "Search privilages & merchants",
                      )),

                  SizedBox(
                    height: screenHeight(context,dividedBy: 20),
                  ),



                  SizedBox(
                    height: screenHeight(context,dividedBy: 100),
                  ),


                  CustomGridView(
                                        itemCount: 5,
                    tile: HomeTile(width: 2.8,height: 5,isSubHeading: false,likeBar: true,
                      likeButtonBrown: false,
                      likes: index,
                      itemDescription:"Workspace Espresso Bar (Guoco Tower)",
                      itemImage: "assets/images/card_image.png",
                      boxHeight: 4.2,
                      borderRadius: 6,

                    ),aspectoRatio: 2/2,




                  ),


                  SizedBox(
                    height: screenWidth(context,dividedBy: 20),
                  ),








                  SizedBox(
                    height: screenHeight(context,dividedBy: 20),
                  )









                ],
              ),
            ),
            Container(
              height: screenHeight(context, dividedBy: 11),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Colors.black26,
                        Colors.black26,
                        Colors.black26,
                        Colors.black26,
                        Colors.black12,
                      ])),
            ),




            Column(
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 11),
                  decoration: BoxDecoration(
                      color: Colors.black.withOpacity(0.2),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black12,
                            offset: Offset(8, 8),
                            blurRadius: 2,spreadRadius: 3
                        )

                      ]
                  ),
                  child: AppBarBookingPage(
                      leftIcon: "assets/icons/arrow_back.png",
                      rightIcon: "assets/icons/bell_icon.png",
                      pageTitle: "DINING",
                      transparent: true),
                ),
              ],
            ),

            Positioned(
                bottom: 0,
                child: CustomBottomNavigationBar(
                  value: "home",
                ))
          ],
        )
      ),
    );
  }
}
