import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/my_profile_widget_1.dart';
import 'package:guoco_priv_app/src/ui/widgets/my_profile_widget_2.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:sliding_switch/sliding_switch.dart';

class MyProfile extends StatefulWidget {
  @override
  _MyProfileState createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  bool buttonStatus;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[7],
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[7],
        actions: [
          Container(
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                GestureDetector(
                  child: Container(
                    child: SvgPicture.asset('assets/icons/back_button.svg'),
                    width: screenWidth(context, dividedBy: 6),
                  ),
                  onTap: () {
                    //do something here
                  },
                ),
                Text(
                  'MY PROFILE',
                  style: TextStyle(
                      fontFamily: 'MuliBold',
                      fontSize: 16,
                      color: Constants.kitGradients[0]),
                ),
                GestureDetector(
                  child: Container(
                    child: SvgPicture.asset('assets/icons/edit_button.svg'),
                    width: screenWidth(context, dividedBy: 6),
                  ),
                  onTap: () {
                    //do something here
                  },
                ),
              ],
            ),
          )
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 35),
            vertical: screenHeight(context, dividedBy: 55)),
        child: Column(
          children: [
           Row(
             mainAxisAlignment: MainAxisAlignment.center,
             children: [
               Text(
                 'Mr John Smith',
                 style: TextStyle(
                     fontFamily: 'MuliBold',
                     fontSize: 26,
                     fontWeight: FontWeight.w300,
                     color: Constants.kitGradients[0]),
               ),
             ],
           ),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            MyProfileWidget1(title1: 'Birthday',title2: 'January 1960',),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
           MyProfileWidget1(title1: 'Contact No.',title2: '9876 5432',),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
           MyProfileWidget1(title1: 'Email Address',title2: 'johnsmith@gmail.com',),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
           MyProfileWidget1(title1: 'Postal Code',title2: '237992',),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
           MyProfileWidget1(title1: 'Unit',title2: '#12-01',),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
           MyProfileWidget1(title1: 'Block / Tower',title2: '01',),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            Container(
              width: screenHeight(context, dividedBy: 1),
              height: 0.5,
              color: Constants.kitGradients[6],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
           MyProfileWidget2(title1: 'Password',title2: 'Last Updated: 01 Nov 2020',),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            Container(
              width: screenHeight(context, dividedBy: 1),
              height: 0.5,
              color: Constants.kitGradients[6],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            MyProfileWidget2(title1: 'My Preferences',title2: 'Entertainment, F&B, Retail',),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            Container(
              width: screenHeight(context, dividedBy: 1),
              height: 0.5,
              color: Constants.kitGradients[6],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Push Notifications',
                  style: TextStyle(
                      fontFamily: 'MuliBold',
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                      color: Constants.kitGradients[0]),
                ),
                SlidingSwitch(
                  value: false,
                  width: screenWidth(context,dividedBy: 5),
                  onChanged: (bool value) {
                    print(value);
                    if(value==false){
                      setState(() {
                        buttonStatus = false;
                      });
                    }else{
                      setState(() {
                        buttonStatus = true;
                      });
                    }
                  },
                  height : screenHeight(context,dividedBy: 18),
                  animationDuration : const Duration(milliseconds: 400),
                  onTap:(){},
                  onDoubleTap:(){},
                  onSwipe:(){},
                  colorOn : const Color(0xfff7f5f7),
                  colorOff : const Color(0xfff7f5f7),
                  background : const Color(0xff6a6a6a),
                  buttonColor : const Color(0xfff7f5f7),
                  inactiveColor : const Color(0xff6a6a6a),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            Container(
              width: screenHeight(context, dividedBy: 1),
              height: 0.5,
              color: Constants.kitGradients[6],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            buttonStatus==true?
            GestureDetector(
              child: Container(
                width: screenWidth(context,dividedBy: 1.1),
                height: screenHeight(context,dividedBy:15),
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Constants.kitGradients[10],
                      width: 0.5
                    ),
                    borderRadius: BorderRadius.circular(25)
                ),
                child: Center(
                  child: Text('DELETE ACCOUNT',
                      style: TextStyle(
                          fontFamily: 'SFPro',
                          fontSize: 15,
                          color: Constants.kitGradients[0])),
                ),
              ),
              onTap: (){
                //do something here
              },
            ):Container()
          ],
        ),
      ),
    );
  }
}
