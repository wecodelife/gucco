import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/ui/screens/help_and_faq_2.dart';
import 'package:guoco_priv_app/src/ui/widgets/help_and_faq_appbar.dart';
import 'package:guoco_priv_app/src/ui/widgets/help_and_faq_faq_categories.dart';
import 'package:guoco_priv_app/src/ui/widgets/help_and_faq_search_bar.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HelpAndFAQ extends StatefulWidget {
  @override
  _HelpAndFAQState createState() => _HelpAndFAQState();
}

class _HelpAndFAQState extends State<HelpAndFAQ> {
  TextEditingController textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 1),
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/help_and_faq_background.png'),
                fit: BoxFit.fill)),
        child: Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 40)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 20),
              ),
              HelpAndFaqAppbar(
                title: 'HELP & FAQ',
                icon: SvgPicture.asset('assets/icons/homescreen_menu_icon.svg'),
                onTap: () {
                  //do something here
                },
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 20),
              ),
              HelpAndFAQSearchBAR(
                hintText: 'How can we help?',
                textEditingController: textEditingController,
                onTap: () {
                  //do something
                },
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 25),
              ),
              HelpAndFAQCategories(
                title: "About GuocoLand Privilege Club and Membership",
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => HelpAndFaq2()));
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
