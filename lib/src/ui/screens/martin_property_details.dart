import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/double_text.dart';
import 'package:guoco_priv_app/src/ui/widgets/drawer_guoco.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class MartinPropertyDetails extends StatefulWidget {
  @override
  _MartinPropertyDetailsState createState() => _MartinPropertyDetailsState();
}

class _MartinPropertyDetailsState extends State<MartinPropertyDetails> {
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      drawer: DrawerGuoco(count: 5,),
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[7],
        leading: Container(),
        actions: [
          AppBarBookingPage(
            // rightIcon: "assets/icons/estate_icon.png",
            noRightIcon: true,
            leftIcon: "assets/icons/facilitiesmenu.png",
            pageTitle: "Property Details",
            // onTapRightIcon: () {
            //   Navigator.pushAndRemoveUntil(
            //       context,
            //       MaterialPageRoute(
            //           builder: (context) => EstatePage(
            //             page: "tower",
            //           )),
            //           (route) => false);
            // },
            onTapLeftIcon: (){
              _globalKey.currentState.openDrawer();
            },
          )
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
         Column(
           children: [
             Container(
               width: screenWidth(context, dividedBy: 1),
               height: screenHeight(context, dividedBy: 3.2),
               decoration: BoxDecoration(
                   image: DecorationImage(
                     image: AssetImage(
                       "assets/images/estate_bg.png",
                     ),
                     fit: BoxFit.fill,
                   )),),
             SizedBox(
               height: screenHeight(context,dividedBy:50),
             ),
             Row(
               children: [
                 SizedBox(
                   width: screenWidth(context,dividedBy: 30),
                 ),
                 Text("Martin Modern",style: TextStyle(fontSize: 18,fontFamily: "SFProTextSemiBold",fontWeight: FontWeight.w600)),
               ],
             ),
             DoubleText(
               label: "Phone Number",
               text1: "+65 9876 5432",
             ),
             DoubleText(
               label: "Email Address",
               text1: "info@martinmodern.com.sg",
             ),
             SizedBox(
               height: screenHeight(context,dividedBy:60),
             ),
             Container(
               width: screenWidth(context,dividedBy: 1),
               height: screenHeight(context,dividedBy: 300),
               color: Constants.kitGradients[26].withOpacity(.15),
             ),
             DoubleText(
               is2line:true,
               label: "Address",
               text1: "8, 10, 12 Martin Place",
               text2: "S237992, S237963, S237964",
             ),
             SizedBox(
               height: screenHeight(context,dividedBy:60),
             ),
             Container(
               width: screenWidth(context,dividedBy: 1),
               height: screenHeight(context,dividedBy: 300),
               color: Constants.kitGradients[26].withOpacity(.15),
             ),
             SizedBox(
               height: screenHeight(context,dividedBy:70),
             ),
           ],
         ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              SizedBox(
                height: screenHeight(context,dividedBy: 50),
              ),
              BottomBar()
            ],
          )
        ],
      ),
    );
  }
}
