import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/login_page.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ForgetPasswordSent extends StatefulWidget {
  final String title;
  ForgetPasswordSent({this.title});
  @override
  _ForgetPasswordSentState createState() => _ForgetPasswordSentState();
}

class _ForgetPasswordSentState extends State<ForgetPasswordSent> {
  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, Login(), false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
          body: Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage(
                  "assets/images/login.png",
                ),
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(
                    Colors.black.withOpacity(0.8), BlendMode.darken),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 89)),
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 10),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Container(
                      child: Text(
                        widget.title,
                        style: TextStyle(
                          color: Constants.kitGradients[0],
                          fontFamily: 'Muli',
                          fontSize: screenWidth(context, dividedBy: 18),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 10),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 20,
                    ),
                    child: Container(
                      child: Text(
                        "(If you don’t see it in your Inbox, it may have been sorted into your Spam folder.)",
                        style: TextStyle(
                          color: Constants.kitGradients[0],
                          fontFamily: 'Muli',
                          fontSize: screenWidth(context, dividedBy: 19),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
