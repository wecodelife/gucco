
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/pdf.dart';
class PDFView extends StatefulWidget {
  @override
  _PDFViewState createState() => _PDFViewState();
}

class _PDFViewState extends State<PDFView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PDF.network(
        'https://google-developer-training.github.io/android-developer-fundamentals-course-concepts/en/android-developer-fundamentals-course-concepts-en.pdf',
        height:  MediaQuery.of(context).size.height,

        width: MediaQuery.of(context).size.width,
        // placeHolder: Image.asset("assets/images/pdf.png",
            // height: 200, width: 100),
      ),
    );
  }
}
