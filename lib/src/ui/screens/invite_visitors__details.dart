import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/visitors_confirmation.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/invite_visitor_body2_optional.dart';
import 'package:guoco_priv_app/src/ui/widgets/invite_visitors_body1.dart';
import 'package:guoco_priv_app/src/ui/widgets/invite_visitors_body2.dart';
import 'package:guoco_priv_app/src/ui/widgets/key_collection_appbar_bottom2.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'martin_modern/estate_page.dart';
import 'home_screen.dart';

class VisitorDetails extends StatefulWidget {
  @override
  _VisitorDetailsState createState() => _VisitorDetailsState();
}

class _VisitorDetailsState extends State<VisitorDetails> {
  ScrollController _scrollController = ScrollController();
  bool mandatoryFieldStatus, optionalFieldStatus;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[7],
        leading: Container(),
        actions: [
          AppBarBookingPage(
            pageTitle: 'MY VISITORS',
            rightIcon: "assets/icons/estate_icon.png",
            leftIcon: "assets/icons/arrow_back.png",
            onTapLeftIcon: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => HomeScreen()),
                  (route) => false);
            },
            onTapRightIcon: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => EstatePage()),
                  (route) => false);
            },
          ),
        ],
      ),
      backgroundColor: Constants.kitGradients[0],
      body: Stack(
        children: [
          SingleChildScrollView(
            controller: _scrollController,
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                TowerName(
                  towerName: "Tower 1",
                  towerSubName: "01 - -01",
                ),
                Column2(
                  title: "Event: Invite Visitor",
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                InviteVisitorBody1(
                  date: '15-April-2020',
                  fromTime: '10:00 AM',
                  toTime: '10:30 AM',
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 22),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                              'assets/images/key_collection_background.png'),
                          fit: BoxFit.fill)),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: InviteVisitorsBody2(
                    mandatoryFieldStatus: mandatoryFieldStatus,
                    title: 'Mandatory Fields',
                    valueChanged: (value) {
                      setState(() {
                        mandatoryFieldStatus = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 22),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(
                              'assets/images/key_collection_background.png'),
                          fit: BoxFit.fill)),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: InviteVisitorBody2Optional(
                    optionalFieldStatus: mandatoryFieldStatus,
                    title: 'Optional Fields',
                    valueChanged: (value) {
                      setState(() {
                        mandatoryFieldStatus = value;
                      });
                    },
                  ),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 5.4),
                ),
                BuildButtonIcon(
                  title: "Confirm Invite",
                  onPressed: () {
                    push(context, VisitorsConfirmation());
                    //do something here
                  },
                  arrowIcon: false,
                  faultReport: true,
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                BottomBar()
              ],
            ),
          ),
        ],
      ),
    );
  }
}
