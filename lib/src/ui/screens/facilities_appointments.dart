import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/facilities_list.dart';
import 'package:guoco_priv_app/src/ui/screens/facilities_list_bookings.dart';
import 'package:guoco_priv_app/src/ui/screens/home_post_move_in.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/booking_calender_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/event_info.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FacilitiesAppointments extends StatefulWidget {
  final String blockName;
  final String floorAndUnitId;
  FacilitiesAppointments({
    this.floorAndUnitId,
    this.blockName,
  });
  @override
  _FacilitiesAppointmentsState createState() => _FacilitiesAppointmentsState();
}

class _FacilitiesAppointmentsState extends State<FacilitiesAppointments> {
  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(
        context,
        HomePostMoveIn(
          blockName: widget.blockName,
          floorAndUnitId: widget.floorAndUnitId,
          condInfoId: ObjectFactory().appHive.getCondoInfoId(),
        ),
        false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        bottom: true,
        child: Scaffold(
            resizeToAvoidBottomPadding: true,
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Constants.kitGradients[7],
              leading: Container(),
              actions: [
                AppBarBookingPage(
                  rightIcon: "assets/icons/estate_icon.png",
                  leftIcon: "assets/icons/facilitiesmenu.png",
                  pageTitle: "FACILITIES",
                  onTapRightIcon: () {
                    Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => EstatePage()),
                        (route) => false);
                  },
                  onTapLeftIcon: () {
                    push(
                        context,
                        HomePostMoveIn(
                          blockName: widget.blockName,
                          floorAndUnitId: widget.floorAndUnitId,
                          condInfoId: ObjectFactory().appHive.getCondoInfoId(),
                        ));
                  },
                )
              ],
            ),
            body: ListView(
              scrollDirection: Axis.vertical,
              children: [
                BookingCalenderPage(
                  towerName: widget.blockName,
                  towerSubName: widget.floorAndUnitId,
                  heading: "My Bookings",
                  rightIcon: true,
                  icon: "assets/icons/keyCollectionIcon.png",
                  bookListTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FacilitiesListBookings(
                                  floorAndUnitId: widget.floorAndUnitId,
                                  blockName: widget.blockName,
                                )));
                  },
                ),
                EventInfo(
                  eventHeading: "Function Room",
                  eventDate: "03-April-2020",
                  eventTime: "10:00 AM -12:30 AM",
                  enable: false,
                  onTapCloseButtonOnly: () {
                    actionAlertBox(
                        questionAlert: "Do you wish to proceed?",
                        cancelAlert: 1,
                        contentNum: 3,
                        context: context,
                        msg: "Cancel Booking",
                        content1: "Function Room",
                        heading1: "Facility:",
                        heading2: "Date",
                        content2: "15-April-2020",
                        heading3: "Time",
                        content3: "10:00 AM -12:00 AM");
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 9),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 10),
                    ),
                    BuildButtonIcon(
                      faultReport: true,
                      icon: "assets/icons/KeyCollectionBookinIcon.png",
                      title: "New Booking",
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => FacilitiesList(
                                      blockName: widget.blockName,
                                      floorAndUnitId: widget.floorAndUnitId,
                                    )));
                      },
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 29),
                ),
                BottomBar(),
              ],
            )),
      ),
    );
    ;
  }
}
