import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/get_current_booking_for_facility_response_model.dart';
import 'package:guoco_priv_app/src/models/get_past_bookings_for_a_facility_response.dart';
import 'package:guoco_priv_app/src/ui/screens/facilities_appointments.dart';
import 'package:guoco_priv_app/src/ui/screens/facilities_list.dart';
import 'package:guoco_priv_app/src/ui/screens/home_post_move_in.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/drawer_guoco.dart';
import 'package:guoco_priv_app/src/ui/widgets/list_view_list_booking.dart';
import 'package:guoco_priv_app/src/ui/widgets/tab_bar_bookings.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FacilitiesListBookings extends StatefulWidget {
  final String blockName;
  final String floorAndUnitId;
  FacilitiesListBookings({
    this.floorAndUnitId,
    this.blockName,
  });
  @override
  _FacilitiesListBookingsState createState() => _FacilitiesListBookingsState();
}

class _FacilitiesListBookingsState extends State<FacilitiesListBookings> {
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();
  EstateBloc estateBloc = new EstateBloc();
  String tabValueSelected = "currentbookings";
  String facilityBookingId;
  bool isLoading = false;
  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(
        context,
        HomePostMoveIn(
          floorAndUnitId: widget.floorAndUnitId,
          blockName: widget.blockName,
          condInfoId: ObjectFactory().appHive.getCondoInfoId(),
        ),
        false);
    return true;
  }

  @override
  void initState() {
    estateBloc.getCurrentBookingsForFacility(
        unitInfoId: ObjectFactory().appHive.getUnitInfoId());

    estateBloc.getPastBookingsForFacility(
        unitInfoId: ObjectFactory().appHive.getUnitInfoId());

    estateBloc.cancelAFacilityBookingResponse.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200) {
        setState(() {
          isLoading = false;
        });
        cancelAlertBox(
            onPressed: () {
              pop(context);
              pushAndRemoveUntil(
                  context,
                  FacilitiesListBookings(
                    blockName: widget.blockName,
                    floorAndUnitId: widget.floorAndUnitId,
                  ),
                  false);
            },
            context: context,
            msg: "Booking Cancelled",
            text1:
                "If You Have already made the deposit,\n you will receive it within 3 working days",
            text2:
                "  To make Another Booking, head to,\n Booking Facilities>NewBooking",
            contentPadding: 40,
            insetPadding: 2.8,
            titlePadding: 3);
      } else {
        Text("Please Check yout Internet Connection");
      }
    });
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        key: _globalKey,
        drawer: DrawerGuoco(
          count: 4,
        ),
        resizeToAvoidBottomPadding: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[7],
          leading: Container(),
          actions: [
            AppBarBookingPage(
              rightIcon: "assets/icons/estate_icon.png",
              leftIcon: "assets/icons/arrow_back.png",
              pageTitle: "FACILITIES",
              onTapLeftIcon: () {
                pop(context);
              },
              onTapRightIcon: () {
                push(context, EstatePage());
              },
            ),
          ],
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              children: [
                TowerName(
                  towerName: widget.blockName,
                  towerSubName: widget.floorAndUnitId,
                ),
                AppBarRightIcon(
                  pageTitle: "My Bookings",
                  boxWidth: 6,
                  icon: "assets/icons/switchviewcalendar.png",
                  rightIcon: false,
                  bookedListTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FacilitiesAppointments(
                                  floorAndUnitId: widget.floorAndUnitId,
                                  blockName: widget.blockName,
                                )));
                  },
                ),
                TabBarBookings(
                  heading1: "CURRENT BOOKINGS",
                  heading2: "PAST BOOKINGS",
                  indicatorColor: Constants.kitGradients[11],
                  tabColor: Constants.kitGradients[0],
                  pageType: "facilities",
                  tabValueSelected: tabValueSelected,
                  onPressedCurrentBookings: () {
                    setState(() {
                      tabValueSelected = "currentbookings";
                    });
                    estateBloc.getCurrentBookingsForFacility(
                        unitInfoId: ObjectFactory().appHive.getUnitInfoId());
                  },
                  onPressedPastBookings: () {
                    setState(() {
                      tabValueSelected = "pastbookings";
                    });
                    estateBloc.getPastBookingsForFacility(
                        unitInfoId: ObjectFactory().appHive.getUnitInfoId());
                  },
                  currentBookingsWidget: StreamBuilder<
                          GetCurrentBookingsForFacilityResponse>(
                      stream: estateBloc.getCurrentBookingsForFacilityResponse,
                      builder: (context, snapshot) {
                        return snapshot.hasData
                            ? Container(
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 1.76),
                                child: ListView.builder(
                                    itemCount: snapshot.data.data.length,
                                    itemBuilder:
                                        (BuildContext cntxt, int index) {
                                      return ListViewListBookings(
                                        title: snapshot
                                            .data.data[index].facilityname,
                                        evenDate:
                                            snapshot.data.data[index].ondate,
                                        eventStartTime:
                                            snapshot.data.data[index].starttime,
                                        eventEndTime:
                                            snapshot.data.data[index].endtime,
                                        onPressed: () {
                                          facilityBookingId =
                                              snapshot.data.data[index].id;

                                          actionAlertBox(
                                              context: context,
                                              cancelAlert: 6,
                                              text1: "",
                                              text2: "",
                                              isLoading: isLoading,
                                              onPressed: () {
                                                setState(() {
                                                  isLoading = true;
                                                });

                                                estateBloc
                                                    .cancelAFacilityBooking(
                                                        facilityBookingId:
                                                            facilityBookingId);
                                              },
                                              contentNum: 5,
                                              msg: "Cancel Booking",
                                              content1: "",
                                              heading1: "",
                                              content2: snapshot.data
                                                  .data[index].facilityname,
                                              heading2: "Facility",
                                              heading3: "",
                                              content3: "",
                                              heading4: "Date",
                                              content4: snapshot
                                                  .data.data[index].ondate,
                                              heading5: "Time",
                                              content5: snapshot
                                                  .data.data[index].starttime,
                                              questionAlert:
                                                  "                       Do you wish to procced  ?");
                                        },
                                      );
                                    }),
                              )
                            : Container(
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 2),
                                child: Center(
                                  child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Constants.kitGradients[1]),
                                  ),
                                ));
                      }),
                  pastBookingsWidget: StreamBuilder<
                          GetPastBookingsForFacilityResponse>(
                      stream: estateBloc.getPastBookingsForFacilityResponse,
                      builder: (context, snapshot) {
                        return snapshot.hasData
                            ? Container(
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 1.76),
                                child: ListView.builder(
                                    itemCount: snapshot.data.data.length,
                                    itemBuilder:
                                        (BuildContext cntxt, int index) {
                                      return ListViewListBookings(
                                        pastBookings: true,
                                        onPressed: () {
                                          facilityBookingId =
                                              snapshot.data.data[index].id;
                                        },
                                        title: snapshot
                                            .data.data[index].facilityname,
                                        evenDate:
                                            snapshot.data.data[index].ondate,
                                        eventStartTime:
                                            snapshot.data.data[index].starttime,
                                        eventEndTime:
                                            snapshot.data.data[index].endtime,
                                      );
                                    }),
                              )
                            : Container(
                                width: screenWidth(context, dividedBy: 1),
                                height: screenHeight(context, dividedBy: 2),
                                child: Center(
                                  child: CircularProgressIndicator(
                                    valueColor:
                                        new AlwaysStoppedAnimation<Color>(
                                            Constants.kitGradients[1]),
                                  ),
                                ));
                      }),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                BuildButton(
                  faultReport: true,
                  title: "New Booking",
                  disabled: true,
                  onPressed: () {
                    print(1);

                    push(
                        context,
                        FacilitiesList(
                          floorAndUnitId: widget.floorAndUnitId,
                          blockName: widget.blockName,
                        ));
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 30),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
