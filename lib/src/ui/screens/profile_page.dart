import 'dart:io';

import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/image_upload_request_model.dart';
import 'package:guoco_priv_app/src/models/unit_users_response.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/change_password.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/copy_past_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/double_text.dart';
import 'package:guoco_priv_app/src/ui/widgets/drawer_guoco.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shimmer/shimmer.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  EstateBloc estateBloc = EstateBloc();
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();
  final picker = ImagePicker();
  File imagePicked;
  String imageId;
  Future getImageGallery() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.gallery, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
      });
    }
    estateBloc.uploadProfileImage(
        imageUploadRequestModel: ImageUploadRequestModel(
            file: imagePicked, category: "PROPERTYINFO-MOBILE"));
  }

  Future getImageCamera() async {
    final pickedFile =
        await picker.getImage(source: ImageSource.camera, imageQuality: 25);
    if (pickedFile != null) {
      setState(() {
        imagePicked = File(pickedFile.path);
      });

      estateBloc.uploadProfileImage(
          imageUploadRequestModel: ImageUploadRequestModel(
              file: imagePicked, category: "PROPERTYINFO-MOBILE"));
    }
  }

  @override
  void initState() {
    estateBloc.getUnitUsers();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        drawer: DrawerGuoco(
          count: 3,
        ),
        key: _globalKey,
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[7],
          leading: Container(),
          actions: [
            AppBarBookingPage(
              pageTitle: "Profile",
              leftIcon: "assets/icons/facilitiesmenu.png",
              rightIcon: "assets/icons/estate_icon.png",
              onTapRightIcon: () {},
              onTapLeftIcon: () {
                _globalKey.currentState.openDrawer();
              },
            ),
          ],
        ),
        body: Column(
          children: [
            Padding(
              padding:
                  EdgeInsets.only(left: screenWidth(context, dividedBy: 90)),
              child: Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 25),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: screenWidth(context, dividedBy: 1.5),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: screenHeight(context, dividedBy: 70),
                            ),
                            Row(
                              children: [
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 30),
                                ),
                                Text(ObjectFactory().appHive.getName(),
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontFamily: "SFProTextSemiBold",
                                        fontWeight: FontWeight.w600)),
                              ],
                            ),
                            Row(
                              children: [
                                DoubleText(
                                  label: "Phone Number",
                                  text1: ObjectFactory()
                                              .appHive
                                              .getContactNo() !=
                                          null
                                      ? ObjectFactory().appHive.getContactNo()
                                      : "No PhoneNumber",
                                ),
                              ],
                            ),
                            DoubleText(
                              label: "Email Address",
                              text1: ObjectFactory().appHive.getEmail(),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: screenWidth(context, dividedBy: 100),
                      ),
                      // GestureDetector(
                      //   onTap: () {
                      //     bottomSheetAddPhotos(context);
                      //   },
                      //   child: ClipRRect(
                      //     borderRadius: BorderRadius.circular(100),
                      //     child: Container(
                      //       width: screenWidth(context, dividedBy: 4),
                      //       height: screenWidth(context, dividedBy: 4),
                      //       decoration: BoxDecoration(
                      //           color: Colors.white, shape: BoxShape.circle),
                      //       child: imagePicked == null
                      //           ? ObjectFactory().appHive.getUserImage() != null
                      //               ? NetworkImage(
                      //                   ObjectFactory().appHive.getUserImage())
                      //               : Image(
                      //                   image: AssetImage(
                      //                       "assets/images/user_image.png"))
                      //           : Image.file(
                      //               imagePicked,
                      //               fit: BoxFit.cover,
                      //             ),
                      //     ),
                      //   ),
                      // )
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Divider(
                    thickness: 0.3,
                    color: Constants.kitGradients[26],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 25),
                      ),
                      Text(
                        "Unit Information",
                        style: TextStyle(
                            color: Constants.kitGradients[26],
                            fontSize: 13,
                            fontFamily: "SFProText-Regular",
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Container(
                    height: screenWidth(context, dividedBy: 6),
                    child: StreamBuilder<UnitUsersResponse>(
                        stream: estateBloc.getUnitUser,
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? snapshot.data.data.isNotEmpty
                                  ? ListView.builder(
                                      physics: ClampingScrollPhysics(),
                                      shrinkWrap: true,
                                      scrollDirection: Axis.vertical,
                                      itemCount: snapshot.data.data.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        // print("data length");
                                        return Column(
                                          children: [
                                            Row(
                                              children: [
                                                SizedBox(
                                                  width: screenWidth(context,
                                                      dividedBy: 25),
                                                ),
                                                Text(
                                                  snapshot.data.data[index]
                                                          .blockname +
                                                      ", #" +
                                                      snapshot.data.data[index]
                                                          .floorname +
                                                      "-" +
                                                      snapshot.data.data[index]
                                                          .unitno,
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontSize: 16,
                                                      fontFamily:
                                                          "SFProText-Regular",
                                                      fontWeight:
                                                          FontWeight.w400),
                                                ),
                                              ],
                                            ),
                                            SizedBox(
                                              height: screenHeight(context,
                                                  dividedBy: 80),
                                            ),
                                          ],
                                        );
                                      })
                                  : Container(
                                      height:
                                          screenHeight(context, dividedBy: 6),
                                      width: screenWidth(context, dividedBy: 1),
                                      child: Center(
                                        child: Text("No units found!",
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 12,
                                            )),
                                      ),
                                    )
                              : Shimmer.fromColors(
                                  highlightColor: Colors.grey,
                                  baseColor: Colors.white,
                                  child: Container(
                                    height: screenHeight(context, dividedBy: 6),
                                    width: screenWidth(context, dividedBy: 1),
                                    decoration:
                                        BoxDecoration(color: Colors.black54),
                                  ),
                                );
                        }),
                  ),
                  Divider(
                    thickness: 0.3,
                    color: Constants.kitGradients[26],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      Text(
                        "Password",
                        style: TextStyle(
                            color: Constants.kitGradients[26],
                            fontSize: 13,
                            fontFamily: "SFProText-Regular",
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 80),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      Text(
                        "Last change on " +
                            ObjectFactory().appHive.getPasswordLastChangesOn(),
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontFamily: "SFProText-Regular",
                            fontWeight: FontWeight.w400),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 30),
                      ),
                      BorderColorGoldButton(
                        boxWidth: 10,
                        borderTrue: true,
                        heading: "Change Password",
                        onTap: () {
                          push(context, ChangePassword());
                        },
                      ),
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 5),
            ),
          ],
        ),
      ),
    );
  }

  void bottomSheetAddPhotos(context) {
    showModalBottomSheet<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(builder: (BuildContext context, setState) {
            return Container(
                color: Colors.white,
                height: screenHeight(context, dividedBy: 4),
                width: screenWidth(context, dividedBy: 1),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(
                          right: screenWidth(context, dividedBy: 10)),
                      child: GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Cancel",
                            style: TextStyle(
                              color: Constants.kitGradients[1],
                              fontFamily: "Poppins",
                              fontSize: 12,
                            ),
                          )),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        GestureDetector(
                            onTap: () {
                              getImageCamera();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.camera,
                              size: 50,
                              color: Constants.kitGradients[1],
                            )),
                        GestureDetector(
                            onTap: () {
                              getImageGallery();
                              Navigator.pop(context);
                            },
                            child: Icon(
                              Icons.image,
                              size: 50,
                              color: Constants.kitGradients[1],
                            )),
                      ],
                    ),
                  ],
                ));
          });
        });
  }
}
