import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/models/create_password_request.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/otp_page.dart';
import 'package:guoco_priv_app/src/ui/screens/customise_exp_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_field_password.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class CreatePassword extends StatefulWidget {
  @override
  _CreatePasswordState createState() => _CreatePasswordState();
}

class _CreatePasswordState extends State<CreatePassword> {
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  TextEditingController rePasswordTextEditingController =
      new TextEditingController();
  AuthBloc userBloc = AuthBloc();
  bool buttonLoading = false;
  bool hasPasswordStrength = false;
  bool enablePasswordChange = false;
  String passwordStrength = "";
  @override
  void initState() {
    userBloc.createPasswordStreamResponse.listen((event) {
      setState(() {
        buttonLoading = false;
      });
      // event.statuscode == 200
      //     ? Navigator.push(context,
      //         MaterialPageRoute(builder: (context) => CustomiseExpPage()))
      //     : print("create password failed with error code: " +
      //         event.statuscode.toString());
      if (event.statuscode == 200 && event.errorcode == 0) {
        ObjectFactory()
            .appHive
            .putUserPassword(email: passwordTextEditingController.text);
        push(context, CustomiseExpPage());
      } else {
        print("create password failed with error code: " +
            event.statuscode.toString());
      }
    });
    userBloc.checkPasswordStrengthResponse.listen((event) {
      setState(() {
        hasPasswordStrength = true;
        passwordStrength = event.data.errorMessage;
      });
      if (event.data.status == true) {
        setState(() {
          enablePasswordChange = true;
        });
      }
    });
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(context, OtpPage());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        body: ListView(
          children: [
            Stack(
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 1),
                  width: screenWidth(context, dividedBy: 1),
                  child: SvgPicture.asset("assets/images/login_bg_image.svg",
                      color: Colors.black.withOpacity(0.80),
                      fit: BoxFit.cover,
                      colorBlendMode: BlendMode.darken),
                ),
                Container(
                  height: screenHeight(context, dividedBy: 1),
                  width: screenWidth(context, dividedBy: 1),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      AppBarGuocoLand(
                        pageTitle: "CREATE PASSWORD",
                        boxWidth: 4,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 6),
                      ),
                      FormFeildPasswordBox(
                        hintText: "Password",
                        textEditingController: passwordTextEditingController,
                        onChanged: () {
                          if (passwordTextEditingController.text.length > 1) {
                            userBloc.checkPasswordStrength(
                                password: passwordTextEditingController.text);
                          }
                        },
                      ),
                      FormFeildPasswordBox(
                        hintText: 'Re-Enter Password',
                        textEditingController: rePasswordTextEditingController,
                      ),
                      Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 10)),
                          child: hasPasswordStrength == true
                              ? Container(
                                  child: Text(
                                    passwordStrength,
                                    style: TextStyle(
                                        color: Constants.kitGradients[0],
                                        fontSize:
                                            screenWidth(context, dividedBy: 25),
                                        fontFamily: "Muli",
                                        fontStyle: FontStyle.italic),
                                  ),
                                )
                              : Container()),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 8),
                      ),
                      BuildButton(
                        title: "SAVE PASSWORD",
                        onPressed: () {
                          if (passwordTextEditingController.text.trim() ==
                              rePasswordTextEditingController.text.trim()) {
                            setState(() {
                              buttonLoading = true;
                            });

                            if (enablePasswordChange == true) {
                              userBloc.createPassword(
                                  createPasswordRequest: CreatePasswordRequest(
                                      password: passwordTextEditingController
                                          .text
                                          .trim()));
                            } else {
                              setState(() {
                                buttonLoading = false;
                              });
                            }
                          } else {
                            showToastLong("passwords entered does not match");
                          }
                        },
                        isLoading: buttonLoading,
                        disabled: true,
                      )
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
