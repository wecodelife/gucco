import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/login_page.dart';
import 'package:guoco_priv_app/src/ui/screens/fb_google_sign_up_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class SelectLoginOrSignUp extends StatefulWidget {
  @override
  _SelectLoginOrSignUpState createState() => _SelectLoginOrSignUpState();
}

class _SelectLoginOrSignUpState extends State<SelectLoginOrSignUp> {
  @override
  Widget build(BuildContext context) {
    return DoubleBack(
      message: "Press back again to close",
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            child: Stack(
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: screenHeight(context, dividedBy: 1),
                  child: SvgPicture.asset("assets/images/login_bg_image.svg",
                      fit: BoxFit.cover,
                      color: Colors.black.withOpacity(0.60),
                      colorBlendMode: BlendMode.darken),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 9)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 10),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Container(
                          height: screenHeight(context, dividedBy: 3),
                          child:
                              SvgPicture.asset("assets/images/Guoco_logo.svg"),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 4.5),
                      ),
                      BuildButton(
                        title: "LOGIN",
                        disabled: true,
                        isLoading: false,
                        onPressed: () {
                          Navigator.push(context,
                              MaterialPageRoute(builder: (context) => Login()));
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: screenHeight(context, dividedBy: 30)),
                        child: BuildButton(
                          title: "CREATE ACCOUNT",
                          disabled: true,
                          transparent: true,
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => FbGoogleSignUp()));
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
    );
  }
}
