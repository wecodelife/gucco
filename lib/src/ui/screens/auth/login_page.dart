import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/models/login_request_model.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/select_login_or_signup.dart';
import 'package:guoco_priv_app/src/ui/screens/fb_google_sign_up_page.dart';
import 'package:guoco_priv_app/src/ui/screens/forget_password.dart';
import 'package:guoco_priv_app/src/ui/screens/new_home_screen.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_field_password.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  TextEditingController emailTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();

  AuthBloc userBloc = AuthBloc();
  bool isLoading = false;

  @override
  void initState() {
    userBloc.loginResponse.listen((event) async {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        await ObjectFactory()
            .appHive
            .putToken(token: event.data.authtoken.toString());
        await ObjectFactory().appHive.putUserId(userId: event.data.userid);
        //
        // Navigator.push(
        //     context, MaterialPageRoute(builder: (context) => NewHomeScreen()));
        pushAndRemoveUntil(context, NewHomeScreen(), false);
      } else if (event.statuscode == 400 || event.statuscode == 409) {
        showToast(event.errormessage);
      } else {
        setState(() {
          isLoading = false;
        });
        showToast(event.errormessage);
      }
    });
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, SelectLoginOrSignUp(), false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        resizeToAvoidBottomPadding: true,
        body: SingleChildScrollView(
          child: Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(color: Colors.black
                // image: DecorationImage(
                //   image: AssetImage(
                //     "assets/images/login.png",
                //   ),
                //   fit: BoxFit.cover,
                //   colorFilter: new ColorFilter.mode(
                //       Colors.black.withOpacity(0.6), BlendMode.darken),
                // ),
                ),
            child: Stack(
              children: [
                Container(
                  width: screenWidth(context,dividedBy: 1),
                  height: screenHeight(context,dividedBy: 1),
                  child: SvgPicture.asset("assets/images/login_bg_image.svg",
                      fit: BoxFit.cover,
                      color: Colors.black.withOpacity(0.60),
                      colorBlendMode: BlendMode.darken),
                ),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 17),
                      ),
                      Container(
                        height: screenHeight(context, dividedBy: 3),
                        width: screenWidth(context, dividedBy: 1.4),
                        child: SvgPicture.asset("assets/images/Guoco_logo.svg"),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      FormFeildUserDetails(
                        hintText: "Email Address",
                        textEditingController: emailTextEditingController,
                        onPressed: () {},
                      ),
                      FormFeildPasswordBox(
                        hintText: "Password",
                        textEditingController: passwordTextEditingController,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 60),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          GestureDetector(
                            child: Text(
                              "Forgot Password?",
                              style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontFamily: 'MuliSemiBold',
                                  fontSize: 14,
                                  decoration: TextDecoration.underline),
                            ),
                            onTap: () {
                              push(context, ForgetPassword());
                            },
                          ),
                          SizedBox(
                              width: screenWidth(
                            context,
                            dividedBy: 10,
                          )),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 6),
                      ),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   crossAxisAlignment: CrossAxisAlignment.center,
                      //   children: [
                      //     AuthButton(
                      //       buttonColor: Constants.kitGradients[2],
                      //       mediaName: "Facebook login",
                      //       mediaSymbol: "assets/icons/facebook.svg",
                      //       boxWidth: 2.7,
                      //       onPressed: () {
                      //         print("dd");
                      //       },
                      //     ),
                      //     SizedBox(
                      //       width: screenWidth(context, dividedBy: 20),
                      //     ),
                      //     AuthButton(
                      //       buttonColor: Constants.kitGradients[3],
                      //       mediaName: "Google login",
                      //       mediaSymbol: "assets/icons/google.svg",
                      //       boxWidth: 2.7,
                      //       onPressed: () {
                      //         Auth().signInWithGoogle();
                      //       },
                      //     )
                      //   ],
                      // ),
                      // SizedBox(
                      //   height: screenHeight(context, dividedBy: 7),
                      // ),
                      BuildButton(
                        title: "LOGIN",
                        disabled: true,
                        isLoading: isLoading,
                        onPressed: () {
                          if ((emailTextEditingController.text.trim().length >
                                  0) &&
                              (passwordTextEditingController.text
                                      .trim()
                                      .length >
                                  0)) {
                            final bool isValid = EmailValidator.validate(
                                emailTextEditingController.text);
                            if (isValid) {
                              setState(() {
                                isLoading = true;
                              });
                              userBloc.login(
                                  loginRequest: LoginRequest(
                                      username: emailTextEditingController.text,
                                      password:
                                          passwordTextEditingController.text));
                              ObjectFactory()
                                  .appHive
                                  .putEmail(emailTextEditingController.text);
                            } else {
                              showToast("please enter a valid email!");
                            }
                          } else if ((emailTextEditingController.text
                                      .trim()
                                      .length ==
                                  0) &&
                              (passwordTextEditingController.text
                                      .trim()
                                      .length ==
                                  0)) {
                            showToast("please enter email and password");
                          } else if (emailTextEditingController.text
                                  .trim()
                                  .length ==
                              0) {
                            showToast("please enter email");
                          } else if (passwordTextEditingController.text
                                  .trim()
                                  .length ==
                              0) {
                            showToast("please enter password");
                          }
                        },
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Expanded(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              "Don't have an account yet? ",
                              style: TextStyle(
                                color: Constants.kitGradients[0],
                                fontFamily: 'Muli',
                                fontSize: screenWidth(context, dividedBy: 22),
                              ),
                            ),
                            GestureDetector(
                              onTap: () {
                                push(context, FbGoogleSignUp());
                              },
                              child: Text(
                                "Sign up here",
                                style: TextStyle(
                                    color: Constants.kitGradients[0],
                                    fontFamily: 'Muli',
                                    fontSize:
                                        screenWidth(context, dividedBy: 22),
                                    decoration: TextDecoration.underline),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ]),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
