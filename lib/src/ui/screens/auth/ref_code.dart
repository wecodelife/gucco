import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/sign_up_form_one.dart';
import 'package:guoco_priv_app/src/ui/widgets/alert_box_ref_code.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

import '../fb_google_sign_up_page.dart';

class RefCode extends StatefulWidget {
  @override
  _RefCodeState createState() => _RefCodeState();
}

class _RefCodeState extends State<RefCode> {
  TextEditingController refCodeTextEditingController =
      new TextEditingController();
  bool disabled = false;
  bool toast = false;
  bool isLoading = false;
  AuthBloc userBloc = new AuthBloc();

  @override
  void initState() {
    userBloc.referenceCodeCheckResponse.listen((event) {
      print("status" + event.statuscode.toString());
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        print("succesful");
        push(context, SignUpFormOne());
      } else {
        disabled = true;
        setState(() {
          toast = true;
        });

        print("NotSuccesfull");
      }
    }, onError: (error) {
      print("errr" + error.toString());
      setState(() {
        isLoading = false;
        toast = true;
      });
    });

    // TODO: implement initState
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(context, FbGoogleSignUp());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        resizeToAvoidBottomPadding: false,
        body: Stack(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 1),
              width: screenWidth(context, dividedBy: 1),
              decoration: BoxDecoration(),
              child: SvgPicture.asset("assets/images/login_bg_image.svg",
                  color: Colors.black.withOpacity(0.80),
                  fit: BoxFit.cover,
                  colorBlendMode: BlendMode.darken),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 50),
                ),
                AppBarGuocoLand(
                  pageTitle: "REFERENCE CODE",
                  icon: true,
                  boxWidth: 4.9,
                  pageNavigation: () {
                    pop(context);
                  },
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 6),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 10)),
                  child: Container(
                    child: Text(
                      "To proceed with signing up, please enter your reference code:",
                      style: TextStyle(
                        color: Constants.kitGradients[0],
                        fontFamily: 'Muli',
                        fontSize: screenWidth(context, dividedBy: 19),
                      ),
                    ),
                  ),
                ),
                FormFeildUserDetails(
                  hintText: "Reference Code",
                  textEditingController: refCodeTextEditingController,
                  onPressed: () {
                    setState(() {
                      toast = false;
                    });
                  },
                  readOnly: isLoading,
                ),
                toast == true ? ToastRefCode() : Container(),
                toast == true
                    ? SizedBox(
                        height: screenHeight(context, dividedBy: 7),
                      )
                    : Container(),
                toast == false
                    ? SizedBox(
                        height: screenHeight(context, dividedBy: 4),
                      )
                    : Container(),
                BuildButton(
                  title: "PROCEED",
                  onPressed: () {
                    // dataUpdated();
                    // push(context,SignUpFormOne());
                    print("ada" + refCodeTextEditingController.text);
                    if (refCodeTextEditingController.text != null &&
                        refCodeTextEditingController.text != '') {
                      setState(() {
                        isLoading = true;
                      });
                      userBloc.referenceCodeCheck(
                          referenceCode: refCodeTextEditingController.text);
                    } else {
                      showToast("Enter the reference code");
                    }
                  },
                  disabled: true,
                  isLoading: isLoading,
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
