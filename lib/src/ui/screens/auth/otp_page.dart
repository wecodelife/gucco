import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/create_password_page.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/terms_and_condition_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/otp_formfeild.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class OtpPage extends StatefulWidget {
  @override
  _OtpPageState createState() => _OtpPageState();
}

class _OtpPageState extends State<OtpPage> {
  TextEditingController otpTexEditingController = new TextEditingController();

  AuthBloc userBloc = new AuthBloc();
  bool isLoading;
  Timer _timerControl;

  void otpSuccessTimer() {
    _timerControl = Timer.periodic(const Duration(seconds: 3), (timer) {
      _timerControl.cancel();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => CreatePassword()),
          (route) => false);
    });
  }

  @override
  void initState() {
    userBloc.registerPrivilegeClubGenerateOtp();
    // userBloc.registerPrivilegeClubOtpGenerate.listen((event) {
    //
    // });
    userBloc.registerPrivilegeClubOtpResponse.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        print("status" + event.data.toString());
        otpAlertBox(
            context: context,
            title: event.data.toString(),
            route: CreatePassword(),
            stayOnPage: false);
        otpSuccessTimer();
      }

      if (event.statuscode == 400) {
        otpTexEditingController.clear();

        otpAlertBox(
            context: context, title: event.errormessage, stayOnPage: true);
      }
    });
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(context, TermsAndConditionPage());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
          resizeToAvoidBottomPadding: false,
          body: Stack(
            children: [
              Container(
                height: screenHeight(context, dividedBy: 1),
                width: screenWidth(context, dividedBy: 1),
                child: SvgPicture.asset("assets/images/login_bg_image.svg",
                    color: Colors.black.withOpacity(0.80),
                    fit: BoxFit.cover,
                    colorBlendMode: BlendMode.darken),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),
                  AppBarGuocoLand(
                    pageTitle: "ACCOUNT VERIFICATION",
                    pageNavigation: () {
                      Navigator.pop(context);
                    },
                    boxWidth: 5,
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 5),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 13)),
                    child: Text(
                      "Enter the 6-digit OTP sent to your email:",
                      style: TextStyle(
                          color: Constants.kitGradients[0],
                          fontFamily: 'Muli',
                          fontSize: screenWidth(context, dividedBy: 18)),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 12),
                  ),
                  OtpFormFeild(
                    otpTextEditingController: otpTexEditingController,
                    otpTap: () {
                      // push(context, CreatePassword());
                      if (otpTexEditingController.text != null) {
                        setState(() {
                          isLoading = true;
                        });
                        userBloc.registerPrivilegeClubOtp(
                            otpCode: otpTexEditingController.text);
                      } else {
                        otpAlertBox(
                            context: context,
                            title: "Enter the otp",
                            stayOnPage: true);
                      }
                    },
                    isLoading: isLoading,
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
