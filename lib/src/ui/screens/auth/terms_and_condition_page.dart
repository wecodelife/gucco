import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/address_form.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/otp_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/check_box.dart';
import 'package:guoco_priv_app/src/ui/widgets/list_view_terms_condition.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class TermsAndConditionPage extends StatefulWidget {
  @override
  _TermsAndConditionPageState createState() => _TermsAndConditionPageState();
}

class _TermsAndConditionPageState extends State<TermsAndConditionPage> {
  AuthBloc userBloc = new AuthBloc();
  bool checkBox1 = false;
  bool checkBox2 = false;
  bool checkBox3;
  bool checkBox4;
  bool disable = true;
  bool pdpaAccept = false;
  bool termsAndCondition = false;
  String statusCode1;
  String statusCode2;

  @override
  void initState() {
    print(ObjectFactory().appHive.getUserId());
    userBloc.privilegeClubTAndCAccept1Response.listen((event) {
      if (event.statuscode == 200 && event.errorcode == 0) {
        pdpaAccept = true;
      } else {
        setState(() {
          checkBox1 = false;
        });
      }
    });

    userBloc.privilegeClubTAndCAccept2Response.listen((event) async {
      if (event.statuscode == 200 && event.errorcode == 0) {
        termsAndCondition = true;
      } else {
        setState(() {
          checkBox2 = false;
        });
      }
    });

    userBloc.privilegeClubTAndCDecline1Response.listen((event) async {
      if (event.statuscode == 200 && event.errorcode == 0) {
        pdpaAccept = false;
      } else {
        setState(() {
          checkBox2 = false;
        });
      }
    });

    userBloc.privilegeClubTAndCDecline2Response.listen((event) async {
      if (event.statuscode == 200 && event.errorcode == 0) {
        termsAndCondition = false;
      } else {
        setState(() {
          checkBox2 = false;
        });
      }
    });

    // TODO: implement initState
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(context, AddressForm());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
          resizeToAvoidBottomPadding: false,
          body: Stack(
            children: [
              Container(
                width: screenWidth(context, dividedBy: 1),
                height: screenHeight(context, dividedBy: 1),
                child: SvgPicture.asset("assets/images/login_bg_image.svg",
                    color: Colors.black.withOpacity(0.80),
                    fit: BoxFit.cover,
                    colorBlendMode: BlendMode.darken),
              ),
              ListView(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 20),
                  ),
                  AppBarGuocoLand(
                    pageTitle: "T&Cs | PDPA",
                    pageNavigation: () {
                      Navigator.pop(context);
                    },
                    boxWidth: 4,
                    icon: true,
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListViewTermAndCondition(),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 100)),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CheckBox(
                          onTap: () {
                            setState(() {
                              checkBox1 = !checkBox1;
                            });
                            if (checkBox1 == true)
                              userBloc.privilegeClubTAndCAccept1(
                                  userId: ObjectFactory().appHive.getUserId());
                            if (checkBox1 == false)
                              userBloc.privilegeClubTAndCDecline1(
                                  userId: ObjectFactory().appHive.getUserId());
                          },
                          checked: checkBox1,
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 100),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 1.2),
                          child: Text(
                            "I agree and consent to receiving marketing materials / promotions relating to the Guoco Club Privileges programme by joining as a member and signing up for the Guoco Club Privileges app.",
                            style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontFamily: 'Muli',
                              fontSize: screenWidth(context, dividedBy: 27),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 100)),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        CheckBox(
                          onTap: () {
                            setState(() {
                              checkBox2 = !checkBox2;
                            });
                            if (checkBox2 == true)
                              userBloc.privilegeClubTAndCAccept2(
                                  userId: ObjectFactory().appHive.getUserId());

                            if (checkBox2 == false)
                              userBloc.privilegeClubTAndCDecline2(
                                  userId: ObjectFactory().appHive.getUserId());
                          },
                          checked: checkBox2,
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 100),
                        ),
                        Container(
                          width: screenWidth(context, dividedBy: 1.2),
                          child: Text(
                            "I agree and consent to receive sales, marketing and promotional information and materials in relation to the use of Guoco Club Privileges mobile application via SMS and/or email.",
                            style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontFamily: 'Muli',
                              fontSize: screenWidth(context, dividedBy: 27),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 40)),
                    child: Divider(
                      thickness: 2,
                      color: Colors.white10,
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: screenWidth(context, dividedBy: 15)),
                    child: Container(
                      width: screenWidth(context, dividedBy: 1.1),
                      child: Text(
                        "By signing up, I acknowledge and agree to the Terms & Conditions and PDPA stated above.",
                        style: TextStyle(
                          color: Constants.kitGradients[0],
                          fontFamily: 'Muli',
                          fontSize: screenWidth(context, dividedBy: 27),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 10),
                      ),
                      BuildButton(
                        title: "SIGN UP",
                        disabled: disable,
                        onPressed: () {
                          if (termsAndCondition == true &&
                              pdpaAccept == true &&
                              checkBox1 == true &&
                              checkBox2 == true) {
                            push(context, OtpPage());
                          } else {
                            // if (termsAndCondition == false) {
                            //
                            // }
                            // if (pdpaAccept == false) {
                            //   showToast("Please agree with PDPA");
                            // }
                            otpAlertBox(
                                context: context,
                                title: "Please agree to terms and conditions",
                                // route: TermsAndConditionPage(),
                                stayOnPage: true);
                            setState(() {
                              checkBox1 = false;
                              checkBox2 = false;
                            });
                          }
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 40),
                  ),
                ],
              ),
            ],
          )),
    );
  }
}
