import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/models/birthday_request_model.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/preference_page.dart';
import 'package:guoco_priv_app/src/ui/screens/new_home_screen.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/description_text.dart';
import 'package:guoco_priv_app/src/ui/widgets/drop_down_salutation.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class BirthdayOne extends StatefulWidget {
  @override
  _BirthdayOneState createState() => _BirthdayOneState();
}

class _BirthdayOneState extends State<BirthdayOne> {
  void yearGenerator() {
    for (int i = 2021; i >= 1920; i--) {
      year.add(i.toString());
    }
  }

  bool isLoading;
  int monthSelected = 0;
  String monthSelectedString = "";
  String yearSelected = "";
  List<String> year = [];
  List<String> month = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"
  ];

  AuthBloc userBloc = new AuthBloc();
  bool enabled = true;
  void dataUpdated() {
    if (monthSelectedString != "" && yearSelected != "") {
      enabled = true;
    } else {
      enabled = false;
    }
  }

  @override
  void initState() {
    userBloc.birthdayResponse.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        print("success");
        pushAndRemoveUntil(context, NewHomeScreen(), false);
      } else {
        showToast(event.errormessage);
      }
    });
    yearGenerator();
    enabled = false;
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(context, PreferencePage());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
            body: ListView(
          children: [
            Container(
              height: screenHeight(context, dividedBy: 1),
              width: screenWidth(context, dividedBy: 1),
              child: Stack(
                children: [
                  Container(
                    child: SvgPicture.asset("assets/images/login_bg_image.svg",
                        color: Colors.black.withOpacity(0.80),
                        fit: BoxFit.cover,
                        colorBlendMode: BlendMode.darken),
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 1),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      ),
                      AppBarGuocoLand(
                        pageTitle: "ADDITIONAL DETAILS",
                        icon: true,
                        boxWidth: 6,
                        pageNavigation: () {
                          Navigator.pop(context);
                        },
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      DescriptionText(
                        descriptionText: "MY SPECIAL DAY IS SOMETIME IN…",
                        textSize: 19,
                        horizontalPadding: 10,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      DropDownSalutation(
                        title: "Month",
                        dropDownList: month,
                        dropDownValue: monthSelectedString,
                        onClicked: (value) {
                          setState(() {
                            monthSelectedString = value;
                            monthSelected = month.indexOf(value);
                            print(month.indexOf(value));
                            dataUpdated();
                          });
                        },
                      ),
                      DropDownSalutation(
                        title: "Year",
                        dropDownList: year,
                        dropDownValue: yearSelected,
                        onClicked: (value) {
                          setState(() {
                            yearSelected = value;
                            dataUpdated();
                          });
                        },
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: screenWidth(context, dividedBy: 11),
                          ),
                          Container(
                            width: screenWidth(context, dividedBy: 1.2),
                            child: Text(
                              "We want to celebrate your birthday month with you and keep you entitled to special deals!",
                              style: TextStyle(
                                  color: Constants.kitGradients[0],
                                  fontFamily: 'Muli',
                                  fontSize: screenWidth(context, dividedBy: 19),
                                  fontStyle: FontStyle.italic),
                            ),
                          ),
                        ],
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      BuildButton(
                        title: "CONTINUE",
                        disabled: enabled,
                        isLoading: isLoading,
                        onPressed: () {
                          setState(() {
                            isLoading = true;
                          });

                          userBloc.birthday(
                              birthdayRequest: BirthdayRequest(
                                month: monthSelected,
                                year: int.parse(yearSelected),
                              ),
                              userId: ObjectFactory().appHive.getUserId());
                        },
                      ),
                      Spacer(
                        flex: 1,
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        )),
      ),
    );
  }
}
