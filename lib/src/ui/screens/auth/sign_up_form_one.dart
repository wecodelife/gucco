import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step1_request.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/address_form.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/ref_code.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/drop_down_salutation.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class SignUpFormOne extends StatefulWidget {
  @override
  _SignUpFormOneState createState() => _SignUpFormOneState();
}

class _SignUpFormOneState extends State<SignUpFormOne> {
  List<String> salutation = ["Mr.", "Ms.", "Mrs."];
  String salutationSelected;
  String salutationUser;
  AuthBloc userBloc = new AuthBloc();
  bool isLoading = false;
  TextEditingController firstNameTextEditingController =
      new TextEditingController();
  TextEditingController lastNameTextEditingController =
      new TextEditingController();
  TextEditingController contactNumberTextEditingController =
      new TextEditingController();
  TextEditingController emailAddressTextEditingController =
      new TextEditingController();

  @override
  void initState() {
    userBloc.registerUserDetailsStep1Response.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        ObjectFactory().appHive.putUserId(userId: event.data.id);
        ObjectFactory().appHive.putXUser(event.data.username);

        push(context, AddressForm());
      } else {
        showToast(event.errormessage);
      }
    }, onError: (error) {
      print("errr" + error.toString());
      setState(() {
        isLoading = false;
      });
    });

    // TODO: implement initState
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(context, RefCode());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    // final bottom = MediaQuery.of(context).viewInsets.bottom;

    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
          // resizeToAvoidBottomInset: false,
          resizeToAvoidBottomPadding: true,
          body: SingleChildScrollView(
            reverse: true,
            child: Stack(
              // padding: EdgeInsets.only(),
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 1),
                  width: screenWidth(context, dividedBy: 1),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(
                        "assets/images/login.png",
                      ),
                      fit: BoxFit.cover,
                      colorFilter: new ColorFilter.mode(
                          Colors.black.withOpacity(0.8), BlendMode.darken),
                    ),
                  ),
                  child: SvgPicture.asset("assets/images/login_bg_image.svg",
                      color: Colors.black.withOpacity(0.80),
                      fit: BoxFit.cover,
                      colorBlendMode: BlendMode.darken),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        AppBarGuocoLand(
                          pageTitle: "BASIC DETAILS ",
                          icon: true,
                          boxWidth: 4.2,
                          pageNavigation: () {
                            Navigator.pop(context);
                          },
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 6),
                        ),
                        DropDownSalutation(
                          title: "Salutation",
                          dropDownList: salutation,
                          dropDownValue: salutationUser,
                          onClicked: (value) {
                            salutationUser = value;
                            print(salutationUser);
                          },
                        ),
                        FormFeildUserDetails(
                          hintText: "First Name *",
                          textEditingController: firstNameTextEditingController,
                          onPressed: () {},
                          readOnly: isLoading,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        FormFeildUserDetails(
                            hintText: "Last Name *",
                            textEditingController:
                                lastNameTextEditingController,
                            onPressed: () {},
                            readOnly: isLoading),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        FormFeildUserDetails(
                            hintText: "Contact No.*",
                            textEditingController:
                                contactNumberTextEditingController,
                            onPressed: () {},
                            readOnly: isLoading),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                        FormFeildUserDetails(
                            hintText: "Email Address*",
                            textEditingController:
                                emailAddressTextEditingController,
                            onPressed: () {},
                            readOnly: isLoading),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 20),
                        ),
                        BuildButton(
                          title: "NEXT",
                          disabled: true,
                          onPressed: () {
                            if (firstNameTextEditingController.text != "" &&
                                emailAddressTextEditingController.text != "" &&
                                contactNumberTextEditingController.text != "" &&
                                lastNameTextEditingController.text != "") {
                              final bool isValid = EmailValidator.validate(
                                  emailAddressTextEditingController.text);
                              if (isValid) {
                                setState(() {
                                  isLoading = true;
                                });
                                ObjectFactory().appHive.putEmail(
                                    emailAddressTextEditingController.text);
                                userBloc.registerUserDetailsStep1(
                                    registerUserDetailsStep1Request:
                                        RegisterUserDetailsStep1Request(
                                            email:
                                                emailAddressTextEditingController
                                                    .text,
                                            firstname:
                                                firstNameTextEditingController
                                                    .text,
                                            lastname:
                                                lastNameTextEditingController
                                                    .text,
                                            salutation: salutationUser,
                                            contactno:
                                                contactNumberTextEditingController
                                                    .text));
                              } else {
                                showToast("Please enter a valid email!");
                              }
                            } else {
                              showToast("Please fill the mandatory feilds");
                            }
                          },
                          isLoading: isLoading,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 30),
                        ),
                      ],
                    )
                  ],
                ),
              ],
            ),
          )),
    );
  }
}
