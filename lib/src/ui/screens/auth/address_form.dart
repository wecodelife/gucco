import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step2_request.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/sign_up_form_one.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/terms_and_condition_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/drop_down_salutation.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_feild_address_page.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class AddressForm extends StatefulWidget {
  @override
  _AddressFormState createState() => _AddressFormState();
}

class _AddressFormState extends State<AddressForm> {
  TextEditingController postalNoTextEditingController =
      new TextEditingController();
  TextEditingController unitNoTextEditingController =
      new TextEditingController();
  TextEditingController blockNoTextEditingController =
      new TextEditingController();
  List<String> zones = ["East", "West", "Central", "North", "South"];
  String zoneSelected;
  AuthBloc userBloc = new AuthBloc();
  bool isLoading = false;

  @override
  void initState() {
    userBloc.registerUserDetailsStep2Response.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        push(context, TermsAndConditionPage());
      } else {
        showToast("Please check your internet connection");
      }
    });

    // TODO: implement initState
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(context, SignUpFormOne());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: _willPopCallback,
        child: Scaffold(
          body: ListView(
            children: [
              Stack(
                children: [
                  SvgPicture.asset("assets/images/login_bg_image.svg",
                      color: Colors.black.withOpacity(0.80),
                      fit: BoxFit.cover,
                      colorBlendMode: BlendMode.darken),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 40),
                      ),
                      AppBarGuocoLand(
                        pageTitle: "ADDRESS OF RESIDENCE",
                        boxWidth: 6.8,
                        icon: true,
                        pageNavigation: () {
                          pop(context);
                        },
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 3.8),
                      ),
                      DropDownSalutation(
                        title: "Zone *",
                        dropDownList: zones,
                        dropDownValue: zoneSelected,
                        onClicked: (value) {
                          zoneSelected = value;
                        },
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: screenWidth(context, dividedBy: 25)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Container(
                                width: screenWidth(context, dividedBy: 2.6),
                                child: FormFeildAddressBox(
                                  hintText: "Postal Code",
                                  textEditingController:
                                      postalNoTextEditingController,
                                  readOnly: isLoading,
                                )),
                            Container(
                                width: screenWidth(context, dividedBy: 2.6),
                                child: FormFeildAddressBox(
                                  hintText: "Unit",
                                  textEditingController:
                                      unitNoTextEditingController,
                                  readOnly: isLoading,
                                )),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      FormFeildUserDetails(
                        hintText: "Block / Tower",
                        textEditingController: blockNoTextEditingController,
                        onPressed: () {},
                        readOnly: isLoading,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 3.7),
                      ),
                      BuildButton(
                        title: "NEXT",
                        onPressed: () {
                          print("zone" + zoneSelected.toString());
                          if (zoneSelected != null) {
                            setState(() {
                              isLoading = true;
                            });
                            userBloc.registerUserDetailsStep2(
                                registerUserDetailsStep2Request:
                                    RegisterUserDetailsStep2Request(
                                        zone: zoneSelected,
                                        unit: unitNoTextEditingController.text,
                                        postalcode:
                                            postalNoTextEditingController.text,
                                        block:
                                            blockNoTextEditingController.text),
                                userId: ObjectFactory().appHive.getUserId());
                          } else {
                            showToast("PLease Select the Zone");
                          }
                        },
                        disabled: true,
                        isLoading: isLoading,
                      )
                    ],
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
