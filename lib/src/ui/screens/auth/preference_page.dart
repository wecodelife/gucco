import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/models/get_preference_response_model.dart';
import 'package:guoco_priv_app/src/models/login_request_model.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/birthday_one.dart';
import 'package:guoco_priv_app/src/ui/screens/customise_exp_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/preference_tile.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class PreferencePage extends StatefulWidget {
  @override
  _PreferencePageState createState() => _PreferencePageState();
}

class _PreferencePageState extends State<PreferencePage> {
  // List<int> text=[1,2,3];
  bool enable;
  int counter;
  bool isLoading = false;
  AuthBloc userBloc = new AuthBloc();
  bool click = false;

  void incrementCounter(bool onClick) {
    print(counter.toString() + "iiii");

    if (onClick == true) {
      counter++;
    } else {
      counter--;
    }
  }

  void enableSelectButton() {
    print(counter.toString() + "eee");
    if (counter >= 2)
      setState(() {
        enable = true;
      });
    else
      setState(() {
        enable = false;
      });
  }

  @override
  void initState() {
    userBloc.loginResponse.listen((event) async {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        await ObjectFactory()
            .appHive
            .putToken(token: event.data.authtoken.toString());
        await ObjectFactory().appHive.putUserId(userId: event.data.userid);
        print("check" + ObjectFactory().appHive.getToken());
        print("check" + ObjectFactory().appHive.getUserId());
        push(context, BirthdayOne());
      } else {
        print("Not Successful");
      }
    });
    userBloc.getPreference();
    // userBloc.attachPreference(userId:"",preferenceId: "" );
    // userBloc.detachPreference(userId: "",preferenceId: "");

    userBloc.attachPreferenceResponse.listen((event) async {
      setState(() {});
      if (event.statuscode == 200) {
        print("succesful");
      } else {
        print("Not Successful");
      }
    });

    userBloc.detachPreferenceResponse.listen((event) async {
      setState(() {});
      if (int.parse(event.statusCode) == 200) {
        print("succesful");
      } else {
        print("Not Successful");
      }
    });

    setState(() {
      enable = false;
      counter = 0;
    });
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(context, CustomiseExpPage());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: screenWidth(context, dividedBy: 1),
              height: screenHeight(context, dividedBy: 1),
              child: SvgPicture.asset("assets/images/login_bg_image.svg",
                  color: Colors.black.withOpacity(0.80),
                  fit: BoxFit.cover,
                  colorBlendMode: BlendMode.darken),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Image.asset(
                        "assets/icons/guoco_logo.png",
                        height: screenHeight(context, dividedBy: 10),
                        width: screenWidth(context, dividedBy: 2.5),
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text("MY LIFESTYLE PREFERENCES",
                              style: TextStyle(
                                  fontSize: screenWidth(context, dividedBy: 24),
                                  color: Colors.white,
                                  fontFamily: "MuliSemiBold")),
                          Text("(Minimum 2)",
                              style: TextStyle(
                                  fontSize: screenWidth(context, dividedBy: 32),
                                  color: Colors.white,
                                  fontFamily: "muliitalic",
                                  fontStyle: FontStyle.italic))
                        ],
                      ),
                      StreamBuilder<GetPreferenceResponse>(
                          stream: userBloc.getPreferenceResponse,
                          builder: (context, snapshot) {
                            return snapshot.hasData
                                ? GridView.builder(
                                    itemCount: snapshot.data.data.length,
                                    shrinkWrap: true,
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 3),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return PreferenceTile(
                                        index: index,
                                        deselectFunction: () {
                                          userBloc.detachPreference(
                                              userId: ObjectFactory()
                                                  .appHive
                                                  .getUserId(),
                                              preferenceId:
                                                  snapshot.data.data[index].id);
                                        },
                                        selectFunction: () {
                                          userBloc.attachPreference(
                                              userId: ObjectFactory()
                                                  .appHive
                                                  .getUserId(),
                                              preferenceId:
                                                  snapshot.data.data[index].id);
                                        },
                                        onSelected: (value) {
                                          incrementCounter(value);
                                          enableSelectButton();
                                        },
                                        shimmer: false,
                                        icon: "assets/icons/dumbbell.png",
                                        description: snapshot
                                            .data.data[index].displayname,
                                      );
                                    },
                                  )
                                : GridView.builder(
                                    itemCount: 8,
                                    shrinkWrap: true,
                                    gridDelegate:
                                        SliverGridDelegateWithFixedCrossAxisCount(
                                            crossAxisCount: 3),
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return PreferenceTile(
                                        shimmer: true,
                                      );
                                    },
                                  );
                          }),
                    ],
                  ),
                  Column(
                    children: [
                      BuildButton(
                        title: "CONTINUE",
                        onPressed: () {
                          setState(() {
                            isLoading = true;
                          });

                          print("user" +
                              ObjectFactory().appHive.getUserPassword() +
                              ObjectFactory().appHive.getEmail());
                          userBloc.login(
                              loginRequest: LoginRequest(
                                  password:
                                      ObjectFactory().appHive.getUserPassword(),
                                  username:
                                      ObjectFactory().appHive.getEmail()));
                        },
                        disabled: enable,
                        isLoading: isLoading,
                      ),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 20),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
