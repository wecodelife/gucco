import 'package:double_back_to_close/double_back_to_close.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/models/user_profile_response.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_homescreen.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_navigation_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/home_drawer.dart';
import 'package:guoco_priv_app/src/ui/widgets/home_tile.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();
  AuthBloc userBloc = new AuthBloc();
  List<int> likes = [1, 2, 3, 4, 5];
  int liked = 0;
  bool isLoading = false;
  // String name;

  void counter(bool onClick, int index) {
    print(onClick.toString());
    if (onClick == true)
      setState(() {
        likes[index]++;
      });
    else
      setState(() {
        likes[index]--;
      });
    print(liked.toString());
  }

  @override
  void initState() {
    userBloc.getUserProfile();
    // userBloc.getProfile.listen((event) {
    //   setState(() {
    //     isLoading=false;
    //   });
    //   if(event.statuscode==200){
    //     setState(() {
    //       ObjectFactory().appHive.putName(name: event.data.firstname);
    //       // name = event.data.firstname;
    //     });
    //     print("name"+event.data.firstname);
    //   }
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DoubleBack(
      message: "Press back again to close",
      child: Scaffold(
        key: _globalKey,
        drawer: HomeDrawer(
          count: 1,
        ),
        backgroundColor: Color(0xFF030201).withOpacity(0.10),
        appBar: AppBar(
          toolbarHeight: screenHeight(context, dividedBy: 7),
          backgroundColor: Colors.black,
          leading: Container(),
          actions: [
            StreamBuilder<ProfileResponseModel>(
                stream: userBloc.getProfile,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    ObjectFactory()
                        .appHive
                        .putName(name: snapshot.data.data.firstname);
                    ObjectFactory().appHive.putEmail(snapshot.data.data.email);
                    ObjectFactory()
                        .appHive
                        .putContactNo(snapshot.data.data.mobileno);
                    ObjectFactory()
                        .appHive
                        .putUserImage(snapshot.data.data.profileimage);
                    ObjectFactory().appHive.putRefId(snapshot.data.data.id);
                  }
                  return ObjectFactory().appHive.getName() != null
                      ? AppBarHomeScreen(
                          name: ObjectFactory().appHive.getName(),
                          isLoading: false,
                          onPressingDrawer: () {
                            _globalKey.currentState.openDrawer();
                          },
                        )
                      : snapshot.hasData
                          ? AppBarHomeScreen(
                              name: snapshot.data.data.firstname,
                              isLoading: snapshot.hasData ? false : true,
                              onPressingDrawer: () {
                                _globalKey.currentState.openDrawer();
                              },
                            )
                          : Shimmer.fromColors(
                              highlightColor: Colors.grey,
                              baseColor: Colors.black38,
                              child: Container(
                                height: screenHeight(context, dividedBy: 20),
                                width: screenWidth(context, dividedBy: 1),
                                decoration:
                                    BoxDecoration(color: Colors.black54),
                              ),
                            );
                })
          ],
        ),
        body: SafeArea(
            bottom: true,
            child: Stack(
              children: [
                SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        height: screenHeight(context, dividedBy: 3.3),
                        width: screenWidth(context, dividedBy: 1),
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: AssetImage(
                              "assets/images/homescreen_background.png",
                            ),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Container(
                          // height: screenHeight(context, dividedBy: 1.4),
                          width: screenWidth(context, dividedBy: 1),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                              topLeft: Radius.circular(10),
                            ),
                            // color: Colors.black.withOpacity(.10),
                            image: DecorationImage(
                              colorFilter: new ColorFilter.mode(
                                  Colors.black.withOpacity(0.60),
                                  BlendMode.darken),
                              image: AssetImage(
                                "assets/images/homescreen_bg1.png",
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "FEATURED MERCHANTS",
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Constants.kitGradients[0],
                                          fontFamily: "Muli",
                                          fontWeight: FontWeight.w600),
                                    ),
                                    GestureDetector(
                                      child: Text(
                                        "View More",
                                        style: TextStyle(
                                            fontSize: 14,
                                            decoration:
                                                TextDecoration.underline,
                                            color: Constants.kitGradients[0],
                                            fontFamily: "Muli",
                                            fontWeight: FontWeight.w400),
                                      ),
                                      onTap: () {},
                                    )
                                  ],
                                ),
                              ),
                              Container(
                                height: screenHeight(context, dividedBy: 3.3),
                                child: ListView.builder(
                                    physics: ClampingScrollPhysics(),
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: 5,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return HomeTile(
                                        isSubHeading: false,
                                        fontSize: 18,
                                        likeButtonBrown: true,
                                        boxHeight: 2.7,
                                        subHeading: "Q1 2021",
                                        tileIcon:
                                            "assets/icons/expressobar.svg",
                                        itemImage:
                                            "assets/images/card_image.png",
                                        itemDescription: "MINISO(GWC)",
                                        borderRadius: 5,
                                        likes: likes[index],
                                        height: screenHeight(context,
                                            dividedBy: 3.5),
                                        width: screenWidth(context,
                                            dividedBy: 2.3),
                                        likeBar: true,
                                        onClicked: (value) {
                                          counter(value, index);
                                          print(
                                              "val" + likes[index].toString());
                                        },
                                      );
                                    }),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "UPCOMING LAUNCHES ",
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Constants.kitGradients[0],
                                        fontFamily: "Muli",
                                        fontWeight: FontWeight.w600),
                                  ),
                                  GestureDetector(
                                    child: Text(
                                      "View More",
                                      style: TextStyle(
                                          fontSize: 14,
                                          decoration: TextDecoration.underline,
                                          color: Constants.kitGradients[0],
                                          fontFamily: "Muli",
                                          fontWeight: FontWeight.w600),
                                    ),
                                    onTap: () {},
                                  )
                                ],
                              ),
                              Container(
                                height: screenWidth(context, dividedBy: 2.5),
                                child: ListView.builder(
                                    physics: ClampingScrollPhysics(),
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: 15,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return HomeTile(
                                        subHeadingLeftPadding: 50,
                                        textelipsis: true,
                                        boxHeight: 5.2,
                                        fontSize: 14,
                                        isSubHeading: true,
                                        subHeading: "Q1 2020",
                                        height: screenWidth(context,
                                            dividedBy: 2.5),
                                        width: screenWidth(context,
                                            dividedBy: 2.5),
                                        likeBar: false,
                                        itemImage:
                                            "assets/images/card_image.png",
                                        itemDescription: "MINICO(GWC)",
                                        borderRadius: 10,
                                      );
                                    }),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "UPCOMING EVENTS",
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Constants.kitGradients[0],
                                        fontFamily: "Muli",
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                              Container(
                                height: screenHeight(context, dividedBy: 6),
                                child: ListView.builder(
                                    physics: ClampingScrollPhysics(),
                                    shrinkWrap: true,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: 15,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return HomeTile(
                                          subHeadingLeftPadding: 50,
                                          textelipsis: true,
                                          fontSize: 14,
                                          isSubHeading: true,
                                          subHeading: "08 Nov 2020",
                                          boxHeight: 8,
                                          height: screenHeight(context,
                                              dividedBy: 6),
                                          width: screenWidth(context,
                                              dividedBy: 2.5),
                                          likeBar: false,
                                          itemDescription: "MINICO(GWC)",
                                          likes: 1,
                                          borderRadius: 10,
                                          itemImage:
                                              "assets/images/card_image.png");
                                    }),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 10),
                              )
                            ],
                          )),
                    ],
                  ),
                ),
                Positioned(
                    bottom: 0,
                    child: CustomBottomNavigationBar(
                      value: "home",
                    ))
              ],
            )),
      ),
    );
  }
}
