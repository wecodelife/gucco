// import 'package:flutter/material.dart';
// import 'package:guoco_priv_app/src/ui/screens/estate_page.dart';
// import 'package:guoco_priv_app/src/ui/screens/home_screen.dart';
// import 'package:guoco_priv_app/src/ui/screens/login_page.dart';
// import 'package:guoco_priv_app/src/ui/screens/preference_page.dart';
// import 'package:guoco_priv_app/src/ui/widgets/crown_icons.dart';
// import 'package:guoco_priv_app/src/utils/constants.dart';
// import 'package:guoco_priv_app/src/ui/widgets/clipper.dart';
// import 'package:guoco_priv_app/src/utils/utils.dart';
//
// class MyBottomBarDemo extends StatefulWidget {
//   @override
//   _MyBottomBarDemoState createState() => new _MyBottomBarDemoState();
// }
//
// class _MyBottomBarDemoState extends State<MyBottomBarDemo> {
//   int _pageIndex = 0;
//   PageController _pageController;
//
//   List<Widget> tabPages = [
//     EstatePage(),
//     HomeScreen(),
//   ];
//
//   @override
//   void initState(){
//     super.initState();
//     _pageController = PageController(initialPage: _pageIndex);
//   }
//
//   @override
//   void dispose() {
//     _pageController.dispose();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       bottomNavigationBar: Container(
//         height: screenHeight(context,dividedBy: 10),
//         child: BottomNavigationBar(fixedColor:Constants.kitGradients[0],
//           unselectedItemColor: Constants.kitGradients[0],
//           currentIndex: _pageIndex,
//           onTap: onTabTapped,
//           backgroundColor: Constants.kitGradients[9],
//           items: <BottomNavigationBarItem>[
//             BottomNavigationBarItem(
//                 icon: ImageIcon(
//                   AssetImage("assets/icons/crown.png"),size: 40,color: Constants.kitGradients[8],
//                 ),
//                 label: "Privilege Club"),
//             BottomNavigationBarItem(
//                 icon:ImageIcon(
//                   AssetImage("assets/icons/estate_icon.png"),size: 40,color: Constants.kitGradients[0],
//                 ),
//                 label: "My Estate"),
//           ],
//
//         ),
//       ),
//       body: Container(
//         color: Colors.blue,
//         child: PageView(
//           children: tabPages,
//           onPageChanged: onPageChanged,
//           controller: _pageController,
//         ),
//       ),
//     );
//   }
//   void onPageChanged(int page) {
//     setState(() {
//       this._pageIndex = page;
//     });
//   }
//
//   void onTabTapped(int index) {
//     this._pageController.animateToPage(index,duration: const Duration(milliseconds: 500),curve: Curves.easeInOut);
//   }
// }