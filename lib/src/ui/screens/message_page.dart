import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/message_tile.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class MessagePage extends StatefulWidget {
  @override
  _MessagePageState createState() => _MessagePageState();
}

class _MessagePageState extends State<MessagePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[7],
        leading: Container(),
        actions: [
          AppBarBookingPage(
            rightIcon: "assets/icons/folder.png",
            // noRightIcon: true,
            leftIcon: "assets/icons/facilitiesmenu.png",
            pageTitle: "Messages",
            // onTapRightIcon: () {
            //   Navigator.pushAndRemoveUntil(
            //       context,
            //       MaterialPageRoute(
            //           builder: (context) => EstatePage(
            //             page: "tower",
            //           )),
            //           (route) => false);
            // },
            onTapLeftIcon: () {},
          )
        ],
      ),
      body: Stack(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 100),
              ),
              Row(
                children: [
                  SizedBox(width: screenWidth(context, dividedBy: 15)),
                  Container(
                    width: screenWidth(context, dividedBy: 1.3),
                    child: TextField(
                      decoration: InputDecoration(
                          hintText: "Search Messages",
                          hintStyle: TextStyle(
                              fontFamily: "SFProText-Regular",
                              fontSize: 16,
                              fontWeight: FontWeight.w400)),
                    ),
                  ),
                  Container(
                    height: screenWidth(context, dividedBy: 15),
                    width: screenWidth(context, dividedBy: 15),
                    child: SvgPicture.asset(
                      "assets/icons/search_icon.svg",
                      fit: BoxFit.fill,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
              SizedBox(height: screenHeight(context, dividedBy: 60)),
              Row(
                children: [
                  SizedBox(width: screenWidth(context, dividedBy: 15)),
                  Text(
                    "INBOX",
                    style: TextStyle(
                        fontFamily: "SFProText-Regular",
                        fontSize: 16,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
              SizedBox(height: screenHeight(context, dividedBy: 500)),
              Divider(
                thickness: 1,
                color: Constants.kitGradients[28],
              ),
              Expanded(
                // flex: 0,
                  child: ListView.builder(
                      physics: ClampingScrollPhysics(),
                      // padding: const EdgeInsets.all(8.0),
                      shrinkWrap: true,
                      scrollDirection: Axis.vertical,
                      itemCount: 15,
                      itemBuilder: (BuildContext context, int index) {
                        // print("data length");
                        return MessageTile();
                      })),

            ],
          ),
          Positioned(child:Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                width: screenWidth(context,dividedBy: 12),
              ),
              BuildButton(title: "Compose New Message",disabled: true,),
              SizedBox(
                width: screenWidth(context,dividedBy: 12),
              ),
            ],
          ),
            bottom: 10,
          )
        ],
      ),
    );
  }
}
