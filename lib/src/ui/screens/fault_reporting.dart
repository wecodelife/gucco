import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/fault_reporting_new_report.dart';
import 'package:guoco_priv_app/src/ui/screens/home_post_move_in.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/fault_report_tab_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/fault_report_listview_widget.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'martin_modern/estate_page.dart';
import 'home_screen.dart';

class FaultReporting extends StatefulWidget {
  @override
  _FaultReportingState createState() => _FaultReportingState();
}

class _FaultReportingState extends State<FaultReporting> {
  bool clickTabStatus = true;
  ScrollController _scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[7],
        leading: Container(),
        actions: [
          AppBarBookingPage(
            pageTitle: 'Fault Reporting',
            rightIcon: "assets/icons/estate_icon.png",
            leftIcon: "assets/icons/fault_reporting_menu.png",
            onTapLeftIcon: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => HomePostMoveIn()),
                  (route) => false);
            },
            onTapRightIcon: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
      backgroundColor: Constants.kitGradients[0],
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TowerName(
            towerName: "Tower 1",
            towerSubName: "01 - -01",
          ),
          FaultBookingTabBar(
            imagePath: "assets/images/key_collection_background.png",
            title: "IN PROGRESS",
            clickTabStatus: clickTabStatus,
            clickStatusValue: (value) {
              setState(() {
                clickTabStatus = value;
              });
            },
          ),
          clickTabStatus == false
              ? Container(
                  height: screenHeight(context,dividedBy: 1.6),
                  child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: 9,
                  controller: _scrollController,
                  itemBuilder: (BuildContext context, int index) {
                    return FaultReportListViewWidget(
                      clickTabStatus: clickTabStatus,
                    );
                  },
                ))
              : Container(
                  height: screenHeight(context,dividedBy: 1.6),
                  child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: 9,
                  controller: _scrollController,
                  itemBuilder: (BuildContext context, int index) {
                    return FaultReportListViewWidget(
                      clickTabStatus: clickTabStatus,
                    );
                  },
                )),

           SizedBox(
             height: screenHeight(context,dividedBy:100 ),
           ),

          Row(
            children: [

              SizedBox(
                width: screenWidth(context,dividedBy:12 ),
              ),
              BuildButtonIcon(

                icon: "assets/icons/fault_report_caution.png",
                title: "Report Fault",
                onPressed: () {
                  push(context, FaultReportingNewReport()
                  );
                  //do something here
                },
                arrowIcon: true,
                faultReport: true,
                leftIcon: true,
              ),
            ],
          ),

          SizedBox(
            height: screenHeight(context,dividedBy: 50),
          ),

          BottomBar()


        ],
      ),
    );
  }
}
