import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/e_application_progress.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/tab_bar_bookings.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';

class EApplication extends StatefulWidget {
  @override
  _EApplicationState createState() => _EApplicationState();
}

class _EApplicationState extends State<EApplication> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[0],
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[7],
        leading: Container(),
        actions: [
          AppBarBookingPage(
            rightIcon: "assets/icons/estate_icon.png",
            leftIcon: "assets/icons/facilitiesmenu.png",
            pageTitle: "E-Applications",
            onTapRightIcon: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => EstatePage()),
                  (route) => false);
            },
            onTapLeftIcon: () {
              Navigator.pop(context);
            },
          )
        ],
      ),
      body: Stack(
        children: [
          Column(
            children: [
              TowerName(
                towerName: "Tower 01",
                towerSubName: "01 - 01",
              ),
              TabBarBookings(
                heading1: "New Application",
                heading2: "Submitted",
                indicatorColor: Constants.kitGradients[11],
                tabColor: Constants.kitGradients[0],
                pageType: "eapplication",
              )
            ],
          ),
          Positioned(
            bottom: 0,
            child: Container(
              width: screenWidth(context, dividedBy: 1),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                    Colors.white.withOpacity(0),
                    Colors.white.withOpacity(.05),
                    Colors.white.withOpacity(.30),
                    Colors.white.withOpacity(.40),
                    Colors.white.withOpacity(.50),
                    Colors.white.withOpacity(.60),
                    Colors.white.withOpacity(.70),
                    Colors.white.withOpacity(.80),
                    Colors.white.withOpacity(.90)
                  ])),
              child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context, dividedBy: 10),
                      ),
                      BuildButtonIcon(
                        // leftIcon: true,
                        arrowIcon: true,
                        // icon: "assets/icons/user.png",
                        title: "New Application",
                        onPressed: () {
                          push(context, EApplicationsProgress());
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 29),
                  ),
                  BottomBar(),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
