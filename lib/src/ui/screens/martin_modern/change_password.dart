import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/change_password_for_user_request_model.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/login_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/change_password_label_widget.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_feild_change_password.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ChangePassword extends StatefulWidget {
  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  TextEditingController oldPasswordTextEditingController =
      new TextEditingController();
  TextEditingController newPasswordTextEditingController =
      new TextEditingController();
  TextEditingController rePasswordTextEditingController =
      new TextEditingController();
  bool buttonLoading;
  EstateBloc estateBloc = new EstateBloc();
  @override
  void initState() {
    estateBloc.changePasswordForUserResponse.listen((event) {
      setState(() {
        buttonLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        if (event.statusmessage == null) {
          showToast("Password changed successfully");
        } else {
          showToast(event.statusmessage);
        }
        pushAndRemoveUntil(context, Login(), true);
        ObjectFactory().appHive.putToken(token: "");
      } else {
        showToast(event.errormessage);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: GestureDetector(
          onTap: () {
            pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
            size: 25,
          ),
        ),
        backgroundColor: Colors.black,
        centerTitle: true,
        title: Text(
          "CHANGE PASSWORD",
          style: TextStyle(
              color: Colors.white,
              fontSize: 18,
              fontFamily: 'SFProDisplay-Regular',
              fontWeight: FontWeight.w600),
        ),
      ),
      body: ListView(
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              SizedBox(
                height: screenHeight(context, dividedBy: 7),
              ),
              Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 10),
                  ),
                  ChangePasswordLabelWidget(
                    heading: "OldPassword",
                  ),
                ],
              ),
              FormFieldChangePasswordBox(
                label: "",
                textEditingController: oldPasswordTextEditingController,
              ),
              Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 10),
                  ),
                  ChangePasswordLabelWidget(
                    heading: "New Password",
                  ),
                ],
              ),
              FormFieldChangePasswordBox(
                label: "",
                textEditingController: newPasswordTextEditingController,
              ),
              Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 10),
                  ),
                  ChangePasswordLabelWidget(
                    heading: "Re-enter New Password",
                  ),
                ],
              ),
              FormFieldChangePasswordBox(
                label: "",
                textEditingController: rePasswordTextEditingController,
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 4),
              ),
              BuildButton(
                title: "Submit Changes",
                disabled: true,
                isLoading: buttonLoading,
                onPressed: () {
                  print(1);
                  if (newPasswordTextEditingController.text ==
                      rePasswordTextEditingController.text) {
                    setState(() {
                      buttonLoading = true;
                    });
                    var oldPasswordBytes =
                        utf8.encode(oldPasswordTextEditingController.text);
                    var base64OldPassword = base64.encode(oldPasswordBytes);
                    var newPasswordBytes =
                        utf8.encode(newPasswordTextEditingController.text);
                    var base64NewPassword = base64.encode(newPasswordBytes);

                    estateBloc.changePasswordForUser(
                        changePasswordForUserRequestModel:
                            ChangePasswordForUserRequestModel(
                                oldpassword: base64OldPassword,
                                newpassword: base64NewPassword));
                  } else {
                    showToastLong("passwords entered does not match");
                  }
                },
              )
            ],
          ),
        ],
      ),
    );
  }
}
