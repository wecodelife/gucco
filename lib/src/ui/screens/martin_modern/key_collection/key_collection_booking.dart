import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/earliest_slot_key_collection_request.dart';
import 'package:guoco_priv_app/src/models/key_collection_time_slot_response.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_appointments_reschedule.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_self.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/booking_calender_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/time_slot_tile.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

class KeyCollectionBooking extends StatefulWidget {
  final String unitId;
  final String blockName;
  final String floorAndUnitId;
  KeyCollectionBooking({this.unitId, this.floorAndUnitId, this.blockName});
  @override
  _KeyCollectionBookingState createState() => _KeyCollectionBookingState();
}

class _KeyCollectionBookingState extends State<KeyCollectionBooking> {
  bool selected;
  bool deselected;
  bool timeSlotAvailability = false;
  int itemNumber;
  String startTimeSelected;
  String startTimeEarliest;
  String endTimeSelected;
  String endTimeEarliest;
  DateTime currentDate;
  String earliestDate = "";
  String selectedDate;
  bool isLoading;

  EstateBloc estateBloc = new EstateBloc();
  Future<bool> _willPopCallback() async {
    push(
        context,
        KeyCollectionAppointmentsReschedule(
          unitId: widget.unitId,
          blockName: widget.blockName,
          floorAndUnitId: widget.floorAndUnitId,
        ));
    return true;
  }

  void initState() {
    // TODO: implement initState
    itemNumber = 100;
    estateBloc.getKeyCollectionTimeSlots(
        new DateFormat('dd-MM-yyyy').format(DateTime.now()), widget.unitId);
    estateBloc.getKeyCollectionTimeSlotResponse.listen((event) {
      if (event.statuscode == 200 && event.errorcode == 0) {
        if (event.data.length > 0) {
          setState(() {
            timeSlotAvailability = true;
          });
        } else {
          setState(() {
            timeSlotAvailability = false;
          });
        }
      }
    });
    estateBloc.getEarliestSlotKeyCollectionResponse.listen((event) async {
      setState(() {
        isLoading = false;
      });
      selectedDate = DateFormat('dd-MM-yyyy').format(currentDate).toString();
      if (event.statuscode == 200) {
        if (event.data.ondate != null) {
          print("Date Null" + event.data.ondate);
          if ((event.data.ondate != null && event.data.starttime != null) &&
              selectedDate != event.data.ondate.trim().split(" ").first) {
            rescheduleAlertBox(
                context: context,
                questionAlert: "Do you wish to procced?",
                msg: "we have an earlier Time Slot Available",
                selectedEndTime: "",
                earliestStartTime: "",
                date: currentDate,
                earliestDate: event.data.ondate.trim().split(" ").first,
                selectedStartTime: startTimeSelected,
                earliestEndTime: "",
                onPressed: () {
                  pushAndReplacement(
                      context,
                      KeyCollectionSelf(
                        unitId: widget.unitId,
                        startTime: startTimeSelected,
                        endTime: endTimeSelected,
                        date: currentDate,
                        startTimeEarliest: startTimeEarliest,
                        floorAndUnitId: widget.floorAndUnitId,
                        blockName: widget.blockName,
                      ));
                });
          } else {
            pushAndReplacement(
                context,
                KeyCollectionSelf(
                  unitId: widget.unitId,
                  startTime: startTimeSelected,
                  endTime: endTimeSelected,
                  date: currentDate,
                  startTimeEarliest: startTimeEarliest,
                  floorAndUnitId: widget.floorAndUnitId,
                  blockName: widget.blockName,
                ));
          }
        } else {
          pushAndReplacement(
              context,
              KeyCollectionSelf(
                unitId: widget.unitId,
                startTime: startTimeSelected,
                endTime: endTimeSelected,
                date: currentDate,
                startTimeEarliest: startTimeEarliest,
                floorAndUnitId: widget.floorAndUnitId,
                blockName: widget.blockName,
              ));
        }
      } else {
        showToast(event.errormessage);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[7],
          leading: Container(),
          actions: [
            AppBarBookingPage(
              pageTitle: "KEY COLLECTION",
              leftIcon: "assets/icons/arrow_back.png",
              rightIcon: "assets/icons/estate_icon.png",
              onTapRightIcon: () {
                push(context, EstatePage());
              },
              onTapLeftIcon: () {
                push(
                    context,
                    KeyCollectionAppointmentsReschedule(
                      unitId: widget.unitId,
                      blockName: widget.blockName,
                      floorAndUnitId: widget.floorAndUnitId,
                    ));
              },
            )
          ],
        ),
        body: ListView(
          scrollDirection: Axis.vertical,
          children: [
            BookingCalenderPage(
              towerName: widget.blockName,
              rightIcon: false,
              towerSubName: widget.floorAndUnitId,
              heading: "Event: Key Collection Appointment",
              valueChanged: (value) {
                setState(() {
                  currentDate = value;
                  estateBloc.getKeyCollectionTimeSlots(
                      new DateFormat('dd-MM-yyyy').format(value),
                      widget.unitId);
                  print(currentDate);
                });
              },
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: 20)),
              child: timeSlotAvailability
                  ? Row(
                      children: [
                        Text(
                          "Available Time Slots",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 16,
                              fontFamily: "SFProText-SemiBold",
                              fontWeight: FontWeight.w400),
                        ),
                      ],
                    )
                  : Container(),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: screenWidth(context, dividedBy: 20)),
                child:
                    // TimeSlot(aspectRatio: 2/1,crossAxisCount: 4,itemCount: 12,),
                    StreamBuilder<KeyCollectionTimeSlotsResponse>(
                        stream: estateBloc.getKeyCollectionTimeSlotResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? timeSlotAvailability == true
                                  ? GridView.builder(
                                      itemCount: snapshot.data.data.length,
                                      shrinkWrap: true,
                                      physics: NeverScrollableScrollPhysics(),
                                      gridDelegate:
                                          SliverGridDelegateWithFixedCrossAxisCount(
                                              childAspectRatio: 2 / 1,
                                              crossAxisCount: 4),
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        itemNumber == index
                                            ? selected = true
                                            : selected = false;

                                        return GestureDetector(
                                            onTap: () {
                                              itemNumber = index;
                                              setState(() {
                                                startTimeSelected = snapshot
                                                    .data.data[index].starttime;

                                                startTimeEarliest = snapshot
                                                    .data.data[0].starttime;
                                                endTimeSelected = snapshot
                                                    .data.data[index].endtime;
                                                endTimeEarliest = snapshot
                                                    .data.data[0].endtime;
                                              });
                                            },
                                            child: TimeSlotTile(
                                                selected: selected,
                                                deselected: false,
                                                time: time(
                                                  snapshot.data.data[index]
                                                      .starttime,
                                                )));
                                      },
                                    )
                                  : Text(
                                      "No Time Slots Available",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontSize: 16,
                                          fontFamily: "SFProText-SemiBold",
                                          fontWeight: FontWeight.w400),
                                    )
                              : Shimmer.fromColors(
                                  highlightColor: Constants.kitGradients[0],
                                  baseColor: Constants.kitGradients[1],
                                  child: Container(
                                    height:
                                        screenHeight(context, dividedBy: 3.1),
                                    width: screenWidth(context, dividedBy: 1),
                                    decoration:
                                        BoxDecoration(color: Colors.black54),
                                  ),
                                );
                        })),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 10),
                ),
                BuildButton(
                  title: "Next",
                  faultReport: true,
                  isLoading: isLoading,
                  onPressed: () {
                    if (startTimeSelected != null) {
                      setState(() {
                        isLoading = true;
                      });
                      estateBloc.earliestSlotKeyCollection(
                          unitInfoId: widget.unitId,
                          earliestSlotKeyCollectionRequest:
                              EarliestSlotKeyCollectionRequest(
                                  appointmentfromtime: startTimeSelected,
                                  appointmentondate:
                                      new DateFormat('dd-MM-yyyy')
                                          .format(currentDate)));
                    } else {
                      showToast("Please select a time slot!");
                    }
                  },
                  disabled: true,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
