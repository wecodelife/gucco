import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/add_unit_login_martin_modern_request.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/estate_bottom_navigation_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_feild%20_user_details.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class AddUnitLogin extends StatefulWidget {
  @override
  _AddUnitLoginState createState() => _AddUnitLoginState();
}

class _AddUnitLoginState extends State<AddUnitLogin> {
  TextEditingController usernameTextEditingController =
      new TextEditingController();
  TextEditingController passwordTextEditingController =
      new TextEditingController();
  EstateBloc estateBloc = new EstateBloc();

  bool isLoading;

  @override
  void initState() {
    estateBloc.addUnitLoginMartinModernResponse.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        pushAndReplacement(context, EstatePage());
      } else {
        showToast(event.errormessage);
      }
    });

    // TODO: implement initState
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    pushAndReplacement(context, EstatePage());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        resizeToAvoidBottomPadding: true,
        bottomNavigationBar: EstateBottomBar(
          iconLeft: "assets/icons/crown.png",
          iconRight: "assets/icons/estate_icon.png",
          titleLeft: "Privilege Club",
          titleRight: "My Estate",
          pageValue: "tower",
        ),
        body: SingleChildScrollView(
          child: Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 2.9),
                  width: screenWidth(context, dividedBy: 1),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                    image: AssetImage(
                      "assets/images/add_unit_bg.png",
                    ),
                    fit: BoxFit.cover,
                    colorFilter: new ColorFilter.mode(
                        Colors.black.withOpacity(0.6), BlendMode.darken),
                  )),
                  child: Column(
                    children: [
                      Spacer(
                        flex: 1,
                      ),
                      GestureDetector(
                        onTap: () {
                          push(context, EstatePage());
                        },
                        child: Row(
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 15),
                            ),
                            Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                              size: 30,
                            ),
                          ],
                        ),
                      ),
                      Spacer(
                        flex: 1,
                      ),
                      Image(
                        image:
                            AssetImage("assets/images/martin_modern_logo.png"),
                      ),
                      Spacer(
                        flex: 2,
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Container(
                    width: screenWidth(context, dividedBy: 1),
                    decoration: BoxDecoration(
                        color: Constants.kitGradients[31],
                        image: DecorationImage(
                          image: AssetImage(
                            "assets/images/bg_image_addunit.png",
                          ),
                          fit: BoxFit.cover,
                        )),
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 15),
                        ),
                        Row(
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 10.7),
                            ),
                            Container(
                              width: screenWidth(context, dividedBy: 1.24),
                              child: Row(
                                children: [
                                  Text(
                                    "ADD PROPERTY",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: 'SFProDisplay-Bold',
                                        fontSize: 18),
                                  ),
                                  Spacer(
                                    flex: 1,
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      otpAlertBox(
                                          context: context,
                                          title:
                                              "Please refer to your lawyer's letter for  login details",
                                          route: AddUnitLogin(),
                                          stayOnPage: true);
                                    },
                                    child: Icon(
                                      Icons.help,
                                      size: 20,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Spacer(
                                    flex: 20,
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 17),
                        ),
                        FormFeildUserDetails(
                          hintText: "Username",
                          textEditingController: usernameTextEditingController,
                          onPressed: () {},
                          readOnly: isLoading,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                        FormFeildUserDetails(
                          hintText: "Password",
                          textEditingController: passwordTextEditingController,
                          onPressed: () {},
                          readOnly: isLoading,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 10),
                        ),
                        BuildButtonIcon(
                          rightIcon: false,
                          leftIcon: false,
                          isLoading: isLoading,
                          onPressed: () {
                            if (usernameTextEditingController.text
                                        .trim()
                                        .length >
                                    0 &&
                                passwordTextEditingController.text
                                        .trim()
                                        .length >
                                    0) {
                              setState(() {
                                isLoading = true;
                              });

                              print("is Loading");
                              estateBloc.addUnitLoginMartinModern(
                                  addUnitLoginMartinModernRequest:
                                      AddUnitLoginMartinModernRequest(
                                          owner: true,
                                          unitusername:
                                              usernameTextEditingController
                                                  .text,
                                          unitpassword:
                                              passwordTextEditingController
                                                  .text));
                              print("password" +
                                  passwordTextEditingController.text);
                            } else {
                              showToast("Enter in all fields!");
                            }
                          },
                          title: "Register Property",
                          buttonColorBrown: true,
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 9.068),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
