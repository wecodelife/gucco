import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/get_key_collection_appointments_response_model.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_appointments_reschedule.dart';
import 'package:guoco_priv_app/src/ui/widgets/attendee_details.dart';
import 'package:guoco_priv_app/src/ui/widgets/booking_details.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/comments_additional_info.dart';
import 'package:guoco_priv_app/src/ui/widgets/documents_to_bring.dart';
import 'package:guoco_priv_app/src/ui/widgets/key_collection_appbar_bottom2.dart';
import 'package:guoco_priv_app/src/ui/widgets/key_collection_venue.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:share/share.dart';

class KeyCollectionConfirmation extends StatefulWidget {
  final String keyCollectionId;
  final String unitId;
  final String blockName;
  final String floorAndUnitId;
  KeyCollectionConfirmation(
      {this.keyCollectionId, this.blockName, this.floorAndUnitId, this.unitId});
  @override
  _KeyCollectionConfirmationState createState() =>
      _KeyCollectionConfirmationState();
}

class _KeyCollectionConfirmationState extends State<KeyCollectionConfirmation> {
  ScrollController _controller = ScrollController();
  bool bookingDetailsStatus = false;
  bool attendeeDetailsStatus = false;
  bool commentStatus = false;
  bool documentsToBring = false;
  bool keyCollectionStatus = false;
  String shareMessage;
  TextEditingController emailAddressTextEditingController =
      TextEditingController();

  Future<bool> _willPopCallback() async {
    push(context, EstatePage());
    return true;
  }

  EstateBloc estateBloc = new EstateBloc();
  bool isLoading;

  @override
  void initState() {
    // TODO: implement initState
    print(widget.keyCollectionId);
    estateBloc.getKeyCollectionAppointment(
        keyCollectionAppointmentId: widget.keyCollectionId);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Constants.kitGradients[7],
          actions: [
            Container(
              width: screenWidth(context, dividedBy: 1),
              color: Constants.kitGradients[7],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  GestureDetector(
                    onTap: () {
                      push(
                          context,
                          KeyCollectionAppointmentsReschedule(
                            unitId: widget.unitId,
                            blockName: widget.blockName,
                            floorAndUnitId: widget.floorAndUnitId,
                          ));
                    },
                    child: Icon(
                      Icons.arrow_back,
                      color: Constants.kitGradients[0],
                    ),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 9),
                  ),
                  Text('KEY COLLECTION',
                      style: TextStyle(
                          fontFamily: 'SFPro',
                          fontSize: 17,
                          color: Constants.kitGradients[0])),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 9),
                  ),
                  GestureDetector(
                    onTap: () {
                      push(context, EstatePage());
                    },
                    child: SvgPicture.asset(
                        'assets/icons/key_collection_self_appbar_icon.svg'),
                  )
                ],
              ),
            ),
          ],
        ),
        body: Column(
          children: [
            TowerName(
              towerName: widget.blockName,
              towerSubName: widget.floorAndUnitId,
            ),
            Column2(
              title: 'Appointment booked!',
            ),
            Expanded(
                child: SingleChildScrollView(
              controller: _controller,
              scrollDirection: Axis.vertical,
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    StreamBuilder<GetKeyCollectionAppointmentResponse>(
                        stream: estateBloc.getKeyCollectionAppointmentResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? Column(
                                  children: [
                                    BookingDetailsWidget(
                                      bookingDetailsStatus:
                                          bookingDetailsStatus,
                                      date: snapshot.data.data.appointmentdate,
                                      earliestStartTime: snapshot
                                          .data.data.appointmentfromtime,
                                      selectedStartTime: snapshot
                                          .data.data.appointmentfromtime,
                                      reschedule: true,
                                      bookedDate: DateTime.now(),
                                      createDate: DateTime.now(),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 50),
                                    ),
                                    Center(
                                      child: Container(
                                        height: 1,
                                        color: Color(0xFF979797),
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 50),
                                    ),
                                    AttendeeDetails(
                                      attendeeDetailsStatus:
                                          attendeeDetailsStatus,
                                      noOfPeople: snapshot.data.data.noofpeople
                                          .toString(),
                                      licensePlate:
                                          snapshot.data.data.vehicleplateno,
                                      contactNo: snapshot.data.data.contactno,
                                      attendee: snapshot.data.data.fullname,
                                      additionalInfo:
                                          snapshot.data.data.additionalinfo,
                                      email: snapshot.data.data.emailaddress,
                                    ),
                                    Center(
                                      child: Container(
                                        height: 1,
                                        color: Color(0xFF979797),
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 50),
                                    ),
                                    CommentsAdditionalInfo(
                                      commentStatus: commentStatus,
                                      comments: snapshot.data.data.comments,
                                    ),
                                    Center(
                                      child: Container(
                                        height: 1,
                                        color: Color(0xFF979797),
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 50),
                                    ),
                                    DocumentsToBring(
                                      documentsToBring: documentsToBring,
                                    ),
                                    Center(
                                      child: Container(
                                        height: 1,
                                        color: Color(0xFF979797),
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 50),
                                    ),
                                    KeyCollectionVenue(
                                      keyCollectionStatus: keyCollectionStatus,
                                    ),
                                    Center(
                                      child: Container(
                                        height: 1,
                                        color: Color(0xFF979797),
                                      ),
                                    ),
                                    SizedBox(
                                      height:
                                          screenHeight(context, dividedBy: 50),
                                    ),
                                  ],
                                )
                              : Container(
                                  height: screenHeight(context, dividedBy: 2),
                                  width: screenWidth(context, dividedBy: 1),
                                  child: Center(
                                    child: CircularProgressIndicator(
                                        valueColor:
                                            new AlwaysStoppedAnimation<Color>(
                                                Constants.kitGradients[1])),
                                  ),
                                );
                        }),

                    // ShareWidget(
                    //     // emailTextEditingController:
                    //     //     emailAddressTextEditingController,
                    //     ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: screenHeight(context, dividedBy: 80),
                          right: screenWidth(context, dividedBy: 1.5)),
                      child: Builder(builder: (BuildContext context) {
                        return BuildButton(
                          disabled: true,
                          faultReport: true,
                          isLoading: isLoading,
                          title: "Share",
                          onPressed: () {
                            estateBloc
                                .shareTextKeyCollection(widget.keyCollectionId);
                            estateBloc.shareTextKeyCollectionResponse
                                .listen((event) {
                              setState(() {
                                isLoading = false;
                                if (event.statuscode == 200) {
                                  shareMessage = event.data;

                                  _onShare(context);
                                }
                              });
                            });
                          },
                        );
                      }),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 250),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 50),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                                'Should you wish to reschedule this appointment,',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontFamily: 'SFProText-Regular',
                                    fontSize: 12,
                                    color: Constants.kitGradients[7])),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'you can do so via ',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'SFProText-Regular',
                                  fontSize: 12,
                                  color: Constants.kitGradients[7]),
                            ),
                            Text(
                              'Appointments > Reschedule.',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontFamily: 'SFProText-Regular',
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                  color: Constants.kitGradients[7]),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    ),
                    // Center(
                    //   child: Text('See you soon!',
                    //       style: TextStyle(
                    //           fontFamily: 'SFProText-Regular',
                    //           fontSize: 15,
                    //           color: Constants.kitGradients[7])),
                    // ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 10),
                    ),
                  ],
                ),
              ),
            )),
          ],
        ),
      ),
    );
  }

  _onShare(BuildContext context) async {
    // A builder is used to retrieve the context immediately
    // surrounding the RaisedButton.
    //
    // The context's `findRenderObject` returns the first
    // RenderObject in its descendent tree when it's not
    // a RenderObjectWidget. The RaisedButton's RenderObject
    // has its position and size after it's built.
    final RenderBox box = context.findRenderObject();

    await Share.share(
        // "The appointment is fixed at " + "10:00" + "on" + "3rd April 2020",
        shareMessage,
        subject: "",
        sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }
}
