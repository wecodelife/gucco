import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/models/key_collection_self_request_model.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_appointments_reschedule.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_confirm.dart';
import 'package:guoco_priv_app/src/ui/widgets/add_people_number.dart';
import 'package:guoco_priv_app/src/ui/widgets/key_collection_appbar_bottom2.dart';
import 'package:guoco_priv_app/src/ui/widgets/key_collection_submit_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/text_field_widget.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:intl/intl.dart';

class KeyCollectionSelf extends StatefulWidget {
  final String unitId;
  final String startTime;
  final String startTimeEarliest;
  final String endTime;
  final DateTime date;
  final bool reschedule;
  final bool isLoading;
  final String blockName;
  final String floorAndUnitId;
  final DateTime createdDate;
  final String vehicleInfo;
  final String additionalInfo;
  final String noOfPeople;
  final String comments;

  KeyCollectionSelf(
      {this.unitId,
      this.startTime,
      this.endTime,
      this.date,
      this.startTimeEarliest,
      this.reschedule,
      this.isLoading,
      this.floorAndUnitId,
      this.blockName,
      this.createdDate,
      this.comments,
      this.additionalInfo,
      this.noOfPeople,
      this.vehicleInfo});
  @override
  _KeyCollectionSelfState createState() => _KeyCollectionSelfState();
}

enum Answer { self, authorisedIndividual }

class _KeyCollectionSelfState extends State<KeyCollectionSelf> {
  ScrollController _controller = ScrollController();
  Answer _answer = Answer.self;
  int count = 1;

  TextEditingController _fullNameTextEditingController =
      TextEditingController();
  TextEditingController _contactNoTextEditingController =
      TextEditingController();
  TextEditingController _emailAddressTextEditingController =
      TextEditingController();
  TextEditingController _noOfPeopleTextEditingController =
      TextEditingController();
  TextEditingController _vehicleInfoTextEditingController =
      TextEditingController();
  TextEditingController _additionalInfoTextEditingController =
      TextEditingController();
  TextEditingController _commentsTextEditingController =
      TextEditingController();
  AuthBloc authBloc = new AuthBloc();
  bool isLoading = false;
  void getSelfData() {
    _fullNameTextEditingController.text =
        ObjectFactory().appHive.getName() != null
            ? ObjectFactory().appHive.getName() +
                " " +
                ObjectFactory().appHive.getLastName()
            : '';
    _emailAddressTextEditingController.text =
        ObjectFactory().appHive.getEmail() != null
            ? ObjectFactory().appHive.getEmail()
            : '';
    _contactNoTextEditingController.text =
        ObjectFactory().appHive.getContactNo() != null
            ? ObjectFactory().appHive.getContactNo()
            : '';
    count = (widget.noOfPeople != null && widget.noOfPeople != "")
        ? int.parse(widget.noOfPeople)
        : 1;
    _vehicleInfoTextEditingController.text =
        widget.vehicleInfo != null ? widget.vehicleInfo : '';
    _additionalInfoTextEditingController.text =
        widget.additionalInfo != null ? widget.additionalInfo : '';
    _commentsTextEditingController.text =
        widget.comments != null ? widget.comments : '';
  }

  @override
  void initState() {
    print("fgfg" + widget.noOfPeople.toString());
    getSelfData();
    authBloc.keyCollectionSelfResponse.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        if (widget.reschedule == true) {
          pushAndReplacement(
              context,
              KeyCollectionConfirm(
                  unitId: widget.unitId,
                  keyCollectionId: event.data.id,
                  reschedule: widget.reschedule,
                  selectedStartTime: widget.startTime,
                  earliestStartTime: widget.startTimeEarliest,
                  date: widget.date.toString(),
                  additionalInfo: _additionalInfoTextEditingController.text,
                  attendee:
                      _answer == Answer.self ? "Self" : "Authorised Individual",
                  comments: _commentsTextEditingController.text,
                  contactNo: _contactNoTextEditingController.text,
                  licensePlate: _vehicleInfoTextEditingController.text,
                  noOfPeople: count.toString(),
                  email: _emailAddressTextEditingController.text,
                  blockName: widget.blockName,
                  floorAndUnitId: widget.floorAndUnitId,
                  fullName: _fullNameTextEditingController.text,
                  createdDate: widget.createdDate,
                  bookedDate: event.data.createdOn));
        } else {
          push(
              context,
              KeyCollectionConfirm(
                  unitId: widget.unitId,
                  keyCollectionId: event.data.id,
                  selectedStartTime: widget.startTime,
                  earliestStartTime: widget.startTimeEarliest,
                  date: widget.date.toString(),
                  additionalInfo: _additionalInfoTextEditingController.text,
                  attendee:
                      _answer == Answer.self ? "Self" : "Authorised Individual",
                  fullName: _fullNameTextEditingController.text,
                  comments: _commentsTextEditingController.text,
                  contactNo: _contactNoTextEditingController.text,
                  licensePlate: _vehicleInfoTextEditingController.text,
                  noOfPeople: count.toString(),
                  email: _emailAddressTextEditingController.text,
                  blockName: widget.blockName,
                  floorAndUnitId: widget.floorAndUnitId,
                  bookedDate: event.data.createdOn));
        }
      } else {
        showToast(event.errormessage);
      }
    });
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(
        context,
        KeyCollectionAppointmentsReschedule(
          unitId: widget.unitId,
          blockName: widget.blockName,
          floorAndUnitId: widget.floorAndUnitId,
          collectionPaymentReschedule:
              ObjectFactory().appHive.getKeyCollectionPaymentStatus(),
        ));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: WillPopScope(
        onWillPop: _willPopCallback,
        child: Scaffold(
          appBar: AppBar(
            actions: [
              Container(
                width: screenWidth(context, dividedBy: 1),
                color: Constants.kitGradients[7],
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: () {
                        push(
                            context,
                            KeyCollectionAppointmentsReschedule(
                              unitId: widget.unitId,
                              blockName: widget.blockName,
                              floorAndUnitId: widget.floorAndUnitId,
                            ));
                      },
                      child: Icon(
                        Icons.arrow_back,
                        color: Constants.kitGradients[0],
                      ),
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 9),
                    ),
                    Text('KEY COLLECTION',
                        style: TextStyle(
                            fontFamily: 'SFProDisplay-Medium',
                            fontSize: 17,
                            color: Constants.kitGradients[0])),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 9),
                    ),
                    SvgPicture.asset(
                        'assets/icons/key_collection_self_appbar_icon.svg')
                  ],
                ),
              ),
            ],
          ),
          body: widget.isLoading == true
              ? Center(
                  child: Container(
                    height: screenHeight(context, dividedBy: 20),
                    width: screenWidth(context, dividedBy: 10),
                    child: CircularProgressIndicator(
                      valueColor:
                          new AlwaysStoppedAnimation<Color>(Colors.black),
                    ),
                  ),
                )
              : Column(
                  children: [
                    TowerName(
                      towerName: widget.blockName,
                      towerSubName: widget.floorAndUnitId,
                    ),
                    Column2(
                      title: 'Event: Key Collection Appointment',
                    ),
                    Expanded(
                      child: SingleChildScrollView(
                        controller: _controller,
                        scrollDirection: Axis.vertical,
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Date:',
                                      style: TextStyle(
                                          fontFamily: 'SFProText-Regular',
                                          fontSize: 15,
                                          color: Constants.kitGradients[7])),
                                  Text(
                                      new DateFormat('dd-MMM-yyyy').format(
                                          DateTime.parse(widget.date != null
                                              ? widget.date.toString()
                                              : DateTime.now().toString())),
                                      style: TextStyle(
                                          fontFamily: 'SFProText-Regular',
                                          fontSize: 15,
                                          color: Constants.kitGradients[7])),
                                ],
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 40),
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Time:',
                                      style: TextStyle(
                                          fontFamily: 'SFProText-Regular',
                                          fontSize: 15,
                                          color: Constants.kitGradients[7])),
                                  Text(time(widget.startTime),
                                      style: TextStyle(
                                          fontFamily: 'SFProText-Regular',
                                          fontSize: 15,
                                          color: Constants.kitGradients[7])),
                                ],
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              Text('Please specify who will be attending:',
                                  style: TextStyle(
                                      fontFamily: 'SFProText-Regular',
                                      fontSize: 15,
                                      color: Constants.kitGradients[7])),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 50),
                              ),
                              Row(
                                children: [
                                  Radio(
                                    activeColor: Constants.kitGradients[10],
                                    value: Answer.self,
                                    groupValue: _answer,
                                    onChanged: (Answer value) {
                                      setState(() {
                                        _answer = value;
                                        getSelfData();
                                      });
                                      print("selected option is: " +
                                          _answer.toString());
                                    },
                                  ),
                                  Text('Owner',
                                      style: TextStyle(
                                          fontFamily: 'SFProText-Regular',
                                          fontSize: 15,
                                          color: Constants.kitGradients[7])),
                                ],
                              ),
                              Row(
                                children: [
                                  Radio(
                                    activeColor: Constants.kitGradients[10],
                                    value: Answer.authorisedIndividual,
                                    groupValue: _answer,
                                    onChanged: (Answer value) {
                                      setState(() {
                                        _answer = value;
                                        _fullNameTextEditingController.text =
                                            "";
                                        _emailAddressTextEditingController
                                            .text = "";
                                        _contactNoTextEditingController.text =
                                            "";
                                        count = 1;
                                        _vehicleInfoTextEditingController.text =
                                            '';
                                        _additionalInfoTextEditingController
                                            .text = '';
                                        _commentsTextEditingController.text =
                                            '';
                                      });
                                      print("selected option is: " +
                                          _answer.toString());
                                    },
                                  ),
                                  Text('Authorised Individual',
                                      style: TextStyle(
                                          fontFamily: 'SFProText-Regular',
                                          fontSize: 15,
                                          color: Constants.kitGradients[7])),
                                ],
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              TextFieldWidget(
                                title: 'First Name and Last Name of Attendee',
                                textEditingController:
                                    _fullNameTextEditingController,
                                readOnly: _answer == Answer.self ? true : false,
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              TextFieldWidget(
                                title: 'Contact No.',
                                textEditingController:
                                    _contactNoTextEditingController,
                                readOnly: _answer == Answer.self ? true : false,
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              TextFieldWidget(
                                title: 'Email Address',
                                textEditingController:
                                    _emailAddressTextEditingController,
                                readOnly: _answer == Answer.self ? true : false,
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              Container(
                                width: screenWidth(context, dividedBy: 1.3),
                                child: Row(
                                  children: [
                                    Text('No of People',
                                        style: TextStyle(
                                            fontFamily: 'SFProText-Regular',
                                            fontSize: 15,
                                            color: Constants.kitGradients[7])),
                                    Spacer(
                                      flex: 1,
                                    ),
                                    GestureDetector(
                                      onTap: () {
                                        otpAlertBox(
                                            context: context,
                                            stayOnPage: true,
                                            title:
                                                "Please limit attendees to 3 max");
                                      },
                                      child: Icon(
                                        Icons.help,
                                        color: Colors.black,
                                      ),
                                    ),
                                    Spacer(
                                      flex: 10,
                                    )
                                  ],
                                ),
                              ),
                              AddPeopleNumber(
                                count: count,
                                decrementOnTap: () {
                                  setState(() {
                                    if (count > 1) count = count - 1;
                                  });
                                },
                                incrementOnTap: () {
                                  setState(() {
                                    if (count < 3) count = count + 1;
                                  });
                                },
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              TextFieldWidget(
                                title: 'Vehicle License Plate No.',
                                textEditingController:
                                    _vehicleInfoTextEditingController,
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              TextFieldWidget(
                                title: 'Additional Info',
                                textEditingController:
                                    _additionalInfoTextEditingController,
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              ),
                              TextFieldWidget(
                                title: 'Comments',
                                textEditingController:
                                    _commentsTextEditingController,
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 5),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  GestureDetector(
                                    child: SubmitButton(
                                      isLoading: isLoading,
                                      title: 'Confirm Appointment',
                                      width: 1.2,
                                      height: 14,
                                    ),
                                    onTap: () {
                                      print(widget.date);
                                      if (count != 0 &&
                                          _emailAddressTextEditingController
                                                  .text
                                                  .trim()
                                                  .length !=
                                              0) {
                                        final bool isValid =
                                            EmailValidator.validate(
                                                _emailAddressTextEditingController
                                                    .text);
                                        if (isValid) {
                                          setState(() {
                                            isLoading = true;
                                          });
                                          authBloc.keyCollectionSelf(
                                              keyCollectionSelfRequest:
                                                  KeyCollectionSelfRequest(
                                                contactno:
                                                    _contactNoTextEditingController
                                                        .text,
                                                additionalinfo:
                                                    _additionalInfoTextEditingController
                                                        .text,
                                                emailaddress:
                                                    _emailAddressTextEditingController
                                                        .text,
                                                fullname:
                                                    _fullNameTextEditingController
                                                        .text,
                                                noofpeople: count,
                                                vehicleplateno:
                                                    _vehicleInfoTextEditingController
                                                        .text,
                                                appointmentfromtime:
                                                    widget.startTime,
                                                appointmenttotime:
                                                    widget.endTime,
                                                appointmentondate:
                                                    new DateFormat('dd-MM-yyyy')
                                                        .format(DateTime.parse(
                                                            widget.date != null
                                                                ? widget.date
                                                                    .toString()
                                                                : DateTime.now()
                                                                    .toString())),
                                                comments:
                                                    _commentsTextEditingController
                                                        .text,
                                                self: _answer == Answer.self
                                                    ? true
                                                    : false,
                                                vip: _answer ==
                                                        Answer
                                                            .authorisedIndividual
                                                    ? true
                                                    : false,
                                              ),
                                              unitId: widget.unitId);
                                        } else {
                                          showToast(
                                              "Please Enter a Valid Email Address");
                                        }
                                      } else {
                                        showToast(
                                            "Please enter necessary details");
                                      } // Navigator.push(context, MaterialPageRoute(builder: (context)=>KeyCollectionConfirm()));
                                    },
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 30),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 30),
                    )
                  ],
                ),
        ),
      ),
    );
  }
}
