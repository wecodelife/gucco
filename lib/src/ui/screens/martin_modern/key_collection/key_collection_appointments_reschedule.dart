// import 'dart:html';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/key_collection_appointments_response.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_appointments.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_booking.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_confirmation.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_reschedule_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/drawer_guoco.dart';
import 'package:guoco_priv_app/src/ui/widgets/list_view_reschedule.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:intl/intl.dart';

class KeyCollectionAppointmentsReschedule extends StatefulWidget {
  final String unitId;
  final String blockName;
  final String floorAndUnitId;
  final bool collectionPaymentReschedule;

  KeyCollectionAppointmentsReschedule(
      {this.unitId,
      this.floorAndUnitId,
      this.blockName,
      this.collectionPaymentReschedule});
  @override
  _KeyCollectionAppointmentsRescheduleState createState() =>
      _KeyCollectionAppointmentsRescheduleState();
}

class _KeyCollectionAppointmentsRescheduleState
    extends State<KeyCollectionAppointmentsReschedule> {
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();

  bool isLoading;
  String lastDate;
  bool reschedule = false;
  EstateBloc estateBloc = new EstateBloc();
  String keyId = "";
  String times = "";
  String dates = "";
  DateTime createdDate;
  String vehicleInfo = "";
  String noOfPeople = "";
  String additionalInfo = "";
  String comments = "";
  bool showButton = true;
  String keyCollectionId;

  @override
  void initState() {
    estateBloc.getKeyCollectionAppointments(widget.unitId);
    estateBloc.getKeyCollectionAppointmentsResponse.listen((event) {
      if (event.data.isNotEmpty) {
        setState(() {
          showButton = false;
        });
      }
    });
    estateBloc.cancelKeyCollectionAppointmentsResponse.listen((event) {
      setState(() {
        isLoading = false;
      });
      print("Unit id" + widget.unitId);
      if (event.statuscode == 200 && event.errorcode == 0) {
        setState(() {
          showButton = true;
        });
        if (reschedule == true) {
          push(
              context,
              KeyCollectionReschedule(
                unitId: widget.unitId,
                keyId: keyId,
                time: times,
                date: dates,
                floorAndUnitId: widget.floorAndUnitId,
                blockName: widget.blockName,
                createdDate: createdDate,
                vehicleInfo: vehicleInfo,
                noOfPeople: noOfPeople,
                additionalInfo: additionalInfo,
                comments: comments,
              ));
        } else {
          showToast("Appointment Cancelled");
          push(
              context,
              KeyCollectionAppointmentsReschedule(
                unitId: widget.unitId,
                blockName: widget.blockName,
                floorAndUnitId: widget.floorAndUnitId,
              ));
        }
        // cancelAlertBox(
        //   context: context,
        //   msg: "Appointment Cancelled",
        //   text1:
        //       "Please book another appointment and complete your Key Collection by",
        //   text2: lastDate,
        //   contentPadding: 40,
        //   insetPadding: 2.8,
        //   onPressed: () {
        //     if (reschedule == true) {
        //       push(
        //           context,
        //           KeyCollectionReschedule(
        //             unitId: widget.unitId,
        //             keyId: keyId,
        //             time: times,
        //             date: dates,
        //             floorAndUnitId: widget.floorAndUnitId,
        //             blockName: widget.blockName,
        //             createdDate: createdDate,
        //             vehicleInfo: vehicleInfo,
        //             noOfPeople: noOfPeople,
        //             additionalInfo: additionalInfo,
        //             comments: comments,
        //           ));
        //     } else {
        //       push(
        //           context,
        //           KeyCollectionAppointmentsReschedule(
        //             unitId: widget.unitId,
        //             blockName: widget.blockName,
        //             floorAndUnitId: widget.floorAndUnitId,
        //           ));
        //     }
        //   },
        //   titlePadding: 40,
        // );
      } else {
        showToast("Please check your internet connection!");
      }
    });
    // TODO: implement initState
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(context, EstatePage());
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _willPopCallback,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          key: _globalKey,
          drawer: DrawerGuoco(),
          appBar: AppBar(
            backgroundColor: Constants.kitGradients[7],
            leading: Container(),
            actions: [
              AppBarBookingPage(
                rightIcon: "assets/icons/estate_icon.png",
                leftIcon: "assets/icons/facilitiesmenu.png",
                pageTitle: "KEY COLLECTION",
                onTapRightIcon: () {
                  push(context, EstatePage());
                },
                onTapLeftIcon: () {
                  _globalKey.currentState.openDrawer();
                },
              ),
            ],
          ),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  TowerName(
                    towerName: widget.blockName,
                    towerSubName: widget.floorAndUnitId,
                  ),

                  AppBarRightIcon(
                    pageTitle: "My Appointments",
                    boxWidth: 6,
                    icon: "assets/icons/switchviewcalendar.png",
                    rightIcon: false,
                    bookedListTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => KeyCollectionAppointments(
                                    unitId: widget.unitId,
                                    floorAndUnitId: widget.floorAndUnitId,
                                    blockName: widget.blockName,
                                  )));
                    },
                  ),
                  // ListViewListBookings(
                  //   evenDate: "30-April-2020",
                  //   visitor: "John Smith",
                  //   relation: "Service Provider",
                  //   eventTime:"10:00 AM -12:00 AM" ,
                  //   visitorList: true,
                  // ),

                  Container(
                    width: screenWidth(context, dividedBy: 1),
                    height: screenHeight(context, dividedBy: 1.6),
                    child: StreamBuilder<KeyCollectionAppointmentsResponse>(
                        stream: estateBloc.getKeyCollectionAppointmentsResponse,
                        builder: (context, snapshot) {
                          return snapshot.hasData
                              ? widget.collectionPaymentReschedule == false
                                  ? Container(
                                      width: screenWidth(context, dividedBy: 1),
                                      height:
                                          screenHeight(context, dividedBy: 1.6),
                                      child: Center(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Container(
                                              child: Text(
                                                "Payment Pending..",
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18,
                                                    fontWeight:
                                                        FontWeight.w700),
                                                textAlign: TextAlign.center,
                                              ),
                                              width: screenWidth(context,
                                                  dividedBy: 1.2),
                                            ),
                                            SizedBox(
                                              height: screenHeight(context,
                                                  dividedBy: 100),
                                            ),
                                            Container(
                                              width: screenWidth(context,
                                                  dividedBy: 1.4),
                                              child: Text(
                                                  "Please try again after completing your payment",
                                                  style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 16,
                                                  ),
                                                  textAlign: TextAlign.center),
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  : snapshot.data.data.isNotEmpty
                                      ? ListView.builder(
                                          itemCount: snapshot.data.data.length,
                                          shrinkWrap: true,
                                          itemBuilder: (BuildContext context,
                                              int index) {
                                            String keyCollectionId =
                                                snapshot.data.data[index].id;
                                            return ListViewReschedule(
                                              onPressed: () {
                                                push(
                                                    context,
                                                    KeyCollectionConfirmation(
                                                        unitId: widget.unitId,
                                                        floorAndUnitId: widget
                                                            .floorAndUnitId,
                                                        blockName:
                                                            widget.blockName,
                                                        keyCollectionId:
                                                            keyCollectionId));
                                              },
                                              title:
                                                  "Key Collection Appointment",
                                              // title: "Key Collection Appointment",
                                              eventTime: time(snapshot
                                                  .data
                                                  .data[index]
                                                  .appointmentfromtime),
                                              evenDate: new DateFormat(
                                                      'dd-MMM-yyyy')
                                                  .format(DateTime.parse(date(
                                                      snapshot.data.data[0]
                                                          .appointmentdate))),
                                              isLoading: isLoading,
                                              lastDate: new DateFormat(
                                                      'dd-MMM-yyyy')
                                                  .format(DateTime.parse(date(
                                                          snapshot.data.data[0]
                                                              .appointmentdate))
                                                      .add(Duration(days: 18))),
                                              onPressedReschedule: () {
                                                actionAlertBox(
                                                    reduceSize: true,
                                                    reschedule: true,
                                                    msg:
                                                        "Appointment Reschedule",
                                                    heading1:
                                                        "Appointment Date:",
                                                    heading2:
                                                        "Appointment Time:",
                                                    content1: time(snapshot
                                                        .data
                                                        .data[index]
                                                        .appointmentfromtime),
                                                    content2: new DateFormat(
                                                            'dd-MMM-yyyy')
                                                        .format(DateTime.parse(
                                                            date(snapshot
                                                                .data
                                                                .data[0]
                                                                .appointmentdate))),
                                                    cancelBoxTitle: "",
                                                    text1: "",
                                                    text2: "",
                                                    context: context,
                                                    contentNum: 2,
                                                    questionAlertNotBold: true,
                                                    cancelAlert: 5,
                                                    questionAlert: "",
                                                    date: "",
                                                    cancelBox: false,
                                                    isLoading: isLoading,
                                                    onPressed: () {
                                                      setState(() {
                                                        reschedule = true;
                                                        isLoading = true;
                                                        keyId = snapshot
                                                            .data.data[0].id;
                                                        times = snapshot
                                                            .data
                                                            .data[0]
                                                            .appointmentfromtime;
                                                        dates = snapshot
                                                            .data
                                                            .data[0]
                                                            .appointmentdate;
                                                        createdDate = snapshot
                                                            .data
                                                            .data[0]
                                                            .createdOn;
                                                        vehicleInfo = snapshot
                                                            .data
                                                            .data[0]
                                                            .vehicleplateno;
                                                        comments = snapshot.data
                                                            .data[0].comments;
                                                        additionalInfo =
                                                            snapshot
                                                                .data
                                                                .data[0]
                                                                .additionalinfo;
                                                        noOfPeople = snapshot
                                                            .data
                                                            .data[0]
                                                            .noofpeople
                                                            .toString();
                                                        lastDate = new DateFormat(
                                                                'dd-MMM-yyyy')
                                                            .format(DateTime.parse(
                                                                    date(snapshot
                                                                        .data
                                                                        .data[0]
                                                                        .appointmentdate))
                                                                .add(Duration(
                                                                    days: 18)));
                                                      });
                                                      estateBloc
                                                          .cancelKeyCollectionAppointments(
                                                              widget.unitId,
                                                              snapshot.data
                                                                  .data[0].id);
                                                    });
                                              },
                                              onPressedAlertBoxCancelButton:
                                                  () {
                                                setState(() {
                                                  reschedule = false;
                                                  isLoading = true;
                                                  lastDate = new DateFormat(
                                                          'dd-MMM-yyyy')
                                                      .format(DateTime.parse(
                                                              date(snapshot
                                                                  .data
                                                                  .data[0]
                                                                  .appointmentdate))
                                                          .add(Duration(
                                                              days: 18)));
                                                });
                                                estateBloc
                                                    .cancelKeyCollectionAppointments(
                                                        widget.unitId,
                                                        snapshot
                                                            .data.data[0].id);
                                              },
                                            );
                                          })
                                      : Container(
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          height: screenHeight(context,
                                              dividedBy: 1.6),
                                          child: Center(
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Container(
                                                  child: Text(
                                                    "No Appointments Made yet.",
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.w700),
                                                    textAlign: TextAlign.center,
                                                  ),
                                                  width: screenWidth(context,
                                                      dividedBy: 1.2),
                                                ),
                                                SizedBox(
                                                  height: screenHeight(context,
                                                      dividedBy: 100),
                                                ),
                                                Container(
                                                  width: screenWidth(context,
                                                      dividedBy: 1.4),
                                                  child: Text(
                                                      "Please make an appointment as soon as possible.",
                                                      style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 16,
                                                      ),
                                                      textAlign:
                                                          TextAlign.center),
                                                ),
                                              ],
                                            ),
                                          ),
                                        )
                              : Container(
                                  width: screenWidth(context, dividedBy: 1),
                                  height: screenHeight(context, dividedBy: 1.6),
                                  child: Center(
                                    child: CircularProgressIndicator(
                                      valueColor:
                                          new AlwaysStoppedAnimation<Color>(
                                              Constants.kitGradients[1]),
                                    ),
                                  ),
                                );
                        }),
                  )
                ],
              ),
              showButton == true
                  ? widget.collectionPaymentReschedule == false
                      ? Container(
                          // color: Colors.grey.withOpacity(.20),
                          // child: Text(
                          //   "Please Try again after your payment is completed",
                          //   style:
                          //       TextStyle(fontSize: 16, color: Colors.black),
                          // ),
                          )
                      : Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            BuildButtonIcon(
                              faultReport: true,
                              title: "Book An Appointment",
                              leftIcon: false,
                              arrowIcon: false,
                              onPressed: () {
                                push(
                                    context,
                                    KeyCollectionBooking(
                                      floorAndUnitId: widget.floorAndUnitId,
                                      unitId: widget.unitId,
                                      blockName: widget.blockName,
                                    ));
                              },
                            ),
                            SizedBox(
                              height: screenHeight(context, dividedBy: 30),
                            ),
                          ],
                        )
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}
