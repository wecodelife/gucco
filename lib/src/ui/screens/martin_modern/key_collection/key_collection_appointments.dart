import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/key_collection_appointments_response.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/home_key_collection_page.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_appointments_reschedule.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_booking.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_reschedule_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/booking_calender_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/drawer_guoco.dart';
import 'package:guoco_priv_app/src/ui/widgets/event_info.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:intl/intl.dart';
import 'package:shimmer/shimmer.dart';

class KeyCollectionAppointments extends StatefulWidget {
  final String unitId;
  final String blockName;
  final String floorAndUnitId;
  KeyCollectionAppointments({
    this.unitId,
    this.floorAndUnitId,
    this.blockName,
  });
  @override
  _KeyCollectionAppointmentsState createState() =>
      _KeyCollectionAppointmentsState();
}

class _KeyCollectionAppointmentsState extends State<KeyCollectionAppointments> {
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();

  EstateBloc estateBloc = new EstateBloc();
  bool isLoading = false;
  DateTime currentDate;
  String lastDate;

  @override
  void initState() {
    estateBloc.getKeyCollectionAppointments(widget.unitId);
    estateBloc.cancelKeyCollectionAppointmentsResponse.listen((event) {
      setState(() {
        isLoading = false;
      });
      if (event.statuscode == 200 && event.errorcode == 0) {
        showToast("Appointment Cancelled");
        pushAndReplacement(
            context,
            KeyCollectionAppointments(
              unitId: widget.unitId,
              blockName: widget.blockName,
              floorAndUnitId: widget.floorAndUnitId,
            ));
        // cancelAlertBox(
        //   context: context,
        //   msg: "Appointment Cancelled",
        //   text1:
        //       "Please book another appointment and complete your Key Collection by",
        //   text2: lastDate,
        //   contentPadding: 40,
        //   insetPadding: 2.8,
        //   onPressed: () {
        //     pushAndReplacement(
        //         context,
        //         KeyCollectionAppointments(
        //           unitId: widget.unitId,
        //           blockName: widget.blockName,
        //           floorAndUnitId: widget.floorAndUnitId,
        //         ));
        //   },
        //   titlePadding: 40,
        // );
      } else {
        showToast("Please check your internet connection!");
      }
    });
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    push(
        context,
        HomeKeyCollection(
          unitId: widget.unitId,
          floorAndUnitId: widget.floorAndUnitId,
          blockName: widget.blockName,
        ));
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      child: WillPopScope(
        onWillPop: _willPopCallback,
        child: Scaffold(
            resizeToAvoidBottomPadding: true,
            backgroundColor: Colors.white,
            key: _globalKey,
            drawer: DrawerGuoco(),
            appBar: AppBar(
              backgroundColor: Constants.kitGradients[7],
              leading: Container(),
              actions: [
                AppBarBookingPage(
                  pageTitle: "KEY COLLECTION",
                  leftIcon: "assets/icons/facilitiesmenu.png",
                  rightIcon: "assets/icons/estate_icon.png",
                  onTapRightIcon: () {
                    push(context, EstatePage());
                  },
                  onTapLeftIcon: () {
                    _globalKey.currentState.openDrawer();
                  },
                )
              ],
            ),
            body: isLoading == false
                ? ListView(
                    scrollDirection: Axis.vertical,
                    children: [
                      BookingCalenderPage(
                          towerName: widget.blockName,
                          towerSubName: widget.floorAndUnitId,
                          heading: "My Appointments",
                          rightIcon: false,
                          icon: "assets/icons/keyCollectionIcon.png",
                          bookListTap: () {
                            push(
                                context,
                                KeyCollectionAppointmentsReschedule(
                                  floorAndUnitId: widget.floorAndUnitId,
                                  blockName: widget.blockName,
                                  unitId: widget.unitId,
                                ));
                          },
                          valueChanged: (value) {
                            setState(() {
                              currentDate = value;
                              print(currentDate);
                            });
                          }),
                      StreamBuilder<KeyCollectionAppointmentsResponse>(
                          stream:
                              estateBloc.getKeyCollectionAppointmentsResponse,
                          builder: (context, snapshot) {
                            return snapshot.hasData
                                ? snapshot.data.data.isNotEmpty
                                    ? EventInfo(
                                        onTapCloseButton: () {
                                          actionAlertBox(
                                              msg: "Appointment Cancellation",
                                              heading1: "Appointment Date:",
                                              heading2: "Appointment Time:",
                                              content1: new DateFormat('dd-MMM-yyyy')
                                                  .format(DateTime.parse(date(snapshot
                                                      .data
                                                      .data[0]
                                                      .appointmentdate))),
                                              content2: time(snapshot.data
                                                  .data[0].appointmentfromtime),
                                              cancelBoxTitle: "",
                                              text1: "",
                                              text2: "",
                                              context: context,
                                              contentNum: 2,
                                              questionAlertNotBold: true,
                                              cancelAlert: 5,
                                              questionAlert:
                                                  "Upon cancellation, you will be required to book another appointment and complete your Key Collection by ",
                                              date: new DateFormat('dd-MMM-yyyy')
                                                  .format(DateTime.parse(date(snapshot
                                                          .data
                                                          .data[0]
                                                          .appointmentdate))
                                                      .add(Duration(days: 18))),
                                              cancelBox: false,
                                              isLoading: isLoading,
                                              onPressed: () {
                                                setState(() {
                                                  isLoading = true;
                                                  lastDate = new DateFormat(
                                                          'dd-MMM-yyyy')
                                                      .format(DateTime.parse(
                                                              date(snapshot
                                                                  .data
                                                                  .data[0]
                                                                  .appointmentdate))
                                                          .add(Duration(
                                                              days: 18)));
                                                });
                                                estateBloc
                                                    .cancelKeyCollectionAppointments(
                                                        widget.unitId,
                                                        snapshot
                                                            .data.data[0].id);
                                              });
                                        },
                                        onTapEditButton: () {
                                          push(
                                              context,
                                              KeyCollectionReschedule(
                                                unitId: widget.unitId,
                                                keyId: snapshot.data.data[0].id,
                                                time: snapshot.data.data[0]
                                                    .appointmentfromtime,
                                                date: snapshot.data.data[0]
                                                    .appointmentdate,
                                                floorAndUnitId:
                                                    widget.floorAndUnitId,
                                                blockName: widget.blockName,
                                                createdDate: snapshot
                                                    .data.data[0].createdOn,
                                                vehicleInfo: snapshot.data
                                                    .data[0].vehicleplateno,
                                                noOfPeople: snapshot
                                                    .data.data[0].noofpeople
                                                    .toString(),
                                                additionalInfo: snapshot.data
                                                    .data[0].additionalinfo,
                                                comments: snapshot
                                                    .data.data[0].comments,
                                              ));
                                        },
                                        eventHeading: "Meeting with CSO",
                                        eventDate: new DateFormat('dd-MMM-yyyy')
                                            .format(DateTime.parse(date(snapshot
                                                .data
                                                .data[0]
                                                .appointmentdate))),
                                        eventTime: time(snapshot
                                            .data.data[0].appointmentfromtime),
                                        enable: true,
                                      )
                                    : Container()
                                : Shimmer.fromColors(
                                    highlightColor: Constants.kitGradients[0],
                                    baseColor: Constants.kitGradients[1],
                                    child: Container(
                                      height:
                                          screenHeight(context, dividedBy: 3.1),
                                      width: screenWidth(context, dividedBy: 1),
                                      decoration:
                                          BoxDecoration(color: Colors.black54),
                                    ),
                                  );
                          }),
                      SizedBox(
                        height: screenHeight(context, dividedBy: 30),
                      ),
                      Row(
                        children: [
                          SizedBox(
                            width: screenWidth(context, dividedBy: 10),
                          ),
                          // BuildButtonIcon(
                          //   leftIcon: true,
                          //   arrowIcon: true,
                          //   icon: "assets/icons/KeyCollectionBookinIcon.png",
                          //   title: "Book An Appointment",
                          //   onPressed: () {
                          //     Navigator.push(
                          //         context,
                          //         MaterialPageRoute(
                          //             builder: (context) => KeyCollectionBooking(unitId: widget.unitId,)));
                          //   },
                          // ),
                          BuildButton(
                            arrowIcon: true,
                            title: "Book An Appointment  ",
                            faultReport: true,
                            onPressed: () {
                              push(
                                  context,
                                  KeyCollectionBooking(
                                    unitId: widget.unitId,
                                    blockName: widget.blockName,
                                    floorAndUnitId: widget.floorAndUnitId,
                                  ));
                            },
                            disabled: true,
                          )
                        ],
                      ),
                    ],
                  )
                : Center(
                    child: Container(
                      height: screenHeight(context, dividedBy: 20),
                      width: screenWidth(context, dividedBy: 10),
                      child: CircularProgressIndicator(
                        valueColor:
                            new AlwaysStoppedAnimation<Color>(Colors.black),
                      ),
                    ),
                  )),
      ),
    );
  }
}
