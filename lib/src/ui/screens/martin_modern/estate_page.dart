import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/condo_info_response.dart';
import 'package:guoco_priv_app/src/models/unit_users_response.dart';
import 'package:guoco_priv_app/src/ui/screens/home_post_move_in.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/home_key_collection_page.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_appointments_reschedule.dart';
import 'package:guoco_priv_app/src/ui/screens/new_home_screen.dart';
import 'package:guoco_priv_app/src/ui/screens/profile_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/drawer_guoco.dart';
import 'package:guoco_priv_app/src/ui/widgets/estate_bottom_navigation_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/estate_list_view.dart';
import 'package:guoco_priv_app/src/ui/widgets/estate_tower_block.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_list_view.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/urls.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';

class EstatePage extends StatefulWidget {
  @override
  _EstatePageState createState() => _EstatePageState();
}

class _EstatePageState extends State<EstatePage> {
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();

  String tabValue;
  int _current = 0;
  double position;
  String title;
  String unitId;
  bool data = false;
  bool keyCollectionModuleEnabled = true;
  bool keyCollectionPaymentStatus = false;
  bool loading = false;
  EstateBloc estateBloc = EstateBloc();
  List<String> imgList = [];
  List<String> happeningsImages = [
    "assets/images/estate_page_image.svg",
    "assets/images/key_collection_happenings.svg",
    "assets/images/falutreport_happenings.svg"
  ];
  List<String> broadcastImages = [
    "assets/images/key_collection_broadcast.svg",
    "assets/images/smart_home_broadcasts.svg"
  ];

  List<String> happeningsText1 = [
    "EXCITING OFFERS",
    "SMART HOME",
    "FAULT REPORTING"
  ];
  List<String> happeningsText2 = ["COMING SOON", "CONVENIENCE", "UPON MOVE-IN"];

  List<String> broadcastsText1 = [
    "KEY COLLECTION",
    "BOOK FACILITIES",
  ];
  List<String> broadcastsText2 = [
    "AVAILABLE NOW",
    "AVAILABLE NOW",
  ];

  // List<String> dummyImgList = ["assets/images/estate_bg.png"];

  // void getImageList(int index, String imgId, String condoId) {
  //   for (int i = 0; i <= index; i++) {
  //     imgList.add(
  //         "http://158.69.138.44:9083/application/api/v1/filemanagement/$condoId/files/view/$imgId");
  //   }
  // }

  void userUnitApiCall(String propertyCode, String tenantId) async {
    await ObjectFactory().appHive.putPropertyCode(propertyCode);
    await ObjectFactory().appHive.putTenantId(tenantId);
    print("asadsa" + ObjectFactory().appHive.getPropertyCode());
    print("asadsdf" + ObjectFactory().appHive.getTenantId());
    estateBloc.getUnitUsers();
  }

  @override
  void initState() {
    tabValue = "happenings";
    estateBloc.getCondosInfo();
    estateBloc.getCondoInfo.listen((event) {
      if (event.statuscode == 200 && event.errorcode == 0) {
        userUnitApiCall(event.data[0].propertycode, event.data[0].id);
        ObjectFactory().appHive.putAddress(event.data[0].address);
        ObjectFactory().appHive.putKeyCollectionVenue(
            event.data[0].address + "," + event.data[0].postalcode);
      }
    });
    estateBloc.checkPaymentStatusResponse.listen((event) {
      setState(() {
        loading = false;
      });
      print("sdsd" + event.data.blockname);
      print("dfdf" + event.data.floorname);
      print("dfdh" + event.data.unitno);
      if (event.statuscode == 200 && event.errorcode == 0) {
        setState(() {
          if (event.data.keycollectionmoduleenabled == true &&
              event.data.keycollectionpaymentstatus == true &&
              event.data.keycollected == false) {
            ObjectFactory().appHive.putKeyCollectionPaymentStatus(
                value: event.data.keycollectionpaymentstatus);
            push(
                context,
                HomeKeyCollection(
                  collectionPaymentStatus:
                      event.data.keycollectionpaymentstatus,
                  // collectionPaymentStatus: ,
                  unitId: unitId,
                  blockName: event.data.blockname,
                  floorAndUnitId:
                      event.data.floorname + " - " + event.data.unitno,
                  imgList: imgList,
                ));
          } else if (event.data.keycollectionpaymentstatus == true &&
              event.data.keycollected == true) {
            print("home post move in" +
                keyCollectionModuleEnabled.toString() +
                keyCollectionPaymentStatus.toString());
            push(
                context,
                HomePostMoveIn(
                  blockName: event.data.blockname,
                  floorAndUnitId:
                      event.data.floorname + " - " + event.data.unitno,
                  imgList: imgList,
                ));
          } else if (event.data.keycollectionpaymentstatus == false) {
            push(
                context,
                KeyCollectionAppointmentsReschedule(
                  collectionPaymentReschedule:
                      event.data.keycollectionpaymentstatus,
                  unitId: unitId,
                  blockName: event.data.blockname,
                  floorAndUnitId:
                      event.data.floorname + " - " + event.data.unitno,
                ));
          }
        });
      }
    });
    super.initState();
  }

  Future<bool> _willPopCallback() async {
    pushAndRemoveUntil(context, NewHomeScreen(), false);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    DotsDecorator(
        activeColor: Colors.red.withOpacity(.50),
        color: Colors.white.withOpacity(.50),
        shape: CircleBorder(),
        size: Size(10, 10));
    return loading == true
        ? Container(
            height: screenHeight(context, dividedBy: 1),
            width: screenWidth(context, dividedBy: 1),
            color: Constants.kitGradients[17],
            child: Center(
              child: CircularProgressIndicator(
                  valueColor: new AlwaysStoppedAnimation<Color>(
                      Constants.kitGradients[1])),
            ),
          )
        : WillPopScope(
            onWillPop: _willPopCallback,
            child: SafeArea(
              top: true,
              child: Scaffold(
                backgroundColor: Constants.kitGradients[17],
                // resizeToAvoidBottomPadding: true,
                key: _globalKey,
                drawer: DrawerGuoco(
                  count: 1,
                ),
                appBar: PreferredSize(
                  preferredSize:
                      Size.fromHeight(screenHeight(context, dividedBy: 10)),
                  child: Container(
                    color: Constants.kitGradients[17],
                    height: screenHeight(context, dividedBy: 10),
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  _globalKey.currentState.openDrawer();
                                },
                                child: Container(
                                  height: screenHeight(context, dividedBy: 17),
                                  width: screenWidth(context, dividedBy: 17),
                                  child: SvgPicture.asset(
                                      "assets/icons/estate_drawer.svg"),
                                ),
                              ),
                              SizedBox(
                                height: screenHeight(context, dividedBy: 70),
                              )
                            ],
                          ),
                          SizedBox(
                            width: screenWidth(context, dividedBy: 70),
                          ),
                          Container(
                              width: screenWidth(context, dividedBy: 3),
                              child:
                                  Image.asset("assets/images/estate_logo.png")),
                          Container(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Container(
                                  width: screenWidth(context, dividedBy: 18),
                                  height: screenHeight(context, dividedBy: 23),
                                  child: SvgPicture.asset(
                                      "assets/icons/notification.svg",
                                      fit: BoxFit.cover),
                                ),
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 30),
                                ),
                                GestureDetector(
                                    onTap: () {
                                      push(context, ProfilePage());
                                    },
                                    child: ObjectFactory()
                                                .appHive
                                                .getUserImage() ==
                                            null
                                        ? Container(
                                            width: screenWidth(context,
                                                dividedBy: 14),
                                            height: screenWidth(context,
                                                dividedBy: 12),
                                            child: SvgPicture.asset(
                                              "assets/icons/person_icon_home.svg",
                                              color: Colors.white,
                                              fit: BoxFit.cover,
                                            ),
                                          )
                                        : Container(
                                            width: screenWidth(context,
                                                dividedBy: 14),
                                            height: screenHeight(context,
                                                dividedBy: 12),
                                            decoration: BoxDecoration(
                                                color: Colors.red,
                                                shape: BoxShape.circle),
                                            child: Image(
                                              image: NetworkImage(
                                                  ObjectFactory()
                                                      .appHive
                                                      .getUserImage()),
                                            ))),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                body: Stack(
                  children: [
                    Container(
                      height: screenHeight(context, dividedBy: 1),
                      child: Column(
                        children: [
                          StreamBuilder<CondoInfoResponse>(
                              stream: estateBloc.getCondoInfo,
                              builder: (context, snapshot) {
                                if (snapshot.hasData) {
                                  for (int i = 0;
                                      i <
                                          snapshot
                                              .data.data[0].mobileimages.length;
                                      i++) {
                                    ObjectFactory().appHive.putPostMoveInImage1(
                                        snapshot
                                            .data.data[0].mobileimages[0].id);
                                    ObjectFactory().appHive.putPostMoveInImage2(
                                        snapshot
                                            .data.data[0].mobileimages[0].id);
                                    ObjectFactory().appHive.putPostMoveInImage3(
                                        snapshot
                                            .data.data[0].mobileimages[0].id);
                                    imgList.add(Urls.baseUrl +
                                        "/application/api/v1/filemanagement/${snapshot.data.data[0].id}/files/view/${snapshot.data.data[0].mobileimages[i].id}");
                                  }
                                }
                                return snapshot.hasData
                                    ? CarouselSlider(
                                        options: CarouselOptions(
                                            aspectRatio: screenWidth(context,
                                                    dividedBy: 1) /
                                                screenHeight(context,
                                                    dividedBy: 4),
                                            viewportFraction: 1,
                                            onPageChanged: (index, reason) {
                                              setState(() {
                                                _current = index;
                                                print(_current);
                                              });
                                            }),
                                        items: imgList.map((i) {
                                          return Builder(
                                            builder: (BuildContext context) {
                                              return Stack(
                                                children: [
                                                  Container(
                                                    width: screenWidth(context,
                                                        dividedBy: 1),
                                                    height: screenHeight(
                                                        context,
                                                        dividedBy: 3.2),
                                                    decoration: BoxDecoration(
                                                        image: DecorationImage(
                                                      image:
                                                          CachedNetworkImageProvider(
                                                        i,
                                                      ),
                                                      fit: BoxFit.fill,
                                                      colorFilter:
                                                          new ColorFilter.mode(
                                                              Colors.black
                                                                  .withOpacity(
                                                                      0.40),
                                                              BlendMode.darken),
                                                    )),
                                                    child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                          child: Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [],
                                                          ),
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                  .all(8.0),
                                                          child: Container(
                                                              child: Column(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Text(
                                                                "WELCOME HOME,",
                                                                style: TextStyle(
                                                                    fontSize: screenWidth(
                                                                        context,
                                                                        dividedBy:
                                                                            25),
                                                                    color: Constants
                                                                            .kitGradients[
                                                                        0],
                                                                    fontFamily:
                                                                        "Montserrat-Light",
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w300),
                                                              ),
                                                              Text(
                                                                  ObjectFactory()
                                                                      .appHive
                                                                      .getName(),
                                                                  style: TextStyle(
                                                                      fontSize: screenWidth(
                                                                          context,
                                                                          dividedBy:
                                                                              23),
                                                                      color:
                                                                          Constants.kitGradients[
                                                                              0],
                                                                      fontFamily:
                                                                          "Montserrat-Light",
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .w600)),
                                                            ],
                                                          )),
                                                        )
                                                      ],
                                                    ),
                                                  ),
                                                  if (snapshot.data.data[0]
                                                          .mobileimages.length >
                                                      1)
                                                    Positioned(
                                                      top: screenHeight(context,
                                                          dividedBy: 4.8),
                                                      left: screenWidth(context,
                                                          dividedBy: 2.5),
                                                      child: Row(
                                                        children: [
                                                          DotsIndicator(
                                                            dotsCount: snapshot
                                                                .data
                                                                .data[0]
                                                                .mobileimages
                                                                .length,
                                                            position: _current
                                                                .toDouble(),
                                                            axis:
                                                                Axis.horizontal,
                                                            decorator: DotsDecorator(
                                                                activeColor: Colors
                                                                    .red
                                                                    .withOpacity(
                                                                        .40),
                                                                color: Colors
                                                                    .white
                                                                    .withOpacity(
                                                                        .40),
                                                                shape:
                                                                    CircleBorder(),
                                                                size: Size(
                                                                    10, 10)),
                                                          ),
                                                        ],
                                                      ),
                                                    )
                                                ],
                                              );
                                            },
                                          );
                                        }).toList(),
                                      )
                                    : Shimmer.fromColors(
                                        highlightColor: Colors.grey,
                                        baseColor: Colors.black38,
                                        child: Container(
                                          height: screenHeight(context,
                                              dividedBy: 3.2),
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          decoration: BoxDecoration(
                                              color: Colors.black54),
                                        ),
                                      );
                              }),
                          StreamBuilder<CondoInfoResponse>(
                              stream: estateBloc.getCondoInfo,
                              builder: (context, snapshot) {
                                return snapshot.hasData
                                    ? EstateTowerBlock(
                                        title:
                                            snapshot.data.data[0].propertyname,
                                        child: StreamBuilder<UnitUsersResponse>(
                                            stream: estateBloc.getUnitUser,
                                            builder: (context, snapshot) {
                                              return snapshot.hasData
                                                  ? Padding(
                                                      padding: EdgeInsets.only(
                                                          left: screenWidth(
                                                              context,
                                                              dividedBy: 40)),
                                                      child: ListView.builder(
                                                          // physics:
                                                          //     ClampingScrollPhysics(),
                                                          shrinkWrap: true,
                                                          scrollDirection:
                                                              Axis.horizontal,
                                                          itemCount: snapshot
                                                              .data.data.length,
                                                          itemBuilder:
                                                              (BuildContext
                                                                      context,
                                                                  int index) {
                                                            // print("data length");
                                                            return TowerListTile(
                                                              keyCollected: snapshot
                                                                  .data
                                                                  .data[index]
                                                                  .keycollected,
                                                              name: snapshot
                                                                  .data
                                                                  .data[index]
                                                                  .blockname,
                                                              unitNo: snapshot
                                                                  .data
                                                                  .data[index]
                                                                  .unitno,
                                                              floorNo: snapshot
                                                                  .data
                                                                  .data[index]
                                                                  .floorname,
                                                              onPressed:
                                                                  () async {
                                                                setState(() {
                                                                  loading =
                                                                      true;
                                                                  unitId = snapshot
                                                                      .data
                                                                      .data[
                                                                          index]
                                                                      .id;
                                                                  ObjectFactory()
                                                                      .appHive
                                                                      .putUnitInfoId(snapshot
                                                                          .data
                                                                          .data[
                                                                              index]
                                                                          .id);
                                                                });

                                                                await estateBloc.checkPaymentStatus(
                                                                    unitId: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .id,
                                                                    blockId: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .blockinfoId,
                                                                    condoInfoId: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .condoinfoId,
                                                                    floorId: snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .floorinfoId);
                                                                ObjectFactory()
                                                                    .appHive
                                                                    .putCondoInfoId(snapshot
                                                                        .data
                                                                        .data[
                                                                            index]
                                                                        .condoinfoId);
                                                              },
                                                            );
                                                          }),
                                                    )
                                                  : Shimmer.fromColors(
                                                      highlightColor:
                                                          Colors.grey,
                                                      baseColor: Colors.black38,
                                                      child: Container(
                                                        height: screenHeight(
                                                            context,
                                                            dividedBy: 3.1),
                                                        width: screenWidth(
                                                            context,
                                                            dividedBy: 1),
                                                        decoration:
                                                            BoxDecoration(
                                                                color: Colors
                                                                    .black54),
                                                      ),
                                                    );
                                            }),
                                        shadowBackGroundHeight: data,
                                      )
                                    : Shimmer.fromColors(
                                        highlightColor: Colors.grey,
                                        baseColor: Colors.black38,
                                        child: Container(
                                          height: screenHeight(context,
                                              dividedBy: 6),
                                          width: screenWidth(context,
                                              dividedBy: 1),
                                          decoration: BoxDecoration(
                                              color: Colors.black54),
                                        ),
                                      );
                              }),
                          Container(
                            width: screenWidth(context, dividedBy: 1),
                            height: screenHeight(context, dividedBy: 15),
                            child: Row(
                              children: [
                                GestureDetector(
                                  child: Container(
                                    width: screenWidth(context, dividedBy: 2),
                                    height:
                                        screenHeight(context, dividedBy: 15),
                                    color: tabValue == "broadcasts"
                                        ? Constants.kitGradients[29]
                                            .withOpacity(0.85)
                                        : Constants.kitGradients[17],
                                    child: Center(
                                        child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "HAPPENINGS",
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: Constants.kitGradients[0],
                                              fontFamily: "SFProText-Medium",
                                              fontWeight: FontWeight.w600),
                                        ),
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 100),
                                        ),
                                        tabValue == "happenings"
                                            ? Container(
                                                color:
                                                    Constants.kitGradients[11],
                                                height: screenHeight(context,
                                                    dividedBy: 300),
                                                width: screenWidth(context,
                                                    dividedBy: 8),
                                              )
                                            : Container(),
                                      ],
                                    )),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      tabValue = "happenings";
                                    });
                                  },
                                ),
                                GestureDetector(
                                  child: Container(
                                    width: screenWidth(context, dividedBy: 2),
                                    height:
                                        screenHeight(context, dividedBy: 15),
                                    color: tabValue == "broadcasts"
                                        ? Constants.kitGradients[29]
                                            .withOpacity(0.85)
                                        : Constants.kitGradients[17],
                                    child: Center(
                                        child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          "BROADCASTS",
                                          style: TextStyle(
                                              fontSize: 13,
                                              color: Constants.kitGradients[0],
                                              fontFamily: "SFProText-Medium",
                                              fontWeight: FontWeight.w600),
                                        ),
                                        SizedBox(
                                          height: screenHeight(context,
                                              dividedBy: 100),
                                        ),
                                        tabValue == "broadcasts"
                                            ? Container(
                                                color:
                                                    Constants.kitGradients[11],
                                                height: screenHeight(context,
                                                    dividedBy: 300),
                                                width: screenWidth(context,
                                                    dividedBy: 8),
                                              )
                                            : Container(),
                                      ],
                                    )),
                                  ),
                                  onTap: () {
                                    setState(() {
                                      tabValue = "broadcasts";
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                          if (tabValue == "happenings")
                            Expanded(
                              child: ListView.builder(
                                  itemCount: happeningsImages.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return EstateListView(
                                      image: happeningsImages[index],
                                      headText: happeningsText1[index],
                                      subText: happeningsText2[index],
                                    );
                                  }),
                            ),
                          if (tabValue == "broadcasts")
                            Expanded(
                              child: ListView.builder(
                                  itemCount: broadcastImages.length,
                                  scrollDirection: Axis.horizontal,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return EstateListView(
                                      image: broadcastImages[index],
                                      headText: broadcastsText1[index],
                                      subText: broadcastsText2[index],
                                    );
                                  }),
                            ),
                          Container(
                            color: Constants.kitGradients[17],
                            width: screenWidth(context, dividedBy: 1),
                            height: screenHeight(context, dividedBy: 13),
                          ),
                        ],
                      ),
                    ),
                    // Positioned(
                    //   top: screenHeight(context, dividedBy: 3.2),
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.center,
                    //     children: imgList.map((image) {
                    //       int index = imgList.indexOf(image);
                    //       return Container(
                    //         width: 8.0,
                    //         height: 8.0,
                    //         margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    //         decoration: BoxDecoration(
                    //           shape: BoxShape.circle,
                    //           color: _current == index ? Colors.blue : Colors.red,
                    //         ),
                    //       );
                    //     }).toList(),
                    //   ),
                    // ),
                    Positioned(
                      bottom: screenHeight(context, dividedBy: 13),
                      child: Container(
                        width: screenWidth(context, dividedBy: 1),
                        height: screenHeight(context, dividedBy: 20),
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topCenter,
                                end: Alignment.bottomCenter,
                                colors: [
                              Colors.black.withOpacity(0),
                              Colors.black.withOpacity(.05),
                              Colors.black.withOpacity(.10),
                              Colors.black.withOpacity(.20),
                              Colors.black.withOpacity(.30),
                              Colors.black.withOpacity(.40),
                              Colors.black.withOpacity(.50),
                              Colors.black.withOpacity(.60),
                              Colors.black.withOpacity(.70)
                            ])),
                      ),
                    ),
                    Positioned(
                        bottom: 0,
                        child: EstateBottomBar(
                          iconLeft: "assets/icons/crown.png",
                          iconRight: "assets/icons/estate_icon.png",
                          titleLeft: "Privilege Club",
                          titleRight: "My Estate",
                          pageValue: "tower",
                        ))
                  ],
                ),
              ),
            ),
          );
  }
}
