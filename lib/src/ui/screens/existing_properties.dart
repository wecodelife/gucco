import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_navigation_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/grid_view.dart';
import 'package:guoco_priv_app/src/ui/widgets/home_tile.dart';
import 'package:guoco_priv_app/src/ui/widgets/search_bar.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ExistingProperties extends StatefulWidget {
  @override
  _ExistingPropertiesState createState() => _ExistingPropertiesState();
}

class _ExistingPropertiesState extends State<ExistingProperties> {

  int count=1;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/bg_blackgradient.png"),
                  fit: BoxFit.cover),
            ),
            child: ListView(
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 11),
                ),
                Container(
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [
                          Colors.black26,
                          Colors.black26,
                          Colors.transparent
                        ])),
                    child: SearchBar(
                      searchIcon: "assets/icons/search_icon.svg",
                      hintText: "Search Properties",
                    )),

                  SizedBox(
                    height: screenHeight(context,dividedBy: 20),
                  ),

                Padding(
                  padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 30)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [

                      Text("RESIDENTIAL",style: TextStyle(color: Colors.white,fontSize: 18,fontFamily: "MuliSemiBold",),),

                      SizedBox(width: screenWidth(context,dividedBy: 2),),

                      count==1?GestureDetector(

                        onTap: (){
                            setState(() {
                              count=0;
                            });
                        },
                          child: SvgPicture.asset("assets/icons/up_arrow.svg",color:Colors.white,)):GestureDetector(
                           onTap: (){
                             setState(() {
                               count=1;
                             });

                           },
                           child:SvgPicture.asset("assets/icons/down_arrow.svg",color:Colors.white,))
                    ],
                  ),
                ),

                SizedBox(
                  height: screenHeight(context,dividedBy: 100),
                ),


                count==1?CustomGridView(
                  itemCount: 5,
                  tile: HomeTile(width: 2.8,height: 5,isSubHeading: false,likeBar: false,

                    itemDescription:"Workspace Espresso Bar (Guoco Tower)",
                    itemImage: "assets/images/card_image.png",
                    boxHeight: 3.2,
                    borderRadius: 6,
                  ),aspectoRatio: 2/2,




                ):Container(),


                SizedBox(
                  height: screenWidth(context,dividedBy: 20),
                ),


                Divider(
                  thickness: 2,
                  color: Constants.kitGradients[1],
                ),

                SizedBox(height: screenHeight(context,dividedBy: 40),),



                Padding(
                  padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 30)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [

                      Text("COMMERCIAL",style: TextStyle(color: Colors.white,fontSize: 18,fontFamily: "MuliSemiBold",),),

                      SizedBox(width: screenWidth(context,dividedBy: 2),),

                      count==2?GestureDetector(

                          onTap: (){
                            setState(() {
                              count=0;
                            });
                          },
                          child: SvgPicture.asset("assets/icons/up_arrow.svg",color:Colors.white,)):GestureDetector(
                          onTap: (){
                            setState(() {
                              count=2;
                            });

                          },
                          child:SvgPicture.asset("assets/icons/down_arrow.svg",color:Colors.white,))
                    ],
                  ),
                ),


                count==2?CustomGridView(
                  itemCount: 5,
                  tile: HomeTile(width: 2.8,height: 5,isSubHeading: false,likeBar: false,

                    itemDescription:"Workspace Espresso Bar (Guoco Tower)",
                    itemImage: "assets/images/card_image.png",
                    boxHeight: 3.2,
                    borderRadius: 6,
                  ),aspectoRatio: 2/2,




                ):Container(),



                SizedBox(
                  height: screenWidth(context,dividedBy: 20),
                ),


                Divider(
                  thickness: 2,
                  color: Constants.kitGradients[1],
                ),

                SizedBox(height: screenHeight(context,dividedBy: 40),),



                Padding(
                  padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 30)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [

                      Text("INTEGRATED DEVELOPMENTS",style: TextStyle(color: Colors.white,fontSize: 18,fontFamily: "MuliSemiBold",),),

                      SizedBox(width: screenWidth(context,dividedBy: 7),),

                      count==3?GestureDetector(

                          onTap: (){
                            setState(() {
                              count=0;
                            });
                          },
                          child: SvgPicture.asset("assets/icons/up_arrow.svg",color:Colors.white,)):GestureDetector(
                          onTap: (){
                            setState(() {
                              count=3;
                            });

                          },
                          child:SvgPicture.asset("assets/icons/down_arrow.svg",color:Colors.white,))
                    ],
                  ),
                ),



             count==3?CustomGridView(
               itemCount: 5,
               tile: HomeTile(width: 2.8,height: 5,isSubHeading: false,likeBar: false,

                 itemDescription:"Workspace Espresso Bar (Guoco Tower)",
                 itemImage: "assets/images/card_image.png",
                 boxHeight: 3.2,
                 borderRadius: 6,
               ),aspectoRatio: 2/2,




             ):Container(),


                SizedBox(
                  height: screenHeight(context,dividedBy: 20),
                )









              ],
            ),
          ),
          Container(
            height: screenHeight(context, dividedBy: 11),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                  Colors.black26,
                  Colors.black26,
                  Colors.black26,
                  Colors.black26,
                  Colors.black12,
                ])),
          ),




          Column(
            children: [
              Container(
                height: screenHeight(context, dividedBy: 11),
                decoration: BoxDecoration(
                    color: Colors.black.withOpacity(0.2),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.black12,
                      offset: Offset(8, 8),
                      blurRadius: 2,spreadRadius: 3
                    )

                  ]
                ),
                child: AppBarBookingPage(
                    leftIcon: "assets/icons/arrow_back.png",
                    rightIcon: "assets/icons/bell_icon.png",
                    pageTitle: "EXISTING PROPERTIES",
                    transparent: true),
              ),
            ],
          ),

          Positioned(
              bottom: 0,
              child: CustomBottomNavigationBar(
                value: "home",
              ))
        ],
      )),
    );
  }
}
