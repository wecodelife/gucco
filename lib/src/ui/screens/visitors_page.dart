import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/screens/my_visitors_date_time.dart';
import 'package:guoco_priv_app/src/ui/screens/visitors_list.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/booking_calender_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/event_info.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class MyVisitorsPage extends StatefulWidget {
  @override
  _MyVisitorsPageState createState() => _MyVisitorsPageState();
}

class _MyVisitorsPageState extends State<MyVisitorsPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: true,
      child: Scaffold(
          resizeToAvoidBottomPadding: true,
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Constants.kitGradients[7],
            leading: Container(),
            actions: [
              AppBarBookingPage(
                rightIcon: "assets/icons/estate_icon.png",
                leftIcon: "assets/icons/arrow_back.png",
                pageTitle: "MY VISITORS",
                onTapRightIcon: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => EstatePage()),
                      (route) => false);
                },
                onTapLeftIcon: () {
                  Navigator.pop(context);
                },
              )
            ],
          ),
          body: ListView(
            scrollDirection: Axis.vertical,
            children: [
              BookingCalenderPage(
                towerName: "Tower 01",
                towerSubName: "04 - 01",
                heading: "All Visitors",
                rightIcon: true,
                icon: "assets/icons/keyCollectionIcon.png",
                bookListTap: () {
                  push(context, VisitorsList());
                },
              ),
              EventInfo(
                subHeading: true,
                eventHeading: "John Smith",
                eventSubHeading: "Service Provider",
                eventDate: "03-April-2020",
                eventTime: "10:00 AM -12:30 AM",
                enable: false,
                onTapCloseButtonOnly: () {
                  actionAlertBox(
                    questionAlert: "Do you wish to proceed?",
                    cancelAlert: 2,
                    context: context,
                    contentNum: 5,
                    msg: "Cancel Invitation",
                    heading1: "Visitor Name:",
                    content1: "John Smith",
                    heading2: "RelationShip:",
                    content2: "Service Provider",
                    heading3: "Date:",
                    content3: "03-April-2020",
                    heading4: "From Time:",
                    content4: "10:30 AM",
                    heading5: "To Time:",
                    content5: "12:30 PM",
                  );
                },
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 9),
              ),
              Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 10),
                  ),
                  BuildButtonIcon(
                    faultReport: true,
                    leftIcon: true,
                    arrowIcon: true,
                    icon: "assets/icons/user.png",
                    title: "Invite Visitor",
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MyVistorsDateTime()));
                    },
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 29),
              ),
              BottomBar(),
            ],
          )),
    );
  }
}
