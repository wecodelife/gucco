import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/my_visitors_date_time.dart';
import 'package:guoco_priv_app/src/ui/screens/visitors_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/list_view_list_booking.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class VisitorsList extends StatefulWidget {
  @override
  _VisitorsListState createState() => _VisitorsListState();
}

class _VisitorsListState extends State<VisitorsList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[7],
        leading: Container(),
        actions: [
          AppBarBookingPage(
            rightIcon: "assets/icons/estate_icon.png",
            leftIcon: "assets/icons/facilitiesmenu.png",
            pageTitle: "MY VISITORS",
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              TowerName(
                towerName: "Tower01",
                towerSubName: "01-01",
              ),

              AppBarRightIcon(
                pageTitle: "All Visitors",
                boxWidth: 6,
                icon: "assets/icons/switchviewcalendar.png",
                rightIcon: true,
                bookedListTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MyVisitorsPage()));
                },
              ),
              ListViewListBookings(
                evenDate: "30-April-2020",
                visitor: "John Smith",
                relation: "Service Provider",
                eventStartTime: "10:00 AM -12:00 AM",
                visitorList: true,
              ),
              // ListViewReschedule(
              //   title: "Function Room",
              //   eventTime: "10:00 AM",
              //   evenDate: "30-April-2020",
              // )
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              BuildButtonIcon(
                faultReport: true,
                title: "Invite Visitor",
                leftIcon: true,
                arrowIcon: true,
                icon: "assets/icons/user.png",
                onPressed: () {
                  push(context, MyVistorsDateTime());
                },
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 30),
              ),
              BottomBar()
            ],
          )
        ],
      ),
    );
  }
}
