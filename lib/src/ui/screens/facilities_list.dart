import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/estate_bloc.dart';
import 'package:guoco_priv_app/src/models/getAllFacilitiesInACondoMiniumResponseModel.dart';
import 'package:guoco_priv_app/src/ui/screens/facility_booking.dart';
import 'package:guoco_priv_app/src/ui/screens/home_post_move_in.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/facilities_list_widget.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/urls.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

import 'martin_modern/estate_page.dart';

class FacilitiesList extends StatefulWidget {
  final String blockName;
  final String floorAndUnitId;
  FacilitiesList({
    this.floorAndUnitId,
    this.blockName,
  });
  @override
  _FacilitiesListState createState() => _FacilitiesListState();
}

class _FacilitiesListState extends State<FacilitiesList> {
  bool showDescription = false;
  EstateBloc estateBloc = new EstateBloc();
  String facilitiesInfoId;
  String rules;
  @override
  void initState() {
    estateBloc.getAllFacilitiesInACondominium(
        condoInfoId: ObjectFactory().appHive.getCondoInfoId());

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Constants.kitGradients[7],
        leading: Container(),
        actions: [
          AppBarBookingPage(
            pageTitle: 'FACILITIES',
            rightIcon: "assets/icons/estate_icon.png",
            leftIcon: "assets/icons/arrow_back.png",
            onTapLeftIcon: () {
              push(
                  context,
                  HomePostMoveIn(
                    blockName: widget.blockName,
                    floorAndUnitId: widget.floorAndUnitId,
                    condInfoId: ObjectFactory().appHive.getCondoInfoId(),
                  ));
            },
            onTapRightIcon: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => EstatePage()),
                  (route) => false);
            },
          ),
        ],
      ),
      backgroundColor: Constants.kitGradients[0],
      body: Column(
        children: [
          TowerName(
            towerName: widget.blockName,
            towerSubName: widget.floorAndUnitId,
          ),
          AppBarRightIcon(
            boxWidth: screenWidth(context, dividedBy: 1),
            pageTitle: 'New booking',
          ),
          Expanded(
            child: Stack(
              children: [
                StreamBuilder<GetAllFacilitiesInACondoMiniumResponse>(
                    stream: estateBloc.getAllFacilitiesInACondominiumResponse,
                    builder: (context, snapshot) {
                      return snapshot.hasData
                          ? ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemCount: snapshot.data.data.length,
                              itemBuilder: (BuildContext context, int index) {
                                return FacilitiesListWidget(
                                    showDescription: showDescription,
                                    onPressed: () {
                                      facilitiesInfoId =
                                          snapshot.data.data[index].id;
                                      ObjectFactory()
                                          .appHive
                                          .putTermsAndCondition(snapshot.data
                                              .data[index].termsandcondition);

                                      push(
                                          context,
                                          FacilitiesBooking(
                                            floorAndUnitId:
                                                widget.floorAndUnitId,
                                            blockName: widget.blockName,
                                            facilitiesInfoId: facilitiesInfoId,
                                            facilityName: snapshot
                                                .data.data[index].facilityname,
                                          ));
                                    },
                                    description:
                                        snapshot.data.data[index].description,
                                    title:
                                        snapshot.data.data[index].facilityname,
                                    imagePath: Urls.baseUrl +
                                        Urls.getImagesFacilitiesInACondminium +
                                        ObjectFactory()
                                            .appHive
                                            .getCondoInfoId() +
                                        "/files/view/" +
                                        snapshot
                                            .data.data[index].webimages[0].id);
                              },
                            )
                          : Container(
                              width: screenWidth(context, dividedBy: 1.2),
                              height: screenHeight(context, dividedBy: 1.4),
                              child: Center(
                                child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      Constants.kitGradients[1]),
                                ),
                              ),
                            );
                    }),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
