import 'package:advance_pdf_viewer/advance_pdf_viewer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/drawer_guoco.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ByLaws extends StatefulWidget {
  @override
  _ByLawsState createState() => _ByLawsState();
}

class _ByLawsState extends State<ByLaws> {
  final GlobalKey<ScaffoldState> _globalKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<ScaffoldState> _globalKeyPdf = new GlobalKey<ScaffoldState>();
  PDFDocument doc;
  int _number = 1;
  bool isLoading = true;
  void loadPdf() async {
    doc = await PDFDocument.fromURL(
        'http://www.africau.edu/images/default/sample.pdf');
    PDFPage pageOne = await doc.get(page: _number);
    setState(() {
      isLoading = false;
    });
  }

  @override
  void initState() {
    setState(() {
      isLoading = true;
    });
    loadPdf();

    // TODO: implement initState
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      drawer: DrawerGuoco(
        count: 6,
      ),
      appBar: AppBar(
        backgroundColor: Colors.black87,
        centerTitle: true,
        leading: GestureDetector(
          onTap: () {
            _globalKey.currentState.openDrawer();
          },
          child: Container(
            child: Icon(Icons.menu_rounded),
          ),
        ),
        title: Text("RESIDENT'S HANDBOOK"),
      ),
      body: Container(
        height: screenHeight(context, dividedBy: 1),
        width: screenHeight(context, dividedBy: 1),
        child: isLoading == false
            ? PDFViewer(
                document: doc,
                pickerButtonColor: Constants.kitGradients[1],
                progressIndicator: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                        Constants.kitGradients[1])),
              )
            : Center(
                child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(
                        Constants.kitGradients[1])),
              ),
      ),
    );
    // Container(
    //   color: Constants.kitGradients[0],
    //   width: screenWidth(context, dividedBy: 1.1),
    //   height: screenHeight(context, dividedBy: 1.17),
    //   child: ListView(
    //     shrinkWrap: true,
    //     children: [
    //       SizedBox(
    //         height: screenHeight(context, dividedBy: 20),
    //       ),
    //       Text("Martin Modern By-Laws",
    //           style: TextStyle(
    //               fontSize: 18,
    //               fontFamily: "SFProTextSemiBold",
    //               fontWeight: FontWeight.w600)),
    //       SizedBox(
    //         height: screenHeight(context, dividedBy: 20),
    //       ),
    //       Container(
    //         width: screenWidth(context, dividedBy: 1.1),
    //         child: Text(
    //             "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec a dolor mattis, pellentesque dui at, mattis tellus. Proin quis lacus sollicitudin, ultrices justo eget, placerat purus. Aenean ut enim in tellus pharetra congue. Donec ultrices euismod volutpat. Suspendisse finibus sem massa, quis ullamcorper dui luctus in. Suspendisse potenti. Aliquam erat volutpat.",
    //             style: TextStyle(
    //                 fontSize: 16,
    //                 fontFamily: "SFProText-Regular",
    //                 fontWeight: FontWeight.w400)),
    //       ),
    //       SizedBox(
    //         height: screenHeight(context, dividedBy: 20),
    //       ),
    //       Container(
    //         width: screenWidth(context, dividedBy: 1.1),
    //         child: Text(
    //             "Sed imperdiet tincidunt mauris, sed efficitur neque viverra et. In hac habitasse platea dictumst. Etiam scelerisque cursus dui non ultrices. Sed quis accumsan sapien. Maecenas consequat neque varius diam varius, at consequat nisl venenatis. Mauris sit amet arcu tincidunt, mollis elit pulvinar, aliquet purus. Suspendisse venenatis erat porta augue ultricies, eu imperdiet arcu posuere. Nam vehicula tortor eu enim convallis, fermentum molestie nisl luctus. Integer sed imperdiet elit, nec eleifend magna. Nunc accumsan, ante et sodales fringilla, diam sem eleifend odio, gravida aliquet orci arcu sit amet erat. Phasellus feugiat, magna quis aliquam eleifend, dui sapien fringilla sapien, a commodo nulla nisi nec dui. In quis euismod felis. Maecenas sed justo ac quam scelerisque ultrices nec a sem.",
    //             style: TextStyle(
    //                 fontSize: 16,
    //                 fontFamily: "SFProText-Regular",
    //                 fontWeight: FontWeight.w400)),
    //       ),
    //       SizedBox(
    //         height: screenHeight(context, dividedBy: 30),
    //       ),
    //     ],
    //   ),
    // ),
  }
}
