import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/fault_reporting_page.dart';
import 'package:guoco_priv_app/src/ui/screens/home_post_move_in.dart';
import 'package:guoco_priv_app/src/ui/widgets/Image%20_uploads.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/appbar_booking_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_bar.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/ui/widgets/coment_box.dart';
import 'package:guoco_priv_app/src/ui/widgets/drop_down%20form%20feild.dart';
import 'package:guoco_priv_app/src/ui/widgets/form_field_report_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FaultReportingNewReport extends StatefulWidget {
  @override
  _FaultReportingNewReportState createState() =>
      _FaultReportingNewReportState();
}

class _FaultReportingNewReportState extends State<FaultReportingNewReport> {
  List<String> reportTopics = ["Room", "Kitchen", "Wash"];
  String reportTopicSelected;
  TextEditingController locationTextEditingController =
      new TextEditingController();
  TextEditingController commentTextEditingController =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          leading: Container(),
          actions: [
            AppBarBookingPage(
              pageTitle: "Fault Reporting",
              rightIcon: "assets/icons/estate_icon.png",
              transparent: false,
              leftIcon: "assets/icons/arrow_back.png",
              onTapRightIcon: () {
                push(context, HomePostMoveIn());
              },
              onTapLeftIcon: () {
                Navigator.pop(context);
              },
            )
          ],
        ),
        body: ListView(
          children: [
            TowerName(
              towerName: "Tower 01",
              towerSubName: "01 - 01",
            ),
            AppBarRightIcon(
              pageTitle: "New Report",
              boxWidth: 30,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 18),
                ),
                Text(
                  "Fault Details",
                  style: TextStyle(
                      color: Constants.kitGradients[11],
                      fontFamily: 'SFProDisplay-Bold',
                      fontSize: 20),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 80),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    Text(
                      "Reporting About ",
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'SFProTextRegular',
                          fontSize: 18),
                    ),
                    Text(
                      "*",
                      style: TextStyle(
                          color: Constants.kitGradients[11],
                          fontFamily: 'SFProText-Regular',
                          fontSize: 18),
                    ),
                  ],
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 2.2),
                )
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 50),
            ),
            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 16),
                ),
                DropDownFormField(
                  dropDownList: reportTopics,
                  onClicked: (value) {
                    setState(() {
                      reportTopicSelected = value;
                    });
                  },
                  dropDownValue: reportTopicSelected,
                  boxWidth: 1.16,
                  underline: true,
                  boxHeight: 14,
                  fontFamily: 'SFProText-Regular',
                ),
              ],
            ),
            FormFieldReportPage(
              textEditingController: locationTextEditingController,
              title: "Location",
              inviteVisitors: false,
              boxWidth: 14,
              sizedBoxWidth: 15,
            ),
            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 15),
                ),
                CommentBox(
                  commentTextEditingController: commentTextEditingController,
                  boxWidth: 1.17,
                  boxHeight: 7,
                  height: 40,
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 100),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 18),
                ),
                Text(
                  "Upload Photo",
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'SFProText-Regular',
                      fontSize: screenWidth(context, dividedBy: 25)),
                ),
                Text(
                  "(Please limit image size to less than 4MB)",
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: 'SFProText-Regular',
                      fontSize: screenWidth(context, dividedBy: 31)),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                )
              ],
            ),
            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 20),
                ),
                ImageUploads(
                  onChanged: (value) {},
                )
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 40),
            ),
            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 10),
                ),
                BuildButton(
                  title: "Submit Fault Report",
                  onPressed: () {
                    push(
                      context,
                      FaultReport(),
                    );
                  },
                  disabled: true,
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 58),
            ),
            BottomBar()
          ],
        ));
  }
}
