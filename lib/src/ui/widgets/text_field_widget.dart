import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';

class TextFieldWidget extends StatefulWidget {
  final String title;
  final bool readOnly;
  final TextEditingController textEditingController;

  TextFieldWidget({
    this.title,
    this.textEditingController,
    this.readOnly,
  });
  @override
  _TextFieldWidgetState createState() => _TextFieldWidgetState();
}

class _TextFieldWidgetState extends State<TextFieldWidget> {
  // bool showCursor = true;
  @override
  void initState() {
    // showCursor = !widget.readOnly;
    // print("value " + showCursor.toString());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // SizedBox(height: screenHeight(context,dividedBy: 30),),
        TextField(
          // enableInteractiveSelection: !widget.readOnly,
          readOnly: widget.readOnly ?? false,
          decoration: InputDecoration(
            labelText: widget.title,
            labelStyle: TextStyle(
                fontFamily: 'SFProText-Regular',
                fontSize: 15,
                color: Constants.kitGradients[7]),
          ),
          controller: widget.textEditingController,
        )
      ],
    );
  }
}
