import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class SearchBox extends StatefulWidget {
  TextEditingController textEditingController =new TextEditingController();
  String hintText;
  SearchBox({this.textEditingController,this.hintText});
  @override
  _SearchBoxState createState() => _SearchBoxState();
}

class _SearchBoxState extends State<SearchBox> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.textEditingController,
      autofocus: false,
      style: TextStyle(
        color: Colors.white70,
        fontFamily: 'MuliSemiBold',
        fontSize:20,),
      cursorColor: Colors.white,
      decoration: InputDecoration(
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
        ),
        hintText: widget.hintText,
        hintStyle: TextStyle(
          color: Colors.white24,
          fontFamily: 'MuliSemiBold',
          fontSize: 18,

        ),

      ),
    );;
  }
}
