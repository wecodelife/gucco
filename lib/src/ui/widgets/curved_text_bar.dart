import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class CurvedTextBar extends StatefulWidget {
  final String label;
  String blockName;
  CurvedTextBar({this.label, this.blockName});
  @override
  _CurvedTextBarState createState() => _CurvedTextBarState();
}

class _CurvedTextBarState extends State<CurvedTextBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 30),
      width: screenWidth(context, dividedBy: 2.9),
      decoration: BoxDecoration(
          color: Constants.kitGradients[31],
          borderRadius: BorderRadius.circular(10)),
      child: Center(
          child: Text(
        widget.label + ",  " + widget.blockName,
        style: TextStyle(
            fontSize: 11,
            fontWeight: FontWeight.w600,
            color: Constants.kitGradients[0],
            fontFamily: "MontserratRegular"),
      )),
    );
  }
}
