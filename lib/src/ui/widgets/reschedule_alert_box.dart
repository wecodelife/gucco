import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/back_button.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:intl/intl.dart';

class RescheduleBox extends StatefulWidget {
  String title;
  bool questionAlertNotBold;
  String questionAlert;
  Function onPressed;
  DateTime date;
  String earliestDate;
  bool isLoading = false;
  String earliestStartTime;
  String earliestEndTime;
  String selectedEndTime;
  String selectedStartTime;
  RescheduleBox(
      {this.title,
      this.questionAlert,
      this.onPressed,
      this.date,
      this.earliestDate,
      this.questionAlertNotBold,
      this.isLoading,
      this.earliestStartTime,
      this.earliestEndTime,
      this.selectedEndTime,
      this.selectedStartTime});

  @override
  _RescheduleBoxState createState() => _RescheduleBoxState();
}

class _RescheduleBoxState extends State<RescheduleBox> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        insetPadding: EdgeInsets.symmetric(
            vertical: screenHeight(context, dividedBy: 3.6)),
        titlePadding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 40),
        ),
        contentPadding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 40)),
        title: Container(
            height: screenHeight(context, dividedBy: 18),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.vertical(top: Radius.circular(10))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 100),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(widget.title),
                  ],
                ),
              ],
            )),
        content: Container(
          height: screenHeight(context, dividedBy: 3),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(10))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              // Text("Time Slot Notice"),

              Text("The earliest available date is "),

              Text(widget.earliestDate),
              Text("You have selected "),
              Text(new DateFormat('dd-MM-yyyy')
                      .format(widget.date ?? DateTime.now()) +
                  ", " +
                  time(widget.selectedStartTime)),
              widget.questionAlertNotBold == false
                  ? Text(
                      widget.questionAlert,
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: 'SFProText-Regular',
                          color: Colors.black,
                          fontWeight: FontWeight.w600),
                    )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        // SizedBox(width: screenWidth(context,dividedBy: 20),),
                        RichText(
                          text: TextSpan(
                            text: widget.questionAlert,
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'SFProText-Regular',
                                color: Colors.black,
                                fontWeight: FontWeight.w200),
                          ),
                        ),
                      ],
                    ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              )
            ],
          ),
        ),
        backgroundColor: Colors.transparent,
        actions: [
          Container(
            height: screenHeight(context, dividedBy: 10),
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ButtonAlertBox(
                  title: "No ",
                  onTap: () {
                    Navigator.pop(context);
                  },
                  backButton: true,
                  reschedule: false,
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 40),
                ),
                ButtonAlertBox(
                  title: "Yes,Proceed",
                  onTap: () {
                    widget.onPressed();
                  },
                  backButton: false,
                  reschedule: false,
                  isLoading: widget.isLoading,
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 40),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
