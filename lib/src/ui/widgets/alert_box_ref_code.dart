import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ToastRefCode extends StatefulWidget {
  @override
  _ToastRefCodeState createState() => _ToastRefCodeState();
}

class _ToastRefCodeState extends State<ToastRefCode> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context,dividedBy: 1.24),
      height:screenHeight(context,dividedBy: 10),
      decoration: BoxDecoration(
        color: Constants.kitGradients[25],
            borderRadius: BorderRadius.circular(10)
      ),
      child: Column(
        children: [
          SizedBox(
            height: screenHeight(context,dividedBy: 100),
          ),

          Text("This reference code is invalid.",
            style: TextStyle(fontSize: 18,fontFamily: 'Muli',color:Constants.kitGradients[0] ),),

          Text("Please try again.",
            style: TextStyle(fontSize: 18,fontFamily: 'Muli',color:Constants.kitGradients[0] ),),


        ],
      ),

    );
  }
}
