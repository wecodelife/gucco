import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ButtonAlertBox extends StatefulWidget {
  String title;
  Function onTap;
  bool backButton;
  bool reschedule;
  bool isLoading;
  ButtonAlertBox(
      {this.backButton,
      this.title,
      this.onTap,
      this.reschedule,
      this.isLoading});
  @override
  _ButtonAlertBoxState createState() => _ButtonAlertBoxState();
}

class _ButtonAlertBoxState extends State<ButtonAlertBox> {
  @override
  Widget build(BuildContext context) {
    return widget.backButton == false
        ? Container(
            height: screenHeight(context, dividedBy: 21),
            decoration: BoxDecoration(
                color: Colors.red,
                borderRadius: BorderRadius.circular(20),
                boxShadow: [
                  BoxShadow(
                    color: Constants.kitGradients[13],
                    spreadRadius: 0,
                    blurRadius: 10,
                    offset: Offset(-0.4, -0.4),
                  )
                ]),
            child: RaisedButton(
                padding: EdgeInsets.only(),
                onPressed: widget.onTap,
                color: Constants.kitGradients[13],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                    side: BorderSide(
                      color: Constants.kitGradients[13],
                      width: 3,
                    )),
                child: Row(
                  children: [
                    SizedBox(
                      width: widget.reschedule == true
                          ? screenWidth(context, dividedBy: 20)
                          : screenWidth(context, dividedBy: 7),
                    ),
                    widget.isLoading == true
                        ? Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              height: screenHeight(context, dividedBy: 25),
                              width: screenWidth(context, dividedBy: 15),
                              child: CircularProgressIndicator(
                                valueColor: new AlwaysStoppedAnimation<Color>(
                                    Constants.kitGradients[0]),
                              ),
                            ),
                          )
                        : Text(
                            widget.title,
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "SFProText-Regular",
                                fontSize: widget.reschedule == true ? 15 : 18),
                          ),
                    widget.isLoading == false
                        ? Icon(Icons.arrow_right_alt_sharp)
                        : Container(),
                    SizedBox(
                      width: widget.reschedule == true
                          ? screenWidth(context, dividedBy: 20)
                          : screenWidth(context, dividedBy: 7),
                    ),
                  ],
                )),
          )
        : Container(
            height: screenHeight(context, dividedBy: 21),
            child: RaisedButton(
                padding: EdgeInsets.only(),
                onPressed: widget.onTap,
                color: Colors.black.withOpacity(0.6),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                    side: BorderSide(
                      color: Constants.kitGradients[13],
                      width: 3,
                    )),
                child: Row(
                  children: [
                    SizedBox(
                        width: widget.reschedule == true
                            ? screenWidth(context, dividedBy: 20)
                            : screenWidth(context, dividedBy: 10)),
                    Text(
                      widget.title,
                      style: TextStyle(
                          color: Colors.white,
                          fontFamily: "SFProText-Regular",
                          fontSize: widget.reschedule == true ? 15 : 18),
                    ),
                    SizedBox(
                        width: widget.reschedule == true
                            ? screenWidth(context, dividedBy: 20)
                            : screenWidth(context, dividedBy: 10)),
                  ],
                )),
          );
  }
}
