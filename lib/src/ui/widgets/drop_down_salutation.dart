import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:hive/hive.dart';

class DropDownSalutation extends StatefulWidget {
  List<String>dropDownList;
  String title;
  String dropDownValue;
  ValueChanged<String> onClicked;
  DropDownSalutation({this.title,this.dropDownList,this.dropDownValue,this.onClicked});
  @override
  _DropDownSalutationState createState() => _DropDownSalutationState();
}
String dropdownValue;
class _DropDownSalutationState extends State<DropDownSalutation> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context,dividedBy: 10),
      width: screenWidth(context,dividedBy:1.23),
      child: DropdownButton<String>(
        underline: Divider(
          thickness: 1.0,
          color: Constants.kitGradients[0],
        ),
         icon: Row(
           children: [
             SizedBox(
               width: screenWidth(context,dividedBy:5),
             ),
             Icon(Icons.keyboard_arrow_down_rounded,size: 30,color: Constants.kitGradients[0],),
           ],
         ),
        iconSize: 20,
        dropdownColor: Colors.black,
        value: widget.dropDownValue==""?dropdownValue:widget.dropDownValue ,
        style: TextStyle(fontSize: 16, color: Constants.kitGradients[0]),
        items: widget.dropDownList
            .map<DropdownMenuItem<String>>((String value) =>
            DropdownMenuItem<String>(value: value, child: Container(
              width: screenWidth(context,dividedBy: 2),
                child: Text(value))))
            .toList(),
        onChanged: (selectedValue) {
          setState(() {
            widget.dropDownValue = selectedValue;
            print(widget.dropDownValue);
          });
          widget.onClicked(widget.dropDownValue);
        },
        hint: Padding(
          padding:  EdgeInsets.all(0 ),
          child: Text(
            widget.title,
            style: TextStyle(fontSize: 18, color: Constants.kitGradients[0]),
          ),
        ),
      ),
    );
  }
}
