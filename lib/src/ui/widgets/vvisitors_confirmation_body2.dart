import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/action_alert_content.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class VisitorsConfirmationBody2 extends StatefulWidget {
  String content1;
  String content2;
  String content3;
  String content4;
  String content5;
  String content6;
  String content7;
  String content8;
  Function onChanged;
  int counter;
  VisitorsConfirmationBody2({this.content1,this.content2,this.content3,this.content4,this.content5,this.content6,this.content7,this.content8,
    this.onChanged,this.counter});
  @override
  _VisitorsConfirmationBody2State createState() => _VisitorsConfirmationBody2State();
}

class _VisitorsConfirmationBody2State extends State<VisitorsConfirmationBody2> {
  int count;
  @override
  Widget build(BuildContext context) {
    return Column(

      children: [

        SizedBox(
          height: screenHeight(context,dividedBy: 20),
        ),

        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text("Visitor Info",style: TextStyle(color:widget.counter==2?Constants.kitGradients[11]:Colors.black,fontFamily: 'SFProText-SemiBold',fontSize: 20,
                fontWeight:FontWeight.w600 ),),

            SizedBox(width: screenWidth(context,dividedBy:2.2 ),),
            GestureDetector(
                onTap: (){

                },
                child:  widget.counter==2?GestureDetector(
                  onTap: (){
                  count=0;
                  widget.onChanged(count);
                  },
                  child:  SvgPicture.asset(
                    'assets/icons/arrow_upward.svg',
                    width: screenWidth(context, dividedBy: 40),
                    height: screenHeight(context, dividedBy: 40),
                    color: Constants.kitGradients[11],
                  ),
                ):GestureDetector(
                  onTap: (){
                    count=2;
                    widget.onChanged(count);
                  },
                  child:  SvgPicture.asset('assets/icons/arrow_down.svg',
                    width: screenWidth(context, dividedBy: 40),
                    height: screenHeight(context, dividedBy: 40),
                    color: Colors.black,
                  ),
                ),
            ),


          ],
        ),

        widget.counter==2?Column(
          children: [
            SizedBox(
              height: screenHeight(context,dividedBy: 50),
            ),

            AlertBoxContent(
              heading: "Main Visitor",content: widget.content1,
            ),

            SizedBox(
              height: screenHeight(context,dividedBy: 100),
            ),

            AlertBoxContent(
              heading: "Contact No.",content: widget.content2,
            ),








            SizedBox(
              height: screenHeight(context,dividedBy: 100),
            ),

            AlertBoxContent(
              heading:"EmailAddress" ,content: widget.content3,
            ),

            SizedBox(
              height: screenHeight(
                  context,dividedBy: 100),
            ),

            AlertBoxContent(
              heading: "Relationship:",content: widget.content4,
            ),
        SizedBox(
          height: screenHeight(
              context,dividedBy: 40),
        ),

        AlertBoxContent(
          heading: "License Plate:",content: widget.content5,
        ),

        SizedBox(
          height: screenHeight(
              context,dividedBy: 100),
        ),

        AlertBoxContent(
          heading: "No. of Visitors:",content: widget.content6,
        ),

        SizedBox(
          height: screenHeight(
              context,dividedBy: 100),
        ),

        AlertBoxContent(
          heading: "Purpose of Visit:",content: widget.content7,
        ),
        SizedBox(
          height: screenHeight(
              context,dividedBy: 30),
        ),

        AlertBoxContent(
          heading: "Access Required?",content: widget.content8,
        ),

          ],
        ):Container(),

        SizedBox(
          height: screenHeight(context,dividedBy: 20),
        ),
        Padding(
          padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
          child: Divider(
            thickness: 2,
            color: Colors.grey,
          ),
        )

      ],
    );










  }
}
