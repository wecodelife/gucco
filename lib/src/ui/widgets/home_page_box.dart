import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
class HomePageBox extends StatefulWidget {
  final String imageName;
  final String boxTitle;
  final Function onPressed;
  HomePageBox({this.boxTitle,this.imageName,this.onPressed});
  @override
  _HomePageBoxState createState() => _HomePageBoxState();
}

class _HomePageBoxState extends State<HomePageBox> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        height: screenHeight(context,dividedBy: 9),
        width: screenWidth(context,dividedBy: 1.2),
        decoration: BoxDecoration(
            color: Colors.black.withOpacity(0.40),
            borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        child: Center(
          child: GestureDetector(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    child: Image(image: AssetImage(widget.imageName))
                ),
                Text(widget.boxTitle,style: TextStyle(fontSize: 16 , color: Constants.kitGradients[0],fontFamily: "SFProDisplay-Medium"),)
              ],
            ),onTap: widget.onPressed,
          ),
        ),
      ),
    );
  }
}
