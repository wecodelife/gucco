import 'package:flutter/material.dart';

class MyCustomClipper extends CustomClipper<Path> {
  // @override
  // Path getClip(Size size) {
  //   Path path = new Path();
  //   // path start with (0.0, 0.0) point
  //   path.lineTo(size.height,0);
  //   path.lineTo(size.width/2, size.height-100);
  //   path.lineTo(0.0, size.height);
  //   return path;
  // }

  @override
  Path getClip(Size size) {
    var points = [
      Offset(0, 0), // point p1
      Offset(0, size.height), // point p2
      Offset(size.width, size.height), // point p3
      Offset(size.width,0) ,
      Offset(size.width/2, size.height/4), // point p2
// point p4
    ];

    Path path = Path()
      ..addPolygon(points, false)
      ..close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}