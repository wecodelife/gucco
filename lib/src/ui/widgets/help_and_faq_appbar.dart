import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HelpAndFaqAppbar extends StatefulWidget {
 final String title;
 final Widget icon;
 final Function onTap;
  HelpAndFaqAppbar({this.onTap ,this.title, this.icon});
  @override
  _HelpAndFaqAppbarState createState() => _HelpAndFaqAppbarState();
}

class _HelpAndFaqAppbarState extends State<HelpAndFaqAppbar> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        GestureDetector(
          child: Container(
           // color: Colors.red,
            width: screenWidth(context, dividedBy: 8),
            height: screenHeight(context, dividedBy: 20),
            child: widget.icon,
          ),
          onTap: widget.onTap,
        ),
        SizedBox(
          width: screenWidth(context,dividedBy: 12),
        ),
        Container(
         // color: Colors.yellow,
          width: screenWidth(context, dividedBy: 2),
          height: screenHeight(context, dividedBy: 20),
          child: Center(
            child: Text(
              widget.title,
              style: TextStyle(
                  color: Constants.kitGradients[0],
                  fontSize: 16,
                  fontFamily: 'MuliBold'),
            ),
          ),
        ),
      ],
    );
  }
}
