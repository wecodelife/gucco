import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/check_box_list_bookings.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ListViewReschedule extends StatefulWidget {
  final String title;
  final String evenDate;
  final String eventTime;
  final Function onPressedReschedule;
  final Function onPressingCancel;
  final String lastDate;
  final bool isLoading;
  final Function onPressedAlertBoxCancelButton;
  final Function onPressed;
  ListViewReschedule(
      {this.title,
      this.eventTime,
      this.evenDate,
      this.onPressingCancel,
      this.onPressedReschedule,
      this.isLoading,
      this.lastDate,
      this.onPressed,
      this.onPressedAlertBoxCancelButton});
  @override
  _ListViewRescheduleState createState() => _ListViewRescheduleState();
}

class _ListViewRescheduleState extends State<ListViewReschedule> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onPressed,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 30),
              ),
              CheckBoxListBookings(),
              SizedBox(
                width: screenWidth(context, dividedBy: 30),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 30),
                  ),
                  Text(
                    widget.evenDate,
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "SFProText-Regular",
                        fontSize: 18),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 10),
                  ),
                  Text(
                    widget.eventTime,
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "SFProText-Regular",
                        fontSize: 18),
                  ),
                ],
              ),
            ],
          ),
          //     :Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //   children: [
          //
          //
          //     CheckBoxListBookings(),
          //
          //     SizedBox(
          //       width: screenWidth(context,dividedBy: 20),
          //     ),
          //     Text(
          //       widget.title,
          //       style: TextStyle(
          //           color: Colors.black,
          //           fontFamily: "SFProText-Regular",
          //           fontSize: 18),
          //     ),
          //
          //     SizedBox(
          //       width: screenWidth(context,dividedBy: 2.4),
          //     )
          //   ],
          // ),

          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                color: Constants.kitGradients[24],
                width: screenWidth(context, dividedBy: 1.2),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    SizedBox(
                      height: screenHeight(context, dividedBy: 70),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      // mainAxisSize: MainAxisSize.max,
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 40),
                        ),
                        Text(
                          widget.title,
                          style: TextStyle(
                              color: Colors.black,
                              fontFamily: "SFProText-Regular",
                              fontSize: 15),
                        ),

                        // SizedBox(
                        //   width: screenWidth(context,dividedBy: 20),
                        // ),
                        // Container(
                        //   height: screenHeight(context, dividedBy: 30),
                        //   width: screenWidth(context, dividedBy: 200),
                        //   color: Colors.black,
                        // ),
                        //
                        // // SizedBox(
                        // //   width: screenWidth(context,dividedBy: 20),
                        // // ),
                        // Text(
                        //   widget.relation,
                        //   style: TextStyle(
                        //       color: Colors.black,
                        //       fontFamily: "SFProText-Regular",
                        //       fontSize: 15),
                        // ),

                        SizedBox(
                          width: screenWidth(context, dividedBy: 20),
                        )
                      ],
                    ),
                    SizedBox(
                      height: screenWidth(context, dividedBy: 76),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        RaisedButton(
                            onPressed: () {
                              widget.onPressedReschedule();
                            },
                            color: Constants.kitGradients[11],
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(9),
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Image(
                                  image:
                                      AssetImage("assets/icons/edit_icon.png"),
                                  color: Colors.white,
                                ),
                                Center(
                                  child: Text(
                                    "Reschedule",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "SFProText-Regular",
                                        fontSize: 18),
                                  ),
                                ),

                                // SizedBox(
                                //   width: screenWidth(context,dividedBy: 3.7),
                                // ),
                              ],
                            )),
                        RaisedButton(
                            onPressed: () {
                              actionAlertBox(
                                  reduceSize: true,
                                  msg: "Appointment Cancellation",
                                  heading1: "Appointment Date:",
                                  heading2: "Appointment Time:",
                                  content1: widget.evenDate,
                                  content2: widget.eventTime,
                                  cancelBoxTitle: "",
                                  text1: "",
                                  text2: "",
                                  context: context,
                                  contentNum: 2,
                                  questionAlertNotBold: true,
                                  cancelAlert: 5,
                                  questionAlert: " ",
                                  date: "",
                                  cancelBox: false,
                                  isLoading: widget.isLoading,
                                  onPressed: () {
                                    widget.onPressedAlertBoxCancelButton();
                                  });
                            },
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(9),
                                side: BorderSide(color: Colors.black)),
                            child: Row(
                              children: [
                                Center(
                                  child: Text(
                                    "Cancel",
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontFamily: "SFProText-Regular",
                                        fontSize: 18),
                                  ),
                                ),

                                // SizedBox(
                                //   width: screenWidth(context,dividedBy: 3.7),
                                // ),
                              ],
                            )),
                      ],
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 90),
                    ),
                  ],
                ),
              ),
            ],
          ),
          //    :Row(
          //   mainAxisAlignment: MainAxisAlignment.end,
          //   children: [
          //     Container(
          //       color: Constants.kitGradients[24],
          //
          //       child:Column(
          //         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //         children: [
          //           SizedBox(
          //             height: screenHeight(context,dividedBy: 70),
          //           ),
          //           Row(
          //             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          //             children: [
          //
          //               SizedBox(
          //                 width: screenWidth(context,dividedBy: 30),
          //               ),
          //               Text(
          //                 widget.evenDate,
          //                 style: TextStyle(
          //                     color: Colors.black,
          //                     fontFamily: "SFProText-Regular",
          //                     fontSize: 16),
          //               ),
          //
          //               SizedBox(
          //                 width: screenWidth(context,dividedBy: 50),
          //               ),
          //               Container(
          //                 height: screenHeight(context, dividedBy: 30),
          //                 width: screenWidth(context, dividedBy: 200),
          //                 color: Colors.black,
          //               ),
          //
          //               SizedBox(
          //                 width: screenWidth(context,dividedBy: 50),
          //               ),
          //               Text(
          //                 widget.eventTime,
          //                 style: TextStyle(
          //                     color: Colors.black,
          //                     fontFamily: "SFProText-Regular",
          //                     fontSize: 16),
          //               ),
          //
          //               SizedBox(
          //                 width: screenWidth(context, dividedBy: 20),
          //               )
          //             ],
          //           ),
          //
          //           SizedBox(
          //             width: screenWidth(context,dividedBy: 76),
          //           ),
          //           Padding(
          //             padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
          //             child: RaisedButton(
          //
          //                 onPressed: (){},
          //                 color: Colors.white,
          //                 shape: RoundedRectangleBorder(
          //                     borderRadius: BorderRadius.circular(9),
          //                     side: BorderSide(color: Colors.black )
          //                 ),
          //                 child: Row(
          //                   children: [
          //
          //                     SizedBox(
          //                       width: screenWidth(context,dividedBy: 4),
          //                     ),
          //                     Center(
          //                       child: Text("Cancel",
          //                         style: TextStyle(
          //                             color: Colors.black,
          //                             fontFamily: "SFProText-Regular",
          //                             fontSize: 18),
          //                       ),
          //
          //                     ),
          //
          //                     SizedBox(
          //                       width: screenWidth(context,dividedBy: 3.7),
          //                     ),
          //                   ],
          //                 )
          //             ),
          //           ),
          //           SizedBox(
          //             height: screenHeight(context,dividedBy: 90),
          //           ),
          //         ],
          //       ),
          //     ),
          //   ],
          // ),

          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),

          Divider(
            thickness: 2,
          )
        ],
      ),
    );
  }
}
