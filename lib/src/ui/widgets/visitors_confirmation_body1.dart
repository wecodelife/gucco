import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/action_alert_content.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class VisitorsConfirmationBody1 extends StatefulWidget {
  String content1;
  String content2;
  Function onValueChanged;
  int counter;
  VisitorsConfirmationBody1({this.content1,this.content2,this.onValueChanged,this.counter});

  @override
  _VisitorsConfirmationBody1State createState() => _VisitorsConfirmationBody1State();
}

class _VisitorsConfirmationBody1State extends State<VisitorsConfirmationBody1> {
  int count;
  @override
  Widget build(BuildContext context) {
    return Column(

        children: [

          SizedBox(
            height: screenHeight(context,dividedBy: 20),
          ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text("Invite Details",style: TextStyle(color:widget.counter==1?Constants.kitGradients[11]:Colors.black,fontFamily: 'SFProText-SemiBold',fontSize: 20

              ,fontWeight: FontWeight.w600),),

              SizedBox(width: screenWidth(context,dividedBy:2.5 ),),
              GestureDetector(
                onTap: (){

                },
                child:  widget.counter==1?GestureDetector(
                  onTap: (){
                    setState(() {
                      count=0;
                      widget.onValueChanged(count);
                    });
                  },
                  child:  SvgPicture.asset(
                    'assets/icons/arrow_upward.svg',
                    width: screenWidth(context, dividedBy: 40),
                    height: screenHeight(context, dividedBy: 40),
                    color: Constants.kitGradients[11],
                  ),
                ):GestureDetector(
                  onTap: (){
                    setState(() {
                      count=1;
                      widget.onValueChanged(count);
                    });

                  },
                  child:  SvgPicture.asset('assets/icons/arrow_down.svg',
                    width: screenWidth(context, dividedBy: 40),
                    height: screenHeight(context, dividedBy: 40),
                    color: Colors.black,
                  ),
                ),
              ),


            ],
          ),

          SizedBox(
            height: screenHeight(context,dividedBy: 50),
          ),

          widget.counter==1?Column(
            children: [
              AlertBoxContent(
                heading: "Date:",content: widget.content1,
              ),

              SizedBox(
                height: screenHeight(context,dividedBy: 100),
              ),

              AlertBoxContent(
                heading: "Time:",content: widget.content2,
              ),

              SizedBox(
                height: screenHeight(context,dividedBy: 16),
              ),

              Row(
                children: [
                  SizedBox(
                    width: screenWidth(context,dividedBy: 15),
                  ),
                  Text("This invite was created on",style: TextStyle(color:Colors.black,
                    fontFamily: 'SFProText-SemiBold',fontSize: screenWidth(context,dividedBy: 26),),),

                  Text("10-Apr-2020, 10:01AM.",style: TextStyle(color:Colors.black,
                      fontFamily: 'SFProText-SemiBold',fontSize: screenWidth(context,dividedBy: 26),fontWeight: FontWeight.w600),),



                  SizedBox(
                    width: screenWidth(context,dividedBy: 30),
                  ),
                ],
              ),


            ],
          ):Container(),



       SizedBox(
         height: screenHeight(context,dividedBy: 60),
       ),


       Padding(
         padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
         child: Divider(
           thickness: 2,
           color: Colors.grey,
         ),
       )

        ],
    );
  }
}
