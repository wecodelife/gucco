import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/e_application_list_view.dart';
import 'package:guoco_priv_app/src/ui/widgets/e_application_list_item.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';


class EApplicationList extends StatefulWidget {
  String tabValue;
  EApplicationList({
    this.tabValue
});
  @override
  _EApplicationListState createState() => _EApplicationListState();
}

class _EApplicationListState extends State<EApplicationList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context,dividedBy: 1.33),
      color: Constants.kitGradients[0],
      child: SingleChildScrollView(
        child: widget.tabValue=="new application"?Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              width: screenWidth(context,dividedBy: 1.1),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context,dividedBy: 40),
                  ),
                  Text("You may submit the E-Applications forms through this app for approval.",style: TextStyle(fontFamily: "SFProText-Regular",fontSize: 15),),
                  SizedBox(
                    height: screenHeight(context,dividedBy:40),
                  ),
                  RichText(
                    text: TextSpan(
                        text: "You may also ",
                        style: TextStyle(fontFamily: "SFProText-Regular",fontSize: 15,color: Colors.black),
                        children: [
                          TextSpan(
                              text: "click any of the below images to download the PDF ",
                              style:TextStyle(fontFamily: "SFProText-SemiBold",fontSize: 15,fontWeight: FontWeight.w600)
                          ),
                          TextSpan(
                              text: "and submit it directly to the  Martin Modern Management Office."
                          )
                        ]
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context,dividedBy:40),
                  ),
                  Text("T&Cs and fee requirements apply accordingly.",),
                  SizedBox(
                    height: screenHeight(context,dividedBy:50),
                  ),
                ],
              ),
            ),
            EApplicationListView(tabValue: "new application",),
            SizedBox(
              height: screenHeight(context,dividedBy: 10),
            )
          ],
        ):EApplicationListView(),
      ),
    );
  }
}
