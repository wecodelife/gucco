import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:guoco_priv_app/src/utils/utils.dart';

class CustomGridView extends StatefulWidget {
  Widget tile;
  double aspectoRatio;
  int itemCount;

  CustomGridView({this.tile,this.aspectoRatio,this.itemCount,
  });
  @override
  _CustomGridViewState createState() => _CustomGridViewState();
}

class _CustomGridViewState extends State<CustomGridView> {
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: widget.itemCount,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(childAspectRatio: widget.aspectoRatio,
          crossAxisCount: 2),
      itemBuilder: (BuildContext context, int index) {

        return
           widget.tile;
      },
    );
  }
}
