import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/search_box.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class SearchBar extends StatefulWidget {
  String searchIcon;
  String hintText;
  SearchBar({this.searchIcon,this.hintText});
  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  TextEditingController searchTextEditingController= new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return  Row(
      children: [
        SizedBox(
          width: screenWidth(context,dividedBy: 20),
        ),
        Container(
            height: screenHeight(context,dividedBy: 30),
            width: screenWidth(context,dividedBy: 1.6),
            child: SearchBox(hintText: widget.hintText,textEditingController:searchTextEditingController ,)),

        SizedBox(
          width: screenWidth(context,dividedBy: 100),
        ),

        SvgPicture.asset(widget.searchIcon,width: screenWidth(context,dividedBy:17 ),color: Colors.white70, ),

        SizedBox(
          width: screenWidth(context,dividedBy: 100),
        ),

        Text("SEARCH",style: TextStyle(color: Colors.white70,fontFamily:'MuliSemiBold',fontSize: 20),),




      ],
    );
  }
}
