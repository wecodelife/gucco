import 'package:flutter/material.dart';

import 'help_and_faq_body1.dart';

class HelpAndFAQ1ListView extends StatefulWidget {
  final String title;
  final Function onTap;

  HelpAndFAQ1ListView({this.onTap, this.title});

  @override
  _HelpAndFAQ1ListViewState createState() => _HelpAndFAQ1ListViewState();
}

class _HelpAndFAQ1ListViewState extends State<HelpAndFAQ1ListView> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        HelpAndFAQBody1(
          title: widget.title,
          onTap: widget.onTap,
        ),
      ],
    );
  }
}
