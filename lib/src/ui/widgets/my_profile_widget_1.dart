import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';

class MyProfileWidget1 extends StatefulWidget {
  String title1,title2;
  MyProfileWidget1({this.title1,this.title2});

  @override
  _MyProfileWidget1State createState() => _MyProfileWidget1State();
}

class _MyProfileWidget1State extends State<MyProfileWidget1> {
  @override
  Widget build(BuildContext context) {
    return  Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          widget.title1,
          style: TextStyle(
              fontFamily: 'MuliBold',
              fontSize: 16,
              fontWeight: FontWeight.w400,
              color: Constants.kitGradients[0]),
        ),
        Text(
          widget.title2,
          style: TextStyle(
              fontFamily: 'MuliBold',
              fontSize: 16,
              fontWeight: FontWeight.w300,
              color: Constants.kitGradients[0]),
        ),
      ],
    );
  }
}
