import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class OtpMessage extends StatefulWidget {
  String title;
  String text1;
  Widget route;
  bool stayOnPage;
  OtpMessage({this.title, this.route, this.stayOnPage});
  @override
  _OtpMessageState createState() => _OtpMessageState();
}

class _OtpMessageState extends State<OtpMessage> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (widget.stayOnPage == false) push(context, widget.route);
        if (widget.stayOnPage == true) pop(context);
      },
      child:
          // Container(
          //   // height: screenHeight(context, dividedBy: 4),
          //   // width: screenWidth(context, dividedBy: 1.22),
          //   // color: Colors.red,
          //   child:
          AlertDialog(
        insetPadding:
            EdgeInsets.symmetric(vertical: screenWidth(context, dividedBy: 10)),
        contentPadding: EdgeInsets.only(),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        content: Container(
          // color: Constants.kitGradients[1],
          height: screenHeight(context, dividedBy: 6),
          width: screenWidth(context, dividedBy: 1.22),
          child: Column(
            children: [
              SizedBox(
                height: screenHeight(context,
                    dividedBy: widget.stayOnPage == true ? 20 : 15),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    child: Flexible(
                      flex: 1,
                      child: Text(
                        widget.title,
                        maxLines: 2,
                        overflow: TextOverflow.visible,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontFamily: "sfProSemiBold",
                            fontSize: widget.stayOnPage == true
                                ? screenWidth(context, dividedBy: 22)
                                : screenWidth(context, dividedBy: 22),
                            fontWeight: widget.stayOnPage == true
                                ? FontWeight.normal
                                : FontWeight.w600),
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context,
                    dividedBy: widget.stayOnPage == true ? 30 : 20),
              ),
              // SizedBox(
              //   height: screenHeight(context, dividedBy: 100),
              // ),
              // Text(
              //   "Tap anywhere to close this pop up.",
              //   style: TextStyle(
              //     color: Colors.black,
              //     fontFamily: 'Muli',
              //     fontSize: 16,
              //   ),
              // ),
            ],
          ),
        ),
      ),
      // ),
    );
  }
}
