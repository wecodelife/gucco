import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class DocumentsToBring extends StatefulWidget {
  bool documentsToBring;
  DocumentsToBring({this.documentsToBring});
  @override
  _DocumentsToBringState createState() => _DocumentsToBringState();
}

class _DocumentsToBringState extends State<DocumentsToBring> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 15),
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Documents to bring',
                    style: widget.documentsToBring == true
                        ? TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[11])
                        : TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[7])),
                widget.documentsToBring == true
                    ? SvgPicture.asset(
                        'assets/icons/arrow_upward.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[11],
                      )
                    : SvgPicture.asset(
                        'assets/icons/arrow_down.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[7],
                      ),
              ],
            ),
          ),
          onTap: () {
            widget.documentsToBring == false
                ? setState(() {
                    widget.documentsToBring = true;
                  })
                : setState(() {
                    widget.documentsToBring = false;
                  });
          },
        ),
        widget.documentsToBring == true
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Text(
                      'Please note that you are required to bring the following documents on the day of your appointment:',
                      style: TextStyle(
                          fontFamily: 'SFProText-Regular',
                          fontSize: 15,
                          color: Constants.kitGradients[7])),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Text('1. Notice Of Vacant Possession Letter (NVP)',
                      style: TextStyle(
                          fontFamily: 'SFProText-Regular',
                          fontSize: 15,
                          color: Constants.kitGradients[7])),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Text('2. IC / Passport / Latest company’s business profile',
                      style: TextStyle(
                          fontFamily: 'SFProText-Regular',
                          fontSize: 15,
                          color: Constants.kitGradients[7])),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Text(
                      '3. Signed original letter of authorisation (Authorised Individual)',
                      style: TextStyle(
                          fontFamily: 'SFProText-Regular',
                          fontSize: 15,
                          color: Constants.kitGradients[7])),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }
}
