import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class MyProfileWidget2 extends StatefulWidget {
  String title1,title2;
  MyProfileWidget2({this.title1,this.title2});
  @override
  _MyProfileWidget2State createState() => _MyProfileWidget2State();
}

class _MyProfileWidget2State extends State<MyProfileWidget2> {
  @override
  Widget build(BuildContext context) {
    return  Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.title1,
              style: TextStyle(
                  fontFamily: 'MuliBold',
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                  color: Constants.kitGradients[0]),
            ),
            Text(
              widget.title2,
              style: TextStyle(
                  fontFamily: 'MuliBold',
                  fontSize: 14,
                  fontWeight: FontWeight.w300,
                  color: Constants.kitGradients[0]),
            ),
          ],
        ),
        GestureDetector(
          child: Container(
            width: screenWidth(context,dividedBy: 4),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  'Change',
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      fontFamily: 'MuliBold',
                      fontSize: 16,
                      fontWeight: FontWeight.w300,
                      color: Constants.kitGradients[0]),
                ),
              ],
            ),
          ),
          onTap: (){
            //do something here
          },
        ),
      ],
    );
  }
}
