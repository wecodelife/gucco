import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class DropDownFormField extends StatefulWidget {
  List<String>dropDownList;
  String title;
  String dropDownValue;
  bool underline;
  bool hintText;
  double boxWidth;
  double boxHeight;
  String fontFamily;
  double hintPadding;
  String hintFontFamily;
  ValueChanged<String> onClicked;
  DropDownFormField({this.fontFamily,this.hintText,this.title,this.dropDownList,
    this.hintPadding,this.dropDownValue,this.onClicked,this.underline,this.boxWidth,this.boxHeight,this.hintFontFamily});
  @override
  _DropDownFormFieldState createState() => _DropDownFormFieldState();
}
String dropdownValue;
class _DropDownFormFieldState extends State<DropDownFormField> {
  @override
  void initState() {
    // TODO: implement initState
    print(widget.dropDownList );
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Column(
                 mainAxisAlignment: MainAxisAlignment.end,
      children: [

        Container(
          height: screenHeight(context,dividedBy: widget.boxHeight),
          width: screenWidth(context,dividedBy: widget.boxWidth),
          color: Colors.white,
          child: DropdownButton<String>(


            underline: widget.underline==true? Column(
              children: [
                SizedBox(
                  height: screenHeight(context,dividedBy: 5),
                ),
                Divider(
                  thickness: screenHeight(context,dividedBy: 760),
                  color: Colors.grey,
                ),
              ],
            ):Container(),
            icon: Container(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Icon(Icons.arrow_drop_down,size: 30,color: Colors.grey,),
                ],
              ),
            ),
            isExpanded: true,
            iconSize: 10,
            dropdownColor: Colors.white,
              value: widget.dropDownValue==""?dropdownValue:widget.dropDownValue ,
            style: TextStyle(fontSize: 18, color: Colors.grey,fontFamily: 'SFProText-Regular',fontWeight: FontWeight.w600),
            items: widget.dropDownList
                .map<DropdownMenuItem<String>>((String value) =>
                DropdownMenuItem<String>(value: value, child: widget.underline==false?Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: screenHeight(context,dividedBy: 80),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [


                        Text(value,style: TextStyle(color: Colors.black,fontSize:screenWidth(context,dividedBy: 24),fontWeight: FontWeight.w400,fontFamily:widget.fontFamily ),),
                      ],
                    )
                  ],

                )
                   :Text(value,style:TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.w400,fontFamily:widget.fontFamily),)))
                .toList(),
            onChanged: (selectedValue) {

              setState(() {
                widget.dropDownValue = selectedValue;
              });
              widget.onClicked(selectedValue);
            },


            hint:widget.hintText==true? Row(
              children: [
                SizedBox(
                  width: screenWidth(context,dividedBy: widget.hintPadding),
                ),

                Text(widget.title,style: TextStyle(color: Colors.black,fontSize: 18,fontFamily:widget.hintFontFamily),
                ),
              ],
            ):Container()

          ),
        ),


      ],
    );
  }
}
