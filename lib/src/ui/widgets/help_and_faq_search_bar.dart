import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HelpAndFAQSearchBAR extends StatefulWidget {
  final String hintText;
  final Function onTap;
  final TextEditingController textEditingController;

  HelpAndFAQSearchBAR({this.hintText, this.onTap, this.textEditingController});

  @override
  _HelpAndFAQSearchBARState createState() => _HelpAndFAQSearchBARState();
}

class _HelpAndFAQSearchBARState extends State<HelpAndFAQSearchBAR> {
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1.7),
          height: screenHeight(context, dividedBy: 25),
          child: Padding(
            padding: const EdgeInsets.only(top: 3.0),
            child: TextField(
                controller: widget.textEditingController,
                style: TextStyle(color: Constants.kitGradients[0]),
                decoration: InputDecoration(
                  hintText: widget.hintText,
                  hintStyle: TextStyle(
                      color: Constants.kitGradients[19],
                      fontSize: 18,
                      fontFamily: 'Muli'),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Constants.kitGradients[1]),
                    //  when the TextFormField in unfocused
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Constants.kitGradients[1]),
                    //  when the TextFormField in focused
                  ),
                )),
          ),
        ),
        GestureDetector(
          child: Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 10),
                  height: screenHeight(context, dividedBy: 20),
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: SvgPicture.asset(
                      "assets/icons/search_icon.svg",
                      color: Colors.white,
                    ),
                  ),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 4),
                  height: screenHeight(context, dividedBy: 20),
                  child: Center(
                    child: Text(
                      "SEARCH",
                      style: TextStyle(
                          color: Constants.kitGradients[0],
                          fontSize: 18,
                          fontFamily: 'MuliSemiBold'),
                    ),
                  ),
                ),
              ],
            ),
          ),
          onTap: widget.onTap,
        ),
      ],
    );
  }
}
