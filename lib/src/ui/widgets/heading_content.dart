import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HeadingContent extends StatefulWidget {
  String heading;
  String content;
  bool fontBold;
  HeadingContent({this.fontBold,this.heading, this.content});
  @override
  _HeadingContentState createState() => _HeadingContentState();
}

class _HeadingContentState extends State<HeadingContent> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 90),
        ),
        Row(
          children: [
            SizedBox(
              width: screenWidth(context, dividedBy: 20),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                      width: screenWidth(context, dividedBy: 2.5),
                      child: Text(
                        widget.heading,
                        style: TextStyle(
                            fontSize: 16,
                            fontFamily: 'SFProText-Regular',
                            color: Colors.black),
                      )),
                  Container(
                      width: screenWidth(context, dividedBy: 2),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            widget.content,
                            style: TextStyle(
                                fontSize: 16,
                                fontFamily: 'SFProText-Regular',
                                color: Colors.black,fontWeight:widget.fontBold==true?FontWeight.w600:FontWeight.w400),
                          ),
                        ],
                      )),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
