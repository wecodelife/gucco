import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/fault_reporting_page.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FaultReportButton extends StatefulWidget {
  final String buttonName;
  final Color buttonNameColor, buttonColor;
  final double buttonWidth;
  Function onPressed;

  FaultReportButton(
      {this.buttonName,
      this.buttonNameColor,
      this.buttonColor,
      this.buttonWidth,
      this.onPressed});

  @override
  _FaultReportButtonState createState() => _FaultReportButtonState();
}

class _FaultReportButtonState extends State<FaultReportButton> {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: widget.buttonColor,
      onPressed: widget.onPressed,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
        side: BorderSide(color: Constants.kitGradients[11], width: 1),
      ),
      child: Container(
        //color: Colors.greenAccent,
        width: widget.buttonWidth,
        child: Center(
          child: Text(
            widget.buttonName,
            style: TextStyle(
                fontSize: screenWidth(context,dividedBy: 27),
                color: widget.buttonNameColor,
                fontFamily: "SFProText-Medium"),
          ),
        ),
      ),
    );
  }
}
