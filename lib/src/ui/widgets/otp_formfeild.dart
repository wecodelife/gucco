import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/animated_otp_feilds.dart';
import 'package:guoco_priv_app/src/ui/widgets/build_button.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class OtpFormFeild extends StatefulWidget {
  TextEditingController otpTextEditingController = new TextEditingController();
  Function otpTap;
  bool isLoading;
  OtpFormFeild({this.otpTextEditingController, this.otpTap, this.isLoading});

  @override
  _OtpFormFeildState createState() => _OtpFormFeildState();
}

class _OtpFormFeildState extends State<OtpFormFeild> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // CircularProgressIndicator(),

        animated_otp_fields(
          widget.otpTextEditingController,
          fieldHeight: screenHeight(context, dividedBy: 11),
          fieldWidth: screenWidth(context, dividedBy: 9),
          OTP_digitsCount: 6,
          animation: TextAnimation.Fading,
          border: Border.all(width: 3, color: Constants.kitGradients[0]),
          borderRadius: BorderRadius.all(Radius.circular(6)),
          contentPadding:
              EdgeInsets.only(top: screenHeight(context, dividedBy: 100)),
          forwardCurve: Curves.linearToEaseOut,
          textStyle: TextStyle(
            color: Constants.kitGradients[0],
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
          onFieldSubmitted: (text) {},
          spaceBetweenFields: 14,
          autoFocus: true,
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 10),
        ),

        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 13)),
          child: Text(
            "(If you don’t see it in your Inbox, it may have been sorted into your Spam folder.)",
            style: TextStyle(
                color: Constants.kitGradients[0],
                fontFamily: 'Muli',
                fontSize: screenWidth(context, dividedBy: 26),
                fontStyle: FontStyle.italic),
          ),
        ),
        // CircularProgressIndicator(),
        SizedBox(
          height: screenHeight(context, dividedBy: 10),
        ),

        BuildButton(
          title: "VERIFY MY ACCOUNT",
          disabled: true,
          onPressed: () {
            widget.otpTap();
            // otpAlertBox(
            //     context: context,
            //     title: "Your account has been verified!",
            //     route: CreatePassword(),
            //     stayOnPage: false);
          },
          isLoading: widget.isLoading,
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 20),
        ),
      ],
    );
  }
}
