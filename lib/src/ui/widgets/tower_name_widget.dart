import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class TowerName extends StatefulWidget {
  String towerName;
  String towerSubName;
  TowerName({this.towerSubName, this.towerName});
  @override
  _TowerNameState createState() => _TowerNameState();
}

class _TowerNameState extends State<TowerName> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 15),
      width: screenWidth(context, dividedBy: 1),
      color: Constants.kitGradients[9],
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 30)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              height: screenHeight(context, dividedBy: 30),
              width: screenWidth(context, dividedBy: 2.5),
              decoration: BoxDecoration(
                  color: Constants.kitGradients[31],
                  borderRadius: BorderRadius.circular(10)),
              child: Center(
                  child: Text(
                widget.towerName + ", " + widget.towerSubName,
                style: TextStyle(
                    fontSize: 12,
                    color: Constants.kitGradients[0],
                    fontFamily: 'Montserrat-Semibold'),
              )),
            ),
          ],
        ),
      ),
    );
  }
}
