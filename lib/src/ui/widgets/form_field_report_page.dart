import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FormFieldReportPage extends StatefulWidget {
  String hintText, title;
  TextEditingController textEditingController;
  Function onPressed;
  bool inviteVisitors;
  double boxWidth;
  double sizedBoxWidth;
  bool noStar;

  FormFieldReportPage({this.textEditingController,
    this.hintText,
    this.title,
    this.inviteVisitors,
    this.onPressed,
    this.boxWidth,
    this.sizedBoxWidth,
    this.noStar
  });

  @override
  _FormFieldReportPageState createState() => _FormFieldReportPageState();
}

class _FormFieldReportPageState extends State<FormFieldReportPage> {
  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      widget.inviteVisitors==false?Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [

          SizedBox(
            width: screenWidth(context,dividedBy: widget.sizedBoxWidth),
          ),

          RichText(
            text: TextSpan(
                text: widget.title,
                style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'SFProText-Regular',
                    fontSize: 18),
                children: <TextSpan>[
                  widget.noStar==true?TextSpan(text: ""):TextSpan(text: " *",
                    style: TextStyle(
                        color: Constants.kitGradients[11],
                        fontFamily: 'SFProDisplay-Bold',
                        fontSize: 18),
                  )
                ]
            ),
          ), ]) :Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              RichText(
                text: TextSpan(
                    text: widget.title,
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'SFProText-Regular',
                        fontSize: 18),
                    children: <TextSpan>[
                      widget.noStar==true?TextSpan(text: ""):TextSpan(text: " *",
                          style: TextStyle(
                              color: Constants.kitGradients[11],
                              fontFamily: 'SFProDisplay-Bold',
                              fontSize: 18),
                      )
                    ]
                ),
              ),
            ],
          ),

      Container(
        width: screenWidth(context, dividedBy: 1),
        height: widget.inviteVisitors == false?screenHeight(context, dividedBy: 12):screenHeight(context, dividedBy: 15),
        child: Padding(
          padding: widget.inviteVisitors == false
              ? EdgeInsets.symmetric(
                  horizontal: screenWidth(context, dividedBy: widget.boxWidth),
                )
              : EdgeInsets.only(),
          child: TextField(
            maxLines: 1,
            controller: widget.textEditingController,
            autofocus: false,
            onTap: widget.onPressed,
            style: TextStyle(
              color: Constants.kitGradients[7],
              fontFamily: 'MuliSemiBold',
              fontSize: 16,
            ),
            cursorColor: widget.inviteVisitors == false?Colors.grey:Constants.kitGradients[7],
            decoration: InputDecoration(
              contentPadding: widget.inviteVisitors==false?EdgeInsets.only():EdgeInsets.only(bottom: 30),
              focusedBorder: UnderlineInputBorder(
                borderSide:
                    BorderSide(color: Colors.grey, style: BorderStyle.solid),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: Colors.grey,
                ),
              ),
              hintStyle: TextStyle(
                color: Colors.grey,
                fontFamily: 'MuliSemiBold',
                fontSize: 18,
              ),
            ),
          ),
        ),
      ),
    ]);
  }
}
