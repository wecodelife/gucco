import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/estate_tile.dart';

class EstateListView extends StatefulWidget {
  String image;
  String headText;
  String subText;
  EstateListView({this.image, this.headText, this.subText});
  @override
  _EstateListViewState createState() => _EstateListViewState();
}

class _EstateListViewState extends State<EstateListView> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0),
      child: Container(
        child: EstateListTile(
          image: widget.image,
          text1: widget.headText,
          text2: widget.subText,
        ),
      ),
    );
  }
}
