import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class DescriptionText extends StatefulWidget {
  double textSize;
  String descriptionText;
  double horizontalPadding;
  DescriptionText({this.descriptionText,this.textSize,this.horizontalPadding});
  @override
  _DescriptionTextState createState() => _DescriptionTextState();
}

class _DescriptionTextState extends State<DescriptionText> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: widget.horizontalPadding,),
      child: Container(
        child: Text(
          widget.descriptionText,
          style: TextStyle(
            color: Constants.kitGradients[0],
            fontFamily: 'Muli',
            fontSize: screenWidth(context, dividedBy: widget.textSize),
          ),
        ),
      ),
    );

  }
}
