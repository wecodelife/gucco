import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/app_bar_right_icon.dart';
import 'package:guoco_priv_app/src/ui/widgets/calender_carousel.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_name_widget.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:intl/intl.dart';

class BookingCalenderPage extends StatefulWidget {
  String towerName;
  String towerSubName;
  bool reschedule;
  String heading;
  bool rightIcon;
  String date;
  String time;
  String icon;
  Function bookListTap;
  ValueChanged valueChanged;
  BookingCalenderPage(
      {this.towerName,
      this.time,
      this.date,
      this.towerSubName,
      this.heading,
      this.rightIcon,
      this.icon,
      this.bookListTap,
      this.reschedule,
      this.valueChanged});

  @override
  _BookingCalenderPageState createState() => _BookingCalenderPageState();
}

class _BookingCalenderPageState extends State<BookingCalenderPage> {
  DateTime currentDate;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        TowerName(
          towerName: widget.towerName,
          towerSubName: widget.towerSubName,
        ),
        if (widget.reschedule == true)
          Container(
            width: screenWidth(context, dividedBy: 1.1),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                Text(
                  "Reschedule: Key Collection Appointment",
                  style:
                      TextStyle(fontSize: 15, fontFamily: "SFProTextSemiBold"),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
                Text(
                  "My Booking: " +
                      new DateFormat('dd-MMM-yyyy').format(
                          DateTime.parse(date(widget.date) + "T18:15:12")) +
                      ", " +
                      time(widget.time),
                  style: TextStyle(
                      fontSize: 15,
                      fontFamily: "SFProText-Regular",
                      fontWeight: FontWeight.w400),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 60),
                ),
              ],
            ),
          ),
        AppBarRightIcon(
            pageTitle: widget.heading,
            boxWidth: 6,
            rightIcon: widget.rightIcon,
            icon: widget.icon,
            bookedListTap: widget.bookListTap),
        SizedBox(
          height: screenHeight(context, dividedBy: 100),
        ),
        CarouselCalender(
          onValueChanged: (value) {
            currentDate = value;
            widget.valueChanged(currentDate);
          },
        ),
        Divider(
          thickness: 5,
          color: Constants.kitGradients[1],
        ),
      ],
    );
  }
}
