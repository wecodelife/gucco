import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FormFeildUserDetails extends StatefulWidget {
  String hintText;
  bool readOnly;
  Function onPressed;
  TextEditingController textEditingController;
  FormFeildUserDetails({this.hintText, this.textEditingController,this.onPressed,this.readOnly});
  @override
  _FormFeildUserDetailsState createState() => _FormFeildUserDetailsState();
}

class _FormFeildUserDetailsState extends State<FormFeildUserDetails> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 10)),
      child: TextField(
        controller: widget.textEditingController,
        autofocus: false,
        readOnly: widget.readOnly ?? false,
        onTap:widget.onPressed,
        style: TextStyle(
    color: Constants.kitGradients[0],
    fontFamily: 'MuliSemiBold',
    fontSize: 16,),
        cursorColor: Constants.kitGradients[0],
        decoration: InputDecoration(
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[0]),
          ),
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: Constants.kitGradients[0]),
          ),
          hintText: widget.hintText,
          hintStyle: TextStyle(
            color: Constants.kitGradients[0],
            fontFamily: 'MuliSemiBold',
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
