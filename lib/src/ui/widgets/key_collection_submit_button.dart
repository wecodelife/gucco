import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

import '../../utils/constants.dart';
import '../../utils/utils.dart';

class SubmitButton extends StatefulWidget {
  final String title;
  final double width,height;
  bool isLoading=false;
  SubmitButton({this.title,this.height,this.width,this.isLoading});

  @override
  _SubmitButtonState createState() => _SubmitButtonState();
}

class _SubmitButtonState extends State<SubmitButton> {
  @override
  Widget build(BuildContext context) {
    return  Container(
      width: screenWidth(context,dividedBy: widget.width),
      height: screenHeight(context,dividedBy: widget.height),
      decoration: BoxDecoration(
          color: Constants.kitGradients[13],
          borderRadius: BorderRadius.circular(25)
      ),
      child: Center(
        child: widget.isLoading==false?
        Text(widget.title,
            style: TextStyle(
                fontFamily: 'SFPro',
                fontSize: 15,
                color: Constants.kitGradients[0])):
        Shimmer.fromColors(
          highlightColor: Constants.kitGradients[0],
          baseColor: Constants.kitGradients[1],
          child: Container(
            height: screenHeight(context, dividedBy: widget.height),
            width: screenWidth(context, dividedBy: widget.width),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Colors.black54
            ),

          ),
        )
        // Container(
        //   height: screenHeight(context,dividedBy: 20),
        //   width: screenWidth(context,dividedBy: 10),
        //   child: CircularProgressIndicator(
        //     valueColor: new AlwaysStoppedAnimation<Color>(Constants.kitGradients[0]),
        //   ),
        // ),
      ),
    );
  }
}
