import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class CheckBoxFacilities extends StatefulWidget {
  bool value;
  ValueChanged onChangedCheckValue;
  CheckBoxFacilities({this.value, this.onChangedCheckValue});
  @override
  _CheckBoxFacilitiesState createState() => _CheckBoxFacilitiesState();
}

class _CheckBoxFacilitiesState extends State<CheckBoxFacilities> {
  bool checkValue;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          widget.value = !widget.value;
          checkValue = widget.value;
        });
        widget.onChangedCheckValue(checkValue);
        print("check box status:" + widget.value.toString());
      },
      child: Container(
        width: screenWidth(context, dividedBy: 10),
        height: screenHeight(context, dividedBy: 19),
        child: Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Constants.kitGradients[0],
                border:
                    Border.all(color: Constants.kitGradients[4], width: 2.0)),
            child: widget.value
                ? Icon(
                    Icons.check,
                    size: 20.0,
                    color: Constants.kitGradients[7],
                  )
                : Container(),
          ),
        ),
      ),
    );
  }
}
