import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FacilitiesListWidget extends StatefulWidget {
  bool showDescription;
  String title, description, imagePath;
  Function onPressed;
  FacilitiesListWidget(
      {this.showDescription,
      this.title,
      this.description,
      this.onPressed,
      this.imagePath});
  @override
  _FacilitiesListWidgetState createState() => _FacilitiesListWidgetState();
}

class _FacilitiesListWidgetState extends State<FacilitiesListWidget> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(5.0),
      child: GestureDetector(
        onTap: () {
          widget.onPressed();
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: Container(
                  width: screenWidth(context, dividedBy: 1.1),
                  height: screenHeight(context, dividedBy: 5),
                  child: Image(
                    image: NetworkImage(
                      widget.imagePath,
                    ),
                    fit: BoxFit.cover,
                  ),
                )),
            SizedBox(
              height: screenHeight(context, dividedBy: 65),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 18),
                ),
                Text(
                  widget.title,
                  style: TextStyle(
                    color: Constants.kitGradients[7],
                    fontFamily: 'sfProSemiBold',
                    fontSize: screenWidth(context, dividedBy: 30),
                  ),
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 2),
                ),
                widget.showDescription == true
                    ? GestureDetector(
                        onTap: () {
                          setState(() {
                            widget.showDescription = false;
                          });
                        },
                        child: Container(
                          width: screenWidth(context, dividedBy: 10),
                          height: screenHeight(context, dividedBy: 25),
                          child: Padding(
                            padding: const EdgeInsets.all(4.5),
                            child:
                                SvgPicture.asset('assets/icons/minus_sign.svg'),
                          ),
                        ))
                    : GestureDetector(
                        onTap: () {
                          setState(() {
                            widget.showDescription = true;
                          });
                        },
                        child: Container(
                          width: screenWidth(context, dividedBy: 10),
                          height: screenHeight(context, dividedBy: 25),
                          child: Padding(
                            padding: const EdgeInsets.all(4.5),
                            child:
                                SvgPicture.asset('assets/icons/plus_sign.svg'),
                          ),
                        )),
              ],
            ),
            widget.showDescription == true
                ? Container(
                    width: screenWidth(context, dividedBy: 1.1),
                    height: screenHeight(context, dividedBy: 9),
                    child: Row(
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 40),
                        ),
                        Center(
                          child: Text(
                            widget.description,
                            style: TextStyle(
                              color: Constants.kitGradients[7],
                              fontFamily: 'sfProRegular',
                              fontSize: 14,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                : Container(),
          ],
        ),
      ),
    );
  }
}
