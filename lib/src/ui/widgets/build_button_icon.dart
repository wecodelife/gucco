import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';

class BuildButtonIcon extends StatefulWidget {
  final String title;
  final bool arrowIcon;
  final bool leftIcon;
  final bool rightIcon;
  final bool faultReport;
  final String icon;
  final bool buttonColorBrown;
  final Function onPressed;
  final bool isLoading;
  BuildButtonIcon(
      {this.isLoading,
      this.title,
      this.arrowIcon,
      this.rightIcon,
      this.onPressed,
      this.faultReport,
      this.icon,
      this.leftIcon,
      this.buttonColorBrown});
  @override
  _BuildButtonIconState createState() => _BuildButtonIconState();
}

class _BuildButtonIconState extends State<BuildButtonIcon> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: screenHeight(context, dividedBy: 17),
        width: screenWidth(context, dividedBy: 1.2),
        decoration: widget.buttonColorBrown != true
            ? BoxDecoration()
            : BoxDecoration(
                color: Constants.kitGradients[33],
                borderRadius: BorderRadius.circular(30),
              ),
        child: RaisedButton(
          onPressed: () {
            widget.onPressed();
          },
          color: widget.buttonColorBrown != true
              ? widget.faultReport == true
                  ? Constants.kitGradients[13]
                  : Constants.kitGradients[1]
              : Constants.kitGradients[33],
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
          child: Center(
              child: widget.isLoading == true
                  ? Shimmer.fromColors(
                      highlightColor: Constants.kitGradients[0],
                      baseColor: Constants.kitGradients[1],
                      child: Container(
                        height: screenHeight(context, dividedBy: 17),
                        width: screenWidth(context, dividedBy: 1.2),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(30),
                          color: Colors.black54,
                        ),
                      ),
                    )
                  // Container(
                  //   height: screenHeight(context,dividedBy: 20),
                  //   width: screenWidth(context,dividedBy: 10),
                  //   child: CircularProgressIndicator(
                  //     valueColor: new AlwaysStoppedAnimation<Color>(Constants.kitGradients[0]),
                  //   ),
                  // )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        widget.leftIcon == true
                            ? Image(image: AssetImage(widget.icon))
                            : Container(),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          widget.title,
                          style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontFamily: 'SFProDisplay-Semibold',
                              fontSize: 16),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        widget.arrowIcon == true
                            ? Icon(
                                Icons.arrow_right_alt_sharp,
                                color: Colors.white,
                                size: 20,
                              )
                            : Container()
                      ],
                    )),
        ));
  }
}
