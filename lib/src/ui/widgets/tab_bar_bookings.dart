import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/e_application_list.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class TabBarBookings extends StatefulWidget {
  bool noTab;
  String heading1;
  String heading2;
  Color tabColor;
  Color indicatorColor;
  Color headingColor;
  String pageType;
  Function onPressedCurrentBookings;
  Function onPressedPastBookings;
  Widget currentBookingsWidget;
  Widget pastBookingsWidget;
  String tabValueSelected;
  // Widget gridViewWidget1;
  // Widget gridviewWidget2;
  TabBarBookings(
      {this.indicatorColor,
      this.tabColor,
      this.heading1,
      this.onPressedCurrentBookings,
      this.heading2,
      this.headingColor,
      this.noTab,
      this.onPressedPastBookings,
      this.currentBookingsWidget,
      this.pastBookingsWidget,
      this.pageType,
      this.tabValueSelected});
  @override
  _TabBarBookingsState createState() => _TabBarBookingsState();
}

class _TabBarBookingsState extends State<TabBarBookings> {
  String tabValue = "currentbookings";
  @override
  void initState() {
    print(widget.tabValueSelected);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(mainAxisAlignment: MainAxisAlignment.start, children: [
      Container(
        color: widget.tabColor,
        width: screenWidth(context, dividedBy: 1),
        height: screenHeight(context, dividedBy: 15),
        child: Row(
          children: [
            GestureDetector(
              child: Container(
                width: screenWidth(context, dividedBy: 2),
                height: screenHeight(context, dividedBy: 15),
                decoration: widget.pageType == "eapplication"
                    ? tabValue == "currentbookings"
                        ? BoxDecoration(color: Colors.white)
                        : BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage("assets/images/tab_bar_bg.png"),
                                fit: BoxFit.fill))
                    : BoxDecoration(color: Colors.white),
                child: Center(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      widget.heading1,
                      style: TextStyle(
                          fontSize: tabValue == "currentbookings" ? 16 : 15,
                          color: widget.pageType == "eapplication"
                              ? tabValue == "currentbookings"
                                  ? widget.headingColor
                                  : Colors.white
                              : widget.headingColor,
                          fontFamily: tabValue == "currentbookings"
                              ? 'SFProText-SemiBold'
                              : 'SFProText-Regular'),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 100),
                    ),
                    tabValue == "currentbookings"
                        ? Container(
                            color: widget.indicatorColor,
                            height: screenHeight(context, dividedBy: 300),
                            width: screenWidth(context, dividedBy: 8),
                          )
                        : Container(),
                  ],
                )),
              ),
              onTap: () {
                widget.onPressedCurrentBookings();

                setState(() {
                  tabValue = widget.tabValueSelected;
                });

                print(tabValue);
              },
            ),
            widget.pageType == "facilities"
                ? Container(
                    height: screenHeight(context, dividedBy: 25),
                    width: screenWidth(context, dividedBy: 200),
                    color: Colors.grey)
                : Container(),
            GestureDetector(
              child: Container(
                width: widget.pageType == "eapplication"
                    ? screenWidth(context, dividedBy: 2)
                    : screenWidth(context, dividedBy: 2.1),
                height: screenHeight(context, dividedBy: 15),
                decoration: widget.pageType == "eapplication"
                    ? widget.tabValueSelected == "pastbookings"
                        ? BoxDecoration(color: Colors.white)
                        : BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage("assets/images/tab_bar_bg.png"),
                                fit: BoxFit.fill))
                    : BoxDecoration(color: Colors.white),
                child: Center(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      widget.heading2,
                      style: TextStyle(
                          fontSize: tabValue == "pastbookings" ? 16 : 15,
                          color: widget.pageType == "eapplication"
                              ? tabValue == "pastbookings"
                                  ? widget.headingColor
                                  : Colors.white
                              : widget.headingColor,
                          fontFamily: tabValue == "pastbookings"
                              ? 'SFProText-SemiBold'
                              : 'SFProText-Regular'),
                    ),
                    SizedBox(
                      height: screenHeight(context, dividedBy: 100),
                    ),
                    tabValue == "pastbookings"
                        ? Container(
                            color: widget.indicatorColor,
                            height: screenHeight(context, dividedBy: 300),
                            width: screenWidth(context, dividedBy: 8),
                          )
                        : Container(),
                  ],
                )),
              ),
              onTap: () {
                widget.onPressedPastBookings();

                setState(() {
                  tabValue = widget.tabValueSelected;
                });
              },
            ),
          ],
        ),
      ),
      if (widget.pageType == "facilities")
        Divider(
          thickness: 2.0,
        ),
      if (tabValue == "currentbookings" && widget.pageType == "facilities")
        widget.currentBookingsWidget,
      if (tabValue == "currentbookings" && widget.pageType == "eapplication")
        EApplicationList(
          tabValue: "new application",
        ),
      if (tabValue == "pastbookings" && widget.pageType == "eapplication")
        EApplicationList(),
      if (tabValue == "pastbookings" && widget.pageType == "facilities")
        widget.pastBookingsWidget
    ]);
  }
}
