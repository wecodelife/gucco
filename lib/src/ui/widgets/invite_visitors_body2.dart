import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/inviteVisitorsBody3A.dart';
import 'package:guoco_priv_app/src/ui/widgets/invite_visitors_body3.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

import 'form_field_report_page.dart';

class InviteVisitorsBody2 extends StatefulWidget {
  bool mandatoryFieldStatus;
  String title;
  ValueChanged valueChanged;

  InviteVisitorsBody2(
      {this.mandatoryFieldStatus, this.title, this.valueChanged});

  @override
  _InviteVisitorsBody2State createState() => _InviteVisitorsBody2State();
}

class _InviteVisitorsBody2State extends State<InviteVisitorsBody2> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 15),
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(widget.title,
                    style: widget.mandatoryFieldStatus == true
                        ? TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 17,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[11])
                        : TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 17,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[7])),
                widget.mandatoryFieldStatus == true
                    ? SvgPicture.asset(
                        'assets/icons/arrow_upward.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[11],
                      )
                    : SvgPicture.asset(
                        'assets/icons/arrow_down.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[7],
                      ),
              ],
            ),
          ),
          onTap: () {
            // widget.mandatoryFieldStatus == false
            //     ? setState(() {
            //         widget.mandatoryFieldStatus = true;
            //       })
            //     : setState(() {
            //         widget.mandatoryFieldStatus = false;
            //       });
            setState(() {
              widget.mandatoryFieldStatus = true;
            });
            widget.valueChanged(widget.mandatoryFieldStatus);

          },
        ),
        widget.mandatoryFieldStatus == true
            ? InviteVisitorsBody3Mandatory()
            : Container(),
      ],
    );
  }
}
