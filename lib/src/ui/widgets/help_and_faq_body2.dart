import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HelpAndFAQBody2 extends StatefulWidget {
  final Function onTap;
  final String title;

  HelpAndFAQBody2({this.onTap, this.title});

  @override
  _HelpAndFAQBody2State createState() => _HelpAndFAQBody2State();
}

class _HelpAndFAQBody2State extends State<HelpAndFAQBody2> {
  bool Q1;

  @override
  void initState() {
    setState(() {
      Q1 = false;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          width: screenWidth(context, dividedBy: 1.0),
          height: screenHeight(context, dividedBy: 8),
          decoration: BoxDecoration(
              color: Constants.kitGradients[1],
              borderRadius: Q1 == true
                  ? BorderRadius.only(
                      topRight: Radius.circular(5.0),
                      topLeft: Radius.circular(5.0))
                  : BorderRadius.circular(5.0)),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: screenWidth(context, dividedBy: 20)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 1.3),
                  child: Text(
                    widget.title,
                    overflow: TextOverflow.visible,
                    style: TextStyle(
                        color: Constants.kitGradients[0],
                        fontSize: 18,
                        fontFamily: 'MuliBold'),
                  ),
                ),
                GestureDetector(
                  child: Container(
                    width: screenWidth(context, dividedBy: 14),
                    height: screenHeight(context, dividedBy: 18),
                    child: Padding(
                      padding: const EdgeInsets.all(4.0),
                      child: Q1 == false
                          ? SvgPicture.asset('assets/icons/arrow_down.svg',
                              color: Constants.kitGradients[0])
                          : SvgPicture.asset(
                              'assets/icons/arrow_upward.svg',
                              color: Constants.kitGradients[0],
                            ),
                    ),
                  ),
                  onTap: () {
                    Q1 == false
                        ? setState(() {
                            Q1 = true;
                          })
                        : setState(() {
                            Q1 = false;
                          });
                  },
                )
              ],
            ),
          ),
        ),
        Q1 == true
            ? Container(
                width: screenWidth(context, dividedBy: 1.0),
                decoration: BoxDecoration(
                  color: Constants.kitGradients[0],
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(5.0), bottomRight: Radius.circular(5.0))
                ),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: screenWidth(context, dividedBy: 20),
                      vertical: screenHeight(context, dividedBy: 28)),
                  child: Container(
                    width: screenWidth(context, dividedBy: 1.3),
                    child: Text(
                      'GuocoLand Privilege Club welcomes all Singapore citizens, Permanent Residents of '
                      'Singapore and Work Permit holders in Singapore who are residents of GuocoLand’s properties. '
                      'Residents can register with your email '
                      'and residential addresses and must be at least twenty one years old to be eligible.',
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                          color: Constants.kitGradients[7],
                          fontSize: 16,
                          fontFamily: 'Muli'),
                    ),
                  ),
                ),
              )
            : Container(),
      ],
    );
  }
}
