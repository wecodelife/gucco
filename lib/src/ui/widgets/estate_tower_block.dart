import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/bloc/auth_bloc.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/add_unit_login.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class EstateTowerBlock extends StatefulWidget {
  final String title;
  final Widget child;
  final bool shadowBackGroundHeight;
  EstateTowerBlock({this.title, this.child, this.shadowBackGroundHeight});
  @override
  _EstateTowerBlockState createState() => _EstateTowerBlockState();
}

class _EstateTowerBlockState extends State<EstateTowerBlock> {
  AuthBloc userBloc = new AuthBloc();
  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.shadowBackGroundHeight == false
          ? screenHeight(context, dividedBy: 3.733)
          : screenHeight(context, dividedBy: 7),
      width: screenWidth(context, dividedBy: 1),
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage(
          "assets/images/estate_bg1.png",
        ),
        fit: BoxFit.fill,
      )),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: widget.shadowBackGroundHeight == false
                ? screenHeight(context, dividedBy: 4.3)
                : screenHeight(context, dividedBy: 15),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(
              color: Colors.black.withOpacity(0.1),
            ),
            child: Center(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 40),
                        ),
                        Text(
                          widget.title,
                          style: TextStyle(
                              color: Constants.kitGradients[0],
                              fontSize: 16,
                              fontFamily: "sfProSemiBold"),
                        ),
                        Spacer(
                          flex: 1,
                        ),
                        GestureDetector(
                          child: Text(
                            "+ ADD UNIT",
                            style: TextStyle(
                                fontSize: 16,
                                decoration: TextDecoration.underline,
                                color: Colors.white,
                                fontFamily: "SFProDisplay-Semibold"),
                          ),
                          onTap: () {
                            pushAndReplacement(context, AddUnitLogin());
                          },
                        )
                      ],
                    ),
                  ),
                  widget.shadowBackGroundHeight == false
                      ? Container(
                          height: screenHeight(context, dividedBy: 6),
                          child: widget.child)
                      : Container(),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 100),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
