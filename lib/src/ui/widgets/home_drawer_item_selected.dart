import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HomeDrawerItemSelected extends StatefulWidget {
  final String label;
  HomeDrawerItemSelected({this.label});
  @override
  _HomeDrawerItemSelectedState createState() => _HomeDrawerItemSelectedState();
}

class _HomeDrawerItemSelectedState extends State<HomeDrawerItemSelected> {
  @override
  void initState() {
    print(widget.label);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: [
            // Text("Home",style: TextStyle(fontFamily: "Muli",fontWeight: FontWeight.w800,fontSize: 20,color: Constants.kitGradients[0]),),
            // SizedBox(height: screenHeight(context,dividedBy: 15),),
            Text(
              widget.label,
              style: TextStyle(
                  fontFamily: "Muli",
                  fontWeight: FontWeight.w800,
                  fontSize: screenWidth(context, dividedBy: 21),
                  color: widget.label == "HOME" || widget.label == "LOG OUT"
                      ? Constants.kitGradients[0]
                      : Constants.kitGradients[0].withOpacity(0.2)),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 100),
            ),
            Container(
              color: Constants.kitGradients[1],
              width: screenWidth(context, dividedBy: 8),
              height: 3,
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 18),
            ),
          ],
        ),
      ),
    );
  }
}
