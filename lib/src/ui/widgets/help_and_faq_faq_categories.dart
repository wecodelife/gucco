import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'help_and_faq_body1.dart';
import 'help_and_faq_list_view.dart';

class HelpAndFAQCategories extends StatefulWidget {
  final Function onTap;
  final String title;

  HelpAndFAQCategories({this.onTap, this.title});

  @override
  _HelpAndFAQCategoriesState createState() => _HelpAndFAQCategoriesState();
}

class _HelpAndFAQCategoriesState extends State<HelpAndFAQCategories> {
  ScrollController scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        controller: scrollController,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  "FAQ CATEGORIES",
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontSize: 20,
                      fontFamily: 'MuliSemiBold'),
                ),
              ],
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
            ListView.builder(
                controller: scrollController,
                itemCount: 3,
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: screenHeight(context, dividedBy: 70),
                    ),
                    child: HelpAndFAQ1ListView(
                      title: widget.title,
                      onTap: widget.onTap,
                    ),
                  );
                }),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
          ],
        ),
      ),
    );
  }
}
