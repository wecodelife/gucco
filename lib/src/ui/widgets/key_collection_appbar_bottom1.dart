import 'package:flutter/material.dart';

import '../../utils/constants.dart';
import '../../utils/utils.dart';

class Column1 extends StatefulWidget {
  String title1,title2;
  Column1({this.title1,this.title2});
  @override
  _Column1State createState() => _Column1State();
}

class _Column1State extends State<Column1> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Constants.kitGradients[9],
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 12),
      child: Padding(
        padding: const EdgeInsets.only(left: 15.0, right: 15.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: screenHeight(context,dividedBy: 26),
              width: screenWidth(context,dividedBy: 6),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Constants.kitGradients[11]),
              child: Center(
                  child: Text(widget.title1,
                      style: TextStyle(
                          fontFamily: 'Montserrat-SemiBold',
                          fontSize: 11,
                          color: Constants.kitGradients[0]))),
            ),
            Container(
              height: screenHeight(context,dividedBy: 26),
              width: screenWidth(context,dividedBy: 6),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: Constants.kitGradients[11]),
              child: Center(child: Text(widget.title2,style: TextStyle(fontFamily: 'Montserrat-SemiBold',fontSize: 11,color: Constants.kitGradients[0]))),
            )
          ],
        ),
      ),
    );
  }
}
