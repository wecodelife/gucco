import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';

class CheckBox extends StatefulWidget {
  bool checked;
  Function onTap;
  CheckBox({this.checked,this.onTap});
  @override
  _CheckBoxState createState() => _CheckBoxState();
}

class _CheckBoxState extends State<CheckBox> {
  bool checked=false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
          onTap:widget.onTap,
          child: Icon( widget.checked==true?Icons.check_box_rounded:Icons.check_box_outline_blank,color:Constants.kitGradients[1],)),
    );
  }
}
