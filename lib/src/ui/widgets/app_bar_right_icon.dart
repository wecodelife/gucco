import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class AppBarRightIcon extends StatefulWidget {
  Function pageNavigation;
  String pageTitle;
  double boxWidth;
  Function bookedListTap;
  bool rightIcon = true;
  String icon;
  String iconOrText;
  AppBarRightIcon(
      {this.pageNavigation,
      this.pageTitle,
      this.boxWidth,
      this.rightIcon,
      this.icon,
      this.iconOrText,
      this.bookedListTap});
  @override
  _AppBarRightIconState createState() => _AppBarRightIconState();
}

class _AppBarRightIconState extends State<AppBarRightIcon> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 15),
      width: screenWidth(context, dividedBy: 1),
      decoration: BoxDecoration(
          color: Constants.kitGradients[31],
          image: DecorationImage(
            image: AssetImage("assets/images/key_collection_bg.png"),
            fit: BoxFit.cover,
          )),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: screenWidth(context, dividedBy: 20),
          ),
          Text(
            widget.pageTitle,
            style: TextStyle(
                color: Constants.kitGradients[0],
                fontFamily: 'SFProText-SemiBold',
                fontSize: screenWidth(context, dividedBy: 23)),
          ),
          Spacer(
            flex: 1,
          ),
          GestureDetector(
              onTap: () {
                widget.bookedListTap();
              },
              child: widget.rightIcon == true
                  ? (widget.iconOrText == "text"
                      ? Text(
                          "Cancel",
                          style: TextStyle(
                            fontSize: screenWidth(context, dividedBy: 25),
                            color: Constants.kitGradients[0],
                            decoration: TextDecoration.underline,
                          ),
                        )
                      : Image(
                          image: AssetImage(widget.icon),
                          width: screenWidth(context, dividedBy: 7),
                        ))
                  : Container()),
        ],
      ),
    );
  }
}
