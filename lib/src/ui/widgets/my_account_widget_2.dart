import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class MyAccountWidget2 extends StatefulWidget {
  String title,icon;
  MyAccountWidget2({this.title,this.icon});
  @override
  _MyAccountWidget2State createState() => _MyAccountWidget2State();
}

class _MyAccountWidget2State extends State<MyAccountWidget2> {
  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        Container(
          width: screenWidth(context,dividedBy: 6),
          height: screenHeight(context,dividedBy: 11),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(35),
            color: Constants.kitGradients[6],
          ),
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: SvgPicture.asset(widget.icon,
            ),
          ),
        ),
        Text(
          widget.title,
          style: TextStyle(
              fontFamily: 'Muli',
              fontSize: 14,
              fontWeight: FontWeight.w300,
              color: Constants.kitGradients[0]),
        ),
      ],
    );
  }
}
