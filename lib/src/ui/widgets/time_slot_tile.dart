import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
class TimeSlotTile extends StatefulWidget {
  bool selected;
  bool deselected;
  String time;
  TimeSlotTile({this.selected,this.deselected,this.time});

  @override
  _TimeSlotTileState createState() => _TimeSlotTileState();
}

class _TimeSlotTileState extends State<TimeSlotTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context,dividedBy: 30),
      width: screenWidth(context,dividedBy: 5),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15),
          color:widget.deselected==false?widget.selected==true?Constants.kitGradients[12]:Colors.white:Colors.white,
          border:Border.all(color:widget.deselected==false?widget.selected==true?Constants.kitGradients[10]:Colors.white:Colors.white,
              width: 3)

      ),

      child: Center(
        child: Text(widget.time,style: TextStyle(
            color: widget.deselected==false?Colors.black:Colors.grey,
            fontFamily: "SFProText-Regular"
        ),),
      ),
    );
  }
}
