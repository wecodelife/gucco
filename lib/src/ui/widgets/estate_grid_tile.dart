import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class EstateTile extends StatefulWidget {
  String image;
  String text1;
  String text2;
  EstateTile({this.image, this.text1, this.text2});
  @override
  _EstateTileState createState() => _EstateTileState();
}

class _EstateTileState extends State<EstateTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 2),
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage(widget.image),
        fit: BoxFit.fill,
        colorFilter: new ColorFilter.mode(
            Colors.black.withOpacity(0.40), BlendMode.darken),
      )),
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            widget.text1,
            style: TextStyle(
                fontSize: 31,
                color: Constants.kitGradients[0],
                fontWeight: FontWeight.w600),
          ),
          Text(
            widget.text2,
            style: TextStyle(fontSize: 21, color: Constants.kitGradients[0]),
          )
        ],
      )),
    );
  }
}
