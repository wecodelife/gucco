import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';

class ShareWidget extends StatefulWidget {
  TextEditingController emailTextEditingController;
  ShareWidget({this.emailTextEditingController});

  @override
  _ShareWidgetState createState() => _ShareWidgetState();
}

class _ShareWidgetState extends State<ShareWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text('Share',
            style: TextStyle(
                fontFamily: 'SFProText-Regular',
                fontSize: 15,
                fontWeight: FontWeight.w600,
                color: Constants.kitGradients[7])),

        // Column(
        //  crossAxisAlignment: CrossAxisAlignment.start,
        //   children: [
        //     SizedBox(
        //       height: screenHeight(context, dividedBy: 50),
        //     ),
        //     Row(
        //       mainAxisAlignment: MainAxisAlignment.start,
        //       children: [
        //         Text('Please share this invitation’s details with:',
        //             style: TextStyle(
        //                 fontFamily: 'SFProText-Regular',
        //                 fontSize: 15,
        //                 color: Constants.kitGradients[7])),
        //       ],
        //     ),
        //     // SizedBox(
        //     //   height: screenHeight(context, dividedBy: 50),
        //     // ),
        //     TextField(
        //       controller: widget.emailTextEditingController,
        //       decoration: InputDecoration(
        //         hintText: 'Email Address',
        //         hintStyle: TextStyle(
        //             fontFamily: 'SFProText-Regular',
        //             fontSize: 15,
        //             color: Constants.kitGradients[4]
        //         )
        //       ),
        //     ),
        //     SizedBox(
        //       height: screenHeight(context, dividedBy: 50),
        //     ),
        //   ],
        // ),
      ],
    );
  }
}
