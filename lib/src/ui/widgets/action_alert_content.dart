import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class AlertBoxContent extends StatefulWidget {
  String heading;
  String content;
  bool cancelBox;
  String title;
  String text1;
  String text2;
  AlertBoxContent({this.heading,this.content,this.cancelBox,this.text2,this.text1,this.title});
  @override
  _AlertBoxContentState createState() => _AlertBoxContentState();
}

class _AlertBoxContentState extends State<AlertBoxContent> {
  @override
  Widget build(BuildContext context) {
    return widget.cancelBox==true?
    Container(
      // height: screenHeight(context,dividedBy: 10),
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(10))
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            height: screenHeight(context,dividedBy: 150),
          ),

          Row(
            crossAxisAlignment: CrossAxisAlignment.center,

            children: [


              SizedBox(
                width: screenWidth(context,dividedBy: 4),
              ),

              Text(widget.title,style: TextStyle(fontFamily: "sfProSemiBold",fontSize: 18),),
            ],
          ),
          SizedBox(
            height: screenHeight(context,dividedBy: 150),
          ),
          Row(
            children: [

              SizedBox(
                width: screenWidth(context,dividedBy: 20),
                child: Container(
                  color: Colors.white,
                ),
              ),
              Container(
                width: screenWidth(context,dividedBy: 1.2),
                child: Text(
                  // "If you have already made the deposit, you will receive it within 3 working days.  ",
                  widget.text1,
                  style: TextStyle(
                    color: Colors.black,
                    fontFamily: 'SFProText-Regular',
                    fontSize: 16,
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(
                width: screenWidth(context,dividedBy: 20),
              ),

            ],
          ),

          Text(
            // "To make another booking,head to, ",
            widget.text2,
            style: TextStyle(
                color: Colors.black,
                fontFamily: 'SFProText-Regular',
                fontSize: 16,
                fontWeight: FontWeight.w600
            ),
          ),
          SizedBox(
            height: screenHeight(context,dividedBy: 100),
          ),
          // Text(
          //   "Tap anywhere to close this pop up.",
          //   style: TextStyle(
          //     color: Colors.grey,
          //     fontFamily: 'SFProText-Regular',
          //     fontSize: 16,
          //   ),
          // ),



        ],
      ),

    )
      :Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          SizedBox(
            width: screenWidth(context,dividedBy: 20),
          ),

          Container(
              width: screenWidth(context,dividedBy: 2.5),
              child: Text(widget.heading,style: TextStyle(fontSize: 16,fontFamily: 'SFProText-Regular',color: Colors.black),)),



          Container(
              width: screenWidth(context,dividedBy: 2.4),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(widget.content,style: TextStyle(fontSize: 16,fontFamily: 'SFProText-Regular',color: Colors.black),),
                ],
              )),

          SizedBox(
            width: screenWidth(context,dividedBy: 20),
          ),

        ],

      ),
    );
  }
}
