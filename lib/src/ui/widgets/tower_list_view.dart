import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/tower_tile.dart';

class TowerListTile extends StatefulWidget {
  final int itemCount;
  final String unitNo;
  final String floorNo;
  final String name;
  final int index;
  final bool keyCollected;
  final String condoInfoId;
  final List<String> imgList;
  final Function onPressed;
  TowerListTile(
      {this.condoInfoId,
      this.keyCollected,
      this.floorNo,
      this.unitNo,
      this.itemCount,
      this.index,
      this.name,
      this.imgList,
      this.onPressed});
  @override
  _TowerListTileState createState() => _TowerListTileState();
}

class _TowerListTileState extends State<TowerListTile> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          widget.onPressed();
        },
        child: TowerTile(
          floorNo: widget.floorNo,
          unitNo: widget.unitNo,
          name: widget.name,
          keyCollected: widget.keyCollected,
        ));
  }
}
