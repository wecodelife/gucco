import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FacilitiesBody extends StatefulWidget {
  String title, description;
  FacilitiesBody({this.description, this.title});
  @override
  _FacilitiesBodyState createState() => _FacilitiesBodyState();
}

class _FacilitiesBodyState extends State<FacilitiesBody> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 25),
              vertical: screenHeight(context, dividedBy: 60)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                widget.title,
                style: TextStyle(
                    fontSize: 18,
                    color: Constants.kitGradients[7],
                    fontWeight: FontWeight.w600,
                    fontFamily: "SFProText-Regular"),
              ),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 25),
          ),
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            child: Text(
              widget.description,
              style: TextStyle(
                  height: 1.1,
                  fontSize: 15,
                  color: Constants.kitGradients[7],
                  fontFamily: "SFProText-Regular"),
            ),
          ),
        ),
      ],
    );
  }
}
