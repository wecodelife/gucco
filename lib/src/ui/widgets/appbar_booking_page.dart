import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class AppBarBookingPage extends StatefulWidget {
  String pageTitle;
  String rightIcon;
  String leftIcon;
  Function onTapLeftIcon, onTapRightIcon;
  bool fontBold;
  bool noRightIcon;
  bool transparent;
  AppBarBookingPage(
      {this.fontBold,
      this.pageTitle,
      this.rightIcon,
      this.leftIcon,
      this.onTapLeftIcon,
      this.onTapRightIcon,
      this.noRightIcon,
      this.transparent});
  @override
  _AppBarBookingPageState createState() => _AppBarBookingPageState();
}

class _AppBarBookingPageState extends State<AppBarBookingPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      color: widget.transparent != true
          ? Constants.kitGradients[7]
          : Colors.black.withOpacity(0.1),
      child: Row(
        children: [
          SizedBox(
            width: screenWidth(context, dividedBy: 40),
          ),
          GestureDetector(
              onTap: widget.onTapLeftIcon,
              child: Container(
                width: screenWidth(context, dividedBy: 20),
                height: screenHeight(context, dividedBy: 1),
                child: Image(
                  image: AssetImage(widget.leftIcon),
                  color: Colors.white,
                  fit: BoxFit.contain,
                ),
              )),
          SizedBox(
            width: screenWidth(context, dividedBy: 7),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1.8),
            child: Center(
              child: Text(
                widget.pageTitle,
                style: TextStyle(
                    color: Constants.kitGradients[0],
                    fontFamily: 'SFProDisplay-Medium',
                    fontSize: screenWidth(context, dividedBy: 24)),
              ),
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 7.3),
          ),
          widget.noRightIcon == true
              ? Container()
              : GestureDetector(
                  onTap: widget.onTapRightIcon,
                  child: Container(
                    width: screenWidth(context, dividedBy: 15),
                    height: screenHeight(context, dividedBy: 1),
                    child: Image(
                      image: AssetImage(
                        widget.rightIcon,
                      ),
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
          SizedBox(
            width: screenWidth(context, dividedBy: 60),
          )
        ],
      ),
    );
  }
}
