import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class MessageTile extends StatefulWidget {
  @override
  _MessageTileState createState() => _MessageTileState();
}

class _MessageTileState extends State<MessageTile> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context,dividedBy: 1),
      height: screenHeight(context,dividedBy: 8),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
              width: screenWidth(context,dividedBy:100),),

              Container(
                height: screenWidth(context,dividedBy: 6),
                width: screenWidth(context,dividedBy: 6),
                decoration: new BoxDecoration(
                  color: Constants.kitGradients[1],
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: Image.asset("assets/icons/message_icon.png",fit: BoxFit.fill,),
                ),
              ),
              Container(
                width: screenWidth(context,dividedBy: 1.4),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text("Martin Modern Admin",style: TextStyle(fontFamily: "SFProText-Regular",fontWeight: FontWeight.w600,fontSize: 18),),
                        Text("10.30 AM",style: TextStyle(fontFamily: "SFProText-Regular",fontWeight: FontWeight.w400,fontSize: 14),),

                      ],
                    ),

                    Text("RE: Help Request",style: TextStyle(fontFamily: "SFProText-Regular",fontSize: 16),),
                    Text("Hi Stormie, We appreciate that yo…",style: TextStyle(fontFamily: "SFProText-Regular",fontSize: 16),),

                  ],
                ),
              )
            ],
          ),
          SizedBox(height: screenHeight(context,dividedBy: 100),),
          Divider(height: 1,color: Constants.kitGradients[28],)
        ],
      ),
    );
  }
}
