import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/help_and_faq2_list_view.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

import 'help_and_faq_body2.dart';

class FAQCategories1 extends StatefulWidget {
  @override
  _FAQCategories1State createState() => _FAQCategories1State();
}

class _FAQCategories1State extends State<FAQCategories1> {
  ScrollController scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        controller: scrollController,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: screenWidth(context, dividedBy: 1),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: screenWidth(context, dividedBy: 1.1),
                    child: Text(
                      "About GuocoLand Privilege Club and Membership",
                      overflow: TextOverflow.visible,
                      style: TextStyle(
                          color: Constants.kitGradients[0],
                          fontSize: 20,
                          fontFamily: 'MuliSemiBold'),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 195),
            ),
            ListView.builder(
              itemCount: 5,
              controller: scrollController,
              scrollDirection: Axis.vertical,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index){
                return Padding(
                  padding:  EdgeInsets.symmetric(vertical: screenHeight(context,dividedBy: 70),),
                  child: HelpAndFAQListView(),
                );
                }
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 45),
            ),
          ],
        ),
      ),
    );
  }
}
