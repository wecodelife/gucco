import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/select_login_or_signup.dart';
import 'package:guoco_priv_app/src/ui/screens/new_home_screen.dart';
import 'package:guoco_priv_app/src/ui/widgets/home_drawer_item.dart';
import 'package:guoco_priv_app/src/ui/widgets/home_drawer_item_selected.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HomeDrawer extends StatefulWidget {
  final int count;
  final bool selected;
  HomeDrawer({this.count, this.selected});
  @override
  _HomeDrawerState createState() => _HomeDrawerState();
}

class _HomeDrawerState extends State<HomeDrawer> {
  int count;
  @override
  void initState() {
    count = widget.count;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Constants.kitGradients[27].withOpacity(.9),
      child: Column(
        children: [
          SizedBox(
            height: screenHeight(context, dividedBy: 5),
          ),
          GestureDetector(
            onTap: () {
              setState(() {
                count = 1;
              });
              pushAndReplacement(context, NewHomeScreen());
            },
            child: count == 1
                ? HomeDrawerItemSelected(
                    label: "HOME",
                  )
                : HomeDrawerItem(
                    label: "HOME",
                  ),
          ),
          GestureDetector(
              onTap: () {
                // setState(() {
                //   count = 2;
                // });
              },
              child: count == 2
                  ? HomeDrawerItemSelected(
                      label: "MY ACCOUNT",
                    )
                  : HomeDrawerItem(
                      label: "MY ACCOUNT - Coming Soon",
                    )),
          GestureDetector(
              onTap: () {
                // setState(() {
                //   count = 3;
                // });
              },
              child: count == 3
                  ? HomeDrawerItemSelected(
                      label: "FAVOURITES",
                    )
                  : HomeDrawerItem(
                      label: "FAVOURITES - Coming Soon",
                    )),
          GestureDetector(
              onTap: () {
                // setState(() {
                //   count = 4;
                // });
              },
              child: count == 4
                  ? HomeDrawerItemSelected(
                      label: "MAILBOX",
                    )
                  : HomeDrawerItem(
                      label: "MAILBOX - Coming Soon ",
                    )),
          GestureDetector(
              onTap: () {
                //   setState(() {
                //     count = 5;
                //   });
              },
              child: count == 5
                  ? HomeDrawerItemSelected(
                      label: "HELP & FAQ",
                    )
                  : HomeDrawerItem(
                      label: "HELP & FAQ - Coming Soon",
                    )),
          GestureDetector(
            onTap: () {
              setState(() {
                count = 6;
              });
              ObjectFactory().appHive.hiveClear(key: "token");

              pushAndRemoveUntil(context, SelectLoginOrSignUp(), false);
            },
            child: HomeDrawerItem(label: "LOG OUT"),
          ),
          GestureDetector(
            onTap: () {
              pop(context);
            },
            child: Container(
              height: screenWidth(context, dividedBy: 5),
              width: screenWidth(context, dividedBy: 5),
              child: Image.asset(
                "assets/icons/cancel_drawer_icon.png",
                fit: BoxFit.fill,
              ),
            ),
          )
        ],
      ),
    );
  }
}
