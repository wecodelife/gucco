import 'package:flutter/material.dart';

import '../../utils/constants.dart';
import '../../utils/utils.dart';

class Column2 extends StatefulWidget {
  String title;
  Column2({this.title});
  @override
  _Column2State createState() => _Column2State();
}

class _Column2State extends State<Column2> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 12),
      decoration: BoxDecoration(
        color: Constants.kitGradients[31],
        image: DecorationImage(
            image: AssetImage('assets/images/key_collection_bg.png'),
            fit: BoxFit.fill),
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 20),
        ),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            widget.title,
            style: TextStyle(
                fontFamily: 'SFProText-SemiBold',
                fontSize: screenWidth(context, dividedBy: 23),
                color: Constants.kitGradients[0]),
          ),
        ),
      ),
    );
  }
}
