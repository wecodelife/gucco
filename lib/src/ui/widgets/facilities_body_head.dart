import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FacilitiesBodyHead extends StatefulWidget {
  String title, value;
  FacilitiesBodyHead({this.title,this.value});

  @override
  _FacilitiesBodyHeadState createState() => _FacilitiesBodyHeadState();
}

class _FacilitiesBodyHeadState extends State<FacilitiesBodyHead> {
  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: EdgeInsets.only(
          left: screenWidth(context, dividedBy: 25),
          right: screenWidth(context, dividedBy: 25),
          top: screenHeight(context, dividedBy: 60)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            widget.title,
            style: TextStyle(
                fontSize: 15,
                color: Constants.kitGradients[7],
                fontFamily: "SFProText-Regular"),
          ),
          Text(
            widget.value,
            style: TextStyle(
                fontSize: 15,
                color: Constants.kitGradients[7],
                fontFamily: "SFProText-Regular"),
          ),
        ],
      ),
    );
  }
}
