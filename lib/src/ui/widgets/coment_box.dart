import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class CommentBox extends StatefulWidget {
  TextEditingController commentTextEditingController =
      new TextEditingController();
  double boxWidth;
  double boxHeight;
  double height;
  CommentBox({this.commentTextEditingController,this.boxWidth,this.boxHeight,this.height});
  @override
  _CommentBoxState createState() => _CommentBoxState();
}

class _CommentBoxState extends State<CommentBox> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text("Comments ",style: TextStyle(color:Colors.black,fontFamily: 'SFProText-Regular',fontSize: 16),),

            SizedBox(
              width: screenWidth(context,dividedBy: 1.7),
            ),
          ],
        ),

        SizedBox(
          height: screenHeight(context,dividedBy: widget.height),
        ),


        Row(children: [

          Container(
            width: screenWidth(context, dividedBy: widget.boxWidth),
            height: screenHeight(context, dividedBy:widget.boxHeight),
            child: TextField(
              controller: widget.commentTextEditingController,
              autofocus: false,
              style: TextStyle(
                color: Colors.grey,
                fontFamily: 'MuliSemiBold',
                fontSize: 18,
              ),
              maxLines: 8,
              cursorColor: Colors.grey,
              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderSide:
                      BorderSide(color: Colors.grey, style: BorderStyle.solid),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: Colors.grey,
                  ),
                ),
                hintStyle: TextStyle(
                  color: Colors.grey,
                  fontFamily: 'MuliSemiBold',
                  fontSize: 18,
                ),
              ),
            ),
          ),
        ]),
      ],
    );
  }
}
