import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class CommentsAdditionalInfo extends StatefulWidget {
  bool commentStatus;
  String comments;
  CommentsAdditionalInfo({this.commentStatus, this.comments});

  @override
  _CommentsAdditionalInfoState createState() => _CommentsAdditionalInfoState();
}

class _CommentsAdditionalInfoState extends State<CommentsAdditionalInfo> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 15),
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Comments / Additional Info ',
                    style: widget.commentStatus == true
                        ? TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[11])
                        : TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[7])),
                widget.commentStatus == true
                    ? SvgPicture.asset(
                        'assets/icons/arrow_upward.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[11],
                      )
                    : SvgPicture.asset(
                        'assets/icons/arrow_down.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[7],
                      ),
              ],
            ),
          ),
          onTap: () {
            widget.commentStatus == false
                ? setState(() {
                    widget.commentStatus = true;
                  })
                : setState(() {
                    widget.commentStatus = false;
                  });
          },
        ),
        widget.commentStatus == true
            ? Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Comments:',
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                      Text(widget.comments ?? "-",
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 55),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }
}
