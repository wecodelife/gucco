import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HomeDrawerItem extends StatefulWidget {
  final String label;
  HomeDrawerItem({this.label});
  @override
  _HomeDrawerItemState createState() => _HomeDrawerItemState();
}

class _HomeDrawerItemState extends State<HomeDrawerItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: [
            Text(
              widget.label,
              style: TextStyle(
                  fontFamily: "Muli",
                  fontWeight: FontWeight.w400,
                  fontSize: screenWidth(context, dividedBy: 21),
                  color: widget.label == "HOME" || widget.label == "LOG OUT"
                      ? Constants.kitGradients[0]
                      : Constants.kitGradients[0].withOpacity(0.2)),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 15),
            ),
          ],
        ),
      ),
    );
  }
}
