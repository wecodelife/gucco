import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class BottomBar extends StatefulWidget {
  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context,dividedBy: 30),
      width: screenWidth(context,dividedBy: 1),

      color: Color(0xFFF8F8F8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              SizedBox(
                height: screenHeight(context,dividedBy:60 ),
              ),

              Container(
                  height: screenHeight(context,dividedBy: 100),
                  width: screenWidth(context,dividedBy: 2.5),

                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(5),),
                    color: Colors.black,
                  )
              ),
            ],
          ),
        ],
      ),
    );
  }
}
