import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class CheckBoxListBookings extends StatefulWidget {
  @override
  _CheckBoxListBookingsState createState() => _CheckBoxListBookingsState();
}

class _CheckBoxListBookingsState extends State<CheckBoxListBookings> {
  bool checked=false;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
          onTap: (){
            setState(() {
              checked=!checked;
            });

          },
          child: Container(
                  height: screenHeight(context,dividedBy: 25),
                  width: screenWidth(context,dividedBy: 12),

              decoration: BoxDecoration(
                color: Constants.kitGradients[23],
                borderRadius: BorderRadius.circular(10)
              ),
              child: Icon( checked==true?Icons.radio_button_unchecked:Icons.radio_button_checked,color:Colors.blue,size:20))),
    );
  }
}
