import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class InviteVisitorsBody3Question extends StatefulWidget {
  final String question, option1, option2;
  bool accessAnswer;
  ValueChanged valueChanged;

  InviteVisitorsBody3Question(
      {this.option1, this.option2, this.question, this.valueChanged});

  @override
  _InviteVisitorsBody3QuestionState createState() =>
      _InviteVisitorsBody3QuestionState();
}

class _InviteVisitorsBody3QuestionState
    extends State<InviteVisitorsBody3Question> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 25),
        ),
        Text(
          widget.question,
          style: TextStyle(
              color: Colors.black,
              fontFamily: 'SFProText-Regular',
              fontSize: 18),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 45),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            GestureDetector(
              child: Container(
                width: 16,
                height: 16,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Constants.kitGradients[0],
                    border: Border.all(
                        color: Constants.kitGradients[11], width: 0.5)),
                child: widget.accessAnswer == true
                    ? Padding(
                        padding: const EdgeInsets.all(3.0),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Constants.kitGradients[11],
                              shape: BoxShape.circle),
                        ),
                      )
                    : Container(),
              ),
              onTap: () {
                setState(() {
                  widget.accessAnswer = true;
                });
                widget.valueChanged(widget.accessAnswer);
                print("Access answer is " + widget.accessAnswer.toString());
              },
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 20),
            ),
            Text(
              widget.option1,
              style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'SFProText-Regular',
                  fontSize: 18),
            ),
          ],
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 45),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            GestureDetector(
              child: Container(
                width: 16,
                height: 16,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Constants.kitGradients[0],
                    border: Border.all(
                        color: Constants.kitGradients[11], width: 0.5)),
                child: widget.accessAnswer == false
                    ? Padding(
                        padding: const EdgeInsets.all(3.0),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Constants.kitGradients[11],
                              shape: BoxShape.circle),
                        ),
                      )
                    : Container(),
              ),
              onTap: () {
                setState(() {
                  widget.accessAnswer = false;
                });
                widget.valueChanged(widget.accessAnswer);
                print("Access answer is " + widget.accessAnswer.toString());
              },
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 20),
            ),
            Text(
              widget.option2,
              style: TextStyle(
                  color: Colors.black,
                  fontFamily: 'SFProText-Regular',
                  fontSize: 18),
            ),
          ],
        )
      ],
    );
  }
}
