import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg_provider/flutter_svg_provider.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class EstateListTile extends StatefulWidget {
  String image;
  String text1;
  String text2;
  EstateListTile({this.image, this.text1, this.text2});
  @override
  _EstateListTileState createState() => _EstateListTileState();
}

class _EstateListTileState extends State<EstateListTile> {
  @override
  Widget build(BuildContext context) {
    return new Container(
      width: screenWidth(context, dividedBy: 2),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
          image: DecorationImage(
            image: Svg(
              widget.image,
            ),
            fit: BoxFit.cover,
            colorFilter: new ColorFilter.mode(
                Colors.black.withOpacity(0.40), BlendMode.darken),
          )),
      child: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            widget.text1,
            style: TextStyle(
                fontSize: screenWidth(context, dividedBy: 20),
                color: Constants.kitGradients[0],
                fontWeight: FontWeight.w600,
                fontFamily: "SFProText-Medium"),
          ),
          Text(
            widget.text2,
            style: TextStyle(
                fontSize: screenWidth(context, dividedBy: 23),
                color: Constants.kitGradients[0],
                fontFamily: "SFProText-Medium"),
          )
        ],
      )),
    );
  }
}
