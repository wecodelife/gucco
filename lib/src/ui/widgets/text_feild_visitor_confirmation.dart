import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class TextFieldVisitorConfirmation extends StatefulWidget {
  String hintText;
  bool labelEnabled;
  TextEditingController textEditingController;
  TextFieldVisitorConfirmation({this.hintText, this.textEditingController,this.labelEnabled});
  @override
  _TextFieldVisitorConfirmationState createState() => _TextFieldVisitorConfirmationState();
}

class _TextFieldVisitorConfirmationState extends State<TextFieldVisitorConfirmation> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [

        SizedBox(
          width: screenWidth(context,dividedBy: 20),
        ),
        Container(

          height: screenHeight(context,dividedBy: 10),
          width: screenWidth(context,dividedBy: 1.13),
          child: TextField(
            controller: widget.textEditingController,
            autofocus: false,
            style: TextStyle(
              color: Colors.grey,
              fontFamily: 'MuliSemiBold',
              fontSize: 16,),

            cursorColor: Colors.grey,
            decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color:Colors.grey,width: 2),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey,width: 2),
              ),
              hintText:widget.hintText,
              contentPadding: EdgeInsets.symmetric(horizontal:screenWidth(context,dividedBy:30 )),
              hintStyle: TextStyle(
                color: Colors.grey,
                fontFamily: 'MuliSemiBold',
                fontSize: 16,
                fontWeight: FontWeight.w500
              ),
            ),
          ),
        ),
      ],
    );
  }
}
