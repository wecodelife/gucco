import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/upload_files.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
class UploadFilesEAppForm extends StatefulWidget {
  ValueChanged onChanged;
  UploadFilesEAppForm({this.onChanged});
  @override
  _UploadFilesEAppFormState createState() => _UploadFilesEAppFormState();
}

class _UploadFilesEAppFormState extends State<UploadFilesEAppForm> {

  bool imagePicked=false;
  String imageName;
  void uploadImages() async{

    FilePickerResult result = await FilePicker.platform.pickFiles (
    type: FileType.custom,
    allowedExtensions: ['jpg', 'pdf']
    );

    if(result != null) {
      File file = File(result.files.single.path);
      setState(() {
        imagePicked=true;
        imageName= file.toString().split("/").last.split("'").first;

      });
      print(file.toString());
    } else {
      print("Image not Picked");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            RichText(text: TextSpan(text: "Upload From (PDF or JPG only)",style:TextStyle(color: Colors.black,fontSize: screenWidth(context,dividedBy: 26),fontFamily: "SFProText-Regular",


            ),
                children: [
                  TextSpan(text: "*",style: TextStyle(color:Constants.kitGradients[15],fontSize: screenWidth(context,dividedBy: 26))),
                ]
            ),
            ),
            SizedBox(
              height: screenHeight(context,dividedBy: 50),
            ),

            imagePicked==false?Text("No file uploaded yet:",style:TextStyle(color:Constants.kitGradients[19],
              fontSize: 16,fontFamily: "SFProText-Regular",),):Text(imageName.toString(),style:TextStyle(color:Constants.kitGradients[19],
              fontSize: screenWidth(context,dividedBy: 25),fontFamily: "SFProText-Regular",),)
          ],
        ),

        SizedBox(
          width:screenWidth(context,dividedBy: 15) ,
        ),

        UploadFiles(buttonTitle: "Upload",onTap: (){
          uploadImages();


          },),
      ],
    );
  }
}
