import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FormFieldChangePasswordBox extends StatefulWidget {
  String label;
  TextEditingController textEditingController;
  Function onChanged;
  FormFieldChangePasswordBox(
      {this.label, this.textEditingController, this.onChanged});
  @override
  _FormFieldChangePasswordBoxState createState() =>
      _FormFieldChangePasswordBoxState();
}

class _FormFieldChangePasswordBoxState
    extends State<FormFieldChangePasswordBox> {
  bool visibility;
  @override
  void initState() {
    visibility = false;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: screenHeight(context, dividedBy: 20),
          child: Padding(
            padding: EdgeInsets.symmetric(
              horizontal: screenWidth(context, dividedBy: 10),
            ),
            child: TextField(
              controller: widget.textEditingController,
              onChanged: (val) {
                widget.onChanged();
              },
              style: TextStyle(
                  color: Colors.black, fontFamily: 'MulSemiBold', fontSize: 16),
              autofocus: false,
              obscureText: false,
              cursorColor: Colors.black,
              decoration: InputDecoration(
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.black),
                ),
                hintStyle: TextStyle(
                  color: Constants.kitGradients[0],
                  fontFamily: 'MuliSemiBold',
                  fontSize: 16,
                ),
                labelText: widget.label,
                // suffixIcon: GestureDetector(
                //   onTap: () {
                //     setState(() {
                //       visibility = !visibility;
                //       print(visibility);
                //     });
                //   },
                //   child: visibility == false
                //       ? Icon(
                //           Icons.visibility_off_outlined,
                //           color: Colors.grey,
                //         )
                //       : Icon(
                //           Icons.visibility_off_outlined,
                //           color: Colors.grey,
                //         ),
                // ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
