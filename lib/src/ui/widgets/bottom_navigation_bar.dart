import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/home_screen.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/bottom_navigator_bar_icon.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class CustomBottomNavigationBar extends StatefulWidget {
  String value;
  CustomBottomNavigationBar({this.value});
  @override
  _CustomBottomNavigationBarState createState() =>
      _CustomBottomNavigationBarState();
}

class _CustomBottomNavigationBarState extends State<CustomBottomNavigationBar> {
  bool home;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return
        // ClipPath(
        // clipper: MyCustomClipper(),
        // child:
        Container(
      height: screenHeight(context, dividedBy: 11),
      width: screenWidth(context, dividedBy: 1),
      color: Constants.kitGradients[1],
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            BottomNavigationIcon(
              value: widget.value == "home" ? true : false,
              label: "Home",
              selectedIcon: "assets/icons/home_selected.svg",
              unselectedIcon: "assets/icons/home.svg",
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => HomeScreen()),
                );
                setState(() {
                  widget.value = "home";
                });
              },
            ),
            BottomNavigationIcon(
              value: widget.value == "discover" ? true : false,
              unselectedIcon: "assets/icons/discover.svg",
              selectedIcon: "assets/icons/discover_selected.svg",
              label: "Discover",
              onPressed: () {
                setState(() {
                  widget.value = "discover";
                });
              },
            ),
            BottomNavigationIcon(
              unselectedIcon: "assets/icons/smart_card.svg",
              label: "Smart Card",
              onPressed: () {},
            ),
            BottomNavigationIcon(
              unselectedIcon: "assets/icons/estate.svg",
              label: "My Estate",
              onPressed: () {
                push(context, EstatePage());
              },
            ),
            BottomNavigationIcon(
              unselectedIcon: "assets/icons/privileges.svg",
              label: "Privileges",
              onPressed: () {},
            ),
          ],
        ),
      ),
    )
        // )
        ;
  }
}
