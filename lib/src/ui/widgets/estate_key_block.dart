import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/key_collection/key_collection_appointments_reschedule.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class EstateKeyBlock extends StatefulWidget {
  final String unitId;
  final String blockName;
  final String floorAndUnitId;
  final bool collectionPaymentStatus;
  EstateKeyBlock(
      {this.unitId,
      this.blockName,
      this.floorAndUnitId,
      this.collectionPaymentStatus});
  @override
  _EstateKeyBlockState createState() => _EstateKeyBlockState();
}

class _EstateKeyBlockState extends State<EstateKeyBlock> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => KeyCollectionAppointmentsReschedule(
                        unitId: widget.unitId,
                        blockName: widget.blockName,
                        floorAndUnitId: widget.floorAndUnitId,
                        collectionPaymentReschedule:
                            widget.collectionPaymentStatus,
                      )));
        },
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Container(
            height: screenHeight(context, dividedBy: 4),
            width: screenWidth(context, dividedBy: 1),
            decoration: BoxDecoration(
                color: Constants.kitGradients[33],
                image: DecorationImage(
                  image: AssetImage(
                    "assets/images/key_collection_bg.png",
                  ),
                  fit: BoxFit.fill,
                  colorFilter: new ColorFilter.mode(
                      Colors.white.withOpacity(0.10), BlendMode.darken),
                )),
            child: Center(
              child: Text(
                "          KEY \n"
                "COLLECTION",
                style: TextStyle(
                    fontSize: 26,
                    color: Constants.kitGradients[0],
                    fontFamily: "SFProDisplay-Medium"),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
