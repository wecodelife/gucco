import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/fault_reporting_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/e_application_button.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ListItemBody extends StatefulWidget {
  final String title, status, dateTime, button1Name, button2Name;
  final Color statusTextColor;

  ListItemBody(
      {this.title,
        this.status,
        this.button1Name,
        this.button2Name,
        this.dateTime,
        this.statusTextColor,});

  @override
  _ListItemBodyState createState() => _ListItemBodyState();
}

class _ListItemBodyState extends State<ListItemBody> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1.19),
      height: screenHeight(context, dividedBy: 4.7),
      //color: Colors.red,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              // SizedBox(
              //   width: screenWidth(context, dividedBy: 30),
              // ),
              Text(
                widget.title,
                style: TextStyle(
                    fontSize: 17,
                    color: Constants.kitGradients[7],
                    fontFamily: "sfProRegular"),
              ),

              Text(
                widget.status,
                style: TextStyle(
                    fontSize: 17,
                    color: widget.statusTextColor,
                    fontFamily: "sfProRegular"),
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 40),
          ),
          Container(
            color: Constants.kitGradients[24],
            child: Column(
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: 0.5,
                  color: Constants.kitGradients[22],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 30),
                    ),
                    Text(
                      widget.dateTime,
                      style: TextStyle(
                          fontSize: 15,
                          color: Constants.kitGradients[7],
                          fontFamily: "sfProRegular"),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 30),
                    ),
                    EApplicationButton(
                      buttonName: widget.status == "In Progress"
                          ? widget.button1Name
                          : "Details",
                      buttonColor: Constants.kitGradients[0],
                      onPressed: (){
                      },
                      buttonNameColor: Constants.kitGradients[11],
                      buttonWidth: widget.status == "In Progress"
                          ? screenWidth(context, dividedBy: 6)
                          : screenWidth(context, dividedBy: 1.47),
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 50),
                    ),
                    widget.status == "In Progress"
                        ? EApplicationButton(
                      buttonName: widget.button2Name,
                      buttonNameColor: Constants.kitGradients[0],
                      buttonColor: Constants.kitGradients[11],
                      buttonWidth: screenWidth(context, dividedBy: 2.5),
                      onPressed: (){

                      },
                    )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: 0.5,
                  color: Constants.kitGradients[22],
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),
        ],
      ),
    );
  }
}
