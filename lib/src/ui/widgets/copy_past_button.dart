import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class BorderColorGoldButton extends StatefulWidget {
  String heading;
  Function onTap;
  bool borderTrue;
  double boxWidth;
  BorderColorGoldButton({this.boxWidth,this.borderTrue,this.heading,this.onTap});
  @override
  _BorderColorGoldButtonState createState() => _BorderColorGoldButtonState();
}

class _BorderColorGoldButtonState extends State<BorderColorGoldButton> {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      padding: EdgeInsets.only(),
      onPressed: widget.onTap,
      color: widget.borderTrue==true?Colors.white:Constants.kitGradients[13],
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30),
          side: BorderSide(
            color: Constants.kitGradients[13],
            width: 3,
          )),
      child: Column(
        children: [

          SizedBox(
            height: screenHeight(context,dividedBy: 60),
          ),
          Row(children: [
            SizedBox(
              width: screenWidth(context, dividedBy: widget.boxWidth),
            ),
            Text(
              widget.heading,
              style: TextStyle(
                  color:widget.borderTrue==true?Constants.kitGradients[13]:Colors.white,
                  fontFamily: "SFProText-Regular",
                  fontSize: 18),
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: widget.boxWidth),
            ),
          ]),

          SizedBox(
            height: screenHeight(context,dividedBy: 60),
          ),
        ],
      ),
    );
  }
}
