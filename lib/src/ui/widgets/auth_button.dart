import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class AuthButton extends StatefulWidget {
  Color buttonColor;
  String mediaSymbol;
  String mediaName;
  double boxWidth;
  Function onPressed;
  AuthButton({this.boxWidth,this.buttonColor,this.mediaName,this.mediaSymbol,this.onPressed});
  @override
  _AuthButtonState createState() => _AuthButtonState();
}

class _AuthButtonState extends State<AuthButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
         width: screenWidth(context,dividedBy:widget.boxWidth),
        height: screenHeight(context,dividedBy: 20),
        child: RaisedButton(
          padding: EdgeInsets.only(),
          color: widget.buttonColor,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(3)),
          onPressed: () {
            widget.onPressed();
          },
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SvgPicture.asset(widget.mediaSymbol,width: screenWidth(context,dividedBy:17 ),),
                Text(
                  widget.mediaName,
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontFamily: 'MuliBold',
                      fontSize: screenWidth(context,dividedBy: 29),),
                  overflow: TextOverflow.clip,
                ),
              ],
            ),
          ),
        ));;
  }
}
