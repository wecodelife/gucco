import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/application_list_item_body.dart';
import 'package:guoco_priv_app/src/ui/widgets/fault_report_body1.dart';
import 'package:guoco_priv_app/src/ui/widgets/fault_report_check_box.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class EapplicationListItem extends StatefulWidget {
  @override
  _EapplicationListItemState createState() => _EapplicationListItemState();
}

class _EapplicationListItemState extends State<EapplicationListItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 4.1),
      //color: Colors.yellow,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 35),
              ),
              FaultReportCheck(
                boxColor: Constants.kitGradients[20],
                boxBorderColor: Constants.kitGradients[21],
                onTap: () {
                  //do something here
                },
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 35),
              ),
              ListItemBody(
                title: "Second Car Registration",
                status: "In Progress",
                statusTextColor: Constants.kitGradients[21],
                dateTime: "05-Apr-2020  |  10:32AM",
                button1Name: "Cancel",
                button2Name: "Details",
              ),
            ],
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 2,
            color: Constants.kitGradients[24],
          ),
        ],
      ),
    );
  }
}
