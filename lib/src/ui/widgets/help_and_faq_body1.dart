import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HelpAndFAQBody1 extends StatefulWidget {
  final String title;
  final Function onTap;
  HelpAndFAQBody1({this.title, this.onTap});
  @override
  _HelpAndFAQBody1State createState() => _HelpAndFAQBody1State();
}

class _HelpAndFAQBody1State extends State<HelpAndFAQBody1> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: screenWidth(context,dividedBy: 1.0),
        height: screenHeight(context,dividedBy: 8),
        decoration: BoxDecoration(
            color: Constants.kitGradients[1],
            borderRadius: BorderRadius.circular(5.0)
        ),
        child: Padding(
          padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width:screenWidth(context,dividedBy: 1.3),
                child: Text(
                  widget.title,
                  overflow: TextOverflow.visible,
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontSize: 18,
                      fontFamily: 'MuliBold'),
                ),
              ),
              Icon(Icons.arrow_forward_ios_rounded,color: Constants.kitGradients[0],)
            ],
          ),
        ),
      ),
      onTap: widget.onTap,
    );
  }
}
