import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FaultBookingTabBar extends StatefulWidget {
  bool clickTabStatus;
  final String title, imagePath;
  ValueChanged clickStatusValue;

  FaultBookingTabBar(
      {this.clickTabStatus, this.title, this.imagePath, this.clickStatusValue});

  @override
  _FaultBookingTabBarState createState() => _FaultBookingTabBarState();
}

class _FaultBookingTabBarState extends State<FaultBookingTabBar> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 2),
            height: screenHeight(context, dividedBy: 13),
            decoration: widget.clickTabStatus == true
                ? BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                        color: Constants.kitGradients[4], width: 0.01))
                : BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(widget.imagePath), fit: BoxFit.fill),
                    border: Border.all(
                        color: Constants.kitGradients[4], width: 0.01)),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.title,
                    style: TextStyle(
                        color: widget.clickTabStatus==true?Constants.kitGradients[7]:Colors.white,
                        fontFamily: widget.clickTabStatus==true?'SFProText-Regular':'SFProText-SemiBold',
                        fontSize: 13),
                  ),
                  widget.clickTabStatus == true
                      ? Container(
                          height: 3,
                          width: screenWidth(context, dividedBy: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(1.5),
                            color: Constants.kitGradients[11],
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
          onTap: () {
            setState(() {
              widget.clickTabStatus = true;
            });
            widget.clickStatusValue(widget.clickTabStatus);
          },
        ),
        GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 2),
            height: screenHeight(context, dividedBy: 13),
            decoration: widget.clickTabStatus == true
                ? BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage(
                            'assets/images/key_collection_background.png'),
                        fit: BoxFit.fill),
                    border: Border.all(
                        color: Constants.kitGradients[4], width: 0.01))
                : BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                        color: Constants.kitGradients[4], width: 0.01)),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "COMPLETED",
                    style: TextStyle(
                        color: widget.clickTabStatus==false?Constants.kitGradients[7]:Colors.white,
                        fontFamily: widget.clickTabStatus==false?'SFProText-Regular':'SFProText-SemiBold',
                        fontSize: 13),
                  ),
                  widget.clickTabStatus == false
                      ? Container(
                          height: 3,
                          width: screenWidth(context, dividedBy: 10),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(1.5),
                            color: Constants.kitGradients[11],
                          ),
                        )
                      : Container(),
                ],
              ),
            ),
          ),
          onTap: () {
            setState(() {
              widget.clickTabStatus = false;
            });
            widget.clickStatusValue(widget.clickTabStatus);
          },
        ),
      ],
    );
  }
}
