import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/screens/new_home_screen.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class EstateBottomBar extends StatefulWidget {
  String pageValue;
  String iconRight;
  String iconLeft;
  String iconCenter;
  String titleRight;
  String titleLeft;
  String titleCenter;
  final bool logoTrue;
  bool bottomBarBlack;
  bool bottomBarGold;
  bool divider;
  Function pageNavigationRightIcon;
  Function pageNavigationLeftIcon;
  EstateBottomBar(
      {this.pageValue,
      this.logoTrue,
      this.iconLeft,
      this.iconRight,
      this.iconCenter,
      this.titleCenter,
      this.titleLeft,
      this.divider,
      this.titleRight,
      this.bottomBarBlack,
      this.pageNavigationRightIcon,
      this.pageNavigationLeftIcon,
      this.bottomBarGold});
  @override
  _EstateBottomBarState createState() => _EstateBottomBarState();
}

class _EstateBottomBarState extends State<EstateBottomBar> {
  bool value1 = false;
  bool value2;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 13),
      width: screenWidth(context, dividedBy: 1),
      color: widget.bottomBarBlack == true
          ? Colors.black
          : widget.bottomBarGold == true
              ? Constants.kitGradients[1]
              : Constants.kitGradients[17],
      child: widget.pageValue == "key" || widget.pageValue == "postmovein"
          ? Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: screenHeight(context, dividedBy: 10),
                  width: screenWidth(context, dividedBy: 2.1),
                  child: Center(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              widget.pageNavigationLeftIcon();
                              setState(() {
                                value1 = true;
                                value2 = false;
                              });
                            },
                            child: Column(
                              children: [
                                Container(
                                    height:
                                        screenHeight(context, dividedBy: 22),
                                    width: screenWidth(context, dividedBy: 5),
                                    child: Image.asset(widget.iconLeft,
                                        fit: BoxFit.contain,
                                        color: widget.bottomBarGold == true
                                            ? value1 == true
                                                ? Colors.white
                                                : Colors.white
                                            : value1 == true
                                                ? Constants.kitGradients[1]
                                                : Colors.white)),
                                widget.logoTrue != true
                                    ? Text(
                                        widget.titleLeft,
                                        style: TextStyle(
                                            fontSize: 13,
                                            color: Constants.kitGradients[0],
                                            fontFamily: "SFProDisplay-Medium"),
                                      )
                                    : Container()
                              ],
                            ),
                          ),
                        ]),
                  ),
                ),
                widget.divider == false
                    ? Container()
                    : Container(
                        height: screenHeight(context, dividedBy: 15),
                        width: screenWidth(context, dividedBy: 200),
                        color: Colors.grey.withOpacity(.60),
                      ),
                Container(
                  height: screenHeight(context, dividedBy: 10),
                  width: screenWidth(context, dividedBy: 2.1),
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            widget.pageNavigationRightIcon();
                            setState(() {
                              value2 = true;
                              value1 = false;
                            });
                          },
                          child: Column(
                            children: [
                              Container(
                                  height: screenHeight(context, dividedBy: 22),
                                  width: screenWidth(context, dividedBy: 13),
                                  child: SvgPicture.asset(
                                    widget.iconRight,
                                    fit: BoxFit.contain,
                                    color: value2 == true
                                        ? widget.bottomBarGold == true
                                            ? Colors.white
                                            : Constants.kitGradients[1]
                                        : Colors.white,
                                  )),
                              Text(
                                widget.titleRight,
                                style: TextStyle(
                                    fontSize: 13,
                                    color: Colors.white,
                                    fontFamily: "SFProDisplay-Medium"),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            )
          : Row(
              children: [
                GestureDetector(
                  onTap: () {
                    pushAndRemoveUntil(context, NewHomeScreen(), false);
                  },
                  child: Container(
                    width: screenWidth(context, dividedBy: 1),
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                              height: screenHeight(context, dividedBy: 22),
                              width: screenWidth(context, dividedBy: 4),
                              child: Image.asset(
                                "assets/images/privilege_logo.png",
                                fit: BoxFit.contain,
                              )),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
    );
  }
}
