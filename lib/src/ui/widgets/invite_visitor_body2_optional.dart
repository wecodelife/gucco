import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

import 'inviteVisitorsBody3A.dart';

class InviteVisitorBody2Optional extends StatefulWidget {
  bool optionalFieldStatus;
  ValueChanged valueChanged;
  String title;

  InviteVisitorBody2Optional(
      {this.optionalFieldStatus, this.valueChanged, this.title});

  @override
  _InviteVisitorBody2OptionalState createState() =>
      _InviteVisitorBody2OptionalState();
}

class _InviteVisitorBody2OptionalState
    extends State<InviteVisitorBody2Optional> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 15),
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(widget.title,
                    style: widget.optionalFieldStatus == false
                        ? TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 17,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[11])
                        : TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 17,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[7])),
                widget.optionalFieldStatus == false
                    ? SvgPicture.asset(
                        'assets/icons/arrow_upward.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[11],
                      )
                    : SvgPicture.asset(
                        'assets/icons/arrow_down.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[7],
                      ),
              ],
            ),
          ),
          onTap: () {
            // widget.optionalFieldStatus == false
            //     ? setState(() {
            //         widget.optionalFieldStatus = true;
            //       })
            //     : setState(() {
            //         widget.optionalFieldStatus = false;
            //       });
            setState(() {
              widget.optionalFieldStatus = false;
            });
            widget.valueChanged(widget.optionalFieldStatus);

          },
        ),
        widget.optionalFieldStatus == false
            ? InviteVisitorsBodyOptional()
            : Container(),
      ],
    );
  }
}
