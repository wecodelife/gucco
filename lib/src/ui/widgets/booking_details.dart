import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:intl/intl.dart';

class BookingDetailsWidget extends StatefulWidget {
  bool bookingDetailsStatus;
  String date;
  String earliestStartTime;
  String selectedStartTime;
  bool reschedule;
  DateTime createDate;
  DateTime bookedDate;
  BookingDetailsWidget(
      {this.bookingDetailsStatus,
      this.bookedDate,
      this.date,
      this.selectedStartTime,
      this.earliestStartTime,
      this.reschedule,
      this.createDate});
  @override
  _BookingDetailsWidgetState createState() => _BookingDetailsWidgetState();
}

class _BookingDetailsWidgetState extends State<BookingDetailsWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 15),
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Booking Details',
                    style: widget.bookingDetailsStatus == true
                        ? TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[11])
                        : TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[7])),
                widget.bookingDetailsStatus == true
                    ? SvgPicture.asset(
                        'assets/icons/arrow_upward.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[11],
                      )
                    : SvgPicture.asset(
                        'assets/icons/arrow_down.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[7],
                      ),
              ],
            ),
          ),
          onTap: () {
            widget.bookingDetailsStatus == false
                ? setState(() {
                    widget.bookingDetailsStatus = true;
                  })
                : setState(() {
                    widget.bookingDetailsStatus = false;
                  });
          },
        ),
        widget.bookingDetailsStatus == true
            ? Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Appoinment Date',
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                      Text(
                          widget.date.trim() != "null"
                              ? widget.date
                              : new DateFormat('dd-MMM-yyyy').format(
                                  DateTime.parse(DateTime.now().toString())),
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 55),
                  ),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Appointment Time',
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                      Text(time(widget.selectedStartTime),
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  // RichText(textAlign: TextAlign.center,
                  //   text: TextSpan(
                  //       text: "This booking was made on ",
                  //       style: TextStyle(fontFamily: "SFProText-Regular",fontSize: 15,color: Colors.black),
                  //       children: [
                  //         TextSpan(
                  //             text: new DateFormat('dd-MMM-yyyy, h:mma').format(widget.bookedDate),
                  //             style:TextStyle(fontFamily: "SFProText-SemiBold",fontSize: 15,fontWeight: FontWeight.w600)
                  //         ),
                  //
                  //       ]
                  //   ),
                  // ),
                  //
                  // SizedBox(
                  //   height: screenHeight(context, dividedBy: 50),
                  // ),
                  // RichText(textAlign: TextAlign.center,
                  //   text: TextSpan(
                  //       text: "At time of booking, the earliest slot available was\n",
                  //       style: TextStyle(fontFamily: "SFProText-Regular",fontSize: 15,color: Colors.black),
                  //       children: [
                  //         TextSpan(
                  //             text: new DateFormat('dd-MMM-yyyy').format(DateTime.parse(widget.date.trim()!="null"?widget.date:DateTime.now().toString()))+", "+time(widget.earliestStartTime),
                  //             style:TextStyle(fontFamily: "SFProText-SemiBold",fontSize: 15,fontWeight: FontWeight.w600)
                  //         ),
                  //
                  //       ]
                  //   ),
                  // ),
                  //
                  // SizedBox(
                  //   height: screenHeight(context, dividedBy: 30),
                  // ),
                  if (widget.reschedule == true)
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(
                          text: "Your first appointment booking was made on\n",
                          style: TextStyle(
                              fontFamily: "SFProText-Regular",
                              fontSize: 15,
                              color: Colors.black),
                          children: [
                            TextSpan(
                                text: new DateFormat('dd-MMM-yyyy, h:mma')
                                    .format(widget.createDate),
                                style: TextStyle(
                                    fontFamily: "SFProText-SemiBold",
                                    fontSize: 15,
                                    fontWeight: FontWeight.w600)),
                          ]),
                    ),

                  SizedBox(
                    height: screenHeight(context, dividedBy: 50),
                  ),
                  // if(widget.reschedule==true)
                  // RichText(textAlign: TextAlign.center,
                  //   text: TextSpan(
                  //       text: "At time of booking, the earliest slot available was\n",
                  //       style: TextStyle(fontFamily: "SFProText-Regular",fontSize: 15,color: Colors.black),
                  //       children: [
                  //         TextSpan(
                  //             text: new DateFormat('dd-MMM-yyyy').format(DateTime.parse(widget.date.trim()!="null"?widget.date:DateTime.now().toString()))+", "+time(widget.earliestStartTime),
                  //             style:TextStyle(fontFamily: "SFProText-SemiBold",fontSize: 15,fontWeight: FontWeight.w600)
                  //         ),
                  //
                  //       ]
                  //   ),
                  // ),
                ],
              )
            : Container(),
      ],
    );
  }
}
