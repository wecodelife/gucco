// import 'package:flutter/material.dart';
// import 'package:guoco_priv_app/src/utils/constants.dart';
// import 'package:guoco_priv_app/src/utils/utils.dart';
//
// class EstateTab extends StatefulWidget {
//   String label;
//   String tabValue;
//   EstateTab({this.label,this.tabValue});
//   @override
//   _EstateTabState createState() => _EstateTabState();
// }
//
// class _EstateTabState extends State<EstateTab> {
//   String value;
//   @override
//   Widget build(BuildContext context) {
//     return
//       GestureDetector(
//         child: Container(
//           width: screenWidth(context,dividedBy: 2),
//           height: screenHeight(context,dividedBy: 15),
//           child: Center(child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: [
//               Text(widget.label,style:TextStyle(fontSize: 13,color: Constants.kitGradients[0]),),
//               SizedBox(
//                 height: screenHeight(context,dividedBy: 100) ,
//               ),
//               widget.tabValue=="happenings"?Container(
//                 color: Constants.kitGradients[1],
//                 height: screenHeight(context,dividedBy: 300),
//                 width: screenWidth(context,dividedBy: 8),
//               ):Container(),
//             ],
//           )),
//         ),
//         onTap: (){
//           setState(() {
//             widget.tabValue="happenings";
//           });
//         },
//       );
//   }
// }
