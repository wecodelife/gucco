import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/action_alert_content.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class VisitorsConfirmationBody4 extends StatefulWidget {
  String content1;

  Function onChanged;
  int counter;
  VisitorsConfirmationBody4({this.content1,
    this.onChanged,this.counter,});
  @override
  _VisitorsConfirmationBody4State createState() => _VisitorsConfirmationBody4State();
}

class _VisitorsConfirmationBody4State extends State<VisitorsConfirmationBody4> {
  int count;
  @override
  Widget build(BuildContext context) {
    return Column(

      children: [

        SizedBox(
          height: screenHeight(context,dividedBy: 20),
        ),

        Row(
          children: [

            SizedBox(
              width: screenWidth(context,dividedBy: 17),
            ),
            Container(
                width: screenWidth(context,dividedBy: 2),
                child: Text("Visitor Notes",style: TextStyle(color:widget.counter==4?Constants.kitGradients[11]:Colors.black,
                    fontFamily: 'SFProText-SemiBold',fontSize: 20,fontWeight: FontWeight.w600),)),

            SizedBox(width: screenWidth(context,dividedBy:3.1 ),),
            GestureDetector(
              onTap: (){

              },
              child:  widget.counter==4?GestureDetector(
                onTap: (){
                  setState(() {
                    count=0;
                    widget.onChanged(count);
                  });

                },
                child:  SvgPicture.asset(
                  'assets/icons/arrow_upward.svg',
                  width: screenWidth(context, dividedBy: 40),
                  height: screenHeight(context, dividedBy: 40),
                  color: Constants.kitGradients[11],
                ),
              ):GestureDetector(
                onTap: (){
                  setState(() {
                    count=4;
                    widget.onChanged(count);
                  });

                },
                child:  SvgPicture.asset('assets/icons/arrow_down.svg',
                  width: screenWidth(context, dividedBy: 40),
                  height: screenHeight(context, dividedBy: 40),
                  color: Colors.black,
                ),
              ),
            ),


          ],
        ),

        widget.counter==4?Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: screenHeight(context,dividedBy: 20),
            ),

            Container(
                width: screenWidth(context,dividedBy: 1.2),
                child: Text("Please note that all visitors are required to bring and present the following upon arrival:",style: TextStyle(color:Colors.black,
                    fontFamily: 'SFProText-SemiBold',),)),

            SizedBox(
              height:screenHeight(context,dividedBy: 30),
            ),

            Text("1.Invite QRCode",style: TextStyle(color:Colors.black,
              fontFamily: 'SFProText-SemiBold',),),


            SizedBox(
              height:screenHeight(context,dividedBy: 30),
            ),

            Text("2.IC / Passport / FN ",style: TextStyle(color:Colors.black,
              fontFamily: 'SFProText-SemiBold',),),

            SizedBox(
              height:screenHeight(context,dividedBy: 30),
            ),

            Container(
              width: screenWidth(context,dividedBy: 1.2),
              child: Text("Visitors should also note that in accordance with Singapore Law, max. 5 visitors will be allowed per unit. ",style: TextStyle(color:Colors.black,
                fontFamily: 'SFProText-SemiBold',),textAlign: TextAlign.start,),
            ),






          ],
        ):Container(),

        SizedBox(
          height: screenHeight(context,dividedBy: 20),
        ),
        Padding(
          padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
          child: Divider(
            thickness: 2,
            color: Colors.grey,
          ),
        )

      ],
    );










  }
}
