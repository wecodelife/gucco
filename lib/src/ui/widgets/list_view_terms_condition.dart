import 'package:draggable_scrollbar/draggable_scrollbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ListViewTermAndCondition extends StatefulWidget {
  @override
  _ListViewTermAndConditionState createState() =>
      _ListViewTermAndConditionState();
}

class _ListViewTermAndConditionState extends State<ListViewTermAndCondition> {
  ScrollController scrollController = new ScrollController();
  String termsAndCondition;
  String pdpa = "";
  bool isLoading;

  Future<String> getFileData(String path) async {
    return await rootBundle.loadString(path);
  }

  getTermsAndCondition() async {
    termsAndCondition = await getFileData("assets/Text/TermsAndCondition.txt");
    // pdpa = await getFileData("assets/Text/pdpa.txt");
    setState(() {
      isLoading = false;
    });

    print(termsAndCondition);
  }

  @override
  void initState() {
    getTermsAndCondition();
    print(termsAndCondition);
    isLoading = true;

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: screenHeight(context, dividedBy: 1.5),
        width: screenWidth(context, dividedBy: 1.1),
        decoration: BoxDecoration(border: Border.all(color: Colors.white)),
        child: DraggableScrollbar(
          padding: EdgeInsets.all(5),
          alwaysVisibleScrollThumb: true,
          controller: scrollController,
          child: ListView.builder(
              controller: scrollController,
              itemCount: 4,
              shrinkWrap: true,
              itemBuilder: (Buildcontext, int index) {
                return Column(
                  children: [
                    index == 0
                        ? Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: screenWidth(context, dividedBy: 20),
                                vertical: screenHeight(context, dividedBy: 40)),
                            child: Text(
                              'Guocco Club Terms and Conditions ("Terms and condition")',
                              style: TextStyle(
                                color: Constants.kitGradients[0],
                                fontFamily: 'Muli',
                                fontSize: screenWidth(context, dividedBy: 25),
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          )
                        : Container(),
                    index == 1
                        ? Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: screenWidth(context, dividedBy: 20),
                                vertical: screenHeight(context, dividedBy: 30)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                isLoading == false
                                    ? Text(
                                        termsAndCondition,
                                        style: TextStyle(
                                          height: 2,
                                          color: Constants.kitGradients[0],
                                          fontFamily: 'Muli',
                                          fontSize: screenWidth(context,
                                              dividedBy: 25),
                                        ),
                                      )
                                    : Container(
                                        height:
                                            screenHeight(context, dividedBy: 2),
                                        child: Center(
                                          child: CircularProgressIndicator(
                                            valueColor:
                                                new AlwaysStoppedAnimation<
                                                        Color>(
                                                    Constants.kitGradients[0]),
                                          ),
                                        ),
                                      ),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 30),
                                ),
                              ],
                            ),
                          )
                        : Container(),
                    index == 3
                        ? Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: screenWidth(context, dividedBy: 20),
                                vertical: screenHeight(context, dividedBy: 30)),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                isLoading == false
                                    ? Text(
                                        pdpa,
                                        style: TextStyle(
                                          height: 2,
                                          color: Constants.kitGradients[0],
                                          fontFamily: 'Muli',
                                          fontSize: screenWidth(context,
                                              dividedBy: 25),
                                        ),
                                      )
                                    : Container(),
                                SizedBox(
                                  height: screenHeight(context, dividedBy: 30),
                                ),
                              ],
                            ),
                          )
                        : Container()
                  ],
                );
              }),
          heightScrollThumb: 48.0,
          backgroundColor: Colors.blue,
          scrollThumbBuilder: (
            Color backgroundColor,
            Animation<double> thumbAnimation,
            Animation<double> labelAnimation,
            double height, {
            Text labelText,
            BoxConstraints labelConstraints,
          }) {
            return FadeTransition(
              opacity: thumbAnimation,
              child: Container(
                height: height,
                width: 5,
                color: Colors.grey,
              ),
            );
          },
        ));
  }
}
