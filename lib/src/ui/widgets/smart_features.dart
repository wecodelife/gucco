import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/screens/facilities_list_bookings.dart';
import 'package:guoco_priv_app/src/ui/screens/message_page.dart';
import 'package:guoco_priv_app/src/ui/screens/visitors_list.dart';
import 'package:guoco_priv_app/src/ui/widgets/smart_feature_box.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class SmartFeatures extends StatefulWidget {
  String imageName;
  String boxTitle;
  Widget homeBox;
  Function onTap;
  Function onTapMyVisitors;
  String blockName;
  String floorAndUnitId;
  SmartFeatures(
      {this.boxTitle,
      this.imageName,
      this.homeBox,
      this.onTap,
      this.onTapMyVisitors,
      this.floorAndUnitId,
      this.blockName});

  @override
  _SmartFeaturesState createState() => _SmartFeaturesState();
}

class _SmartFeaturesState extends State<SmartFeatures> {
  @override
  void initState() {
    print(widget.blockName);
    print(widget.floorAndUnitId);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 10)),
      child: Container(
          height: screenHeight(context, dividedBy: 4.5),
          width: screenWidth(context, dividedBy: 1),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SmartFeatureBox(
                    onPressed: () {
                      push(
                          context,
                          FacilitiesListBookings(
                            floorAndUnitId: widget.floorAndUnitId,
                            blockName: widget.blockName,
                          ));
                    },
                    image: "assets/icons/calendericon.png",
                    heading: "Book Facility",
                    topLeft: true,
                  ),
                  SizedBox(width: screenWidth(context, dividedBy: 40)),
                  SmartFeatureBox(
                    onPressed: () {
                      push(context, VisitorsList());
                    },
                    image: "assets/icons/location.png",
                    heading: "Invite Visitors",
                  ),
                  SizedBox(width: screenWidth(context, dividedBy: 40)),
                  SmartFeatureBox(
                    onPressed: () {
                      openNovade();
                    },
                    image: "assets/icons/settingsicon.png",
                    heading: "Fault Reporting",
                    topRight: true,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SmartFeatureBox(
                    onPressed: () {},
                    image: "assets/icons/submitforms.png",
                    heading: "Submit Forms",
                    bottomLeft: true,
                  ),
                  SizedBox(width: screenWidth(context, dividedBy: 40)),
                  SmartFeatureBox(
                    onPressed: () {
                      push(context, MessagePage());
                    },
                    image: "assets/icons/messages.png",
                    heading: "Messaging",
                    topRight: true,
                  ),
                  SizedBox(width: screenWidth(context, dividedBy: 40)),
                  Row(
                    children: [
                      Container(
                        child: GestureDetector(
                          onTap: () {
                            openFibaro();
                          },
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              SvgPicture.asset("assets/icons/home.svg"),
                              Container(
                                child: Text(
                                  "Smart Home",
                                  style: TextStyle(
                                    color: Constants.kitGradients[0],
                                    fontFamily: 'SFProText-Regular',
                                    fontSize:
                                        screenWidth(context, dividedBy: 40),
                                  ),
                                  textAlign: TextAlign.center,
                                ),
                                width: screenWidth(context, dividedBy: 7),
                              ),
                            ],
                          ),
                        ),
                        width: screenWidth(context, dividedBy: 4),
                        height: screenHeight(context, dividedBy: 10),
                        decoration: BoxDecoration(
                            color: Constants.kitGradients[33],
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(5))),
                      ),
                    ],
                  )
                ],
              )
            ],
          )),
    );
  }
}
