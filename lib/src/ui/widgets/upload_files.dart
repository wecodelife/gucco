import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';

class UploadFiles extends StatefulWidget {
  String buttonTitle;
  Function onTap;
  ValueChanged onChanged;
  UploadFiles({this.buttonTitle,this.onTap,this.onChanged});
  @override
  _UploadFilesState createState() => _UploadFilesState();
}

class _UploadFilesState extends State<UploadFiles> {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(onPressed: widget.onTap,
      color: Constants.kitGradients[15],
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Text(widget.buttonTitle,style: TextStyle(color:Constants.kitGradients[0],fontFamily: "SFProText-Medium",fontSize: 16),),
    );
  }
}
