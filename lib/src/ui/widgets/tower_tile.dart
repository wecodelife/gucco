import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class TowerTile extends StatefulWidget {
  final String unitNo;
  final String floorNo;
  final String name;
  final bool keyCollected;

  TowerTile({this.floorNo, this.unitNo, this.name, this.keyCollected});

  @override
  _TowerTileState createState() => _TowerTileState();
}

class _TowerTileState extends State<TowerTile> {
  @override
  void initState() {
    print(widget.keyCollected);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: screenHeight(context, dividedBy: 6.5),
        width: screenWidth(context, dividedBy: 2.5),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            color: Colors.black.withOpacity(0.20)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              widget.name,
              style: TextStyle(
                  color: Constants.kitGradients[0],
                  fontSize: 18,
                  fontFamily: "SFProDisplay-Semibold"),
            ),
            Text(
              widget.floorNo + " - " + widget.unitNo,
              style: TextStyle(
                  color: Constants.kitGradients[0],
                  fontSize: 30,
                  fontFamily: "SFProDisplay-Light"),
            ),
            Text(
              widget.keyCollected == true
                  ? "Key Collected"
                  : "Pending Key Collection",
              style: TextStyle(
                  color: Constants.kitGradients[0],
                  fontSize: 10,
                  fontFamily: "SFProDisplay-Light"),
            )
          ],
        ),
      ),
    );
  }
}
