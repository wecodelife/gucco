import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';

class BuildButton extends StatefulWidget {
  final String title;
  final Function onPressed;
  final bool transparent;
  final bool faultReport;
  final bool disabled;
  final bool arrowIcon;
  final bool isLoading;
  BuildButton(
      {this.title,
      this.onPressed,
      this.transparent,
      this.disabled,
      this.arrowIcon,
      this.faultReport,
      this.isLoading});
  @override
  _BuildButtonState createState() => _BuildButtonState();
}

class _BuildButtonState extends State<BuildButton> {
  bool disabled = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: screenHeight(context, dividedBy: 15),
        width: screenWidth(context, dividedBy: 1.3),
        child: widget.transparent == true
            ? RaisedButton(
                color: Colors.transparent,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                    side: BorderSide(color: Constants.kitGradients[1])),
                onPressed: () {
                  widget.onPressed();
                },
                child: Center(
                    child: Text(
                  widget.title,
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontFamily: 'MuliBold',
                      fontSize: 16),
                )),
              )
            : RaisedButton(
                color: widget.disabled == false
                    ? Constants.kitGradients[1].withOpacity(0.38)
                    : widget.faultReport == true
                        ? Constants.kitGradients[13]
                        : Constants.kitGradients[1],
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30)),
                onPressed: () {
                  if (widget.disabled == true) widget.onPressed();
                },
                child: Center(
                    child: widget.isLoading != true
                        ? Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text(
                                widget.title,
                                style: TextStyle(
                                    color: widget.disabled == false
                                        ? Constants.kitGradients[0]
                                            .withOpacity(0.38)
                                        : Constants.kitGradients[0],
                                    fontFamily: 'Muli',
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold),
                              ),
                              widget.arrowIcon == true
                                  ? Icon(
                                      Icons.arrow_right_alt_sharp,
                                      color: Colors.white,
                                      size: 20,
                                    )
                                  : Container()
                            ],
                          )
                        : Shimmer.fromColors(
                            highlightColor: Constants.kitGradients[0],
                            baseColor: Constants.kitGradients[1],
                            child: Container(
                              height: screenHeight(context, dividedBy: 15),
                              width: screenWidth(context, dividedBy: 1.2),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30),
                                  color: Colors.black54),
                            ),
                          )
                    // Container(
                    //   height: screenHeight(context,dividedBy: 20),
                    //   width: screenWidth(context,dividedBy: 10),
                    //   child: CircularProgressIndicator(
                    //     valueColor: new AlwaysStoppedAnimation<Color>(Constants.kitGradients[0]),
                    //   ),
                    // ),
                    ),
              ));
  }
}
