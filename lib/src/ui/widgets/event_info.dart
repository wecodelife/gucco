import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/action_alert_box.dart';
import 'package:guoco_priv_app/src/ui/widgets/cancel_alert_box.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class EventInfo extends StatefulWidget {
  String eventHeading;
  String eventSubHeading;
  String eventDate;
  String eventTime;
  bool enable;
  Function onTapCloseButtonOnly;
  Function onTapCloseButton;
  Function onTapEditButton;
  bool subHeading;
  EventInfo({this.onTapCloseButtonOnly,this.onTapEditButton,this.eventHeading,this.eventDate,this.eventTime,this.enable,this.eventSubHeading,this.subHeading,this.onTapCloseButton});
  @override
  _EventInfoState createState() => _EventInfoState();
}

class _EventInfoState extends State<EventInfo> {
  @override
  Widget build(BuildContext context) {
    return  Container(
        height: screenHeight(context, dividedBy: 8),
        width: screenWidth(context, dividedBy: 1),
        child: widget.enable==true? Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Row(
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 14),
                ),
                Text(
                  widget.eventHeading,
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: "SFProText-Medium",
                      fontSize: 18),
                ),
              ],
            ),
            widget.subHeading==true?
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: screenWidth(context, dividedBy: 14),
                ),
                Text(
                  widget.eventSubHeading,
                  style: TextStyle(
                      color: Colors.black,
                      fontFamily: "SFProText-Regular",
                      fontSize: 13),
                ),
              ],
            ):Container(),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  width: screenWidth(context,dividedBy:20 ),
                ),
                Text(
                  widget.eventDate,
                  style: TextStyle(
                      color: Colors.grey,
                      fontFamily: "SFProText-Regular",
                      fontSize: 14),
                ),

                Container(
                  height: screenHeight(context,dividedBy: 30),
                  width: screenWidth(context,dividedBy: 200),
                  color: Colors.grey,
                ),

                Text(
                  widget.eventTime,
                  style: TextStyle(
                      color: Colors.grey,
                      fontFamily: "SFProText-Regular",
                      fontSize: 14),
                ),

                SizedBox(
                  width: screenWidth(context,dividedBy: 10),
                ),
               

                GestureDetector(
                  onTap:widget.onTapEditButton,
                  child: Container(
                    height: screenHeight(context,dividedBy: 20),
                    width: screenWidth(context,dividedBy: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Constants.kitGradients[12],

                    ),
                    child: Icon(Icons.edit,color:Constants.kitGradients[11],),
                  ),
                ),

                GestureDetector(
                  onTap: widget.onTapCloseButton,
                  child: Container(
                    height: screenHeight(context,dividedBy: 20),
                    width: screenWidth(context,dividedBy: 10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Constants.kitGradients[12],

                    ),
                    child: Icon(Icons.close,color:Constants.kitGradients[11],),
                  ),
                ),
                SizedBox(
                  width: screenWidth(context,dividedBy: 50),
                )
              ],
            ),
            Divider(
              thickness: 2,
            )
          ],
        ):Stack(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 14),
                    ),
                    Text(
                      widget.eventHeading,
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: "SFProText-Medium",
                          fontSize: 18),
                    ),
                  ],
                ),
                widget.subHeading==true?
                Row(
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 14),
                    ),
                    Text(
                      widget.eventSubHeading,
                      style: TextStyle(
                          color: Colors.black,
                          fontFamily: "SFProText-Regular",
                          fontSize: 13),
                    ),
                  ],
                ):Container(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: screenWidth(context,dividedBy:14 ),
                    ),
                    Text(
                      widget.eventDate,
                      style: TextStyle(
                          color: Colors.grey,
                          fontFamily: "SFProText-Medium",
                          fontSize: 14),
                    ),
                    SizedBox(
                      width: screenWidth(context,dividedBy:70 ),
                    ),
                    Container(
                      height: screenHeight(context,dividedBy: 30),
                      width: screenWidth(context,dividedBy: 200),
                      color: Colors.grey,
                    ),
                    SizedBox(
                      width: screenWidth(context,dividedBy:70 ),
                    ),
                    Text(
                      widget.eventTime,
                      style: TextStyle(
                          color: Colors.grey,
                          fontFamily: "SFProText-Medium",
                          fontSize: 14),
                    ),
                    // SizedBox(
                    //   width: screenWidth(context,dividedBy: 3.4),
                    // ),


                  ],
                ),
                Divider(
                  thickness: 2,
                )
              ],
            ),

            Padding(
              padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 1.2),top: screenHeight(context,dividedBy: 100)),
              child: GestureDetector(
                onTap: widget.onTapCloseButtonOnly,
                child: Container(
                  height: screenHeight(context,dividedBy: 20),
                  width: screenWidth(context,dividedBy: 10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Constants.kitGradients[12],

                  ),
                  child: Icon(Icons.close,color:Constants.kitGradients[11],),
                ),
              ),
            ),
          ],
        )
    );
  }
}
