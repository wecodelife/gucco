import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FaultReportCheck extends StatefulWidget {
  final Color boxColor, boxBorderColor;
  Function onTap;


  FaultReportCheck({this.boxColor, this.boxBorderColor,this.onTap});

  @override
  _FaultReportCheckState createState() => _FaultReportCheckState();
}

class _FaultReportCheckState extends State<FaultReportCheck> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        width: screenWidth(context, dividedBy: 10),
        height: screenHeight(context, dividedBy: 20),
        decoration: BoxDecoration(
            color: widget.boxColor, borderRadius: BorderRadius.circular(8.0)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Container(
            decoration: BoxDecoration(
                border: Border.all(color: widget.boxBorderColor, width: 2.0),
                shape: BoxShape.circle),
          ),
        ),
      ),
      onTap: widget.onTap,
    );
  }
}
