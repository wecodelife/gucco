import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/fault_report_body1.dart';
import 'package:guoco_priv_app/src/ui/widgets/fault_report_check_box.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FaultReportListViewWidget extends StatefulWidget {
  bool clickTabStatus;

  FaultReportListViewWidget({this.clickTabStatus});

  @override
  _FaultReportListViewWidgetState createState() =>
      _FaultReportListViewWidgetState();
}

class _FaultReportListViewWidgetState extends State<FaultReportListViewWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: screenWidth(context, dividedBy: 1),
      height: screenHeight(context, dividedBy: 4.1),
      //color: Colors.yellow,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 35),
              ),
              FaultReportCheck(
                boxColor: Constants.kitGradients[20],
                boxBorderColor: Constants.kitGradients[21],
                onTap: () {
                  //do something here
                  print(widget.clickTabStatus.toString());
                },
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 35),
              ),
              FaultBody1(
                title: "Cleanliness",
                status: "In Progress",
                statusTextColor: Constants.kitGradients[21],
                dateTime: "05-Apr-2020  |  10:32AM",
                button1Name: "Cancel",
                button2Name: "Details",
                clickTabStatus: widget.clickTabStatus,
              ),
            ],
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 2,
            color: Constants.kitGradients[24],
          ),
        ],
      ),
    );
  }
}
