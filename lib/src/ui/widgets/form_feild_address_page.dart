import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FormFeildAddressBox extends StatefulWidget {
  String hintText;
  bool labelEnabled;
  bool readOnly;
  TextEditingController textEditingController;
  FormFeildAddressBox({this.hintText, this.textEditingController,this.labelEnabled,this.readOnly});
  @override
  _FormFeildAddressBoxState createState() => _FormFeildAddressBoxState();
}

class _FormFeildAddressBoxState extends State<FormFeildAddressBox> {
  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.textEditingController,
      autofocus: false,
      readOnly: widget.readOnly ?? false,
      style: TextStyle(
        color: Constants.kitGradients[0],
        fontFamily: 'MuliSemiBold',
        fontSize: 16,),

      cursorColor: Constants.kitGradients[0],
      decoration: InputDecoration(
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Constants.kitGradients[0]),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Constants.kitGradients[0]),
        ),
        hintText:widget.hintText,
        hintStyle: TextStyle(
          color: Constants.kitGradients[0],
          fontFamily: 'MuliSemiBold',
          fontSize: 16,
        ),
      ),
    );
  }
}
