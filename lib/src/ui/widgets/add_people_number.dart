import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class AddPeopleNumber extends StatefulWidget {
  int count;
  Function decrementOnTap;
  Function incrementOnTap;
  AddPeopleNumber({this.count, this.incrementOnTap, this.decrementOnTap});
  @override
  _AddPeopleNumberState createState() => _AddPeopleNumberState();
}

class _AddPeopleNumberState extends State<AddPeopleNumber> {
  int count;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 30),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: screenWidth(
                context,
                dividedBy: 5,
              ),
            ),
            widget.count > 1
                ? GestureDetector(
                    onTap: widget.decrementOnTap,
                    child: Icon(Icons.horizontal_rule,
                        size: 30, color: Colors.black))
                : Icon(Icons.horizontal_rule,
                    size: 30, color: Colors.black.withOpacity(0.8)),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  widget.count.toString(),
                  style:
                      TextStyle(color: Constants.kitGradients[1], fontSize: 18),
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 150),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 5),
                  height: 2,
                  color: Colors.white10,
                ),
                SizedBox(
                  width: screenWidth(
                    context,
                    dividedBy: 3,
                  ),
                ),
              ],
            ),
            widget.count < 5
                ? GestureDetector(
                    onTap: widget.incrementOnTap,
                    child: Icon(Icons.add, size: 30, color: Colors.black))
                : Icon(Icons.add,
                    size: 30, color: Colors.black.withOpacity(0.8)),
          ],
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 30),
        ),
      ],
    );
  }
}
