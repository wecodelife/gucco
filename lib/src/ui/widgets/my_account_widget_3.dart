import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class MyAccountWidget3 extends StatefulWidget {
  String title1,title2,title3;
  MyAccountWidget3({this.title1,this.title2,this.title3});

  @override
  _MyAccountWidget3State createState() => _MyAccountWidget3State();
}

class _MyAccountWidget3State extends State<MyAccountWidget3> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 8),
      width: screenWidth(context, dividedBy: 1.0),
      decoration: BoxDecoration(
          border: Border.all(color: Constants.kitGradients[6], width: 0.5)),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              widget.title1,
              style: TextStyle(
                  fontFamily: 'Muli',
                  fontSize: 14,
                  fontWeight: FontWeight.w300,
                  color: Constants.kitGradients[0]),
            ),
            Text(
              widget.title2,
              style: TextStyle(
                  fontFamily: 'MuliBold',
                  fontSize: 18,
                  color: Constants.kitGradients[0]),
            ),
            Text(
              widget.title3,
              style: TextStyle(
                  fontFamily: 'Muli',
                  fontSize: 14,
                  fontWeight: FontWeight.w300,
                  color: Constants.kitGradients[0]),
            ),
          ],
        ),
      ),
    );
  }
}
