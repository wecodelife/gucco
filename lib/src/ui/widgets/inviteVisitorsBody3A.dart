import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

import 'form_field_report_page.dart';

class InviteVisitorsBodyOptional extends StatefulWidget {
  TextEditingController emailAddress = new TextEditingController();
  TextEditingController vehicle = new TextEditingController();
  TextEditingController visitors = new TextEditingController();
  TextEditingController additional = new TextEditingController();

  @override
  _InviteVisitorsBodyOptionalState createState() =>
      _InviteVisitorsBodyOptionalState();
}

class _InviteVisitorsBodyOptionalState
    extends State<InviteVisitorsBodyOptional> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        FormFieldReportPage(
          noStar: true,
          inviteVisitors: true,
          title: "Email Address",
          textEditingController: widget.emailAddress,
          hintText: '',
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        FormFieldReportPage(
          noStar: true,
          inviteVisitors: true,
          title: "Vehicle License Plate No.",
          textEditingController: widget.vehicle,
          hintText: '',
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        FormFieldReportPage(
          noStar: true,
          inviteVisitors: true,
          title: "Total No. of Visitors",
          textEditingController: widget.visitors,
          hintText: '',
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        Text(
          "Note: In accordance with Singapore Law, max. 5 visitorswill be allowed per unit. ",
          style: TextStyle(
              fontFamily: 'SFProText-Regular',
              fontSize: 13,
              fontWeight: FontWeight.w500,
              color: Constants.kitGradients[19]),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        FormFieldReportPage(
          noStar: true,
          inviteVisitors: true,
          title: "Additional Info",
          textEditingController: widget.additional,
          hintText: '',
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
      ],
    );
  }
}
