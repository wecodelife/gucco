import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/time_slot_tile.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class TimeSlot extends StatefulWidget {
  double aspectRatio;
  int crossAxisCount;
  int itemCount;
  TimeSlot({this.aspectRatio,this.crossAxisCount,this.itemCount});
  @override
  _TimeSlotState createState() => _TimeSlotState();
}

class _TimeSlotState extends State<TimeSlot> {
  bool selected;
  bool deselected;
  int itemNumber;
  @override
  void initState() {
    // TODO: implement initState
    itemNumber=100;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      itemCount: widget.itemCount,
      shrinkWrap: true,
      physics: NeverScrollableScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(childAspectRatio:widget.aspectRatio ,
          crossAxisCount:widget.crossAxisCount ),
      itemBuilder: (BuildContext context, int index) {

        itemNumber==index?selected=true:selected=false;

        return GestureDetector(
                     onTap: (){
                       itemNumber=index;
                       setState(() {
                       });
                     },
            child: TimeSlotTile(selected:selected ,deselected: false,time: "10:00 AM",));

      },
    );
  }
}
