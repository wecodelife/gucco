import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/e_application_progress.dart';
import 'package:guoco_priv_app/src/ui/screens/facilities_list_bookings.dart';
import 'package:guoco_priv_app/src/ui/screens/fault_reporting_page.dart';
import 'package:guoco_priv_app/src/ui/screens/visitors_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/action_alert_content.dart';
import 'package:guoco_priv_app/src/ui/widgets/back_button.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ActionAlertBox extends StatefulWidget {
  String title;
  bool reduceSize;
  bool questionAlertNotBold;
  String questionAlert;
  String content1;
  String content2;
  String content3;
  String heading1;
  String heading2;
  String heading3;
  String content4;
  String content5;
  String content6;
  String heading4;
  String heading5;
  String heading6;
  String date;
  int contentNum;
  int cancelAlert;
  String cancelBoxTitle;
  String text1;
  String text2;
  String lastDate;
  bool cancelBox;
  Function onPressed;
  bool isLoading;
  bool reschedule;
  ActionAlertBox(
      {this.title,
      this.reschedule,
      this.reduceSize,
      this.questionAlert,
      this.content1,
      this.content2,
      this.content3,
      this.heading1,
      this.heading2,
      this.heading3,
      this.heading4,
      this.heading5,
      this.heading6,
      this.content4,
      this.content5,
      this.content6,
      this.contentNum,
      this.lastDate,
      this.date,
      this.questionAlertNotBold,
      this.cancelAlert,
      this.text2,
      this.text1,
      this.cancelBoxTitle,
      this.cancelBox,
      this.onPressed,
      this.isLoading});

  @override
  _ActionAlertBoxState createState() => _ActionAlertBoxState();
}

class _ActionAlertBoxState extends State<ActionAlertBox> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        insetPadding: EdgeInsets.symmetric(
            vertical: screenHeight(context, dividedBy: 3.6)),
        titlePadding: EdgeInsets.symmetric(
          horizontal: screenWidth(context, dividedBy: 40),
        ),
        contentPadding: EdgeInsets.symmetric(
            horizontal: screenWidth(
          context,
          dividedBy: 40,
        )),
        title: Container(
            height: screenHeight(context, dividedBy: 15),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.vertical(top: Radius.circular(10))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 100),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(widget.title),
                  ],
                ),
              ],
            )),
        content: Container(
          height: widget.reduceSize == true
              ? screenHeight(context, dividedBy: 7)
              : screenHeight(context, dividedBy: 3),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(10))),
          child: Column(
            mainAxisAlignment: widget.reduceSize == true
                ? MainAxisAlignment.spaceBetween
                : MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              if (widget.reduceSize == true)
                SizedBox(
                  height: screenHeight(context, dividedBy: 70),
                ),
              widget.contentNum >= 1
                  ? AlertBoxContent(
                      cancelBox: widget.cancelBox,
                      text1: widget.text1,
                      text2: widget.text2,
                      title: widget.cancelBoxTitle,
                      content: widget.content1,
                      heading: widget.heading1,
                    )
                  : Container(),
              widget.contentNum >= 2
                  ? AlertBoxContent(
                      cancelBox: widget.cancelBox,
                      heading: widget.heading2,
                      content: widget.content2,
                      text1: widget.text1,
                      text2: widget.text2,
                      title: widget.cancelBoxTitle,
                    )
                  : Container(),
              widget.contentNum >= 3
                  ? AlertBoxContent(
                      cancelBox: widget.cancelBox,
                      heading: widget.heading3,
                      content: widget.content3,
                      text1: widget.text1,
                      text2: widget.text2,
                      title: widget.cancelBoxTitle,
                    )
                  : Container(),
              widget.contentNum >= 4
                  ? AlertBoxContent(
                      cancelBox: widget.cancelBox,
                      content: widget.content4,
                      heading: widget.heading4,
                      text1: widget.text1,
                      text2: widget.text2,
                      title: widget.cancelBoxTitle,
                    )
                  : Container(),
              widget.contentNum >= 5
                  ? AlertBoxContent(
                      cancelBox: widget.cancelBox,
                      heading: widget.heading5,
                      content: widget.content5,
                      text1: widget.text1,
                      text2: widget.text2,
                      title: widget.cancelBoxTitle,
                    )
                  : Container(),
              widget.contentNum >= 6
                  ? AlertBoxContent(
                      cancelBox: widget.cancelBox,
                      heading: widget.heading6,
                      content: widget.content6,
                      text1: widget.text1,
                      text2: widget.text2,
                      title: widget.cancelBoxTitle,
                    )
                  : Container(),
              widget.questionAlertNotBold == false
                  ? Text(
                      widget.questionAlert,
                      style: TextStyle(
                          fontSize: screenWidth(context, dividedBy: 10),
                          fontFamily: 'SFProText-Regular',
                          color: Colors.black,
                          fontWeight: FontWeight.w600),
                    )
                  : Row(
                      children: [
                        SizedBox(
                          width: screenWidth(context, dividedBy: 20),
                        ),
                        Container(
                            width: screenWidth(context, dividedBy: 1.15),
                            child: RichText(
                              text: TextSpan(
                                text: widget.questionAlert,
                                style: TextStyle(
                                    fontSize:
                                        screenWidth(context, dividedBy: 26),
                                    fontFamily: 'SFProText-Regular',
                                    color: Colors.black,
                                    fontWeight: FontWeight.w200),
                                children: <TextSpan>[
                                  TextSpan(
                                    text: widget.date,
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontFamily: 'SFProText-Regular',
                                        color: Colors.black,
                                        fontWeight: FontWeight.w600),
                                  ),
                                ],
                              ),
                            )),
                      ],
                    ),
              SizedBox(
                height: screenHeight(context, dividedBy: 60),
              )
            ],
          ),
        ),
        backgroundColor: Colors.transparent,
        actions: [
          Container(
            height: screenHeight(context, dividedBy: 10),
            width: screenWidth(context, dividedBy: 1),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ButtonAlertBox(
                  title: "Back",
                  onTap: () {
                    Navigator.pop(context);
                  },
                  backButton: true,
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 40),
                ),
                ButtonAlertBox(
                  title:
                      widget.reschedule == true ? "Yes,Confirm" : "Yes,Cancel",
                  isLoading: widget.isLoading,
                  onTap: () {
                    widget.onPressed();
                    Navigator.pop(context);
                    widget.cancelAlert == 1
                        ? cancelAlertBox(
                            context: context,
                            msg: "Booking Cancelled",
                            text1:
                                "If you have already made the deposit, you will receive it within 3 working days.",
                            text2: "To make another booking,head to,",
                            onPressed: () {
                              push(context, FacilitiesListBookings());
                            },
                            contentPadding: 40,
                            insetPadding: 3.6,
                            titlePadding: 40,
                          )
                        : Container();
                    widget.cancelAlert == 2
                        ? cancelAlertBox(
                            context: context,
                            msg: "Invitation Cancelled",
                            text1: "To invite another visitor, head to",
                            text2: "My Visitors > Invite Visitor.",
                            contentPadding: 40,
                            insetPadding: 2.8,
                            titlePadding: 40,
                            onPressed: () {
                              push(context, MyVisitorsPage());
                            })
                        : Container();
                    widget.cancelAlert == 3
                        ? cancelAlertBox(
                            context: context,
                            msg: "Fault Report Cancelled",
                            text1: "To make another report,head to",
                            text2: "Fault Reporting > Fault Report",
                            contentPadding: 40,
                            insetPadding: 2.8,
                            titlePadding: 40,
                            onPressed: () {
                              push(context, FaultReport());
                            })
                        : Container();
                    widget.cancelAlert == 4
                        ? cancelAlertBox(
                            context: context,
                            msg: "E-Application Cancelled",
                            text1: "To make another report,head to",
                            text2: "E-Applications> Submit Application.",
                            contentPadding: 40,
                            insetPadding: 2.8,
                            onPressed: () {
                              push(context, EApplicationsProgress());
                            },
                            titlePadding: 40,
                          )
                        : Container();

                    widget.cancelAlert == 5 ? widget.onPressed() : Container();
                  },
                  backButton: false,
                ),
                SizedBox(
                  width: screenWidth(context, dividedBy: 40),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
