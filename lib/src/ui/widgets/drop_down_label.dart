import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/drop_down%20form%20feild.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class DropDownLabel extends StatefulWidget {
  String label;
  DropDownLabel({this.label});
  @override
  _DropDownLabelState createState() => _DropDownLabelState();
}

class _DropDownLabelState extends State<DropDownLabel> {
  List<String>fromTime=["10:30 AM","11:00 AM","11:30 AM"];
  List<String>toTime=["10:30 AM","11:00 AM","11:30 AM"];
  String selectedTime;
  @override
  Widget build(BuildContext context) {
    return Container(

      child: Row(
        children: [

          SizedBox(
            width: screenWidth(context,dividedBy: 20),
          ),

          Container(
            width: screenWidth(context,dividedBy: 2.4),
            height: screenHeight(context,dividedBy: 18),


            child: Row(

              children: [


                Text(widget.label,style: TextStyle(color: Colors.black,fontSize: 18,fontFamily: 'SFProText-SemiBold'),),



              ],
            ),
          ),

          SizedBox(
            width: screenWidth(context,dividedBy: 4),
          ),

          Column(
            children: [
              DropDownFormField(title: "Time",dropDownList:toTime ,boxWidth: 3.8,underline: false,onClicked: (value){

                selectedTime=value;
              },boxHeight: 20,hintText: true,hintPadding: 20,fontFamily: 'SFProText-Regular',dropDownValue:selectedTime ,
                hintFontFamily: 'SFProText-SemiBold',),
            ],
          ),

        ],
      ),
    );
  }
}
