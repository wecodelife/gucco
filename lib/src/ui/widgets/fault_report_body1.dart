import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/fault_reporting_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/fault_report_button.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FaultBody1 extends StatefulWidget {
  final String title, status, dateTime, button1Name, button2Name;
  final Color statusTextColor;
  bool clickTabStatus;

  FaultBody1(
      {this.title,
      this.status,
      this.button1Name,
      this.button2Name,
      this.dateTime,
      this.statusTextColor,
      this.clickTabStatus});

  @override
  _FaultBody1State createState() => _FaultBody1State();
}

class _FaultBody1State extends State<FaultBody1> {
  @override
  Widget build(BuildContext context) {
    print("click nav status is " + widget.clickTabStatus.toString());
    return Container(
      width: screenWidth(context, dividedBy: 1.19),
      height: screenHeight(context, dividedBy: 4.7),
      //color: Colors.red,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              SizedBox(
                width: screenWidth(context, dividedBy: 30),
              ),
              Text(
                widget.title,
                style: TextStyle(
                    fontSize: 17,
                    color: Constants.kitGradients[7],
                    fontFamily: "sfProRegular"),
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 3.5),
              ),
              Text(
                widget.status,
                style: TextStyle(
                    fontSize: 17,
                    color: widget.statusTextColor,
                    fontFamily: "SFProText-Regular"),
              ),
            ],
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),
          Container(
            color: Constants.kitGradients[24],
            child: Column(
              children: [
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: 0.5,
                  color: Constants.kitGradients[22],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 30),
                    ),
                    Text(
                      widget.dateTime,
                      style: TextStyle(
                          fontSize: 15,
                          color: Constants.kitGradients[7],
                          fontFamily: "SFProText-Regular"),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(
                      width: screenWidth(context, dividedBy: 30),
                    ),
                    GestureDetector(
                      onTap: (){
                        push(context, FaultReport());
                      },
                      child: FaultReportButton(
                        onPressed: (){

                        },
                        buttonName: widget.clickTabStatus == true
                            ? widget.button1Name
                            : "Details",
                        buttonColor: Constants.kitGradients[0],
                        buttonNameColor: Constants.kitGradients[11],
                        buttonWidth: widget.clickTabStatus == true
                            ? screenWidth(context, dividedBy: 6)
                            : screenWidth(context, dividedBy: 1.47),
                      ),
                    ),
                    SizedBox(
                      width: screenWidth(context, dividedBy: 50),
                    ),
                    widget.clickTabStatus == true
                        ? FaultReportButton(
                      onPressed: (){},
                            buttonName: widget.button2Name,
                            buttonNameColor: Constants.kitGradients[0],
                            buttonColor: Constants.kitGradients[11],
                            buttonWidth: screenWidth(context, dividedBy: 2.5),
                          )
                        : Container(),
                  ],
                ),
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                Container(
                  width: screenWidth(context, dividedBy: 1),
                  height: 0.5,
                  color: Constants.kitGradients[22],
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 80),
          ),
        ],
      ),
    );
  }
}
