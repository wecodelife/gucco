import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class AppBarGuocoLand extends StatefulWidget {
  Function pageNavigation;
  String pageTitle;
  double boxWidth;
  bool icon = true;
  bool homeScreen;
  AppBarGuocoLand(
      {this.pageNavigation,
      this.pageTitle,
      this.boxWidth,
      this.icon,
      this.homeScreen});
  @override
  _AppBarGuocoLandState createState() => _AppBarGuocoLandState();
}

class _AppBarGuocoLandState extends State<AppBarGuocoLand> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: screenWidth(context, dividedBy: 40),
        ),
        GestureDetector(
            onTap: () {
              widget.pageNavigation();
            },
            child: widget.icon == true
                ? Icon(
                    Icons.arrow_back,
                    size: screenWidth(context, dividedBy: 13),
                    color: Constants.kitGradients[0],
                  )
                : Container()),
        SizedBox(
          width: screenWidth(context, dividedBy: widget.boxWidth),
        ),
        Container(
          width: screenWidth(context, dividedBy: 1.6),
          child: Text(
            widget.pageTitle,
            style: TextStyle(
                color: Constants.kitGradients[0],
                fontFamily: 'MuliBold',
                fontSize: screenWidth(context, dividedBy: 22)),
          ),
        ),
      ],
    );
  }
}
