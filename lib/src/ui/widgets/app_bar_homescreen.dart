import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';

class AppBarHomeScreen extends StatefulWidget {
  final String name;
  final bool isLoading;
  final Function onPressingDrawer;
  AppBarHomeScreen({this.name, this.isLoading, this.onPressingDrawer});
  @override
  _AppBarHomeScreenState createState() => _AppBarHomeScreenState();
}

class _AppBarHomeScreenState extends State<AppBarHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 40),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              child: SvgPicture.asset(
                "assets/icons/homescreen_menu_icon.svg",
                fit: BoxFit.cover,
                color: Colors.white,
                height: screenHeight(context, dividedBy: 25),
                width: screenWidth(context, dividedBy: 25),
              ),
              onTap: () {
                widget.onPressingDrawer();
              },
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 4.7),
            ),
            Container(
                height: screenHeight(context, dividedBy: 20),
                width: screenWidth(context, dividedBy: 3),
                child: Image.asset(
                  "assets/icons/guoco_logo.png",
                  fit: BoxFit.scaleDown,
                  // color: Colors.white,
                )),
            SizedBox(
              width: screenWidth(context, dividedBy: 4.3),
            ),
            GestureDetector(
              child: Container(
                  height: screenHeight(context, dividedBy: 27),
                  width: screenHeight(context, dividedBy: 27),
                  child: SvgPicture.asset(
                    "assets/icons/home_bell_icon.svg",
                    fit: BoxFit.cover,
                    color: Colors.white,
                  )),
              onTap: () {},
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 30),
            )
          ],
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 70),
        ),
        widget.isLoading == false
            ? Row(
                children: [
                  SizedBox(
                    width: screenWidth(context, dividedBy: 70),
                  ),
                  Text(
                    "Welcome back, ",
                    style: TextStyle(
                        fontSize: 24, color: Colors.white, fontFamily: "Muli"),
                  ),
                  Text(
                    widget.name ?? "User",
                    style: TextStyle(
                        fontSize: 24,
                        color: Colors.white,
                        fontFamily: "Muli",
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )
            : Shimmer.fromColors(
                highlightColor: Colors.grey,
                baseColor: Colors.black38,
                child: Container(
                  height: screenHeight(context, dividedBy: 20),
                  width: screenWidth(context, dividedBy: 1),
                  decoration: BoxDecoration(color: Colors.black54),
                ),
              ),
      ],
    );
  }
}
