import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/fault_reporting_page.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class EApplicationButton extends StatefulWidget {
  final String buttonName;
  final Color buttonNameColor, buttonColor;
  final double buttonWidth;
  final Function onPressed;

  EApplicationButton(
      {this.buttonName,
        this.buttonNameColor,
        this.buttonColor,
        this.buttonWidth,
        this.onPressed
      });

  @override
  _EApplicationButtonState createState() => _EApplicationButtonState();
}

class _EApplicationButtonState extends State<EApplicationButton> {
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      color: widget.buttonColor,
      onPressed: () {
       widget.onPressed();
      },
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
        side: BorderSide(color: Constants.kitGradients[11], width: 1),
      ),
      child: Container(
        //color: Colors.greenAccent,
        width: widget.buttonWidth,
        child: Center(
          child: Text(
            widget.buttonName,
            style: TextStyle(
                fontSize: 15,
                color: widget.buttonNameColor,
                fontFamily: "sfProMedium"),
          ),
        ),
      ),
    );
  }
}
