import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class DoubleText extends StatefulWidget {
 final String label;
 final String text1;
 final String text2;
 final bool is2line;

  DoubleText({
   this.text1,this.text2,this.label,this.is2line
});
  @override
  _DoubleTextState createState() => _DoubleTextState();
}

class _DoubleTextState extends State<DoubleText> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          SizedBox(
            width: screenWidth(context,dividedBy: 30),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: screenHeight(context,dividedBy: 50),
              ),
              Text(widget.label,style: TextStyle(color: Constants.kitGradients[26],fontSize:13,fontFamily: "SFProText-Regular",fontWeight: FontWeight.w400),),
              SizedBox(
                height: screenHeight(context,dividedBy: 100),
              ),
              Text(widget.text1,style: TextStyle(fontSize: 16,fontFamily: "SFProText-Regular",fontWeight: FontWeight.w400),),
              widget.is2line==true? SizedBox(
                height: screenHeight(context,dividedBy: 100),
              ):Container(),
              widget.is2line==true?Text(widget.text2,style: TextStyle(fontSize: 16,fontFamily: "SFProText-Regular",fontWeight: FontWeight.w400),):Container(),
            ],
          ),
        ],
      ),
    );
  }
}
