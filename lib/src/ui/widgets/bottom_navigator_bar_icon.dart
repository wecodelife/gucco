import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class BottomNavigationIcon extends StatefulWidget {
  final bool value;
  final Function onPressed;
  final String selectedIcon;
  final String unselectedIcon;
  final String label;
  BottomNavigationIcon(
      {this.value,
      this.onPressed,
      this.selectedIcon,
      this.unselectedIcon,
      this.label});
  @override
  _BottomNavigationIconState createState() => _BottomNavigationIconState();
}

class _BottomNavigationIconState extends State<BottomNavigationIcon> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          widget.value != true
              ? Container(
                  height: screenHeight(context, dividedBy: 30),
                  width: screenHeight(context, dividedBy: 30),
                  child: SvgPicture.asset(
                    widget.unselectedIcon,
                    // "assets/icons/discover.svg",
                    fit: BoxFit.fill,
                  ),
                )
              : Container(
                  height: screenHeight(context, dividedBy: 25),
                  width: screenHeight(context, dividedBy: 25),
                  child: SvgPicture.asset(
                    // "assets/icons/discover_selected.svg",
                    widget.selectedIcon,
                    fit: BoxFit.fill,
                  ),
                ),
          Container(
            child: Text(
              widget.label,
              style: TextStyle(
                  fontSize: 11,
                  color: Constants.kitGradients[0],
                  fontFamily: "Muli",
                  fontWeight: FontWeight.normal),
            ),
          )
        ],
      ),
      onTap: () {
        widget.onPressed();
      },
    );
  }
}
