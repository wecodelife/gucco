import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ChangePasswordLabelWidget extends StatefulWidget {
  String heading;
  ChangePasswordLabelWidget({this.heading});
  @override
  _ChangePasswordLabelWidgetState createState() =>
      _ChangePasswordLabelWidgetState();
}

class _ChangePasswordLabelWidgetState extends State<ChangePasswordLabelWidget> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 20),
        ),
        Row(
          children: [
            RichText(
              text: TextSpan(
                text: widget.heading,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15,
                  fontFamily: 'SFProText-Regular',
                ),
                children: <TextSpan>[
                  TextSpan(
                      text: ' *',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.red)),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }
}
