import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/check_box_list_bookings.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class ListViewListBookings extends StatefulWidget {
  String title;
  String evenDate;
  String eventStartTime;
  String visitor;
  String relation;
  String eventEndTime;
  bool visitorList;
  Function onPressed;
  bool pastBookings;
  ListViewListBookings(
      {this.title,
      this.pastBookings,
      this.eventStartTime,
      this.eventEndTime,
      this.evenDate,
      this.relation,
      this.visitor,
      this.onPressed,
      this.visitorList});
  @override
  _ListViewListBookingsState createState() => _ListViewListBookingsState();
}

class _ListViewListBookingsState extends State<ListViewListBookings> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 80),
        ),
        widget.visitorList == true
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  CheckBoxListBookings(),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  ),
                  Text(
                    widget.evenDate,
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "SFProText-Regular",
                        fontSize: 18),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 30),
                  ),
                  Text(
                    widget.eventStartTime + "-" + widget.eventEndTime,
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "SFProText-Regular",
                        fontSize: 18),
                  ),
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  CheckBoxListBookings(),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 20),
                  ),
                  Text(
                    widget.title,
                    style: TextStyle(
                        color: Colors.black,
                        fontFamily: "SFProText-Regular",
                        fontSize: 18),
                  ),
                  SizedBox(
                    width: screenWidth(context, dividedBy: 2.4),
                  )
                ],
              ),
        SizedBox(
          height: screenHeight(context, dividedBy: 80),
        ),
        widget.visitorList == true
            ? Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    color: Constants.kitGradients[24],
                    width: screenWidth(context, dividedBy: 1.2),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 70),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          // mainAxisSize: MainAxisSize.max,
                          children: [
                            // SizedBox(
                            //   width: screenWidth(context,dividedBy: 50),
                            // ),
                            Text(
                              widget.visitor,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: "SFProText-Regular",
                                  fontSize: 15),
                            ),

                            // SizedBox(
                            //   width: screenWidth(context,dividedBy: 20),
                            // ),
                            Container(
                              height: screenHeight(context, dividedBy: 30),
                              width: screenWidth(context, dividedBy: 200),
                              color: Colors.black,
                            ),

                            // SizedBox(
                            //   width: screenWidth(context,dividedBy: 20),
                            // ),
                            Text(
                              widget.relation,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: "SFProText-Regular",
                                  fontSize: 15),
                            ),

                            SizedBox(
                              width: screenWidth(context, dividedBy: 20),
                            )
                          ],
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 76),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: screenWidth(context, dividedBy: 20)),
                          child: RaisedButton(
                              onPressed: () {},
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(9),
                                  side: BorderSide(color: Colors.black)),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: screenWidth(context, dividedBy: 4),
                                  ),
                                  Center(
                                    child: Text(
                                      "Cancel",
                                      style: TextStyle(
                                          color: Colors.black,
                                          fontFamily: "SFProText-Regular",
                                          fontSize: 18),
                                    ),
                                  ),

                                  // SizedBox(
                                  //   width: screenWidth(context,dividedBy: 3.7),
                                  // ),
                                ],
                              )),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 90),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    color: Constants.kitGradients[24],
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 70),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            SizedBox(
                              width: screenWidth(context, dividedBy: 30),
                            ),
                            Text(
                              widget.evenDate,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: "SFProText-Regular",
                                  fontSize: 16),
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 50),
                            ),
                            Container(
                              height: screenHeight(context, dividedBy: 30),
                              width: screenWidth(context, dividedBy: 200),
                              color: Colors.black,
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 50),
                            ),
                            Text(
                              widget.eventStartTime + "-" + widget.eventEndTime,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontFamily: "SFProText-Regular",
                                  fontSize: 16),
                            ),
                            SizedBox(
                              width: screenWidth(context, dividedBy: 20),
                            )
                          ],
                        ),
                        SizedBox(
                          width: screenWidth(context, dividedBy: 76),
                        ),
                        widget.pastBookings != true
                            ? Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        screenWidth(context, dividedBy: 20)),
                                child: RaisedButton(
                                    onPressed: () {
                                      widget.onPressed();
                                    },
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(9),
                                        side: BorderSide(color: Colors.black)),
                                    child: Row(
                                      children: [
                                        SizedBox(
                                          width: screenWidth(context,
                                              dividedBy: 4),
                                        ),
                                        Center(
                                          child: Text(
                                            "Cancel",
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontFamily: "SFProText-Regular",
                                                fontSize: 18),
                                          ),
                                        ),
                                        SizedBox(
                                          width: screenWidth(context,
                                              dividedBy: 3.7),
                                        ),
                                      ],
                                    )),
                              )
                            : Container(
                                width: screenWidth(context, dividedBy: 1.2),
                              ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 90),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
        SizedBox(
          height: screenHeight(context, dividedBy: 80),
        ),
        Divider(
          thickness: 2,
        )
      ],
    );
  }
}
