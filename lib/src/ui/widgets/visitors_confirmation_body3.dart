import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:guoco_priv_app/src/ui/widgets/action_alert_content.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class VisitorsConfirmationBody3 extends StatefulWidget {
  String content1;


  Function onChanged;
  int counter;
  VisitorsConfirmationBody3({this.content1,
    this.onChanged,this.counter,});
  @override
  _VisitorsConfirmationBody3State createState() => _VisitorsConfirmationBody3State();
}

class _VisitorsConfirmationBody3State extends State<VisitorsConfirmationBody3> {
  int count;
  @override
  Widget build(BuildContext context) {
    return Column(

      children: [

        SizedBox(
          height: screenHeight(context,dividedBy: 20),
        ),

        Row(
          children: [

            SizedBox(
              width: screenWidth(context,dividedBy: 17),
            ),
            Container(
                width: screenWidth(context,dividedBy: 2),
                child: Text("Additional Info",style: TextStyle(color:widget.counter==3?Constants.kitGradients[11]:Colors.black,
                    fontFamily: 'SFProText-SemiBold',fontSize: 20,fontWeight: FontWeight.w600),)),

            SizedBox(width: screenWidth(context,dividedBy:3.1 ),),
            GestureDetector(
              onTap: (){

              },
              child:  widget.counter==3?GestureDetector(
                onTap: (){
                  setState(() {
                    count=0;
                    widget.onChanged(count);
                  });

                },
                child:  SvgPicture.asset(
                  'assets/icons/arrow_upward.svg',
                  width: screenWidth(context, dividedBy: 40),
                  height: screenHeight(context, dividedBy: 40),
                  color: Constants.kitGradients[11],
                ),
              ):GestureDetector(
                onTap: (){
                  setState(() {
                    count=3;
                    widget.onChanged(count);
                  });

                },
                child:  SvgPicture.asset('assets/icons/arrow_down.svg',
                  width: screenWidth(context, dividedBy: 40),
                  height: screenHeight(context, dividedBy: 40),
                  color: Colors.black,
                ),
              ),
            ),


          ],
        ),

        widget.counter==3?Column(
          children: [
            SizedBox(
              height: screenHeight(context,dividedBy: 20),
            ),

            AlertBoxContent(
              heading: "Additional Info:",content: widget.content1,
            ),

          ],
        ):Container(),

        SizedBox(
          height: screenHeight(context,dividedBy: 20),
        ),
        Padding(
          padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20)),
          child: Divider(
            thickness: 2,
            color: Colors.grey,
          ),
        )

      ],
    );










  }
}
