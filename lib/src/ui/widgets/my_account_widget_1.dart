import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class MyAccountWidget1 extends StatefulWidget {
  String name,membership;
  MyAccountWidget1({this.name,this.membership});
  @override
  _MyAccountWidget1State createState() => _MyAccountWidget1State();
}

class _MyAccountWidget1State extends State<MyAccountWidget1> {
  @override
  Widget build(BuildContext context) {
    return  Stack(
      children: [
        Container(
            width: screenWidth(context, dividedBy: 1.0),
            height: screenHeight(context, dividedBy: 4.5),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
                        'assets/images/golden_background_layer1.png'),
                    fit: BoxFit.fill))),
        Container(
            width: screenWidth(context, dividedBy: 1.0),
            height: screenHeight(context, dividedBy: 4.5),
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: AssetImage(
                        'assets/images/golden_background_layer2.png'),
                    fit: BoxFit.fill))),
        Center(
            child: Image.asset(
              'assets/icons/label_icon.png',
              width: screenWidth(context, dividedBy: 2.5),
              height: screenHeight(context, dividedBy: 5),
            )),
        Positioned(
          top: screenHeight(context,dividedBy: 6.8),
          left: screenWidth(context,dividedBy: 12),
          child: Row(
            children: [
              Container(
                width:screenWidth(context,dividedBy: 2.8),
                child: Text(
                  widget.name,
                  overflow: TextOverflow.visible,
                  maxLines: 2,
                  style: TextStyle(
                    fontFamily: 'MuliBold',
                    fontSize: 15,
                    color: Constants.kitGradients[0],
                    shadows: <Shadow>[
                      Shadow(
                        offset: Offset(0.0, 0.0),
                        blurRadius: 3,
                        color: Color.fromARGB(255, 0, 0, 0),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                width: screenWidth(context,dividedBy: 5.5),
              ),
              Text(
                widget.membership,
                style: TextStyle(
                  fontFamily: 'MuliBold',
                  fontSize: 15,
                  color: Constants.kitGradients[0],
                  shadows: <Shadow>[
                    Shadow(
                      offset: Offset(0.0, 0.0),
                      blurRadius: 3,
                      color: Color.fromARGB(255, 0, 0, 0),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    );
  }
}
