import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/invite_visitors_body3_question.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

import 'form_field_report_page.dart';

class InviteVisitorsBody3Mandatory extends StatefulWidget {
  TextEditingController fullNameText = new TextEditingController();
  TextEditingController contactText = new TextEditingController();
  TextEditingController relationText = new TextEditingController();
  TextEditingController purposeText = new TextEditingController();
  bool accessAnswer;

  @override
  _InviteVisitorsBody3MandatoryState createState() =>
      _InviteVisitorsBody3MandatoryState();
}

class _InviteVisitorsBody3MandatoryState
    extends State<InviteVisitorsBody3Mandatory> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        FormFieldReportPage(
          inviteVisitors: true,
          title: "Full Name of Main Visitor",
          textEditingController: widget.fullNameText,
          hintText: '',
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        FormFieldReportPage(
          inviteVisitors: true,
          title: "Contact No:",
          textEditingController: widget.contactText,
          hintText: '',
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        FormFieldReportPage(
          inviteVisitors: true,
          title: "Relationship",
          textEditingController: widget.relationText,
          hintText: '',
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        FormFieldReportPage(
          inviteVisitors: true,
          title: "Purpose of Visit",
          textEditingController: widget.purposeText,
          hintText: '',
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        InviteVisitorsBody3Question(
          question: "Access required?",
          option1: "Yes",
          option2: "No",
          valueChanged: (value) {
            widget.accessAnswer = value;
          },
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
      ],
    );
  }
}
