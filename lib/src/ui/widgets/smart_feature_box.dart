import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class SmartFeatureBox extends StatefulWidget {
  final String image;
  final String heading;
  final Function onPressed;
  final bool topRight;
  final bool topLeft;
  final bool bottomLeft;
  final bool bottomRight;
  SmartFeatureBox({
    this.image,
    this.heading,
    this.topRight,
    this.bottomLeft,
    this.bottomRight,
    this.topLeft,
    this.onPressed,
  });
  @override
  _SmartFeatureBoxState createState() => _SmartFeatureBoxState();
}

class _SmartFeatureBoxState extends State<SmartFeatureBox> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        GestureDetector(
          onTap: widget.onPressed,
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  child: Image(
                    image: AssetImage(widget.image),
                    height: screenHeight(context, dividedBy: 30),
                  ),
                ),
                Container(
                  child: Text(
                    widget.heading,
                    style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontFamily: 'SFProText-Regular',
                      fontSize: screenWidth(context, dividedBy: 40),
                    ),
                    textAlign: TextAlign.center,
                  ),
                  width: screenWidth(context, dividedBy: 7),
                ),
              ],
            ),
            width: screenWidth(context, dividedBy: 4),
            height: screenHeight(context, dividedBy: 10),
            decoration: BoxDecoration(
                color: Constants.kitGradients[32],
                borderRadius: BorderRadius.only(
                    topRight: Radius.circular(widget.topRight != true ? 0 : 5),
                    topLeft: Radius.circular(widget.topLeft != true ? 0 : 5),
                    bottomLeft:
                        Radius.circular(widget.bottomLeft != true ? 0 : 5),
                    bottomRight:
                        Radius.circular(widget.bottomRight != true ? 0 : 5))),
          ),
        ),
      ],
    );
  }
}
