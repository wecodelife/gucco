import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class KeyCollectionVenue extends StatefulWidget {
  bool keyCollectionStatus;

  KeyCollectionVenue({this.keyCollectionStatus});

  @override
  _KeyCollectionVenueState createState() => _KeyCollectionVenueState();
}

class _KeyCollectionVenueState extends State<KeyCollectionVenue> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 15),
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Key Collection Venue',
                    style: widget.keyCollectionStatus == true
                        ? TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[11])
                        : TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 15,
                            fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[7])),
                widget.keyCollectionStatus == true
                    ? SvgPicture.asset(
                        'assets/icons/arrow_upward.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[11],
                      )
                    : SvgPicture.asset(
                        'assets/icons/arrow_down.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[7],
                      ),
              ],
            ),
          ),
          onTap: () {
            widget.keyCollectionStatus == false
                ? setState(() {
                    widget.keyCollectionStatus = true;
                  })
                : setState(() {
                    widget.keyCollectionStatus = false;
                  });
          },
        ),
        widget.keyCollectionStatus == true
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text('The venue for Key Collection will be at:',
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 60),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(ObjectFactory().appHive.getKeyCollectionVenue(),
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }
}
