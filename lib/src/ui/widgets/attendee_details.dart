import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class AttendeeDetails extends StatefulWidget {
  bool attendeeDetailsStatus;
  String attendee;
  String contactNo;
  String email;
  String noOfPeople;
  String licensePlate;
  String additionalInfo;
  AttendeeDetails({this.attendeeDetailsStatus,this.noOfPeople,this.licensePlate,this.email,this.contactNo,this.attendee,this.additionalInfo});

  @override
  _AttendeeDetailsState createState() => _AttendeeDetailsState();
}

class _AttendeeDetailsState extends State<AttendeeDetails> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          child: Container(
            width: screenWidth(context, dividedBy: 1),
            height: screenHeight(context, dividedBy: 15),
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('Attendee Details',
                    style: widget.attendeeDetailsStatus == true
                        ? TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 15,
                        fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[11])
                        : TextStyle(
                            fontFamily: 'SFProText-SemiBold',
                            fontSize: 15,
                        fontWeight: FontWeight.w500,
                            color: Constants.kitGradients[7])),
                widget.attendeeDetailsStatus == true
                    ? SvgPicture.asset(
                        'assets/icons/arrow_upward.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[11],
                      )
                    : SvgPicture.asset(
                        'assets/icons/arrow_down.svg',
                        width: screenWidth(context, dividedBy: 40),
                        height: screenHeight(context, dividedBy: 40),
                        color: Constants.kitGradients[7],
                      ),
              ],
            ),
          ),
          onTap: () {
            widget.attendeeDetailsStatus == false
                ? setState(() {
                    widget.attendeeDetailsStatus = true;
                  })
                : setState(() {
                    widget.attendeeDetailsStatus = false;
                  });
          },
        ),
        widget.attendeeDetailsStatus == true
            ? Column(
                children: [
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Attendee:',
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                      Text(widget.attendee.trim().length!=0 ? widget.attendee:"-",
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 55),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Contact No:',
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                      Text(widget.contactNo.trim().length!=0 ? widget.contactNo:"-",
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Email Address:',
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                      Text(widget.email,
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 15),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('No: of people',
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                      Text(widget.noOfPeople,
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('License Plate',
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                      Text(widget.licensePlate.trim().length!=0 ? widget.licensePlate:"-",
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Additional Info',
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                      Text(widget.additionalInfo.trim().length!=0 ? widget.additionalInfo:"-",
                          style: TextStyle(
                              fontFamily: 'SFProText-Regular',
                              fontSize: 15,
                              color: Constants.kitGradients[7])),
                    ],
                  ),
                  SizedBox(
                    height: screenHeight(context, dividedBy: 30),
                  ),
                ],
              )
            : Container(),
      ],
    );
  }
}
