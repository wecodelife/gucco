import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class InviteVisitorBody1 extends StatefulWidget {
  final String date, fromTime, toTime;

  InviteVisitorBody1({this.date, this.fromTime, this.toTime});

  @override
  _InviteVisitorBody1State createState() => _InviteVisitorBody1State();
}

class _InviteVisitorBody1State extends State<InviteVisitorBody1> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('Date:',
                  style: TextStyle(
                      fontFamily: 'SFProText-Regular',
                      fontSize: 15,
                      color: Constants.kitGradients[7])),
              Text(widget.date,
                  style: TextStyle(
                      fontFamily: 'SFProText-Regular',
                      fontSize: 15,
                      color: Constants.kitGradients[7])),
            ],
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 40),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('From Time:',
                  style: TextStyle(
                      fontFamily: 'SFProText-Regular',
                      fontSize: 15,
                      color: Constants.kitGradients[7])),
              Text(widget.fromTime,
                  style: TextStyle(
                      fontFamily: 'SFProText-Regular',
                      fontSize: 15,
                      color: Constants.kitGradients[7])),
            ],
          ),
        ),
        SizedBox(
          height: screenHeight(context, dividedBy: 30),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('To Time:',
                  style: TextStyle(
                      fontFamily: 'SFProText-Regular',
                      fontSize: 15,
                      color: Constants.kitGradients[7])),
              Text(widget.toTime,
                  style: TextStyle(
                      fontFamily: 'SFProText-Regular',
                      fontSize: 15,
                      color: Constants.kitGradients[7])),
            ],
          ),
        ),
      ],
    );
  }
}
