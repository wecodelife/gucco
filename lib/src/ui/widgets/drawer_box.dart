import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class DrawerBox extends StatefulWidget {
  Function onTap;
  String title;
  DrawerBox({this.title, this.onTap});
  @override
  _DrawerBoxState createState() => _DrawerBoxState();
}

class _DrawerBoxState extends State<DrawerBox> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    widget.title,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: screenWidth(context, dividedBy: 23),
                      fontFamily: widget.title == "Home"
                          ? 'MontserratRegular'
                          : 'MontserratMedium',
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: screenHeight(context, dividedBy: 90),
              ),
              Container(
                width: screenWidth(context, dividedBy: 1.65),
                height: 2,
                color: Colors.white,
              )
            ],
          ),
        ],
      ),
    );
  }
}
