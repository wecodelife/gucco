import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';

class PreferenceTile extends StatefulWidget {
  String icon;
  String description;
  bool shimmer;
  int index;
  Function onTap;
  Function selectFunction;
  Function deselectFunction;

  final ValueChanged<bool> onSelected;
  PreferenceTile(
      {this.onSelected,
      this.icon,
      this.description,
      this.shimmer,
      this.index,
      this.onTap,
      this.selectFunction,
      this.deselectFunction});
  @override
  _PreferenceTileState createState() => _PreferenceTileState();
}

class _PreferenceTileState extends State<PreferenceTile> {
  List<Color> tileColor1 = [
    Constants.kitGradients[6],
    Constants.kitGradients[5]
  ];
  List<Color> tileColor2 = [
    Constants.kitGradients[4],
    Constants.kitGradients[7]
  ];
  bool onClick;
  @override
  void initState() {
    setState(() {
      onClick = false;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.shimmer == false
        ? GestureDetector(
            child: Padding(
              padding: EdgeInsets.all(4),
              child: Container(
                  height: screenHeight(context, dividedBy: 4),
                  width: screenWidth(context, dividedBy: 3.1),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10.0),
                          bottomRight: Radius.circular(10.0),
                          topLeft: Radius.circular(10.0),
                          bottomLeft: Radius.circular(10.0)),
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: onClick == true ? tileColor1 : tileColor2)),
                  child: new GridTile(
                    child: Padding(
                      padding: EdgeInsets.all(8),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          // Container(
                          //   child: Image.asset(
                          //     widget.icon,
                          //     fit: BoxFit.cover,
                          //     color: Colors.white,
                          //   ),
                          //   height: screenHeight(context, dividedBy: 20),
                          //   width: screenHeight(context, dividedBy: 20),
                          // ),
                          Container(
                            width: screenWidth(context, dividedBy: 4),
                            child: Center(
                              child: Text(widget.description,
                                  style: TextStyle(
                                      fontSize:
                                          screenWidth(context, dividedBy: 22),
                                      color: Colors.white,
                                      fontFamily: "Muli"),
                                  textAlign: TextAlign.center),
                            ),
                          )
                        ],
                      ),
                    ),
                  )),
            ),
            onTap: () {
              setState(() {
                onClick = !onClick;
              });
              if (onClick == true)
                widget.selectFunction();
              else
                widget.deselectFunction();
              print(onClick);

              widget.onSelected(onClick);
            },
          )
        : Shimmer.fromColors(
            highlightColor: Colors.grey,
            baseColor: Colors.black38,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: screenHeight(context, dividedBy: 4),
                width: screenWidth(context, dividedBy: 3.1),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10.0),
                        bottomRight: Radius.circular(10.0),
                        topLeft: Radius.circular(10.0),
                        bottomLeft: Radius.circular(10.0)),
                    color: Colors.black54),
              ),
            ),
          );
  }
}
