import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class FormFeildPasswordBox extends StatefulWidget {
  String hintText;
  TextEditingController textEditingController;
  Function onChanged;
  FormFeildPasswordBox(
      {this.hintText, this.textEditingController, this.onChanged});
  @override
  _FormFeildPasswordBoxState createState() => _FormFeildPasswordBoxState();
}

class _FormFeildPasswordBoxState extends State<FormFeildPasswordBox> {
  bool visibility;
  @override
  void initState() {
    visibility = true;
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: screenWidth(context, dividedBy: 10),
            vertical: screenHeight(context, dividedBy: 60)),
        child: TextField(
          controller: widget.textEditingController,
          onChanged: (val) {
            widget.onChanged();
          },
          style: TextStyle(
              color: Constants.kitGradients[0],
              fontFamily: 'MulSemiBold',
              fontSize: 16),
          autofocus: false,
          obscureText: visibility,
          cursorColor: Constants.kitGradients[0],
          decoration: InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Constants.kitGradients[0]),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Constants.kitGradients[0]),
              ),
              hintText: widget.hintText,
              hintStyle: TextStyle(
                color: Constants.kitGradients[0],
                fontFamily: 'MuliSemiBold',
                fontSize: 16,
              ),
              suffixIcon: GestureDetector(
                onTap: () {
                  setState(() {
                    visibility = !visibility;
                    print(visibility);
                  });
                },
                child: visibility == false
                    ? Icon(
                        Icons.visibility_off_outlined,
                        color: Colors.grey,
                      )
                    : Icon(
                        Icons.visibility_outlined,
                        color: Colors.grey,
                      ),
              )),
        ),
      ),
    );
  }
}
