import 'package:flutter/cupertino.dart';
import 'package:guoco_priv_app/src/ui/screens/pdf_view.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:guoco_priv_app/src/ui/widgets/e_application_list_item.dart';

class EApplicationListView extends StatefulWidget {
  String tabValue;
  EApplicationListView({
    this.tabValue
  });
  @override
  _EApplicationListViewState createState() => _EApplicationListViewState();
}

class _EApplicationListViewState extends State<EApplicationListView> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.tabValue=="new application"?screenWidth(context,dividedBy: 1.1):screenWidth(context,dividedBy: 1),
      child: ListView.builder(
        physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          scrollDirection: Axis.vertical,
          itemCount: 3,
          itemBuilder:
              (BuildContext context, int index) {
            return widget.tabValue=="new application"?GestureDetector(
              onTap: (){

              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: screenHeight(context,dividedBy: 80),
                  ),
                  Container(
                    width: screenWidth(context,dividedBy: 1.1),
                    height: screenHeight(context,dividedBy: 5),
                    decoration: BoxDecoration
                      (
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        image: DecorationImage(
                          image: AssetImage("assets/images/car.png"),
                          fit: BoxFit.fill,

                        )
                    ),
                  ),
                  SizedBox(
                    height: screenHeight(context,dividedBy: 80),
                  ),
                  Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context,dividedBy: 30),
                      ),
                      Text("First Car Registration",style: TextStyle(fontFamily: "SFProText-SemiBold",fontSize: 17,fontWeight: FontWeight.w600),),
                    ],
                  )
                ],
              ),
            ):EapplicationListItem();
          }),
    );
  }
}
