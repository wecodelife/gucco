import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/widgets/flutter_calender.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';
import 'package:intl/intl.dart' show DateFormat;

class CarouselCalender extends StatefulWidget {
  ValueChanged onValueChanged;
  CarouselCalender({Key key, this.onValueChanged}) : super(key: key);
  @override
  _CarouselCalenderState createState() => new _CarouselCalenderState();
}

class _CarouselCalenderState extends State<CarouselCalender> {
  DateTime _currentDate = DateTime(2021, 1, 14);
  String _currentMonthYear = DateFormat.yMMM().format(DateTime.now());
  DateTime _targetDateTime = new DateTime.now();
  DateTime _minSelectedDate = DateTime.now().add(Duration(days: 2));
  DateTime _maxSelectedDate = DateTime(
      DateTime.now().year, DateTime.now().month + 2, DateTime.now().day);
  DateTime todayDate = DateTime.now();

  String currentYear;
  String currentMonth;
  CalendarCarousel _calendarCarouselHeader;
  String selectedValue;
  String calenderMonth;
  bool currentDateSelected;

  List<String> month = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec"
  ];
  List<String> year = [];
  String selectedYear;
  String selectedMonth;

  void yearGenerator() {
    for (int i = 2021; i <= 2025; i++) {
      year.add(i.toString());
    }
  }

  @override
  void initState() {
    currentYear = _targetDateTime.year.toString();
    calenderMonth = _currentMonthYear;

    print(calenderMonth);
    print(currentYear);
    yearGenerator();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _calendarCarouselHeader = CalendarCarousel<Event>(
      selectableDates: [DateTime.now().add(Duration(days: 2))],
      onDayPressed: (DateTime date, List<Event> events) {
        this.setState(() => _currentDate = date);
        currentDateSelected = false;
        print("dad" + _targetDateTime.toString());
        widget.onValueChanged(date);
        events.forEach((event) => print(event.title));
      },
      scrollDirection: Axis.horizontal,
      selectedDayBorderColor: Constants.kitGradients[10],
      showOnlyCurrentMonthDate: false,
      selectedDayButtonColor: Constants.kitGradients[12],
      childAspectRatio: screenHeight(context, dividedBy: 10) /
          screenHeight(context, dividedBy: 10),
      dayCrossAxisAlignment: CrossAxisAlignment.center,

      weekendTextStyle: TextStyle(
        color: Colors.grey,
      ),
      thisMonthDayBorderColor: Colors.white,
      weekFormat: false,
//      firstDayOfWeek: 4,
      height: screenHeight(context, dividedBy: 2.4),
      selectedDateTime: _currentDate,
      targetDateTime: _targetDateTime,
      customGridViewPhysics: NeverScrollableScrollPhysics(),

      showHeader: false,
      todayTextStyle: TextStyle(
        color: currentDateSelected == false
            ? Colors.black
            : Constants.kitGradients[11],
      ),

      weekdayTextStyle: TextStyle(color: Colors.black),
      dayButtonColor: Constants.kitGradients[0],
      todayBorderColor: Colors.white,

      todayButtonColor: Colors.white,
      selectedDayTextStyle: TextStyle(
        color: Constants.kitGradients[11],
      ),
      minSelectedDate: _minSelectedDate,
      maxSelectedDate: _maxSelectedDate,
      prevDaysTextStyle: TextStyle(
        fontSize: 16,
        color: Colors.grey,
      ),

      inactiveDaysTextStyle: TextStyle(
        color: Colors.grey,
        fontSize: 16,
      ),
      onCalendarChanged: (DateTime date) {
        this.setState(() {
          _targetDateTime = date;
          _currentMonthYear = DateFormat.yMMM().format(_targetDateTime);
          print(_targetDateTime);
        });
      },
    );

    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: screenHeight(context, dividedBy: 130),
        ),

        Container(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                width: screenWidth(context, dividedBy: 20),
              ),

              Container(
                  width: screenWidth(context, dividedBy: 4),
                  child: Text(
                    _currentMonthYear,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontFamily: 'SFProText-Medium',
                    ),
                  )),

              SizedBox(
                width: screenWidth(context, dividedBy: 2.3),
              ),

              // DropDownFormField(title:currentYear,underline: false,
              //   dropDownList: year,onClicked: (value){
              //   selectedYear=value;
              //   setState(() {
              //     _targetDateTime=DateTime(int.parse(selectedYear),_targetDateTime.month);
              //   });
              //     },boxHeight: 20,boxWidth: 5,dropDownValue: selectedYear,hintText:true,fontFamily: 'SFProText-Medium',
              //   hintPadding: 300,
              //
              // ),

              // DropDownFormField(title:calenderMonth,underline: false,
              //   dropDownList: month,onClicked: (value){
              //     selectedMonth=value;
              //     setState(() {
              //       _targetDateTime=DateTime(_targetDateTime.year,month.indexOf(selectedMonth)+1);
              //     });
              //   },boxHeight: 20,boxWidth: 5,dropDownValue: selectedMonth,hintText:true,fontFamily: 'SFProText-Medium',
              //   hintPadding: 40,
              //
              // ),

              GestureDetector(
                child: Icon(
                  Icons.arrow_back_ios,
                  size: 18,
                ),
                onTap: () {
                  // if (_currentMonthYear !=
                  //     DateFormat.yMMM().format(DateTime.now()))
                  setState(() {
                    _targetDateTime = DateTime(
                        _targetDateTime.year, _targetDateTime.month - 1);
                    print(currentMonth.toString());
                  });
                },
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 10),
              ),
              GestureDetector(
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: 18,
                ),
                onTap: () {
                  setState(() {
                    _targetDateTime = DateTime(
                        _targetDateTime.year, _targetDateTime.month + 1);
                  });
                },
              )
            ],
          ),
        ),

        SizedBox(
          height: screenHeight(context, dividedBy: 50),
        ),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 16.0),
          child: _calendarCarouselHeader,
        ), //
      ],
    ));
  }
}
