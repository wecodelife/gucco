import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class HomeTile extends StatefulWidget {
  final double height;
  final double width;
  final bool likeBar;
  final int likes;
  final bool likeButtonBrown;
  final String tileIcon;
  final String itemImage;
  final String subHeading;
  final bool isSubHeading;
  final double fontSize;
  double borderRadius;
  double boxHeight;
  bool textelipsis;
  final double subHeadingLeftPadding;
  final String itemDescription;
  final ValueChanged<bool> onClicked;
  HomeTile(
      {this.height,
      this.width,
      this.likeBar,
      this.likes,
      this.onClicked,
      this.itemDescription,
      this.itemImage,
      this.borderRadius,
      this.tileIcon,
      this.likeButtonBrown,
      this.subHeading,
      this.isSubHeading,
      this.boxHeight,
      this.fontSize,
      this.subHeadingLeftPadding,
      this.textelipsis});
  @override
  _HomeTileState createState() => _HomeTileState();
}

class _HomeTileState extends State<HomeTile> {
  @override
  void initState() {
    setState(() {
      like = false;
    });
    super.initState();
  }

  bool like;
  @override
  Widget build(BuildContext context) {
    print(widget.height);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: widget.height,
        width: widget.width,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
            image: DecorationImage(
              image: AssetImage(widget.itemImage),
              colorFilter: new ColorFilter.mode(
                  Colors.black.withOpacity(0.30), BlendMode.darken),
              fit: BoxFit.cover,
            )),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 80),
                ),
                widget.likeBar == true
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          // SvgPicture.asset(widget.tileIcon),

                          SizedBox(
                            width: screenWidth(context, dividedBy: 6),
                          ),

                          Container(
                            height: screenHeight(context, dividedBy: 30),
                            width: screenWidth(context, dividedBy: 6),
                            decoration: BoxDecoration(
                                color: Constants.kitGradients[1],
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(widget.borderRadius),
                                  bottomLeft:
                                      Radius.circular(widget.borderRadius),
                                )),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Text(
                                  widget.likes.toString(),
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: Constants.kitGradients[0]),
                                ),
                                GestureDetector(
                                  child: like == false
                                      ? Stack(
                                          children: [
                                            ImageIcon(
                                              AssetImage(
                                                  "assets/icons/likebrown.png"),
                                              color: Constants.kitGradients[5],
                                            ),
                                            ImageIcon(
                                              AssetImage(
                                                  "assets/icons/unlike.png"),
                                              color: Constants.kitGradients[0],
                                            ),
                                          ],
                                        )
                                      : widget.likeButtonBrown == true
                                          ? Image(
                                              image: AssetImage(
                                                  "assets/icons/likebrown.png"),
                                            )
                                          : ImageIcon(
                                              AssetImage(
                                                  "assets/icons/like.png"),
                                              color: Constants.kitGradients[3],
                                            ),
                                  onTap: () {
                                    setState(() {
                                      like = !like;
                                    });
                                    widget.onClicked(like);
                                  },
                                )
                              ],
                            ),
                          )
                        ],
                      )
                    : Container()
              ],
            ),
            SizedBox(
              height: screenWidth(context, dividedBy: widget.boxHeight),
            ),
            SizedBox(
              width: screenWidth(context, dividedBy: 30),
            ),
            Container(
              width: screenWidth(context, dividedBy: 2.7),
              child: Text(widget.itemDescription,
                  style: TextStyle(
                      fontSize: widget.fontSize,
                      color: Constants.kitGradients[0],
                      fontFamily: "Muli",
                      fontWeight: FontWeight.bold),
                  overflow: widget.textelipsis == true
                      ? TextOverflow.ellipsis
                      : TextOverflow.visible),
            ),
            widget.isSubHeading == true
                ? Row(
                    children: [
                      SizedBox(
                        width: screenWidth(context,
                            dividedBy: widget.subHeadingLeftPadding),
                      ),
                      Text(
                        widget.subHeading,
                        style: TextStyle(
                            fontSize: 14,
                            color: Constants.kitGradients[0],
                            fontFamily: "Muli",
                            fontWeight: FontWeight.bold),
                      ),
                    ],
                  )
                : Container(height: 1),
          ],
        ),
      ),
    );
  }
}
