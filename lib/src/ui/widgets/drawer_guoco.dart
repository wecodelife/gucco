import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/ui/screens/by_laws.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_modern/estate_page.dart';
import 'package:guoco_priv_app/src/ui/screens/martin_property_details.dart';
import 'package:guoco_priv_app/src/ui/screens/profile_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/drawer_box.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

class DrawerGuoco extends StatefulWidget {
  final int count;
  final String unitId;
  final String blockName;
  final String floorAndUnitId;
  DrawerGuoco({this.count, this.floorAndUnitId, this.blockName, this.unitId});
  @override
  _DrawerGuocoState createState() => _DrawerGuocoState();
}

class _DrawerGuocoState extends State<DrawerGuoco> {
  @override
  void initState() {
    print(widget.count);
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context, dividedBy: 1),
      width: screenWidth(context, dividedBy: 1.2),
      decoration: BoxDecoration(
          color: Constants.kitGradients[31],
          image: DecorationImage(
            image: AssetImage("assets/images/bg_drawer.png"),
            // colorFilter: new ColorFilter.mode(Constants.kitGradients[1].withOpacity(0.8), BlendMode.dstATop),
            fit: BoxFit.cover,
          )),
      child: Padding(
        padding: EdgeInsets.only(left: screenWidth(context, dividedBy: 6)),
        child: Row(
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: screenHeight(context, dividedBy: 7),
                ),

                widget.count == 1
                    ? DrawerBox(
                        title: "Home",
                        onTap: () {},
                      )
                    : GestureDetector(
                        onTap: () {
                          push(context, EstatePage());
                        },
                        child: Text(
                          "Home",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: screenWidth(context, dividedBy: 23),
                              fontFamily: 'MontserratRegular'),
                        )),

                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),

                // widget.count == 2
                //     ? GestureDetector(
                //         onTap: () {},
                //         child: Row(
                //           children: [
                //             Column(
                //               crossAxisAlignment: CrossAxisAlignment.start,
                //               children: [
                //                 Row(
                //                   children: [
                //                     RichText(
                //                         text: TextSpan(
                //                             text: "Messages",
                //                             style: TextStyle(
                //                                 color: Colors.white,
                //                                 fontSize: screenWidth(context,
                //                                     dividedBy: 19),
                //                                 fontFamily: 'MontserratMedium',
                //                                 fontWeight: FontWeight.w700),
                //                             children: [
                //                           TextSpan(
                //                             text: "( New 1)",
                //                             style: TextStyle(
                //                                 color: Colors.white,
                //                                 fontSize: screenWidth(context,
                //                                     dividedBy: 19),
                //                                 fontFamily: 'MontserratMedium',
                //                                 fontWeight: FontWeight.w700,
                //                                 fontStyle: FontStyle.italic),
                //                           )
                //                         ])),
                //                   ],
                //                 ),
                //                 SizedBox(
                //                   height: screenHeight(context, dividedBy: 60),
                //                 ),
                //                 Container(
                //                   width: screenWidth(context, dividedBy: 1.65),
                //                   height: 2,
                //                   color: Colors.white,
                //                 )
                //               ],
                //             ),
                //           ],
                //         ),
                //       )
                //     : GestureDetector(
                //         onTap: () {},
                //         child: Row(
                //           mainAxisAlignment: MainAxisAlignment.start,
                //           children: [
                //             RichText(
                //                 text: TextSpan(
                //                     text: "Messages",
                //                     style: TextStyle(
                //                         color: Colors.white,
                //                         fontSize:
                //                             screenWidth(context, dividedBy: 20),
                //                         fontFamily: 'MontserratRegular'),
                //                     children: [
                //                   TextSpan(
                //                     text: "( New 1)",
                //                     style: TextStyle(
                //                       color: Colors.white,
                //                       fontSize:
                //                           screenWidth(context, dividedBy: 20),
                //                       fontFamily: 'MontserratRegular',
                //                       fontStyle: FontStyle.italic,
                //                     ),
                //                   )
                //                 ])),
                //           ],
                //         ),
                //       ),

                // SizedBox(
                //   height: screenHeight(context, dividedBy: 15),
                // ),

                widget.count == 3
                    ? DrawerBox(
                        title: "Profile",
                        onTap: () {
                          pop(context);
                        },
                      )
                    : GestureDetector(
                        onTap: () {
                          push(context, ProfilePage());
                        },
                        child: Text(
                          "Profile",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: screenWidth(context, dividedBy: 23),
                              fontFamily: 'MontserratMedium'),
                        )),

                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),

                // widget.count == 4
                //     ? DrawerBox(
                //         title: "Bookings",
                //         onTap: () {},
                //       )
                //     : GestureDetector(
                //         onTap: () {
                //           pop(context);
                //         },
                //         child: GestureDetector(
                //             onTap: () {
                //               push(context, FacilitiesListBookings());
                //             },
                //             child: Text(
                //               "Bookings",
                //               style: TextStyle(
                //                   color: Colors.white,
                //                   fontSize: screenWidth(context, dividedBy: 23),
                //                   fontFamily: 'MontserratMedium'),
                //             ))),
                //
                // SizedBox(
                //   height: screenHeight(context, dividedBy: 15),
                // ),

                widget.count == 5
                    ? DrawerBox(
                        title: "Property Details",
                        onTap: () {
                          pop(context);
                        },
                      )
                    : GestureDetector(
                        onTap: () {
                          push(context, MartinPropertyDetails());
                        },
                        child: Text(
                          "Property Details",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: screenWidth(context, dividedBy: 23),
                              fontFamily: 'MontserratMedium'),
                        )),

                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),

                widget.count == 6
                    ? DrawerBox(
                        title: "Resident's Handbook",
                        onTap: () {
                          pop(context);
                        },
                      )
                    : GestureDetector(
                        onTap: () {
                          push(context, ByLaws());
                        },
                        child: Text(
                          "Resident's Handbook",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: screenWidth(context, dividedBy: 23),
                              fontFamily: 'MontserratMedium'),
                        )),

                SizedBox(
                  height: screenHeight(context, dividedBy: 15),
                ),

                // GestureDetector(
                //     onTap: (){
                //       ObjectFactory().appHive.hiveClear(key: "token");
                //
                //       push(context, SelectLoginOrSignUp());
                //     },
                //     child: Text("LOGOUT",style: TextStyle(color:Colors.white,fontSize: screenWidth(context,dividedBy: 19),fontFamily:'MontserratMedium'),)),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
