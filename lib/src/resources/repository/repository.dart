import 'package:guoco_priv_app/src/models/add_unit_login_martin_modern_request.dart';
import 'package:guoco_priv_app/src/models/birthday_request_model.dart';
import 'package:guoco_priv_app/src/models/book_a_facility_request_model.dart';
import 'package:guoco_priv_app/src/models/change_password_for_user_request_model.dart';
import 'package:guoco_priv_app/src/models/create_password_request.dart';
import 'package:guoco_priv_app/src/models/earliest_slot_key_collection_request.dart';
import 'package:guoco_priv_app/src/models/image_upload_request_model.dart';
import 'package:guoco_priv_app/src/models/key_collection_appointments_request.dart';
import 'package:guoco_priv_app/src/models/key_collection_booking_request_model.dart';
import 'package:guoco_priv_app/src/models/key_collection_confirm_request.dart';
import 'package:guoco_priv_app/src/models/key_collection_self_request_model.dart';
import 'package:guoco_priv_app/src/models/login_request_model.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step1_request.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step2_request.dart';
import 'package:guoco_priv_app/src/models/state.dart';
import 'package:guoco_priv_app/src/models/timeSlotsForFacilityRequest.dart';
import 'package:guoco_priv_app/src/resources/api_providers/user_api_provider.dart';

/// Repository is an intermediary class between network and data
class Repository {
  final userApiProvider = UserApiProvider();

  Future<State> login({LoginRequest loginRequest}) =>
      UserApiProvider().loginCall(loginRequest);

  Future<State> forgotPassword({String email}) =>
      UserApiProvider().forgotPassword(email);

  Future<State> attachPreference({String preferenceId, String userId}) =>
      UserApiProvider().attachPreferenceCall(preferenceId, userId);

  Future<State> getPreference() => UserApiProvider().getPreferenceCall();
  Future<State> checkPasswordStrength({String password}) =>
      UserApiProvider().passwordStrength(password: password);

  Future<State> detachPreference({String preferenceId, String userId}) =>
      UserApiProvider().detachPreferenceCall(preferenceId, userId);

  Future<State> loyaltymanagement_referencecode_validate(
          {String referenceCode}) =>
      UserApiProvider().loyaltymanagement_referencecode_validate(referenceCode);

  Future<State> registerUserDetailsStep1(
          {RegisterUserDetailsStep1Request registerUserDetailsStep1Request}) =>
      UserApiProvider()
          .registerUserProfileStep1(registerUserDetailsStep1Request);

  Future<State> registerUserDetailsStep2(
          {RegisterUserDetailsStep2Request registerUserDetailsStep2Request,
          String userId}) =>
      UserApiProvider()
          .registerUserDetailsStep2(registerUserDetailsStep2Request, userId);

  Future<State> privilegeClubTAndCAccept1({String userId}) =>
      UserApiProvider().privilegeClubTAndCAccept1(userId);

  Future<State> privilegeClubTAndCDecline1({String userId}) =>
      UserApiProvider().privilegeClubTAndCDecline1(userId);

  Future<State> privilegeClubTAndCAccept2({String userId}) =>
      UserApiProvider().privilegeClubTAndCAccept2(userId);

  Future<State> privilegeClubTAndCDecline2({String userId}) =>
      UserApiProvider().privilegeClubTAndCDecline2(userId);

  Future<State> birthday({BirthdayRequest birthdayRequest, String userId}) =>
      UserApiProvider().birthdayCall(birthdayRequest, userId);

  Future<State> registerPrivilegeClubOtp({String userId, String otpCode}) =>
      UserApiProvider().registerPrivilegeClubOtpCall(otpCode);

  Future<State> registerPrivilegeClubOtpGenerate({String userId}) =>
      UserApiProvider().registerPrivilegeClubOtpGenerate();

  Future<State> createPassword({CreatePasswordRequest createPasswordRequest}) =>
      UserApiProvider()
          .createPassword(createPasswordRequest: createPasswordRequest);

  Future<State> getProfile() => UserApiProvider().getProfile();

  Future<State> keyCollectionAppointments(
          {KeyCollectionAppointmentsRequest
              keyCollectionAppointmentsRequest}) =>
      UserApiProvider()
          .keyCollectionAppointments(keyCollectionAppointmentsRequest);

  Future<State> keyCollectionBooking(
          {KeyCollectionBookingRequest keyCollectionBookingRequest}) =>
      UserApiProvider().keyCollectionBookingCall(keyCollectionBookingRequest);

  Future<State> keyCollectionConfirm(
          {KeyCollectionConfirmRequest keyCollectionConfirmRequest}) =>
      UserApiProvider().keyCollectionConfirmCall(keyCollectionConfirmRequest);

  Future<State> keyCollectionSelf(
          {KeyCollectionSelfRequest keyCollectionSelfRequest, String unitId}) =>
      UserApiProvider().keyCollectionSelfCall(keyCollectionSelfRequest, unitId);

  Future<State> getCondoInfo() => UserApiProvider().getCondoInfo();

  Future<State> getUnitUsers() => UserApiProvider().getUnitUsers();

  Future<State> addUnitLoginMartinModern(
          {AddUnitLoginMartinModernRequest addUnitLoginMartinModernRequest}) =>
      UserApiProvider()
          .addUnitLoginMartinModernCall(addUnitLoginMartinModernRequest);

  Future<State> getBlockId(String condoId, String blockId) =>
      UserApiProvider().getBlockIdCall(condoId);

  Future<State> getFloorId(String condoId, String blockId) =>
      UserApiProvider().getFloorIdCall(condoId, blockId);

  Future<State> getKeyCollectionTimeSlots(String date, String unitId) =>
      UserApiProvider().getKeyCollectionTimeSlots(date, unitId);

  Future<State> getKeyCollectionAppointments(String unitId) =>
      UserApiProvider().getKeyCollectionAppointments(unitId);

  Future<State> cancelKeyCollectionAppointments(String unitId, String keyId) =>
      UserApiProvider().cancelKeyCollectionAppointments(unitId, keyId);

  Future<State> getMartinModern() => UserApiProvider().getMartinModernProfile();

  Future<State> uploadProfileImage(
          {ImageUploadRequestModel imageUploadRequestModel}) =>
      UserApiProvider().uploadProfileImage(imageUploadRequestModel);

  Future<State> checkPaymentStatus(
          String condoInfoId, String floorId, String blockId, String unitId) =>
      UserApiProvider()
          .checkPaymentStatus(condoInfoId, floorId, blockId, unitId);

  Future<State> changePasswordForUser(
          {ChangePasswordForUserRequestModel
              changePasswordForUserRequestModel}) =>
      UserApiProvider()
          .changePasswordForUser(changePasswordForUserRequestModel);

  Future<State> shareTextKeyCollectionAppointment(
          {String keyCollectionAppointmentId}) =>
      UserApiProvider()
          .shareTextKeyCollectionAppointment(keyCollectionAppointmentId);
  Future<State> getKeyCollectionAppointment(
          {String keyCollectionAppointmentId}) =>
      UserApiProvider().getKeyCollectionAppointment(keyCollectionAppointmentId);
  Future<State> earliestSlotKeyCollection(
          {EarliestSlotKeyCollectionRequest earliestSlotKeyCollectionRequest,
          String unitInfoId}) =>
      UserApiProvider().earliestSlotKeyCollection(
          earliestSlotKeyCollectionRequest, unitInfoId);
  Future<State> getAllFacilitiesInACondominium({String condoInfoId}) =>
      UserApiProvider().getAllFacilitiesInACondominium(condoInfoId);
  Future<State> timeSlotsForFacilities(
          {String facilitiesInfoId,
          String onDate,
          TimeSlotsForFacilitiesRequest timeSlotsForFacilitiesRequest}) =>
      UserApiProvider().timeSlotsForFacilitiesCall(
          facilitiesInfoId, onDate, timeSlotsForFacilitiesRequest);

  Future<State> bookAFacility(
          {BookAFacilityRequest bookAFacilityRequest, String facilityInfoId}) =>
      UserApiProvider().bookAFacilityCall(bookAFacilityRequest, facilityInfoId);
  Future<State> getCurrentBookingsForFacility({String unitInfoId}) =>
      UserApiProvider().getCurrentBookingsForFacility(unitInfoId);
  Future<State> getPastBookingsForFacility({String unitInfoId}) =>
      UserApiProvider().getPastBookingsForFacility(unitInfoId);

  Future<State> cancelABookingFacility({String facilityBookingId}) =>
      UserApiProvider().cancelAFacilityBooking(facilityBookingId);
}
