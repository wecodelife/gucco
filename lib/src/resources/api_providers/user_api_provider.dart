import 'package:guoco_priv_app/src/models/add_unit_login_martin_modern_request.dart';
import 'package:guoco_priv_app/src/models/add_unit_login_martin_modern_response.dart';
import 'package:guoco_priv_app/src/models/attach_preference_response_model.dart';
import 'package:guoco_priv_app/src/models/birthday_request_model.dart';
import 'package:guoco_priv_app/src/models/birthday_response_model.dart';
import 'package:guoco_priv_app/src/models/book_a_facility_request_model.dart';
import 'package:guoco_priv_app/src/models/book_a_facility_response_model.dart';
import 'package:guoco_priv_app/src/models/cancelAFacilityBookingResponse.dart';
import 'package:guoco_priv_app/src/models/change_password_for_user_request_model.dart';
import 'package:guoco_priv_app/src/models/change_password_for_user_response_model.dart';
import 'package:guoco_priv_app/src/models/check_password_strength_response_model.dart';
import 'package:guoco_priv_app/src/models/condo_info_response.dart';
import 'package:guoco_priv_app/src/models/create_password_request.dart';
import 'package:guoco_priv_app/src/models/create_password_response.dart';
import 'package:guoco_priv_app/src/models/detach_preference_response_model.dart';
import 'package:guoco_priv_app/src/models/earliest_slot_key_collection_request.dart';
import 'package:guoco_priv_app/src/models/earliest_slot_key_collection_response.dart';
import 'package:guoco_priv_app/src/models/forgot_password_response_model.dart';
import 'package:guoco_priv_app/src/models/getAllFacilitiesInACondoMiniumResponseModel.dart';
import 'package:guoco_priv_app/src/models/get_blockid_response.dart';
import 'package:guoco_priv_app/src/models/get_current_booking_for_facility_response_model.dart';
import 'package:guoco_priv_app/src/models/get_floor_id_response.dart';
import 'package:guoco_priv_app/src/models/get_key_collection_appointments_response_model.dart';
import 'package:guoco_priv_app/src/models/get_past_bookings_for_a_facility_response.dart';
import 'package:guoco_priv_app/src/models/get_preference_response_model.dart';
import 'package:guoco_priv_app/src/models/image_upload_request_model.dart';
import 'package:guoco_priv_app/src/models/image_upload_response_model.dart';
import 'package:guoco_priv_app/src/models/key_collection_appointments_request.dart';
import 'package:guoco_priv_app/src/models/key_collection_appointments_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_booking_request_model.dart';
import 'package:guoco_priv_app/src/models/key_collection_booking_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_cancel_appointment_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_confirm_request.dart';
import 'package:guoco_priv_app/src/models/key_collection_confirm_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_self_request_model.dart';
import 'package:guoco_priv_app/src/models/key_collection_self_response.dart';
import 'package:guoco_priv_app/src/models/key_collection_time_slot_response.dart';
import 'package:guoco_priv_app/src/models/login_request_model.dart';
import 'package:guoco_priv_app/src/models/login_response_model.dart';
import 'package:guoco_priv_app/src/models/payment_status_rseponse.dart';
import 'package:guoco_priv_app/src/models/privilege_club_t_and_c_accept_1_response.dart';
import 'package:guoco_priv_app/src/models/privilege_club_t_and_c_decline_1_response.dart';
import 'package:guoco_priv_app/src/models/privilege_club_t_and_c_decline_2_response.dart';
import 'package:guoco_priv_app/src/models/privlege_club_t_and_c_accept_2_response.dart';
import 'package:guoco_priv_app/src/models/reference_code_check_response_model.dart';
import 'package:guoco_priv_app/src/models/register_privilege_club_otp_response.dart';
import 'package:guoco_priv_app/src/models/register_privilege_otp_generate.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step1_request.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step1_response.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step2_request.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step2_response.dart';
import 'package:guoco_priv_app/src/models/share_text_response_model.dart';
import 'package:guoco_priv_app/src/models/state.dart';
import 'package:guoco_priv_app/src/models/timeSlotsForFacilityRequest.dart';
import 'package:guoco_priv_app/src/models/time_slots_for_facilities_resposne.dart';
import 'package:guoco_priv_app/src/models/unit_users_response.dart';
import 'package:guoco_priv_app/src/models/user_profile_response.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';

class UserApiProvider {
  Future<State> loginCall(LoginRequest loginRequest) async {
    final response = await ObjectFactory().apiClient.loginRequest(loginRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<LoginResponse>.success(
          LoginResponse.fromJson(response.data));
    } else
      return null;
  }

  ///forgot password
  Future<State> forgotPassword(String email) async {
    final response = await ObjectFactory().apiClient.forgotPassword(email);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<ForgotPasswordResponseModel>.success(
          ForgotPasswordResponseModel.fromJson(response.data));
    } else
      return null;
  }

  /// attach preference
  Future<State> attachPreferenceCall(String preferenceId, String userId) async {
    final response = await ObjectFactory()
        .apiClient
        .attachPreferenceRequest(userId: userId, preferenceId: preferenceId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<AttachPreferenceResponse>.success(
          AttachPreferenceResponse.fromJson(response.data));
    } else
      return null;
  }

  /// getPreference
  Future<State> getPreferenceCall() async {
    final response = await ObjectFactory().apiClient.getPreferenceResponse();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<GetPreferenceResponse>.success(
          GetPreferenceResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> passwordStrength({String password}) async {
    final response = await ObjectFactory()
        .apiClient
        .checkPasswordStrength(password: password);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<CheckPasswordStrengthResponseModel>.success(
          CheckPasswordStrengthResponseModel.fromJson(response.data));
    } else
      return null;
  }

  /// detachPreference
  Future<State> detachPreferenceCall(String preferenceId, String userId) async {
    final response = await ObjectFactory()
        .apiClient
        .detachPreferenceRequest(userId: userId, preferenceId: preferenceId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<DetachPreferenceResponse>.success(
          DetachPreferenceResponse.fromJson(response.data));
    } else
      return null;
  }

  /// referenceCodeCheck

  Future<State> loyaltymanagement_referencecode_validate(
      String referenceCode) async {
    print(referenceCode + " 1 referenceCode");
    final response = await ObjectFactory()
        .apiClient
        .loyaltymanagement_referencecode_validate(referenceCode: referenceCode);
    print("referenceCodeCheck " + response.toString());
    print(1);
    if (response.statusCode == 200 || true) {
      return State<ReferenceCodeCheckResponse>.success(
          ReferenceCodeCheckResponse.fromJson(response.data));
    } else
      return null;
  }

  ///registerUserProfileStep1

  Future<State> registerUserProfileStep1(
      RegisterUserDetailsStep1Request registerUserDetailsStep1Request) async {
    final response = await ObjectFactory()
        .apiClient
        .registerUserProfileStep1Request(
            registerUserDetailsStep1Request: registerUserDetailsStep1Request);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<RegisterUserDetailsStep1Response>.success(
          RegisterUserDetailsStep1Response.fromJson(response.data));
    } else
      return null;
  }

  /// registerUserDetailsStep2
  Future<State> registerUserDetailsStep2(
      RegisterUserDetailsStep2Request registerUserDetailsStep2Request,
      String userId) async {
    final response = await ObjectFactory()
        .apiClient
        .registerUserDetailsStep2Request(
            registerUserDetailsStep2Request: registerUserDetailsStep2Request,
            userId: userId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<RegisterUserDetailsStep2Response>.success(
          RegisterUserDetailsStep2Response.fromJson(response.data));
    } else
      return null;
  }

  /// addUserProfile

  Future<State> privilegeClubTAndCAccept1(String userId) async {
    final response = await ObjectFactory()
        .apiClient
        .privilegeClubTAndCAccept1Request(userId: userId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<PrivilegeClubTAndCAccept1Response>.success(
          PrivilegeClubTAndCAccept1Response.fromJson(response.data));
    } else
      return null;
  }

  Future<State> privilegeClubTAndCDecline1(String userId) async {
    final response = await ObjectFactory()
        .apiClient
        .privilegeClubTAndCDecline1Request(userId: userId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<PrivilegeClubTAndCDecline1Response>.success(
          PrivilegeClubTAndCDecline1Response.fromJson(response.data));
    } else
      return null;
  }

  Future<State> privilegeClubTAndCAccept2(String userId) async {
    final response = await ObjectFactory()
        .apiClient
        .privilegeClubTAndCAccept2Request(userId: userId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<PrivilegeClubTAndCAccept2Response>.success(
          PrivilegeClubTAndCAccept2Response.fromJson(response.data));
    } else
      return null;
  }

  Future<State> privilegeClubTAndCDecline2(String userId) async {
    final response = await ObjectFactory()
        .apiClient
        .privilegeClubTAndCDecline2Request(userId: userId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<PrivilegeClubTAndCDecline2Response>.success(
          PrivilegeClubTAndCDecline2Response.fromJson(response.data));
    } else
      return null;
  }

  Future<State> registerPrivilegeClubOtpCall(String otpCode) async {
    final response = await ObjectFactory()
        .apiClient
        .registerPrivilegeClubOtp(otpCode: otpCode);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<RegisterPrivilegeClubOtpResponse>.success(
          RegisterPrivilegeClubOtpResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> birthdayCall(
      BirthdayRequest birthdayRequest, String userId) async {
    final response = await ObjectFactory()
        .apiClient
        .birthdayRequest(birthdayRequest: birthdayRequest, userId: userId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<BirthdayResponse>.success(
          BirthdayResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> registerPrivilegeClubOtpGenerate() async {
    final response =
        await ObjectFactory().apiClient.registerPrivilegeClubOtpGenerate();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<RegisterPrivilegeClubOtpGenerateResponse>.success(
          RegisterPrivilegeClubOtpGenerateResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> createPassword(
      {CreatePasswordRequest createPasswordRequest}) async {
    final response = await ObjectFactory()
        .apiClient
        .createPassword(createPasswordRequest: createPasswordRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<CreatePasswordResponse>.success(
          CreatePasswordResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getProfile() async {
    final response = await ObjectFactory().apiClient.getProfile();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<ProfileResponseModel>.success(
          ProfileResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> keyCollectionAppointments(
      KeyCollectionAppointmentsRequest keyCollectionAppointmentsRequest) async {
    final response = await ObjectFactory().apiClient.keyCollectionAppointments(
        keyCollectionAppointmentsRequest: keyCollectionAppointmentsRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<KeyCollectionAppointmentsResponse>.success(
          KeyCollectionAppointmentsResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> keyCollectionBookingCall(
      KeyCollectionBookingRequest keyCollectionBookingRequest) async {
    final response = await ObjectFactory().apiClient.keyCollectionBooking(
        keyCollectionBookingRequest: keyCollectionBookingRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<KeyCollectionBookingResponse>.success(
          KeyCollectionBookingResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> keyCollectionConfirmCall(
      KeyCollectionConfirmRequest keyCollectionConfirmRequest) async {
    final response = await ObjectFactory().apiClient.keyCollectionConfirm(
        keyCollectionConfirmRequest: keyCollectionConfirmRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<KeyCollectionConfirmResponse>.success(
          KeyCollectionConfirmResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> keyCollectionSelfCall(
      KeyCollectionSelfRequest keyCollectionSelfRequest, String unitId) async {
    final response = await ObjectFactory().apiClient.keyCollectionSelf(
        keyCollectionSelfRequest: keyCollectionSelfRequest, unitId: unitId);
    print(response.toString());
    if (response.statusCode == 200 || response.statusCode == 409) {
      return State<KeyCollectionSelfResponse>.success(
          KeyCollectionSelfResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getCondoInfo() async {
    final response = await ObjectFactory().apiClient.getCondoInfo();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<CondoInfoResponse>.success(
          CondoInfoResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getUnitUsers() async {
    final response = await ObjectFactory().apiClient.getUnitUsers();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<UnitUsersResponse>.success(
          UnitUsersResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> addUnitLoginMartinModernCall(
      AddUnitLoginMartinModernRequest addUnitLoginMartinModernRequest) async {
    final response = await ObjectFactory().apiClient.addUnitLoginMartinModern(
        addUnitLoginMartinModernRequest: addUnitLoginMartinModernRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<AddUnitLoginMartinModernResponse>.success(
          AddUnitLoginMartinModernResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getBlockIdCall(String condoId) async {
    final response = await ObjectFactory().apiClient.getBlockId(condoId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<GetBlockIdResponse>.success(
          GetBlockIdResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getFloorIdCall(String condoId, String blockId) async {
    final response =
        await ObjectFactory().apiClient.getFloorId(condoId, blockId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<GetFloorIdResponse>.success(
          GetFloorIdResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getKeyCollectionTimeSlots(String date, String unitId) async {
    final response = await ObjectFactory()
        .apiClient
        .getKeyCollectionTimeSlots(unitId: unitId, date: date);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<KeyCollectionTimeSlotsResponse>.success(
          KeyCollectionTimeSlotsResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getKeyCollectionAppointments(String unitId) async {
    final response = await ObjectFactory()
        .apiClient
        .getKeyCollectionAppointments(unitId: unitId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<KeyCollectionAppointmentsResponse>.success(
          KeyCollectionAppointmentsResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> cancelKeyCollectionAppointments(
      String unitId, String keyId) async {
    final response = await ObjectFactory()
        .apiClient
        .cancelKeyCollectionAppointments(unitId: unitId, keyId: keyId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<KeyCollectionCancelAppointment>.success(
          KeyCollectionCancelAppointment.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getMartinModernProfile() async {
    final response = await ObjectFactory().apiClient.getMartinModernProfile();
    print(response.toString());
    if (response.statusCode == 200) {
      return State<ProfileResponseModel>.success(
          ProfileResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> uploadProfileImage(
      ImageUploadRequestModel imageUploadRequestModel) async {
    final response = await ObjectFactory()
        .apiClient
        .uploadProfileImageRequest(imageUploadRequestModel);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<UploadProfileImageResponseModel>.success(
          UploadProfileImageResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> displayProfileImage(String fileId) async {
    final response =
        await ObjectFactory().apiClient.displayProfileImageRequest(fileId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<UploadProfileImageResponseModel>.success(
          UploadProfileImageResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> checkPaymentStatus(
      String condoInfoId, String floorId, String blockId, String unitId) async {
    final response = await ObjectFactory()
        .apiClient
        .checkPaymentStatus(condoInfoId, floorId, blockId, unitId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<PaymentStatusResponse>.success(
          PaymentStatusResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> changePasswordForUser(
      ChangePasswordForUserRequestModel
          changePasswordForUserRequestModel) async {
    final response = await ObjectFactory().apiClient.changePasswordForUser(
        changePasswordForUserRequestModel: changePasswordForUserRequestModel);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<ChangePasswordForUserResponseModel>.success(
          ChangePasswordForUserResponseModel.fromJson(response.data));
    } else
      return null;
  }

  Future<State> shareTextKeyCollectionAppointment(
      String keyCollectionAppointmentId) async {
    final response = await ObjectFactory()
        .apiClient
        .shareTextKeyCollectionResponse(
            keyCollectionAppointmentId: keyCollectionAppointmentId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<ShareTextResponse>.success(
          ShareTextResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getKeyCollectionAppointment(
      String keyCollectionAppointmentId) async {
    final response = await ObjectFactory()
        .apiClient
        .getKeyCollectionAppointmentResponse(
            keyCollectionAppointmentId: keyCollectionAppointmentId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<GetKeyCollectionAppointmentResponse>.success(
          GetKeyCollectionAppointmentResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> earliestSlotKeyCollection(
      EarliestSlotKeyCollectionRequest earliestSlotKeyCollectionRequest,
      String unitInfoId) async {
    final response = await ObjectFactory()
        .apiClient
        .getEarliestTimeSlotAvailableRequest(
            earliestSlotKeyCollectionRequest: earliestSlotKeyCollectionRequest,
            unitInfoId: unitInfoId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<EarliestSlotKeyCollectionResponse>.success(
          EarliestSlotKeyCollectionResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getAllFacilitiesInACondominium(String condoInfoId) async {
    final response = await ObjectFactory()
        .apiClient
        .getAllFacilitiesInACondominiumResponse(condoInfoId: condoInfoId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<GetAllFacilitiesInACondoMiniumResponse>.success(
          GetAllFacilitiesInACondoMiniumResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> timeSlotsForFacilitiesCall(
      String facilitiesInfoId,
      String onDate,
      TimeSlotsForFacilitiesRequest timeSlotsForFacilitiesRequest) async {
    final response = await ObjectFactory()
        .apiClient
        .timeSlotsForFacilitiesRequest(
            facilityInfoId: facilitiesInfoId,
            onDate: onDate,
            timeSlotsForFacilitiesRequest: timeSlotsForFacilitiesRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<TimeSlotForFacilitiesResponse>.success(
          TimeSlotForFacilitiesResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> bookAFacilityCall(
      BookAFacilityRequest bookAFacilityRequest, String facilityInfoId) async {
    final response = await ObjectFactory().apiClient.bookAFacilityRequest(
        facilityInfoId: facilityInfoId,
        bookAFacilityRequest: bookAFacilityRequest);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<BookAFacilityResponse>.success(
          BookAFacilityResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getCurrentBookingsForFacility(String unitInfoId) async {
    final response = await ObjectFactory()
        .apiClient
        .getCurrentBookingsForFacilityResponse(unitInfoId: unitInfoId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<GetCurrentBookingsForFacilityResponse>.success(
          GetCurrentBookingsForFacilityResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> getPastBookingsForFacility(String unitInfoId) async {
    final response = await ObjectFactory()
        .apiClient
        .getPastBookingsForFacilityResponse(unitInfoId: unitInfoId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<GetPastBookingsForFacilityResponse>.success(
          GetPastBookingsForFacilityResponse.fromJson(response.data));
    } else
      return null;
  }

  Future<State> cancelAFacilityBooking(String facilityBookingId) async {
    final response = await ObjectFactory()
        .apiClient
        .cancelAFacilityBooking(facilityBookingId: facilityBookingId);
    print(response.toString());
    if (response.statusCode == 200) {
      return State<CancelAFacilityBookingResponse>.success(
          CancelAFacilityBookingResponse.fromJson(response.data));
    } else
      return null;
  }
}
