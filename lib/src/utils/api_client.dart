import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:guoco_priv_app/src/models/add_unit_login_martin_modern_request.dart';
import 'package:guoco_priv_app/src/models/birthday_request_model.dart';
import 'package:guoco_priv_app/src/models/book_a_facility_request_model.dart';
import 'package:guoco_priv_app/src/models/change_password_for_user_request_model.dart';
import 'package:guoco_priv_app/src/models/create_password_request.dart';
import 'package:guoco_priv_app/src/models/earliest_slot_key_collection_request.dart';
import 'package:guoco_priv_app/src/models/header_model.dart';
import 'package:guoco_priv_app/src/models/image_upload_request_model.dart';
import 'package:guoco_priv_app/src/models/key_collection_appointments_request.dart';
import 'package:guoco_priv_app/src/models/key_collection_booking_request_model.dart';
import 'package:guoco_priv_app/src/models/key_collection_confirm_request.dart';
import 'package:guoco_priv_app/src/models/key_collection_self_request_model.dart';
import 'package:guoco_priv_app/src/models/login_request_model.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step1_request.dart';
import 'package:guoco_priv_app/src/models/register_user_details_step2_request.dart';
import 'package:guoco_priv_app/src/models/timeSlotsForFacilityRequest.dart';
import 'package:guoco_priv_app/src/utils/object_factory.dart';
import 'package:guoco_priv_app/src/utils/urls.dart';
import 'package:guoco_priv_app/src/utils/utils.dart';

enum AccessMode { READ, WRITE }
HeaderModel authHeaderModel = new HeaderModel();

Future setAuthHeaderModel({var accessMode, String xReferenceId}) async {
  if (ObjectFactory().appHive != null &&
      ObjectFactory().appHive.getToken() != null &&
      ObjectFactory().appHive.getToken().trim().length > 0)
    authHeaderModel.authorization =
        "Bearer " + ObjectFactory().appHive.getToken();
  authHeaderModel.xAccessMode = accessMode.toString();
  authHeaderModel.xUdid = await getUDID();
  authHeaderModel.xRequestTime = getRequestTime();
  authHeaderModel.xReferenceId = xReferenceId;
  authHeaderModel.xSession = getUUID();
  if (ObjectFactory().appHive != null &&
      ObjectFactory().appHive.getXUser() != null &&
      ObjectFactory().appHive.getXUser().trim().length > 0)
    authHeaderModel.xUser = ObjectFactory().appHive.getXUser();
  if (ObjectFactory().appHive != null &&
      ObjectFactory().appHive.getUserId() != null &&
      ObjectFactory().appHive.getUserId().trim().length > 0)
    authHeaderModel.xUserId = ObjectFactory().appHive.getUserId();
  if (ObjectFactory().appHive.getPropertyCode() != null &&
      ObjectFactory().appHive.getPropertyCode().length > 0) {
    print("sdjd" + ObjectFactory().appHive.getPropertyCode());
    authHeaderModel.xPropertyCode = ObjectFactory().appHive.getPropertyCode();
  }
  if (ObjectFactory().appHive.getTenantId() != null &&
      ObjectFactory().appHive.getTenantId().length > 0) {
    authHeaderModel.xTenantId = ObjectFactory().appHive.getTenantId();
    print("asahjdfshddsdf" + ObjectFactory().appHive.getTenantId());
  }
  authHeaderModel.refId = ObjectFactory().appHive.getRefId();
  print(ObjectFactory().appHive.getRefId());

  // print("Token "+authHeaderModel.xTenantId);
  // print("Token "+authHeaderModel.xTenantId);
}

// void setAuthHeaderModel(
//     {var accessMode, String xReferenceId}) async{
//     authHeaderModel.authorization ="Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJuaXNoYW50aHB2MjAwMUBnbWFpbC5jb20iLCJzY29wZXMiOiJST0xFX1BST1BFUlRZX09XTkVSLFJPTEVfUFJPUEVSVFlfVEVOQU5UIiwiaWF0IjoxNjExNzY1Mzk5LCJleHAiOjE2MTE3ODMzOTl9.XHJaQnv05_r7M6ajbieVY5NGg4Z0uk55P-jM3hvL1To";
//   // authHeaderModel.xAccessMode = AccessMode.WRITE.toString();
//   // authHeaderModel.xUdid = "0d373018-2b2f-49be-a535-73e789d151cc";
//   // await getUDID();
//   // authHeaderModel.xRequestTime = "1611765403087";
//   // authHeaderModel.xReferenceId = "scrn01";
//   authHeaderModel.xSession =   "e39c964c-dc9f-4b4d-93e1-217cc2d8701d";
//   // authHeaderModel.xUser = "";
//   //   authHeaderModel.xUserId = "e0c5751d-9f6a-41f2-913f-b491c359ec3e";
//   // print("Token "+authHeaderModel.xTenantId);
// }

class ApiClient {
  HeaderModel loginModel = new HeaderModel(
    xSession: getUUID().toString(),
    xAccessMode: AccessMode.WRITE.toString(),
    xUdid: getUUID().toString(),
    xRequestTime: getRequestTime(),
  );

  ///  user login
  Future<Response> loginRequest(LoginRequest loginRequest) {
    print(loginRequest.toString());

    return ObjectFactory()
        .appDio
        .loginPost(url: Urls.loginUrl, data: loginRequest, header: loginModel);
  }

  ///  forgot password
  Future<Response> forgotPassword(String email) {
    // setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn05");
    setAuthHeaderModel(
      accessMode: AccessMode.READ,
      xReferenceId: "scrn01",
    );
    // print(authHeaderModel.xRequestTime);
    // print("token" + authHeaderModel.authorization);
    // print("userid" + authHeaderModel.xUserId);
    // // print("xuser" + authHeaderModel.xUser);
    // print("property" + authHeaderModel.xPropertyCode);
    // print("tenant" + authHeaderModel.xTenantId);
    // print("UUID" + authHeaderModel.xSession);
    // print("udid" + authHeaderModel.xUdid);
    // print("time" + authHeaderModel.xRequestTime);

    return ObjectFactory().appDio.loginPost(
        url: Urls.forgotPassword + email + "/forgotpassword",
        header: authHeaderModel,
        data: {});
  }

  /// attach prefernce

  Future<Response> attachPreferenceRequest(
      {String preferenceId, String userId}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    print(Urls.baseUrl +
        "/userservices/api/v1/usermanagement/user/$userId/preferences/$preferenceId/attach");
    return ObjectFactory().appDio.post(
        url: Urls.baseUrl +
            "/userservices/api/v1/usermanagement/user/$userId/preferences/$preferenceId/attach",
        header: authHeaderModel);
  }

  Future<Response> getPreferenceResponse() {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    print(authHeaderModel.authorization);
    print(Urls.getPreferenceCategory);

    return ObjectFactory()
        .appDio
        .get(url: Urls.getPreferenceCategory, header: authHeaderModel);
  }

  Future<Response> checkPasswordStrength({String password}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    print(authHeaderModel.authorization);
    print(Urls.getPreferenceCategory);
    var bytes = utf8.encode(password);
    var base64Str = base64.encode(bytes);

    return ObjectFactory()
        .appDio
        .get(url: Urls.passwordStrength + base64Str, header: authHeaderModel);
  }

  Future<Response> detachPreferenceRequest(
      {String preferenceId, String userId}) {
    return ObjectFactory().appDio.post(
        url: Urls.baseUrl +
            "/userservices/api/v1/usermanagement/user/$userId/preferences/$preferenceId/detach",
        header: authHeaderModel,
        data: {});
  }

  /// checkReferenceCodeRequest
  Future<Response> loyaltymanagement_referencecode_validate(
      {String referenceCode}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    print("check" + authHeaderModel.authorization.toString());

    print("ReferenceCode apiClient " + referenceCode);
    return ObjectFactory().appDio.get(
        header: authHeaderModel,
        url: Urls.loyaltymanagement_referencecode_validate + referenceCode);
  }

  /// registerUserProfileStep1

  Future<Response> registerUserProfileStep1Request(
      {RegisterUserDetailsStep1Request registerUserDetailsStep1Request}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.post(
        url: Urls.registerUserprofileStep1,
        data: registerUserDetailsStep1Request,
        header: authHeaderModel);
  }

  /// registerUserDetailsStep

  Future<Response> registerUserDetailsStep2Request(
      {RegisterUserDetailsStep2Request registerUserDetailsStep2Request,
      String userId}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    return ObjectFactory().appDio.post(
        url: Urls.registerUserProfileStep2 + "$userId/step2",
        data: registerUserDetailsStep2Request,
        header: authHeaderModel);
  }

  /// registerPrivilegeClubTAndCAccept1

  Future<Response> privilegeClubTAndCAccept1Request({String userId}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.post(
        url: Urls.privilegeClubTAndCAccept1 + "$userId/tandc/1/accept",
        header: authHeaderModel);
  }

  /// registerPrivilegeClubTAndCDecline1

  Future<Response> privilegeClubTAndCDecline1Request({String userId}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.post(
        url: Urls.privilegeClubTAndCDecline1 + "$userId/tandc/1/decline",
        header: authHeaderModel);
  }

  /// registerPrivilegeClubTAndCAccept2
  Future<Response> privilegeClubTAndCAccept2Request({String userId}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.post(
        url: Urls.privilegeClubTAndCAccept1 + "$userId/tandc/2/accept",
        header: authHeaderModel);
  }

  /// registerPrivilegeClubTAndCDecline2

  Future<Response> privilegeClubTAndCDecline2Request({String userId}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.post(
        url: Urls.privilegeClubTAndCDecline1 + "$userId/tandc/2/decline",
        header: authHeaderModel);
  }

  /// privilegeClubOtp
  Future<Response> registerPrivilegeClubOtp({String otpCode}) {
    // print("otp"+userId);
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    return ObjectFactory().appDio.post(
        url: Urls.privilegeClubTAndCDecline1 +
            ObjectFactory().appHive.getUserId().toString() +
            "/otp/$otpCode",
        header: authHeaderModel);
  }

  /// registerPrivilegeClubOtpGenerate
  Future<Response> registerPrivilegeClubOtpGenerate() {
    // print("otp"+userId);
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    return ObjectFactory().appDio.post(
        url: Urls.privilegeClubTAndCDecline1 +
            ObjectFactory().appHive.getUserId().toString() +
            "/otp",
        header: authHeaderModel);
  }

  /// createPassword
  Future<Response> createPassword(
      {CreatePasswordRequest createPasswordRequest}) {
    setAuthHeaderModel(
      accessMode: AccessMode.WRITE,
      xReferenceId: "scrn01",
    );
    return ObjectFactory().appDio.post(
        url: Urls.privilegeClubCreatePassword +
            ObjectFactory().appHive.getUserId() +
            "/changepassword",
        header: authHeaderModel,
        data: createPasswordRequest);
  }

  /// birthdayRequest
  Future<Response> birthdayRequest(
      {BirthdayRequest birthdayRequest, String userId}) {
    print(userId);
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.post(
        url: Urls.birthday + "$userId/birthday",
        data: birthdayRequest,
        header: authHeaderModel);
  }

  /// profile
  Future<Response> getProfile() {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.get(
        url: Urls.profileUrl + ObjectFactory().appHive.getUserId() + "/profile",
        header: authHeaderModel);
  }

  /// keyCollectionHappening
  Future<Response> getKeyCollectionHappeningBlock() {
    setAuthHeaderModel(accessMode: AccessMode.READ, xReferenceId: "scrn01");

    return ObjectFactory().appDio.get(
        url: Urls.profileUrl + ObjectFactory().appHive.getUserId() + "/profile",
        header: authHeaderModel);
  }

  /// keyCollectionAppointments

  Future<Response> keyCollectionAppointments(
      {KeyCollectionAppointmentsRequest keyCollectionAppointmentsRequest}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory()
        .appDio
        .post(url: Urls.birthday, header: authHeaderModel);
  }

  /// keyCollectionBooking

  Future<Response> keyCollectionBooking(
      {KeyCollectionBookingRequest keyCollectionBookingRequest}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.post(
        url: Urls.keyCollectionBooking,
        data: keyCollectionBookingRequest,
        header: authHeaderModel);
  }

  ///keyCollectionConfirm

  Future<Response> keyCollectionConfirm(
      {KeyCollectionConfirmRequest keyCollectionConfirmRequest}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.post(
        url: Urls.keyCollectionConfirm,
        data: keyCollectionConfirmRequest,
        header: authHeaderModel);
  }

  ///keyCollectionSelf

  Future<Response> keyCollectionSelf(
      {KeyCollectionSelfRequest keyCollectionSelfRequest, String unitId}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.addUnitPost(
          url: Urls.keyCollectionSelf + unitId,
          data: keyCollectionSelfRequest,
          header: authHeaderModel,
        );
  }

  ///addUnitLoginMartinModern

  Future<Response> addUnitLoginMartinModern(
      {AddUnitLoginMartinModernRequest addUnitLoginMartinModernRequest}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.addUnitPost(
          url: Urls.addUnitMartinModernLogin,
          data: addUnitLoginMartinModernRequest,
          header: authHeaderModel,
        );
  }

  Future<Response> getCondoInfo() {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory()
        .appDio
        .get(url: Urls.condoIfoUrl, header: authHeaderModel);
  }

  ///unitusers

  Future<Response> getUnitUsers() async {
    // print("tenant"+authHeaderModel.xTenantId);
    // print("property"+authHeaderModel.xPropertyCode);
    await setAuthHeaderModel(
        accessMode: AccessMode.READ, xReferenceId: "scrn01");

    return ObjectFactory().appDio.unitUserGet(
        url: Urls.profileUrl + ObjectFactory().appHive.getUserId() + "/units",
        header: authHeaderModel);
  }

  ///getBlockId

  Future<Response> getBlockId(String condoId) {
    setAuthHeaderModel(accessMode: AccessMode.READ, xReferenceId: "scrn01");

    return ObjectFactory().appDio.unitUserGet(
        url: Urls.getBlockId + "$condoId/blockinfo", header: authHeaderModel);
  }

  ///getFloorId

  Future<Response> getFloorId(String condoId, String blockId) {
    setAuthHeaderModel(accessMode: AccessMode.READ, xReferenceId: "scrn01");

    return ObjectFactory().appDio.unitUserGet(
        url: Urls.getFloorId + "$condoId/block/$blockId/floor",
        header: authHeaderModel);
  }

  Future<Response> getKeyCollectionTimeSlots({String date, String unitId}) {
    print("date" + date);
    print("id" + unitId);
    print("token" + authHeaderModel.authorization);
    print("userid" + authHeaderModel.xUserId);
    print("property" + authHeaderModel.xPropertyCode);
    print("tenant" + authHeaderModel.xTenantId);
    print("UUID" + authHeaderModel.xSession);
    print("udid" + authHeaderModel.xUdid);
    print("time" + authHeaderModel.xRequestTime);
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.unitUserGet(
          url: Urls.keyCollectionTimeSlots + "$unitId/$date",
          header: authHeaderModel,
        );
  }

  Future<Response> getKeyCollectionAppointments({String unitId}) {
    // // print("date"+date);
    // print("id"+unitId);
    // print("token"+authHeaderModel.authorization);
    // print("userid"+authHeaderModel.xUserId);
    // print("property"+authHeaderModel.xPropertyCode);
    // print("tenant"+authHeaderModel.xTenantId);
    // print("UUID"+authHeaderModel.xSession);
    // print("udid"+authHeaderModel.xUdid);
    // print("time"+authHeaderModel.xRequestTime);
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.unitUserGet(
          url: Urls.keyCollectionAppointments + "$unitId/active",
          header: authHeaderModel,
        );
  }

  Future<Response> cancelKeyCollectionAppointments(
      {String unitId, String keyId}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.addUnitPost(
          url: Urls.keyCollectionCancelAppointments + "$unitId/$keyId",
          header: authHeaderModel,
        );
  }

  Future<Response> getMartinModernProfile() {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.get(
        url: Urls.profileUrl + ObjectFactory().appHive.getUserId() + "/profile",
        header: authHeaderModel);
  }

  ///UploadImageFile

  Future<Response> uploadProfileImageRequest(
      ImageUploadRequestModel imageUploadRequestModel) async {
    print(Urls.uploadProfileImage + ObjectFactory().appHive.getRefId());
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    print("Images" + imageUploadRequestModel.file.path.split('/').last);
    print("token" + authHeaderModel.authorization);
    print("userid" + authHeaderModel.xUserId);
    print("property" + authHeaderModel.xPropertyCode);
    print("tenant" + authHeaderModel.xTenantId);
    print("UUID" + authHeaderModel.xSession);
    print("udid" + authHeaderModel.xUdid);
    print("time" + authHeaderModel.xRequestTime);
    print("x-Session" + authHeaderModel.xSession);

    FormData formDataUploadProfileImage = FormData.fromMap({
      if (imageUploadRequestModel.file != null)
        "file": await MultipartFile.fromFile(imageUploadRequestModel.file.path,
            filename: imageUploadRequestModel.file.path.split('/').last),
      "category": imageUploadRequestModel.category,
    });

    return ObjectFactory().appDio.fileUploadPost(
        url: Urls.uploadProfileImage +
            ObjectFactory().appHive.getRefId() +
            "/files/attachsingle",
        header: authHeaderModel,
        data: formDataUploadProfileImage);
  }

  ///DisplayImageFile

  Future<Response> displayProfileImageRequest(String fileId) async {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    return ObjectFactory().appDio.unitUserGet(
          url: Urls.displayProfileImage + fileId,
          header: authHeaderModel,
        );
  }

  ///CheckPaymentStatus

  Future<Response> checkPaymentStatus(
      String condoInfoId, String floorId, String blockId, String unitId) async {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    return ObjectFactory().appDio.unitUserGet(
          url: Urls.checkPaymentStaus +
              "$condoInfoId/block/$blockId/floor/$floorId/unit/$unitId",
          header: authHeaderModel,
        );
  }

  /// CHANGE PASSWORD

  Future<Response> changePasswordForUser(
      {ChangePasswordForUserRequestModel
          changePasswordForUserRequestModel}) async {
    print("Object UserId" + ObjectFactory().appHive.getUserId());
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    return ObjectFactory().appDio.post(
          url: Urls.changePassword +
              ObjectFactory().appHive.getUserId() +
              "/changepassword",
          data: changePasswordForUserRequestModel,
          header: authHeaderModel,
        );
  }

  /// ShareTextKeyCollectionAppointment

  Future<Response> shareTextKeyCollectionResponse(
      {String keyCollectionAppointmentId}) async {
    print("KeyCollection Id" + keyCollectionAppointmentId);
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    print(authHeaderModel.authorization);
    print(authHeaderModel.xPropertyCode);
    print(authHeaderModel.xTenantId);
    return ObjectFactory().appDio.get(
          url: Urls.shareText + "$keyCollectionAppointmentId/share",
          header: authHeaderModel,
        );
  }

  Future<Response> getKeyCollectionAppointmentResponse(
      {String keyCollectionAppointmentId}) async {
    print("KeyCollection Id" + keyCollectionAppointmentId);
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.get(
          url: Urls.shareText + "$keyCollectionAppointmentId",
          header: authHeaderModel,
        );
  }

  Future<Response> getEarliestTimeSlotAvailableRequest(
      {EarliestSlotKeyCollectionRequest earliestSlotKeyCollectionRequest,
      String unitInfoId}) async {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    print(
        "AppointMentDate" + earliestSlotKeyCollectionRequest.appointmentondate);
    print("AppointmentTime" +
        earliestSlotKeyCollectionRequest.appointmentfromtime);
    print(authHeaderModel.authorization);
    print(authHeaderModel.xPropertyCode);
    print(authHeaderModel.xTenantId);
    print("unitInfoId" + unitInfoId);
    print(
      Urls.getEarliestTimeSlot + "$unitInfoId/earliestslot",
    );

    return ObjectFactory().appDio.post(
        url: Urls.getEarliestTimeSlot + "$unitInfoId/earliestslot",
        header: authHeaderModel,
        data: earliestSlotKeyCollectionRequest);
  }

  /// get All Facilities in A Condominium

  Future<Response> getAllFacilitiesInACondominiumResponse(
      {String condoInfoId}) async {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    return ObjectFactory().appDio.get(
        url: Urls.getAllFacilitiesInACondMinium + "$condoInfoId/facility",
        header: authHeaderModel);
  }

  /// getSlotsForFacilitiesResponse

  Future<Response> timeSlotsForFacilitiesRequest(
      {String facilityInfoId,
      String onDate,
      TimeSlotsForFacilitiesRequest timeSlotsForFacilitiesRequest}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");

    print("Auth" + authHeaderModel.authorization);
    print("xproperty" + authHeaderModel.xPropertyCode);
    print("xTenant" + authHeaderModel.xTenantId);
    print("facilityInfoId" + facilityInfoId);
    print("onDate" + onDate);
    print("userId" + timeSlotsForFacilitiesRequest.userId);

    return ObjectFactory().appDio.post(
          url: Urls.timeSlotsForFacilities + "$facilityInfoId/slots/$onDate",
          data: timeSlotsForFacilitiesRequest,
          header: authHeaderModel,
        );
  }

  /// bookAFacilityRequest
  Future<Response> bookAFacilityRequest(
      {BookAFacilityRequest bookAFacilityRequest,
      String facilityInfoId}) async {
    print("Object UserId" + ObjectFactory().appHive.getUserId());
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    return ObjectFactory().appDio.post(
          url: Urls.bookAFacility +
              "$facilityInfoId/" +
              ObjectFactory().appHive.getUserId() +
              "/book",
          data: bookAFacilityRequest,
          header: authHeaderModel,
        );
  }

  Future<Response> getCurrentBookingsForFacilityResponse({String unitInfoId}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    print("unitInfoId" + unitInfoId);
    return ObjectFactory().appDio.unitUserGet(
          url: Urls.getCurrentBookingsForFacility +
              ObjectFactory().appHive.getUserId() +
              "/bookings/$unitInfoId/open",
          header: authHeaderModel,
        );
  }

  Future<Response> getPastBookingsForFacilityResponse({String unitInfoId}) {
    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    print("unitInfoId" + unitInfoId);
    return ObjectFactory().appDio.unitUserGet(
          url: Urls.getPastBookingsForFacility +
              ObjectFactory().appHive.getUserId() +
              "/bookings/$unitInfoId/closed",
          header: authHeaderModel,
        );
  }

  Future<Response> cancelAFacilityBooking({String facilityBookingId}) async {
    print("Object UserId" + ObjectFactory().appHive.getUserId());

    setAuthHeaderModel(accessMode: AccessMode.WRITE, xReferenceId: "scrn01");
    return ObjectFactory().appDio.post(
          url: Urls.cancelAFacilityBooking + "$facilityBookingId/cancel",
          header: authHeaderModel,
        );
  }
}
