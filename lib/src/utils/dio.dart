import 'dart:async';
import 'dart:io';

import 'package:cookie_jar/cookie_jar.dart';
import 'package:dio/adapter.dart';
import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/models/header_model.dart';
import 'package:guoco_priv_app/src/utils/urls.dart';

class AppDio {
  AppDio() {
    initClient();
  }

//for api client testing only
  AppDio.test({@required this.dio});

  Dio dio;
  BaseOptions _baseOptions;

  initClient() async {
    _baseOptions = new BaseOptions(
        baseUrl: Urls.baseUrl,
        connectTimeout: 30000,
        receiveTimeout: 1000000,
        followRedirects: true,
        headers: {
          HttpHeaders.contentTypeHeader: 'application/json',
          HttpHeaders.acceptHeader: 'application/json',
        },
        responseType: ResponseType.json,
        receiveDataWhenStatusError: true);

    dio = Dio(_baseOptions);

    (dio.httpClientAdapter as DefaultHttpClientAdapter).onHttpClientCreate =
        (client) {
      client.badCertificateCallback =
          (X509Certificate cert, String host, int port) {
        return true;
      };
    };

    dio.interceptors.add(CookieManager(new CookieJar()));
  }

  ///dio get
  Future<Response> get({String url, HeaderModel header}) {
    // dio.options.headers.map((key, value) => null);
    dio.options.headers.addAll({
      "X-Session": header.xSession,
      "X-RequestTime": header.xRequestTime,
      "X-UDID": header.xUdid,
      "X-ReferenceId": header.xReferenceId,
      "X-AccessMode": header.xAccessMode,
      "X-User": header.xUser,
      "X-UserId": header.xUserId,
      "Authorization": header.authorization,
    });

    return dio.get(url);
  }

  /// unituser get
  Future<Response> unitUserGet({String url, HeaderModel header}) {
    // print(
    //     "ten" + header.xPropertyCode + "          " + "bhh" + header.xTenantId);
    dio.options.headers.addAll(
        //     {
        //   "X-Session": header.xSession,
        //   "X-RequestTime": header.xRequestTime,
        //   "X-UDID": header.xUdid,
        //   "X-ReferenceId": header.xReferenceId,
        //   "X-AccessMode": header.xAccessMode,
        //   "X-User": header.xUser,
        //   "X-UserId": header.xUserId,
        //   "Authorization": header.authorization,
        //   "X-TenantId": header.xTenantId,
        //   "X-PropertyCode": header.xPropertyCode
        // }
        header.toJson());

    return dio.get(url);
  }

  ///dio login post
  Future<Response> loginPost({String url, HeaderModel header, var data}) {
    dio.options.headers.addAll({
      "X-Session": header.xSession,
      "X-RequestTime": header.xRequestTime,
      "X-UDID": header.xUdid,
      "X-ReferenceId": header.xReferenceId,
      "X-AccessMode": header.xAccessMode,
    });

    return dio.post(url, data: data);
  }

  Future<Response> logPost({String url, HeaderModel header, var data}) {
    dio.options.headers.addAll(header.toJson());

    return dio.post(url, data: data);
  }

  ///dio  post
  Future<Response> post({String url, HeaderModel header, var data}) {
    dio.options.headers.addAll({
      "X-Session": header.xSession,
      "X-RequestTime": header.xRequestTime,
      "X-UDID": header.xUdid,
      "X-ReferenceId": header.xReferenceId,
      "X-AccessMode": header.xAccessMode,
      "X-User": header.xUser,
      "X-UserId": header.xUserId,
      "Authorization": header.authorization,
      // "X-TenantId": header.xTenantId,
      // "X-PropertyCode":header.xPropertyCode
    });

    return dio.post(url, data: data);
  }

  Future<Response> addUnitPost({String url, HeaderModel header, var data}) {
    dio.options.headers.addAll({
      "X-Session": header.xSession,
      "X-RequestTime": header.xRequestTime,
      "X-UDID": header.xUdid,
      "X-ReferenceId": header.xReferenceId,
      "X-AccessMode": header.xAccessMode,
      "X-User": header.xUser,
      "X-UserId": header.xUserId,
      "Authorization": header.authorization,
      "X-TenantId": header.xTenantId,
      "X-PropertyCode": header.xPropertyCode,
    });

    return dio.post(url, data: data);
  }

  Future<Response> fileUploadPost({String url, HeaderModel header, var data}) {
    dio.options.headers.addAll({
      "X-Session": header.xSession,
      "X-RequestTime": header.xRequestTime,
      "X-UDID": header.xUdid,
      "X-ReferenceId": header.xReferenceId,
      "X-AccessMode": header.xAccessMode,
      "X-User": header.xUser,
      "X-UserId": header.xUserId,
      "Authorization": header.authorization,
      "X-TenantId": header.xTenantId,
      "X-PropertyCode": header.xPropertyCode,
      "Refid": header.refId
    });

    return dio.post(url, data: data);
  }
}
