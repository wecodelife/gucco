import 'package:guoco_priv_app/src/utils/object_factory.dart';

class Urls {
  static final baseUrl = "http://158.69.138.44:10083";
  // static final baseUrl = "http://18ef7eb7a88e.ngrok.io";
  static final loginUrl =
      baseUrl + "/securityservices/api/v1/securitymanagement/login";
  static final getPreferenceCategory =
      "/userservices/api/v1/usermanagement/preferencecategory";
  static final getReferenceCode =
      baseUrl + "/loyaltyservices/api/v1/loyaltymanagement/referencecode";
  static final loyaltymanagement_referencecode_validate =
      baseUrl + "/loyaltyservices/api/v1/loyaltymanagement/referencecode/";
  static final registerUserProfileStep2 = baseUrl +
      "/securityservices/api/v1/usermanagement/privilegeclub/registeruser/";
  static final registerUserprofileStep1 = baseUrl +
      "/securityservices/api/v1/usermanagement/privilegeclub/registeruser/step1";
  static final privilegeClubTAndCAccept1 = baseUrl +
      "/securityservices/api/v1/usermanagement/privilegeclub/registeruser/";
  static final privilegeClubTAndCDecline1 = baseUrl +
      "/securityservices/api/v1/usermanagement/privilegeclub/registeruser/";
  static final privilegeClubTAndCAccept2 = baseUrl +
      "/securityservices/api/v1/usermanagement/privilegeclub/registeruser/";
  static final birthday = baseUrl +
      "/securityservices/api/v1/usermanagement/privilegeclub/registeruser/";
  static final privilegeClubCreatePassword = baseUrl +
      "/securityservices/api/v1/usermanagement/privilegeclub/registeruser/";
  static final profileUrl =
      baseUrl + "/userservices/api/v1/usermanagement/user/";
  static final keyCollectionBooking =
      baseUrl + "/userservices/api/v1/usermanagement/user/";
  static final keyCollectionConfirm =
      baseUrl + "/userservices/api/v1/usermanagement/user/";
  static final keyCollectionAppointments = baseUrl +
      "/keycollectionservices/api/v1/keycollectionmanagement/keycollection/appointments/";
  static final keyCollectionSelf = baseUrl +
      "/keycollectionservices/api/v1/keycollectionmanagement/keycollection/bookappointment/";
  static final keyCollectionTimeSlots = baseUrl +
      "/keycollectionservices/api/v1/keycollectionmanagement/keycollectionslots/";
  static final keyCollectionCancelAppointments = baseUrl +
      "/keycollectionservices/api/v1/keycollectionmanagement/keycollection/cancelappointment/";
  static final addUnitMartinModernLogin = baseUrl +
      "/userservices/api/v1/usermanagement/user/" +
      ObjectFactory().appHive.getUserId() +
      "/unitinfo/register";
  static final condoIfoUrl =
      baseUrl + "/condoservices/api/v1/condomanagement/condoinfo";
  static final getBlockId = baseUrl + "/condoservices/api/v1/condomanagement/";
  static final getFloorId = baseUrl +
      "/condoservices/api/v1/condomanagement/{condoinfo_id}/block/{block_id}/floor";
  static final uploadProfileImage =
      baseUrl + "/application/api/v1/filemanagement/";
  static final displayProfileImage =
      baseUrl + "/application/api/v1/filemanagement/files/view/";
  static final checkPaymentStaus =
      baseUrl + "/condoservices/api/v1/condomanagement/";
  static final forgotPassword =
      baseUrl + "/securityservices/api/v1/securitymanagement/";
  static final passwordStrength = baseUrl +
      "/securityservices/api/v1/securitymanagement/verifypasswordstrength/";
  static final changePassword =
      baseUrl + "/securityservices/api/v1/securitymanagement/user/";

  static final shareText = baseUrl +
      "/keycollectionservices/api/v1/keycollectionmanagement/keycollection/appointment/";
  static final getKeyCollectionAppointment = baseUrl +
      "/keycollectionservices/api/v1/keycollectionmanagement/keycollection/appointment/";
  static final getEarliestTimeSlot = baseUrl +
      "/keycollectionservices/api/v1/keycollectionmanagement/keycollection/appointment/";

  static final getAllFacilitiesInACondMinium =
      baseUrl + "/facilitiesservices/api/v1/facilitiymanagement/";
  static final getImagesFacilitiesInACondminium =
      "/application/api/v1/filemanagement/";
  static final timeSlotsForFacilities =
      baseUrl + "/facilitiesservices/api/v1/facilitiymanagement/facility/";
  static final bookAFacility =
      Urls.baseUrl + "/facilitiesservices/api/v1/facilitiymanagement/facility/";

  static final getCurrentBookingsForFacility =
      Urls.baseUrl + "/facilitiesservices/api/v1/facilitiymanagement/facility/";
  static final getPastBookingsForFacility =
      Urls.baseUrl + "/facilitiesservices/api/v1/facilitiymanagement/facility/";
  static final cancelAFacilityBooking = Urls.baseUrl +
      "/facilitiesservices/api/v1/facilitiymanagement/facility/booking/";
}
