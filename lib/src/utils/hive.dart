import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:hive/hive.dart';

class AppHive {
  // void main (){}

  // void openBox()async{
  //   await Hive.openBox(Constants.BOX_NAME);
  //
  // }
  static const String _USER_ID = "user_id";
  static const String _NAME = "name";
  static const String _TOKEN = "token";
  static const String _EMAIL = "email";
  static const String _PREFERENCE = "preferenceid";
  static const String _LATITUDE = "longitude";
  static const String _XUSER = "newuserid";
  static const String _PASSWORD = "userpassword";
  static const String _PROPERTY_NAME = "property_name";
  static const String _PROPERTY_CODE = "property_CODE";
  static const String _TENANT_ID = "tenant_id";
  static const String _CONTACT_NO = "contact_no";
  static const String _USER_IMAGE = "user_image";
  static const String _REF_ID = "refId";
  static const String _LAST_NAME = "last_name";
  static const String _ADDRESS = "address";
  static const String _PASSWORD_LAST_CHANGED_ON = "passwordlastchangedon";
  static const String _CONDO_INFOID = "condoInfoid";
  static const String _UNIT_INFOID = "unitInfoId";
  static const String _KEYCOLLECTION_VENUE = "keycollectionvenue";
  static const String _KEYCOLLECTIONPAYMENT_STATUS =
      "keycollectionpaymentstatus";
  static const String _POSTMOVEINIMAGE1 = "postmovinimage1";
  static const String _POSTMOVEINIMAGE2 = "postmovinimage2";
  static const String _POSTMOVEINIMAGE3 = "postmovinimage3";
  static const String _TERMSANDCONDITION = "termsandcondition";

  void hivePut({String key, String value}) async {
    await Hive.box(Constants.BOX_NAME).put(key, value);
  }

  String hiveGet({String key}) {
    // openBox();
    return Hive.box(Constants.BOX_NAME).get(key);
  }

  hiveClear({String key}) {
    Hive.box(Constants.BOX_NAME).delete(key);
  }

  hivePutBool({String key, bool value}) {
    Hive.box(Constants.BOX_NAME).put(key, value);
  }

  hiveGetBool({String key}) {
    Hive.box(Constants.BOX_NAME).get(key);
  }

  putUserId({String userId}) {
    hivePut(key: _USER_ID, value: userId);
  }

  putPasswordLastChangesOn({String passwordlastchangedon}) {
    hivePut(key: _PASSWORD_LAST_CHANGED_ON, value: passwordlastchangedon);
  }

  String getPasswordLastChangesOn() {
    return hiveGet(key: _PASSWORD_LAST_CHANGED_ON);
  }

  String getUserId() {
    return hiveGet(key: _USER_ID);
  }

  putUserPassword({String email}) {
    hivePut(key: _PASSWORD, value: email);
  }

  String getUserPassword() {
    return hiveGet(key: _PASSWORD);
  }

  putName({String name}) {
    hivePut(key: _NAME, value: name);
  }

  String getName() {
    return hiveGet(key: _NAME);
  }

  putToken({String token}) {
    hivePut(key: _TOKEN, value: token);
  }

  String getToken() {
    return hiveGet(key: _TOKEN);
  }

  String getEmail() {
    return hiveGet(key: _EMAIL);
  }

  putEmail(String value) {
    return hivePut(key: _EMAIL, value: value);
  }

  putPreference(String value) {
    return hivePut(key: _PREFERENCE, value: value);
  }

  String getPreferenceId() {
    return hiveGet(key: _PREFERENCE);
  }

  putXUser(String value) {
    return hivePut(key: _XUSER, value: value);
  }

  getXUser() {
    return hiveGet(
      key: _XUSER,
    );
  }

  putPropertyName(String value) {
    return hivePut(key: _PROPERTY_NAME, value: value);
  }

  getPropertyName() {
    return hiveGet(
      key: _PROPERTY_NAME,
    );
  }

  putPropertyCode(String value) {
    return hivePut(key: _PROPERTY_CODE, value: value);
  }

  getPropertyCode() {
    return hiveGet(
      key: _PROPERTY_CODE,
    );
  }

  putTenantId(String value) {
    return hivePut(key: _TENANT_ID, value: value);
  }

  getTenantId() {
    return hiveGet(
      key: _TENANT_ID,
    );
  }

  putContactNo(String value) {
    return hivePut(key: _CONTACT_NO, value: value);
  }

  getContactNo() {
    return hiveGet(
      key: _CONTACT_NO,
    );
  }

  putUserImage(String value) {
    return hivePut(key: _USER_IMAGE, value: value);
  }

  getUserImage() {
    return hiveGet(
      key: _USER_IMAGE,
    );
  }

  putRefId(String value) {
    return hivePut(key: _REF_ID, value: value);
  }

  getRefId() {
    return hiveGet(
      key: _REF_ID,
    );
  }

  putLastName(String value) {
    return hivePut(key: _LAST_NAME, value: value);
  }

  getLastName() {
    return hiveGet(
      key: _LAST_NAME,
    );
  }

  putAddress(String value) {
    return hivePut(key: _ADDRESS, value: value);
  }

  getAddress() {
    return hiveGet(
      key: _ADDRESS,
    );
  }

  putCondoInfoId(String value) {
    return hivePut(key: _CONDO_INFOID, value: value);
  }

  getCondoInfoId() {
    return hiveGet(
      key: _CONDO_INFOID,
    );
  }

  putKeyCollectionVenue(String value) {
    return hivePut(key: _KEYCOLLECTION_VENUE, value: value);
  }

  getKeyCollectionVenue() {
    return hiveGet(
      key: _KEYCOLLECTION_VENUE,
    );
  }

  putUnitInfoId(String value) {
    return hivePut(key: _UNIT_INFOID, value: value);
  }

  getUnitInfoId() {
    return hiveGet(
      key: _UNIT_INFOID,
    );
  }

  putKeyCollectionPaymentStatus({bool value}) {
    return hivePutBool(key: _KEYCOLLECTIONPAYMENT_STATUS, value: value);
  }

  getKeyCollectionPaymentStatus() {
    return hiveGetBool(
      key: _KEYCOLLECTIONPAYMENT_STATUS,
    );
  }

  putPostMoveInImage1(String value) {
    return hivePut(key: _POSTMOVEINIMAGE1, value: value);
  }

  getPostMoveInImage1() {
    return hiveGet(
      key: _POSTMOVEINIMAGE1,
    );
  }

  putPostMoveInImage2(String value) {
    return hivePut(key: _POSTMOVEINIMAGE2, value: value);
  }

  getPostMoveInImage2() {
    return hiveGet(
      key: _POSTMOVEINIMAGE2,
    );
  }

  putPostMoveInImage3(String value) {
    return hivePut(key: _POSTMOVEINIMAGE3, value: value);
  }

  getPostMoveInImage3() {
    return hiveGet(
      key: _POSTMOVEINIMAGE3,
    );
  }

  putTermsAndCondition(String value) {
    return hivePut(key: _TERMSANDCONDITION, value: value);
  }

  getTermsAndCondition() {
    return hiveGet(
      key: _TERMSANDCONDITION,
    );
  }

  AppHive();
}
