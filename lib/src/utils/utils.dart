import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:external_app_launcher/external_app_launcher.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:guoco_priv_app/src/ui/screens/auth/login_page.dart';
import 'package:guoco_priv_app/src/ui/widgets/action_alert_box.dart';
import 'package:guoco_priv_app/src/ui/widgets/cancel_alert_box.dart';
import 'package:guoco_priv_app/src/ui/widgets/otp_message.dart';
import 'package:guoco_priv_app/src/ui/widgets/reschedule_alert_box.dart';
import 'package:uuid/uuid.dart';

///it contain common functions
class Utils {
  static String capitalize(String s) {
    if (s != null && s.isNotEmpty) {
      return s[0].toUpperCase() + s.substring(1);
    } else {
      return "";
    }
  }
}

Size screenSize(BuildContext context) {
  return MediaQuery.of(context).size;
}

double screenHeight(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).height / dividedBy;
}

double screenWidth(BuildContext context, {double dividedBy = 1}) {
  return screenSize(context).width / dividedBy;
}

Future<dynamic> push(BuildContext context, Widget route) {
  return Navigator.push(
      context, MaterialPageRoute(builder: (context) => route));
}

void pop(BuildContext context) {
  return Navigator.pop(context);
}

Future<dynamic> pushAndRemoveUntil(
    BuildContext context, Widget route, bool goBack) {
  return Navigator.pushAndRemoveUntil(context,
      MaterialPageRoute(builder: (context) => route), (route) => goBack);
}

Future<dynamic> pushAndReplacement(BuildContext context, Widget route) {
  return Navigator.pushReplacement(
      context, MaterialPageRoute(builder: (context) => route));
}

///common toast
void showToast(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.BOTTOM,
  );
}

void actionAlertBox(
    {context,
    msg,
    contentNum,
    content1,
    content2,
    content3,
    content4,
    content5,
    content6,
    heading1,
    heading2,
    heading3,
    heading4,
    heading5,
    heading6,
    cancelAlert,
    cancelBox,
    cancelBoxTitle,
    text1,
    questionAlert,
    questionAlertNotBold,
    date,
    onPressed,
    isLoading,
    text2,
    reschedule,
    reduceSize,
    lastDate}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return ActionAlertBox(
          reduceSize: reduceSize,
          reschedule: reschedule,
          title: msg,
          questionAlert: questionAlert,
          contentNum: contentNum,
          content1: content1,
          content2: content2,
          content3: content3,
          content4: content4,
          content5: content5,
          content6: content6,
          heading1: heading1,
          heading2: heading2,
          heading3: heading3,
          heading4: heading4,
          heading5: heading5,
          heading6: heading6,
          cancelAlert: cancelAlert,
          cancelBox: cancelBox,
          cancelBoxTitle: cancelBoxTitle,
          text2: text2,
          text1: text1,
          date: date,
          lastDate: lastDate,
          questionAlertNotBold: questionAlertNotBold,
          onPressed: onPressed,
          isLoading: isLoading,
        );
      });
}

void cancelAlertBox(
    {context,
    bool reschedule,
    msg,
    text1,
    text2,
    onPressed,
    double insetPadding,
    double contentPadding,
    double titlePadding}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return CancelAlertBox(
          title: msg,
          text1: text1,
          text2: text2,
          onPressed: onPressed,
          contentPadding: contentPadding,
          titlePadding: titlePadding,
          insetPadding: insetPadding,
        );
      });
}

void rescheduleAlertBox(
    {context,
    questionAlert,
    msg,
    date,
    questionAlertNotBold,
    onPressed,
    isLoading,
    String earliestDate,
    earliestStartTime,
    earliestEndTime,
    selectedStartTime,
    selectedEndTime}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.black.withOpacity(0.8),
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return RescheduleBox(
          title: msg,
          questionAlert: questionAlert,
          date: date,
          questionAlertNotBold: questionAlertNotBold,
          onPressed: onPressed,
          earliestDate: earliestDate,
          isLoading: isLoading,
          earliestEndTime: earliestEndTime,
          earliestStartTime: earliestStartTime,
          selectedEndTime: selectedEndTime,
          selectedStartTime: selectedStartTime,
        );
      });
}

void showAlert(context, String msg) {
  // flutter defined function
  showDialog(
    context: context,
    builder: (BuildContext context) {
      // return object of type Dialog
      return AlertDialog(
        title: new Text(msg),
//        content: new Text("Alert Dialog body"),
        actions: <Widget>[
          // usually buttons at the bottom of the dialog
          new FlatButton(
            child: new Text("OK"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    },
  );
}

void otpAlertBox({context, title, route, stayOnPage}) {
  showGeneralDialog(
      context: context,
      barrierDismissible: false,
      barrierLabel: MaterialLocalizations.of(context).modalBarrierDismissLabel,
      barrierColor: Colors.transparent,
      transitionDuration: const Duration(milliseconds: 200),
      pageBuilder: (BuildContext buildContext, Animation animation,
          Animation secondaryAnimation) {
        return OtpMessage(
          title: title,
          route: route,
          stayOnPage: stayOnPage,
        );
      });
}

///common toast
void showToastLong(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

void showToastConnection(String msg) {
  Fluttertoast.showToast(
    msg: msg,
    toastLength: Toast.LENGTH_LONG,
    gravity: ToastGravity.BOTTOM,
  );
}

String getRequestTime() {
  String time = DateTime.now().toUtc().millisecondsSinceEpoch.toString();
  return time;
}

String getUUID() {
  var uuid = Uuid();
  String v4 = uuid.v4();
  print("uuid" + v4.toString());
  return v4;
}

Future<String> getUDID() async {
  var deviceInfo = DeviceInfoPlugin();
  if (Platform.isIOS) {
    // import 'dart:io'
    var iosDeviceInfo = await deviceInfo.iosInfo;
    return iosDeviceInfo.identifierForVendor; // unique ID on iOS
  } else {
    var androidDeviceInfo = await deviceInfo.androidInfo;
    return androidDeviceInfo.androidId; // unique ID on Android
  }
}

Future<void> openExternalApp() async {
  await LaunchApp.openApp(
    androidPackageName: 'com.facebook.katana',
    iosUrlScheme: 'fb://',
    appStoreLink:
        'https://play.google.com/store/apps/details?id=com.facebook.katana',
  );
}

Future<void> openNovade() async {
  await LaunchApp.openApp(
    androidPackageName: 'com.upvise.novade',
    iosUrlScheme: 'fb://',
    appStoreLink:
        'https://play.google.com/store/apps/details?id=com.upvise.novade',
  );
}

Future<void> openFibaro() async {
  await LaunchApp.openApp(
    androidPackageName: 'com.fibaro.homecenter',
    iosUrlScheme: 'fb://',
    appStoreLink:
        'https://play.google.com/store/apps/details?id=com.fibaro.homecenter',
  );
}

void exitToLogin(int statusCode, BuildContext context) {
  if (statusCode >= 20000) {
    pushAndRemoveUntil(context, Login(), false);
  }
}

String time(String time) {
  DateTime hour;
  // print(int.parse(time.split(":").first));
  String timeData;
  // hour = int.parse(time.split(":").first)>12?(DateTime.parse("2020-07-20T$time:00").subtract(Duration(hours: 12))):DateTime.parse("2020-07-20T$time:12");
  // timeData=int.parse(time.split(":").first)>12?hour.toString().substring(12,16)+" PM":hour.toString().substring(12,16)+" AM";
  // timeData=new DateFormat .jm().format( DateTime.parse("2020-07-20T$time:00"));
  // timeData=new DateFormat .jm().format( DateTime.parse("2020-07-20T$time"));
  timeData = time;
  return timeData;
}

String date(String date) {
  String date1 = date.split("-").first;
  String date2 = date.split("-")[1];
  String date3 = date.split("-").last;
  String dates = "$date3-$date2-$date1";
  return dates;
}
