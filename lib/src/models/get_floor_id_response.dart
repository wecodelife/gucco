// To parse this JSON data, do
//
//     final getFloorIdResponse = getFloorIdResponseFromJson(jsonString);

import 'dart:convert';

GetFloorIdResponse getFloorIdResponseFromJson(String str) => GetFloorIdResponse.fromJson(json.decode(str));

String getFloorIdResponseToJson(GetFloorIdResponse data) => json.encode(data.toJson());

class GetFloorIdResponse {
  GetFloorIdResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final List<Datum> data;

  factory GetFloorIdResponse.fromJson(Map<String, dynamic> json) => GetFloorIdResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.floorno,
    this.status,
    this.units,
  });

  final String id;
  final String floorno;
  final dynamic status;
  final List<dynamic> units;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    floorno: json["floorno"] == null ? null : json["floorno"],
    status: json["status"],
    units: json["units"] == null ? null : List<dynamic>.from(json["units"].map((x) => x)),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "floorno": floorno == null ? null : floorno,
    "status": status,
    "units": units == null ? null : List<dynamic>.from(units.map((x) => x)),
  };
}
