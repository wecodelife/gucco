// To parse this JSON data, do
//
//     final keyCollectionSelfRequest = keyCollectionSelfRequestFromJson(jsonString);

import 'dart:convert';

KeyCollectionSelfRequest keyCollectionSelfRequestFromJson(String str) => KeyCollectionSelfRequest.fromJson(json.decode(str));

String keyCollectionSelfRequestToJson(KeyCollectionSelfRequest data) => json.encode(data.toJson());

class KeyCollectionSelfRequest {
  KeyCollectionSelfRequest({
    this.additionalinfo,
    this.appointmentfromtime,
    this.appointmentondate,
    this.appointmenttotime,
    this.comments,
    this.contactno,
    this.emailaddress,
    this.fullname,
    this.noofpeople,
    this.self,
    this.vehicleplateno,
    this.vip,
  });

  final String additionalinfo;
  final String appointmentfromtime;
  final String appointmentondate;
  final String appointmenttotime;
  final String comments;
  final String contactno;
  final String emailaddress;
  final String fullname;
  final int noofpeople;
  final bool self;
  final String vehicleplateno;
  final bool vip;

  factory KeyCollectionSelfRequest.fromJson(Map<String, dynamic> json) => KeyCollectionSelfRequest(
    additionalinfo: json["additionalinfo"] == null ? null : json["additionalinfo"],
    appointmentfromtime: json["appointmentfromtime"] == null ? null : json["appointmentfromtime"],
    appointmentondate: json["appointmentondate"] == null ? null : json["appointmentondate"],
    appointmenttotime: json["appointmenttotime"] == null ? null : json["appointmenttotime"],
    comments: json["comments"] == null ? null : json["comments"],
    contactno: json["contactno"] == null ? null : json["contactno"],
    emailaddress: json["emailaddress"] == null ? null : json["emailaddress"],
    fullname: json["fullname"] == null ? null : json["fullname"],
    noofpeople: json["noofpeople"] == null ? null : json["noofpeople"],
    self: json["self"] == null ? null : json["self"],
    vehicleplateno: json["vehicleplateno"] == null ? null : json["vehicleplateno"],
    vip: json["vip"] == null ? null : json["vip"],
  );

  Map<String, dynamic> toJson() => {
    "additionalinfo": additionalinfo == null ? null : additionalinfo,
    "appointmentfromtime": appointmentfromtime == null ? null : appointmentfromtime,
    "appointmentondate": appointmentondate == null ? null : appointmentondate,
    "appointmenttotime": appointmenttotime == null ? null : appointmenttotime,
    "comments": comments == null ? null : comments,
    "contactno": contactno == null ? null : contactno,
    "emailaddress": emailaddress == null ? null : emailaddress,
    "fullname": fullname == null ? null : fullname,
    "noofpeople": noofpeople == null ? null : noofpeople,
    "self": self == null ? null : self,
    "vehicleplateno": vehicleplateno == null ? null : vehicleplateno,
    "vip": vip == null ? null : vip,
  };
}
