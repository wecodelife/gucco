// To parse this JSON data, do
//
//     final keyCollectionConfirmResponse = keyCollectionConfirmResponseFromJson(jsonString);

import 'dart:convert';

KeyCollectionConfirmResponse keyCollectionConfirmResponseFromJson(String str) => KeyCollectionConfirmResponse.fromJson(json.decode(str));

String keyCollectionConfirmResponseToJson(KeyCollectionConfirmResponse data) => json.encode(data.toJson());

class KeyCollectionConfirmResponse {
  KeyCollectionConfirmResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  int statuscode;
  dynamic statusmessage;
  int errorcode;
  String errormessage;
  Data data;

  factory KeyCollectionConfirmResponse.fromJson(Map<String, dynamic> json) => KeyCollectionConfirmResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.firstname,
    this.lastname,
    this.zone,
    this.unit,
    this.block,
    this.postalcode,
    this.status,
    this.profileimage,
    this.birthday,
    this.tandcstatus1,
    this.tandcstatus2,
  });

  dynamic id;
  String firstname;
  String lastname;
  String zone;
  String unit;
  String block;
  String postalcode;
  dynamic status;
  dynamic profileimage;
  String birthday;
  bool tandcstatus1;
  bool tandcstatus2;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"],
    firstname: json["firstname"] == null ? null : json["firstname"],
    lastname: json["lastname"] == null ? null : json["lastname"],
    zone: json["zone"] == null ? null : json["zone"],
    unit: json["unit"] == null ? null : json["unit"],
    block: json["block"] == null ? null : json["block"],
    postalcode: json["postalcode"] == null ? null : json["postalcode"],
    status: json["status"],
    profileimage: json["profileimage"],
    birthday: json["birthday"] == null ? null : json["birthday"],
    tandcstatus1: json["tandcstatus1"] == null ? null : json["tandcstatus1"],
    tandcstatus2: json["tandcstatus2"] == null ? null : json["tandcstatus2"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "firstname": firstname == null ? null : firstname,
    "lastname": lastname == null ? null : lastname,
    "zone": zone == null ? null : zone,
    "unit": unit == null ? null : unit,
    "block": block == null ? null : block,
    "postalcode": postalcode == null ? null : postalcode,
    "status": status,
    "profileimage": profileimage,
    "birthday": birthday == null ? null : birthday,
    "tandcstatus1": tandcstatus1 == null ? null : tandcstatus1,
    "tandcstatus2": tandcstatus2 == null ? null : tandcstatus2,
  };
}
