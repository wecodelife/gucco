// To parse this JSON data, do
//
//     final referenceCodeCheckResponse = referenceCodeCheckResponseFromJson(jsonString);

import 'dart:convert';

ReferenceCodeCheckResponse referenceCodeCheckResponseFromJson(String str) => ReferenceCodeCheckResponse.fromJson(json.decode(str));

String referenceCodeCheckResponseToJson(ReferenceCodeCheckResponse data) => json.encode(data.toJson());

class ReferenceCodeCheckResponse {
  ReferenceCodeCheckResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final String data;

  factory ReferenceCodeCheckResponse.fromJson(Map<String, dynamic> json) => ReferenceCodeCheckResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : json["data"],
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : data,
  };
}
