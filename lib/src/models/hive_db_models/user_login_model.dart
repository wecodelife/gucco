import 'package:hive/hive.dart';

@HiveType(typeId: 0)
class UserLoginHive extends HiveObject {
  @HiveField(0)
  String uid;


  UserLoginHive({this.uid});
}