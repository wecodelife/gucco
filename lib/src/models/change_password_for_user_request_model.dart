// To parse this JSON data, do
//
//     final changePasswordForUserRequestModel = changePasswordForUserRequestModelFromJson(jsonString);

import 'dart:convert';

ChangePasswordForUserRequestModel changePasswordForUserRequestModelFromJson(
        String str) =>
    ChangePasswordForUserRequestModel.fromJson(json.decode(str));

String changePasswordForUserRequestModelToJson(
        ChangePasswordForUserRequestModel data) =>
    json.encode(data.toJson());

class ChangePasswordForUserRequestModel {
  ChangePasswordForUserRequestModel({
    this.newpassword,
    this.oldpassword,
  });

  final String newpassword;
  final String oldpassword;

  factory ChangePasswordForUserRequestModel.fromJson(
          Map<String, dynamic> json) =>
      ChangePasswordForUserRequestModel(
        newpassword: json["newpassword"] == null ? null : json["newpassword"],
        oldpassword: json["oldpassword"] == null ? null : json["oldpassword"],
      );

  Map<String, dynamic> toJson() => {
        "newpassword": newpassword == null ? null : newpassword,
        "oldpassword": oldpassword == null ? null : oldpassword,
      };
}
