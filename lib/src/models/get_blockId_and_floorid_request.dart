// To parse this JSON data, do
//
//     final getBlockIdAndFloorIdRequest = getBlockIdAndFloorIdRequestFromJson(jsonString);

import 'dart:convert';

GetBlockIdAndFloorIdRequest getBlockIdAndFloorIdRequestFromJson(String str) => GetBlockIdAndFloorIdRequest.fromJson(json.decode(str));

String getBlockIdAndFloorIdRequestToJson(GetBlockIdAndFloorIdRequest data) => json.encode(data.toJson());

class GetBlockIdAndFloorIdRequest {
  GetBlockIdAndFloorIdRequest({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final String data;

  factory GetBlockIdAndFloorIdRequest.fromJson(Map<String, dynamic> json) => GetBlockIdAndFloorIdRequest(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : json["data"],
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : data,
  };
}
