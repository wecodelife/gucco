// To parse this JSON data, do
//
//     final unitUsersResponse = unitUsersResponseFromJson(jsonString);

import 'dart:convert';

UnitUsersResponse unitUsersResponseFromJson(String str) =>
    UnitUsersResponse.fromJson(json.decode(str));

String unitUsersResponseToJson(UnitUsersResponse data) =>
    json.encode(data.toJson());

class UnitUsersResponse {
  UnitUsersResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final List<Datum> data;

  factory UnitUsersResponse.fromJson(Map<String, dynamic> json) =>
      UnitUsersResponse(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.rowno,
    this.id,
    this.userowner,
    this.usertenant,
    this.condoinfoId,
    this.floorinfoId,
    this.blockinfoId,
    this.blockname,
    this.floorname,
    this.unitdisplayname,
    this.unitno,
    this.active,
    this.allocatedowner,
    this.allocatedtenant,
    this.area,
    this.status,
    this.keycollectionpaymentstatus,
    this.keycollected,
    this.keycollectionmoduleenabled,
    this.showdownloadinvitation,
    this.invitationurl,
    this.unitstatus,
    this.tenantstatus,
  });

  final int rowno;
  final String id;
  final bool userowner;
  final bool usertenant;
  final String condoinfoId;
  final String floorinfoId;
  final String blockinfoId;
  final String blockname;
  final String floorname;
  final String unitdisplayname;
  final String unitno;
  final bool active;
  final bool allocatedowner;
  final bool allocatedtenant;
  final double area;
  final dynamic status;
  final bool keycollectionpaymentstatus;
  final bool keycollected;
  final bool keycollectionmoduleenabled;
  final bool showdownloadinvitation;
  final dynamic invitationurl;
  final dynamic unitstatus;
  final dynamic tenantstatus;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        rowno: json["rowno"] == null ? null : json["rowno"],
        id: json["id"] == null ? null : json["id"],
        userowner: json["userowner"] == null ? null : json["userowner"],
        usertenant: json["usertenant"] == null ? null : json["usertenant"],
        condoinfoId: json["condoinfo_id"] == null ? null : json["condoinfo_id"],
        floorinfoId: json["floorinfo_id"] == null ? null : json["floorinfo_id"],
        blockinfoId: json["blockinfo_id"] == null ? null : json["blockinfo_id"],
        blockname: json["blockname"] == null ? null : json["blockname"],
        floorname: json["floorname"] == null ? null : json["floorname"],
        unitdisplayname:
            json["unitdisplayname"] == null ? null : json["unitdisplayname"],
        unitno: json["unitno"] == null ? null : json["unitno"],
        active: json["active"] == null ? null : json["active"],
        allocatedowner:
            json["allocatedowner"] == null ? null : json["allocatedowner"],
        allocatedtenant:
            json["allocatedtenant"] == null ? null : json["allocatedtenant"],
        area: json["area"] == null ? null : json["area"],
        status: json["status"],
        keycollectionpaymentstatus: json["keycollectionpaymentstatus"] == null
            ? null
            : json["keycollectionpaymentstatus"],
        keycollected:
            json["keycollected"] == null ? null : json["keycollected"],
        keycollectionmoduleenabled: json["keycollectionmoduleenabled"] == null
            ? null
            : json["keycollectionmoduleenabled"],
        showdownloadinvitation: json["showdownloadinvitation"] == null
            ? null
            : json["showdownloadinvitation"],
        invitationurl: json["invitationurl"],
        unitstatus: json["unitstatus"],
        tenantstatus: json["tenantstatus"],
      );

  Map<String, dynamic> toJson() => {
        "rowno": rowno == null ? null : rowno,
        "id": id == null ? null : id,
        "userowner": userowner == null ? null : userowner,
        "usertenant": usertenant == null ? null : usertenant,
        "condoinfo_id": condoinfoId == null ? null : condoinfoId,
        "floorinfo_id": floorinfoId == null ? null : floorinfoId,
        "blockinfo_id": blockinfoId == null ? null : blockinfoId,
        "blockname": blockname == null ? null : blockname,
        "floorname": floorname == null ? null : floorname,
        "unitdisplayname": unitdisplayname == null ? null : unitdisplayname,
        "unitno": unitno == null ? null : unitno,
        "active": active == null ? null : active,
        "allocatedowner": allocatedowner == null ? null : allocatedowner,
        "allocatedtenant": allocatedtenant == null ? null : allocatedtenant,
        "area": area == null ? null : area,
        "status": status,
        "keycollectionpaymentstatus": keycollectionpaymentstatus == null
            ? null
            : keycollectionpaymentstatus,
        "keycollected": keycollected == null ? null : keycollected,
        "keycollectionmoduleenabled": keycollectionmoduleenabled == null
            ? null
            : keycollectionmoduleenabled,
        "showdownloadinvitation":
            showdownloadinvitation == null ? null : showdownloadinvitation,
        "invitationurl": invitationurl,
        "unitstatus": unitstatus,
        "tenantstatus": tenantstatus,
      };
}
