// To parse this JSON data, do
//
//     final earliestSlotKeyCollectionRequest = earliestSlotKeyCollectionRequestFromJson(jsonString);

import 'dart:convert';

EarliestSlotKeyCollectionRequest earliestSlotKeyCollectionRequestFromJson(
        String str) =>
    EarliestSlotKeyCollectionRequest.fromJson(json.decode(str));

String earliestSlotKeyCollectionRequestToJson(
        EarliestSlotKeyCollectionRequest data) =>
    json.encode(data.toJson());

class EarliestSlotKeyCollectionRequest {
  EarliestSlotKeyCollectionRequest({
    this.appointmentfromtime,
    this.appointmentondate,
  });

  final String appointmentfromtime;
  final String appointmentondate;

  factory EarliestSlotKeyCollectionRequest.fromJson(
          Map<String, dynamic> json) =>
      EarliestSlotKeyCollectionRequest(
        appointmentfromtime: json["appointmentfromtime"] == null
            ? null
            : json["appointmentfromtime"],
        appointmentondate: json["appointmentondate"] == null
            ? null
            : json["appointmentondate"],
      );

  Map<String, dynamic> toJson() => {
        "appointmentfromtime":
            appointmentfromtime == null ? null : appointmentfromtime,
        "appointmentondate":
            appointmentondate == null ? null : appointmentondate,
      };
}
