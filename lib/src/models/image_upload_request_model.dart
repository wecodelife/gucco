// To parse this JSON data, do
//
//     final imageUploadRequestModel = imageUploadRequestModelFromJson(jsonString);

import 'dart:convert';

ImageUploadRequestModel imageUploadRequestModelFromJson(String str) =>
    ImageUploadRequestModel.fromJson(json.decode(str));

String imageUploadRequestModelToJson(ImageUploadRequestModel data) =>
    json.encode(data.toJson());

class ImageUploadRequestModel {
  ImageUploadRequestModel({
    this.file,
    this.category,
  });

  dynamic file;
  String category;

  factory ImageUploadRequestModel.fromJson(Map<String, dynamic> json) =>
      ImageUploadRequestModel(
        file: json["file"],
        category: json["category"],
      );

  Map<String, dynamic> toJson() => {
        "file": file,
        "category": category,
      };
}
