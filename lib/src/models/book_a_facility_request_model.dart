// To parse this JSON data, do
//
//     final bookAFacilityRequest = bookAFacilityRequestFromJson(jsonString);

import 'dart:convert';

BookAFacilityRequest bookAFacilityRequestFromJson(String str) =>
    BookAFacilityRequest.fromJson(json.decode(str));

String bookAFacilityRequestToJson(BookAFacilityRequest data) =>
    json.encode(data.toJson());

class BookAFacilityRequest {
  BookAFacilityRequest({
    this.additionalnotes,
    this.endtime,
    this.facilityslotid,
    this.ondate,
    this.starttime,
    this.unitinfoid,
  });

  final String additionalnotes;
  final String endtime;
  final String facilityslotid;
  final String ondate;
  final String starttime;
  final String unitinfoid;

  factory BookAFacilityRequest.fromJson(Map<String, dynamic> json) =>
      BookAFacilityRequest(
        additionalnotes:
            json["additionalnotes"] == null ? null : json["additionalnotes"],
        endtime: json["endtime"] == null ? null : json["endtime"],
        facilityslotid:
            json["facilityslotid"] == null ? null : json["facilityslotid"],
        ondate: json["ondate"] == null ? null : json["ondate"],
        starttime: json["starttime"] == null ? null : json["starttime"],
        unitinfoid: json["unitinfoid"] == null ? null : json["unitinfoid"],
      );

  Map<String, dynamic> toJson() => {
        "additionalnotes": additionalnotes == null ? null : additionalnotes,
        "endtime": endtime == null ? null : endtime,
        "facilityslotid": facilityslotid == null ? null : facilityslotid,
        "ondate": ondate == null ? null : ondate,
        "starttime": starttime == null ? null : starttime,
        "unitinfoid": unitinfoid == null ? null : unitinfoid,
      };
}
