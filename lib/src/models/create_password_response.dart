// To parse this JSON data, do
//
//     final createPasswordResponse = createPasswordResponseFromJson(jsonString);

import 'dart:convert';

CreatePasswordResponse createPasswordResponseFromJson(String str) => CreatePasswordResponse.fromJson(json.decode(str));

String createPasswordResponseToJson(CreatePasswordResponse data) => json.encode(data.toJson());

class CreatePasswordResponse {
  CreatePasswordResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  int statuscode;
  dynamic statusmessage;
  int errorcode;
  String errormessage;
  Data data;

  factory CreatePasswordResponse.fromJson(Map<String, dynamic> json) => CreatePasswordResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.username,
    this.logintype,
    this.email,
    this.mobileno,
    this.emailverified,
    this.mobileverified,
    this.usertype,
    this.status,
    this.lastlogin,
    this.lastaccessed,
    this.lastpasswordchanged,
    this.id,
  });

  String username;
  String logintype;
  String email;
  dynamic mobileno;
  bool emailverified;
  bool mobileverified;
  dynamic usertype;
  dynamic status;
  dynamic lastlogin;
  dynamic lastaccessed;
  dynamic lastpasswordchanged;
  String id;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    username: json["username"] == null ? null : json["username"],
    logintype: json["logintype"] == null ? null : json["logintype"],
    email: json["email"] == null ? null : json["email"],
    mobileno: json["mobileno"],
    emailverified: json["emailverified"] == null ? null : json["emailverified"],
    mobileverified: json["mobileverified"] == null ? null : json["mobileverified"],
    usertype: json["usertype"],
    status: json["status"],
    lastlogin: json["lastlogin"],
    lastaccessed: json["lastaccessed"],
    lastpasswordchanged: json["lastpasswordchanged"],
    id: json["id"] == null ? null : json["id"],
  );

  Map<String, dynamic> toJson() => {
    "username": username == null ? null : username,
    "logintype": logintype == null ? null : logintype,
    "email": email == null ? null : email,
    "mobileno": mobileno,
    "emailverified": emailverified == null ? null : emailverified,
    "mobileverified": mobileverified == null ? null : mobileverified,
    "usertype": usertype,
    "status": status,
    "lastlogin": lastlogin,
    "lastaccessed": lastaccessed,
    "lastpasswordchanged": lastpasswordchanged,
    "id": id == null ? null : id,
  };
}
