// To parse this JSON data, do
//
//     final getCurrentBookingsForFacilityResponse = getCurrentBookingsForFacilityResponseFromJson(jsonString);

import 'dart:convert';

GetCurrentBookingsForFacilityResponse
    getCurrentBookingsForFacilityResponseFromJson(String str) =>
        GetCurrentBookingsForFacilityResponse.fromJson(json.decode(str));

String getCurrentBookingsForFacilityResponseToJson(
        GetCurrentBookingsForFacilityResponse data) =>
    json.encode(data.toJson());

class GetCurrentBookingsForFacilityResponse {
  GetCurrentBookingsForFacilityResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final List<Datum> data;

  factory GetCurrentBookingsForFacilityResponse.fromJson(
          Map<String, dynamic> json) =>
      GetCurrentBookingsForFacilityResponse(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.facilityname,
    this.ondate,
    this.starttime,
    this.endtime,
    this.additionalnotes,
    this.bookingstatus,
    this.fullname,
    this.bookedon,
    this.lastupdatedon,
    this.bookedby,
    this.unitdetails,
    this.emailaddress,
    this.contact,
    this.tenanttype,
  });

  final String id;
  final String facilityname;
  final String ondate;
  final String starttime;
  final String endtime;
  final String additionalnotes;
  final String bookingstatus;
  final String fullname;
  final String bookedon;
  final String lastupdatedon;
  final String bookedby;
  final Unitdetails unitdetails;
  final String emailaddress;
  final String contact;
  final String tenanttype;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        facilityname:
            json["facilityname"] == null ? null : json["facilityname"],
        ondate: json["ondate"] == null ? null : json["ondate"],
        starttime: json["starttime"] == null ? null : json["starttime"],
        endtime: json["endtime"] == null ? null : json["endtime"],
        additionalnotes:
            json["additionalnotes"] == null ? null : json["additionalnotes"],
        bookingstatus:
            json["bookingstatus"] == null ? null : json["bookingstatus"],
        fullname: json["fullname"] == null ? null : json["fullname"],
        bookedon: json["bookedon"] == null ? null : json["bookedon"],
        lastupdatedon:
            json["lastupdatedon"] == null ? null : json["lastupdatedon"],
        bookedby: json["bookedby"] == null ? null : json["bookedby"],
        unitdetails: json["unitdetails"] == null
            ? null
            : Unitdetails.fromJson(json["unitdetails"]),
        emailaddress:
            json["emailaddress"] == null ? null : json["emailaddress"],
        contact: json["contact"] == null ? null : json["contact"],
        tenanttype: json["tenanttype"] == null ? null : json["tenanttype"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "facilityname": facilityname == null ? null : facilityname,
        "ondate": ondate == null ? null : ondate,
        "starttime": starttime == null ? null : starttime,
        "endtime": endtime == null ? null : endtime,
        "additionalnotes": additionalnotes == null ? null : additionalnotes,
        "bookingstatus": bookingstatus == null ? null : bookingstatus,
        "fullname": fullname == null ? null : fullname,
        "bookedon": bookedon == null ? null : bookedon,
        "lastupdatedon": lastupdatedon == null ? null : lastupdatedon,
        "bookedby": bookedby == null ? null : bookedby,
        "unitdetails": unitdetails == null ? null : unitdetails.toJson(),
        "emailaddress": emailaddress == null ? null : emailaddress,
        "contact": contact == null ? null : contact,
        "tenanttype": tenanttype == null ? null : tenanttype,
      };
}

class Unitdetails {
  Unitdetails({
    this.id,
    this.condoinfoId,
    this.floorinfoId,
    this.blockinfoId,
    this.unitdisplayname,
    this.blockname,
    this.floorname,
    this.unitno,
    this.area,
    this.occupancy,
    this.unittype,
    this.status,
    this.keycollectionpaymentstatus,
    this.keycollected,
    this.keycollectionmoduleenabled,
  });

  final String id;
  final String condoinfoId;
  final String floorinfoId;
  final String blockinfoId;
  final String unitdisplayname;
  final String blockname;
  final String floorname;
  final String unitno;
  final double area;
  final int occupancy;
  final dynamic unittype;
  final String status;
  final bool keycollectionpaymentstatus;
  final bool keycollected;
  final bool keycollectionmoduleenabled;

  factory Unitdetails.fromJson(Map<String, dynamic> json) => Unitdetails(
        id: json["id"] == null ? null : json["id"],
        condoinfoId: json["condoinfo_id"] == null ? null : json["condoinfo_id"],
        floorinfoId: json["floorinfo_id"] == null ? null : json["floorinfo_id"],
        blockinfoId: json["blockinfo_id"] == null ? null : json["blockinfo_id"],
        unitdisplayname:
            json["unitdisplayname"] == null ? null : json["unitdisplayname"],
        blockname: json["blockname"] == null ? null : json["blockname"],
        floorname: json["floorname"] == null ? null : json["floorname"],
        unitno: json["unitno"] == null ? null : json["unitno"],
        area: json["area"] == null ? null : json["area"],
        occupancy: json["occupancy"] == null ? null : json["occupancy"],
        unittype: json["unittype"],
        status: json["status"] == null ? null : json["status"],
        keycollectionpaymentstatus: json["keycollectionpaymentstatus"] == null
            ? null
            : json["keycollectionpaymentstatus"],
        keycollected:
            json["keycollected"] == null ? null : json["keycollected"],
        keycollectionmoduleenabled: json["keycollectionmoduleenabled"] == null
            ? null
            : json["keycollectionmoduleenabled"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "condoinfo_id": condoinfoId == null ? null : condoinfoId,
        "floorinfo_id": floorinfoId == null ? null : floorinfoId,
        "blockinfo_id": blockinfoId == null ? null : blockinfoId,
        "unitdisplayname": unitdisplayname == null ? null : unitdisplayname,
        "blockname": blockname == null ? null : blockname,
        "floorname": floorname == null ? null : floorname,
        "unitno": unitno == null ? null : unitno,
        "area": area == null ? null : area,
        "occupancy": occupancy == null ? null : occupancy,
        "unittype": unittype,
        "status": status == null ? null : status,
        "keycollectionpaymentstatus": keycollectionpaymentstatus == null
            ? null
            : keycollectionpaymentstatus,
        "keycollected": keycollected == null ? null : keycollected,
        "keycollectionmoduleenabled": keycollectionmoduleenabled == null
            ? null
            : keycollectionmoduleenabled,
      };
}
