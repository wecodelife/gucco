// To parse this JSON data, do
//
//     final addUserProfileResponse = addUserProfileResponseFromJson(jsonString);

import 'dart:convert';

AddUserProfileResponse addUserProfileResponseFromJson(String str) => AddUserProfileResponse.fromJson(json.decode(str));

String addUserProfileResponseToJson(AddUserProfileResponse data) => json.encode(data.toJson());

class AddUserProfileResponse {
  AddUserProfileResponse({
    this.body,
    this.statusCode,
    this.statusCodeValue,
  });

  final Body body;
  final String statusCode;
  final int statusCodeValue;

  factory AddUserProfileResponse.fromJson(Map<String, dynamic> json) => AddUserProfileResponse(
    body: json["body"] == null ? null : Body.fromJson(json["body"]),
    statusCode: json["statusCode"] == null ? null : json["statusCode"],
    statusCodeValue: json["statusCodeValue"] == null ? null : json["statusCodeValue"],
  );

  Map<String, dynamic> toJson() => {
    "body": body == null ? null : body.toJson(),
    "statusCode": statusCode == null ? null : statusCode,
    "statusCodeValue": statusCodeValue == null ? null : statusCodeValue,
  };
}

class Body {
  Body();

  factory Body.fromJson(Map<String, dynamic> json) => Body(
  );

  Map<String, dynamic> toJson() => {
  };
}
