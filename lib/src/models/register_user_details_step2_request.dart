// To parse this JSON data, do
//
//     final registerUserDetailsStep2Request = registerUserDetailsStep2RequestFromJson(jsonString);

import 'dart:convert';

RegisterUserDetailsStep2Request registerUserDetailsStep2RequestFromJson(String str) => RegisterUserDetailsStep2Request.fromJson(json.decode(str));

String registerUserDetailsStep2RequestToJson(RegisterUserDetailsStep2Request data) => json.encode(data.toJson());

class RegisterUserDetailsStep2Request {
  RegisterUserDetailsStep2Request({
    this.block,
    this.postalcode,
    this.unit,
    this.zone,
  });

  final String block;
  final String postalcode;
  final String unit;
  final String zone;

  factory RegisterUserDetailsStep2Request.fromJson(Map<String, dynamic> json) => RegisterUserDetailsStep2Request(
    block: json["block"] == null ? null : json["block"],
    postalcode: json["postalcode"] == null ? null : json["postalcode"],
    unit: json["unit"] == null ? null : json["unit"],
    zone: json["zone"] == null ? null : json["zone"],
  );

  Map<String, dynamic> toJson() => {
    "block": block == null ? null : block,
    "postalcode": postalcode == null ? null : postalcode,
    "unit": unit == null ? null : unit,
    "zone": zone == null ? null : zone,
  };
}
