// To parse this JSON data, do
//
//     final attachPreferenceResponse = attachPreferenceResponseFromJson(jsonString);

import 'dart:convert';

AttachPreferenceResponse attachPreferenceResponseFromJson(String str) => AttachPreferenceResponse.fromJson(json.decode(str));

String attachPreferenceResponseToJson(AttachPreferenceResponse data) => json.encode(data.toJson());

class AttachPreferenceResponse {
  AttachPreferenceResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  int statuscode;
  dynamic statusmessage;
  int errorcode;
  String errormessage;
  Data data;

  factory AttachPreferenceResponse.fromJson(Map<String, dynamic> json) => AttachPreferenceResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.userid,
    this.preferencecategoryid,
    this.status,
  });

  String id;
  String userid;
  String preferencecategoryid;
  dynamic status;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    userid: json["userid"] == null ? null : json["userid"],
    preferencecategoryid: json["preferencecategoryid"] == null ? null : json["preferencecategoryid"],
    status: json["status"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "userid": userid == null ? null : userid,
    "preferencecategoryid": preferencecategoryid == null ? null : preferencecategoryid,
    "status": status,
  };
}
