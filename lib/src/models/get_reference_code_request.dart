// To parse this JSON data, do
//
//     final getReferenceCodeRequest = getReferenceCodeRequestFromJson(jsonString);

import 'dart:convert';

GetReferenceCodeRequest getReferenceCodeRequestFromJson(String str) => GetReferenceCodeRequest.fromJson(json.decode(str));

String getReferenceCodeRequestToJson(GetReferenceCodeRequest data) => json.encode(data.toJson());

class GetReferenceCodeRequest {
  GetReferenceCodeRequest({
    this.duration,
  });

  final String duration;

  factory GetReferenceCodeRequest.fromJson(Map<String, dynamic> json) => GetReferenceCodeRequest(
    duration: json["duration"] == null ? null : json["duration"],
  );

  Map<String, dynamic> toJson() => {
    "duration": duration == null ? null : duration,
  };
}
