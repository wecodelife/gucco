// To parse this JSON data, do
//
//     final uploadProfileImageResponseModel = uploadProfileImageResponseModelFromJson(jsonString);

import 'dart:convert';

UploadProfileImageResponseModel uploadProfileImageResponseModelFromJson(
        String str) =>
    UploadProfileImageResponseModel.fromJson(json.decode(str));

String uploadProfileImageResponseModelToJson(
        UploadProfileImageResponseModel data) =>
    json.encode(data.toJson());

class UploadProfileImageResponseModel {
  UploadProfileImageResponseModel({
    this.id,
    this.createdOn,
    this.createdBy,
    this.updatedOn,
    this.updatedBy,
    this.approvedBy,
    this.approvedOn,
    this.deleted,
    this.deletedOn,
    this.deletedBy,
    this.active,
    this.status,
    this.metadata,
    this.version,
    this.filetype,
    this.fileuri,
    this.fileextension,
    this.name,
    this.description,
    this.filesize,
    this.refid,
    this.referencetype,
  });

  final String id;
  final DateTime createdOn;
  final String createdBy;
  final DateTime updatedOn;
  final String updatedBy;
  final dynamic approvedBy;
  final dynamic approvedOn;
  final bool deleted;
  final dynamic deletedOn;
  final dynamic deletedBy;
  final bool active;
  final String status;
  final dynamic metadata;
  final int version;
  final String filetype;
  final String fileuri;
  final String fileextension;
  final dynamic name;
  final dynamic description;
  final int filesize;
  final String refid;
  final String referencetype;

  factory UploadProfileImageResponseModel.fromJson(Map<String, dynamic> json) =>
      UploadProfileImageResponseModel(
        id: json["id"] == null ? null : json["id"],
        createdOn: json["createdOn"] == null
            ? null
            : DateTime.parse(json["createdOn"]),
        createdBy: json["createdBy"] == null ? null : json["createdBy"],
        updatedOn: json["updatedOn"] == null
            ? null
            : DateTime.parse(json["updatedOn"]),
        updatedBy: json["updatedBy"] == null ? null : json["updatedBy"],
        approvedBy: json["approvedBy"],
        approvedOn: json["approvedOn"],
        deleted: json["deleted"] == null ? null : json["deleted"],
        deletedOn: json["deletedOn"],
        deletedBy: json["deletedBy"],
        active: json["active"] == null ? null : json["active"],
        status: json["status"] == null ? null : json["status"],
        metadata: json["metadata"],
        version: json["version"] == null ? null : json["version"],
        filetype: json["filetype"] == null ? null : json["filetype"],
        fileuri: json["fileuri"] == null ? null : json["fileuri"],
        fileextension:
            json["fileextension"] == null ? null : json["fileextension"],
        name: json["name"],
        description: json["description"],
        filesize: json["filesize"] == null ? null : json["filesize"],
        refid: json["refid"] == null ? null : json["refid"],
        referencetype:
            json["referencetype"] == null ? null : json["referencetype"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "createdOn": createdOn == null ? null : createdOn.toIso8601String(),
        "createdBy": createdBy == null ? null : createdBy,
        "updatedOn": updatedOn == null ? null : updatedOn.toIso8601String(),
        "updatedBy": updatedBy == null ? null : updatedBy,
        "approvedBy": approvedBy,
        "approvedOn": approvedOn,
        "deleted": deleted == null ? null : deleted,
        "deletedOn": deletedOn,
        "deletedBy": deletedBy,
        "active": active == null ? null : active,
        "status": status == null ? null : status,
        "metadata": metadata,
        "version": version == null ? null : version,
        "filetype": filetype == null ? null : filetype,
        "fileuri": fileuri == null ? null : fileuri,
        "fileextension": fileextension == null ? null : fileextension,
        "name": name,
        "description": description,
        "filesize": filesize == null ? null : filesize,
        "refid": refid == null ? null : refid,
        "referencetype": referencetype == null ? null : referencetype,
      };
}
