// To parse this JSON data, do
//
//     final getKeyCollectionAppointmentResponse = getKeyCollectionAppointmentResponseFromJson(jsonString);

import 'dart:convert';

GetKeyCollectionAppointmentResponse getKeyCollectionAppointmentResponseFromJson(
        String str) =>
    GetKeyCollectionAppointmentResponse.fromJson(json.decode(str));

String getKeyCollectionAppointmentResponseToJson(
        GetKeyCollectionAppointmentResponse data) =>
    json.encode(data.toJson());

class GetKeyCollectionAppointmentResponse {
  GetKeyCollectionAppointmentResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final Data data;

  factory GetKeyCollectionAppointmentResponse.fromJson(
          Map<String, dynamic> json) =>
      GetKeyCollectionAppointmentResponse(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.appointmentdate,
    this.appointmentfromtime,
    this.appointmenttotime,
    this.self,
    this.owner,
    this.appointmentstatus,
    this.noofpeople,
    this.vehicleplateno,
    this.additionalinfo,
    this.comments,
    this.fullname,
    this.contactno,
    this.emailaddress,
    this.createdOn,
    this.unitinfoid,
    this.blockinfoid,
    this.floorinfoid,
    this.blockname,
    this.floorname,
    this.unitno,
    this.unitdisplayname,
  });

  final String id;
  final String appointmentdate;
  final String appointmentfromtime;
  final String appointmenttotime;
  final bool self;
  final bool owner;
  final String appointmentstatus;
  final int noofpeople;
  final String vehicleplateno;
  final String additionalinfo;
  final dynamic comments;
  final String fullname;
  final String contactno;
  final String emailaddress;
  final DateTime createdOn;
  final String unitinfoid;
  final String blockinfoid;
  final String floorinfoid;
  final String blockname;
  final String floorname;
  final String unitno;
  final String unitdisplayname;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        appointmentdate:
            json["appointmentdate"] == null ? null : json["appointmentdate"],
        appointmentfromtime: json["appointmentfromtime"] == null
            ? null
            : json["appointmentfromtime"],
        appointmenttotime: json["appointmenttotime"] == null
            ? null
            : json["appointmenttotime"],
        self: json["self"] == null ? null : json["self"],
        owner: json["owner"] == null ? null : json["owner"],
        appointmentstatus: json["appointmentstatus"] == null
            ? null
            : json["appointmentstatus"],
        noofpeople: json["noofpeople"] == null ? null : json["noofpeople"],
        vehicleplateno:
            json["vehicleplateno"] == null ? null : json["vehicleplateno"],
        additionalinfo:
            json["additionalinfo"] == null ? null : json["additionalinfo"],
        comments: json["comments"],
        fullname: json["fullname"] == null ? null : json["fullname"],
        contactno: json["contactno"] == null ? null : json["contactno"],
        emailaddress:
            json["emailaddress"] == null ? null : json["emailaddress"],
        createdOn: json["createdOn"] == null
            ? null
            : DateTime.parse(json["createdOn"]),
        unitinfoid: json["unitinfoid"] == null ? null : json["unitinfoid"],
        blockinfoid: json["blockinfoid"] == null ? null : json["blockinfoid"],
        floorinfoid: json["floorinfoid"] == null ? null : json["floorinfoid"],
        blockname: json["blockname"] == null ? null : json["blockname"],
        floorname: json["floorname"] == null ? null : json["floorname"],
        unitno: json["unitno"] == null ? null : json["unitno"],
        unitdisplayname:
            json["unitdisplayname"] == null ? null : json["unitdisplayname"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "appointmentdate": appointmentdate == null ? null : appointmentdate,
        "appointmentfromtime":
            appointmentfromtime == null ? null : appointmentfromtime,
        "appointmenttotime":
            appointmenttotime == null ? null : appointmenttotime,
        "self": self == null ? null : self,
        "owner": owner == null ? null : owner,
        "appointmentstatus":
            appointmentstatus == null ? null : appointmentstatus,
        "noofpeople": noofpeople == null ? null : noofpeople,
        "vehicleplateno": vehicleplateno == null ? null : vehicleplateno,
        "additionalinfo": additionalinfo == null ? null : additionalinfo,
        "comments": comments,
        "fullname": fullname == null ? null : fullname,
        "contactno": contactno == null ? null : contactno,
        "emailaddress": emailaddress == null ? null : emailaddress,
        "createdOn": createdOn == null ? null : createdOn.toIso8601String(),
        "unitinfoid": unitinfoid == null ? null : unitinfoid,
        "blockinfoid": blockinfoid == null ? null : blockinfoid,
        "floorinfoid": floorinfoid == null ? null : floorinfoid,
        "blockname": blockname == null ? null : blockname,
        "floorname": floorname == null ? null : floorname,
        "unitno": unitno == null ? null : unitno,
        "unitdisplayname": unitdisplayname == null ? null : unitdisplayname,
      };
}
