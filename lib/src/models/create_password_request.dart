// To parse this JSON data, do
//
//     final createPasswordRequest = createPasswordRequestFromJson(jsonString);

import 'dart:convert';

CreatePasswordRequest createPasswordRequestFromJson(String str) => CreatePasswordRequest.fromJson(json.decode(str));

String createPasswordRequestToJson(CreatePasswordRequest data) => json.encode(data.toJson());

class CreatePasswordRequest {
  CreatePasswordRequest({
    this.password,
  });

  String password;

  factory CreatePasswordRequest.fromJson(Map<String, dynamic> json) => CreatePasswordRequest(
    password: json["password"] == null ? null : json["password"],
  );

  Map<String, dynamic> toJson() => {
    "password": password == null ? null : password,
  };
}
