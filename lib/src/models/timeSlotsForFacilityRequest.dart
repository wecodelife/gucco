// To parse this JSON data, do
//
//     final timeSlotsForFacilitiesRequest = timeSlotsForFacilitiesRequestFromJson(jsonString);

import 'dart:convert';

TimeSlotsForFacilitiesRequest timeSlotsForFacilitiesRequestFromJson(
        String str) =>
    TimeSlotsForFacilitiesRequest.fromJson(json.decode(str));

String timeSlotsForFacilitiesRequestToJson(
        TimeSlotsForFacilitiesRequest data) =>
    json.encode(data.toJson());

class TimeSlotsForFacilitiesRequest {
  TimeSlotsForFacilitiesRequest({
    this.unitinfoId,
    this.userId,
  });

  final String unitinfoId;
  final String userId;

  factory TimeSlotsForFacilitiesRequest.fromJson(Map<String, dynamic> json) =>
      TimeSlotsForFacilitiesRequest(
        unitinfoId: json["unitinfo_id"] == null ? null : json["unitinfo_id"],
        userId: json["user_id"] == null ? null : json["user_id"],
      );

  Map<String, dynamic> toJson() => {
        "unitinfo_id": unitinfoId == null ? null : unitinfoId,
        "user_id": userId == null ? null : userId,
      };
}
