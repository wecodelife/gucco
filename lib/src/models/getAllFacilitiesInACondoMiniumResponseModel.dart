// To parse this JSON data, do
//
//     final getAllFacilitiesInACondoMiniumResponse = getAllFacilitiesInACondoMiniumResponseFromJson(jsonString);

import 'dart:convert';

GetAllFacilitiesInACondoMiniumResponse
    getAllFacilitiesInACondoMiniumResponseFromJson(String str) =>
        GetAllFacilitiesInACondoMiniumResponse.fromJson(json.decode(str));

String getAllFacilitiesInACondoMiniumResponseToJson(
        GetAllFacilitiesInACondoMiniumResponse data) =>
    json.encode(data.toJson());

class GetAllFacilitiesInACondoMiniumResponse {
  GetAllFacilitiesInACondoMiniumResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final List<Datum> data;

  factory GetAllFacilitiesInACondoMiniumResponse.fromJson(
          Map<String, dynamic> json) =>
      GetAllFacilitiesInACondoMiniumResponse(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.webprofileimage,
    this.mobileprofileimage,
    this.webimages,
    this.mobileimages,
    this.id,
    this.facilityname,
    this.description,
    this.bookingavailabledaysinadvance,
    this.bookinginterval,
    this.bookingintervalunit,
    this.slotsperbooking,
    this.bookingsperinterval,
    this.advancedeposit,
    this.closebookingdaysbefore,
    this.autocancellation,
    this.cancellationperiod,
    this.cancellationperiodunit,
    this.termsandcondition,
    this.facilityrequiresbooking,
    this.bookingrequiresconfirmation,
    this.status,
  });

  final dynamic webprofileimage;
  final dynamic mobileprofileimage;
  final List<Webimage> webimages;
  final List<dynamic> mobileimages;
  final String id;
  final String facilityname;
  final String description;
  final int bookingavailabledaysinadvance;
  final int bookinginterval;
  final String bookingintervalunit;
  final int slotsperbooking;
  final int bookingsperinterval;
  final double advancedeposit;
  final int closebookingdaysbefore;
  final bool autocancellation;
  final int cancellationperiod;
  final dynamic cancellationperiodunit;
  final dynamic termsandcondition;
  final bool facilityrequiresbooking;
  final bool bookingrequiresconfirmation;
  final String status;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        webprofileimage: json["webprofileimage"],
        mobileprofileimage: json["mobileprofileimage"],
        webimages: json["webimages"] == null
            ? null
            : List<Webimage>.from(
                json["webimages"].map((x) => Webimage.fromJson(x))),
        mobileimages: json["mobileimages"] == null
            ? null
            : List<dynamic>.from(json["mobileimages"].map((x) => x)),
        id: json["id"] == null ? null : json["id"],
        facilityname:
            json["facilityname"] == null ? null : json["facilityname"],
        description: json["description"] == null ? null : json["description"],
        bookingavailabledaysinadvance:
            json["bookingavailabledaysinadvance"] == null
                ? null
                : json["bookingavailabledaysinadvance"],
        bookinginterval:
            json["bookinginterval"] == null ? null : json["bookinginterval"],
        bookingintervalunit: json["bookingintervalunit"] == null
            ? null
            : json["bookingintervalunit"],
        slotsperbooking:
            json["slotsperbooking"] == null ? null : json["slotsperbooking"],
        bookingsperinterval: json["bookingsperinterval"] == null
            ? null
            : json["bookingsperinterval"],
        advancedeposit:
            json["advancedeposit"] == null ? null : json["advancedeposit"],
        closebookingdaysbefore: json["closebookingdaysbefore"] == null
            ? null
            : json["closebookingdaysbefore"],
        autocancellation:
            json["autocancellation"] == null ? null : json["autocancellation"],
        cancellationperiod: json["cancellationperiod"] == null
            ? null
            : json["cancellationperiod"],
        cancellationperiodunit: json["cancellationperiodunit"],
        termsandcondition: json["termsandcondition"],
        facilityrequiresbooking: json["facilityrequiresbooking"] == null
            ? null
            : json["facilityrequiresbooking"],
        bookingrequiresconfirmation: json["bookingrequiresconfirmation"] == null
            ? null
            : json["bookingrequiresconfirmation"],
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "webprofileimage": webprofileimage,
        "mobileprofileimage": mobileprofileimage,
        "webimages": webimages == null
            ? null
            : List<dynamic>.from(webimages.map((x) => x.toJson())),
        "mobileimages": mobileimages == null
            ? null
            : List<dynamic>.from(mobileimages.map((x) => x)),
        "id": id == null ? null : id,
        "facilityname": facilityname == null ? null : facilityname,
        "description": description == null ? null : description,
        "bookingavailabledaysinadvance": bookingavailabledaysinadvance == null
            ? null
            : bookingavailabledaysinadvance,
        "bookinginterval": bookinginterval == null ? null : bookinginterval,
        "bookingintervalunit":
            bookingintervalunit == null ? null : bookingintervalunit,
        "slotsperbooking": slotsperbooking == null ? null : slotsperbooking,
        "bookingsperinterval":
            bookingsperinterval == null ? null : bookingsperinterval,
        "advancedeposit": advancedeposit == null ? null : advancedeposit,
        "closebookingdaysbefore":
            closebookingdaysbefore == null ? null : closebookingdaysbefore,
        "autocancellation": autocancellation == null ? null : autocancellation,
        "cancellationperiod":
            cancellationperiod == null ? null : cancellationperiod,
        "cancellationperiodunit": cancellationperiodunit,
        "termsandcondition": termsandcondition,
        "facilityrequiresbooking":
            facilityrequiresbooking == null ? null : facilityrequiresbooking,
        "bookingrequiresconfirmation": bookingrequiresconfirmation == null
            ? null
            : bookingrequiresconfirmation,
        "status": status == null ? null : status,
      };
}

class Webimage {
  Webimage({
    this.id,
    this.name,
    this.filetype,
    this.category,
    this.refid,
    this.description,
  });

  final String id;
  final dynamic name;
  final String filetype;
  final String category;
  final String refid;
  final dynamic description;

  factory Webimage.fromJson(Map<String, dynamic> json) => Webimage(
        id: json["id"] == null ? null : json["id"],
        name: json["name"],
        filetype: json["filetype"] == null ? null : json["filetype"],
        category: json["category"] == null ? null : json["category"],
        refid: json["refid"] == null ? null : json["refid"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name,
        "filetype": filetype == null ? null : filetype,
        "category": category == null ? null : category,
        "refid": refid == null ? null : refid,
        "description": description,
      };
}
