// To parse this JSON data, do
//
//     final keyCollectionHappeningResponse = keyCollectionHappeningResponseFromJson(jsonString);

import 'dart:convert';

KeyCollectionHappeningResponse keyCollectionHappeningResponseFromJson(String str) => KeyCollectionHappeningResponse.fromJson(json.decode(str));

String keyCollectionHappeningResponseToJson(KeyCollectionHappeningResponse data) => json.encode(data.toJson());

class KeyCollectionHappeningResponse {
  KeyCollectionHappeningResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final String statusmessage;
  final int errorcode;
  final String errormessage;
  final dynamic data;

  factory KeyCollectionHappeningResponse.fromJson(Map<String, dynamic> json) => KeyCollectionHappeningResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"] == null ? null : json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"],
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage == null ? null : statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data,
  };
}
