// To parse this JSON data, do
//
//     final addUnitLoginMartinModernRequest = addUnitLoginMartinModernRequestFromJson(jsonString);

import 'dart:convert';

AddUnitLoginMartinModernRequest addUnitLoginMartinModernRequestFromJson(String str) => AddUnitLoginMartinModernRequest.fromJson(json.decode(str));

String addUnitLoginMartinModernRequestToJson(AddUnitLoginMartinModernRequest data) => json.encode(data.toJson());

class AddUnitLoginMartinModernRequest {
  AddUnitLoginMartinModernRequest({
    this.owner,
    this.unitpassword,
    this.unitusername,
  });

  final bool owner;
  final String unitpassword;
  final String unitusername;

  factory AddUnitLoginMartinModernRequest.fromJson(Map<String, dynamic> json) => AddUnitLoginMartinModernRequest(
    owner: json["owner"] == null ? null : json["owner"],
    unitpassword: json["unitpassword"] == null ? null : json["unitpassword"],
    unitusername: json["unitusername"] == null ? null : json["unitusername"],
  );

  Map<String, dynamic> toJson() => {
    "owner": owner == null ? null : owner,
    "unitpassword": unitpassword == null ? null : unitpassword,
    "unitusername": unitusername == null ? null : unitusername,
  };
}
