// To parse this JSON data, do
//
//     final earliestSlotKeyCollectionResponse = earliestSlotKeyCollectionResponseFromJson(jsonString);

import 'dart:convert';

EarliestSlotKeyCollectionResponse earliestSlotKeyCollectionResponseFromJson(
        String str) =>
    EarliestSlotKeyCollectionResponse.fromJson(json.decode(str));

String earliestSlotKeyCollectionResponseToJson(
        EarliestSlotKeyCollectionResponse data) =>
    json.encode(data.toJson());

class EarliestSlotKeyCollectionResponse {
  EarliestSlotKeyCollectionResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final Data data;

  factory EarliestSlotKeyCollectionResponse.fromJson(
          Map<String, dynamic> json) =>
      EarliestSlotKeyCollectionResponse(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.ondate,
    this.starttime,
    this.message,
    this.ignoremessage,
  });

  final String ondate;
  final String starttime;
  final String message;
  final bool ignoremessage;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        ondate: json["ondate"] == null ? null : json["ondate"],
        starttime: json["starttime"] == null ? null : json["starttime"],
        message: json["message"] == null ? null : json["message"],
        ignoremessage:
            json["ignoremessage"] == null ? null : json["ignoremessage"],
      );

  Map<String, dynamic> toJson() => {
        "ondate": ondate == null ? null : ondate,
        "starttime": starttime == null ? null : starttime,
        "message": message == null ? null : message,
        "ignoremessage": ignoremessage == null ? null : ignoremessage,
      };
}
