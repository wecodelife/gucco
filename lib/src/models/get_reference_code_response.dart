// To parse this JSON data, do
//
//     final getReferenceCodeResponse = getReferenceCodeResponseFromJson(jsonString);

import 'dart:convert';

GetReferenceCodeResponse getReferenceCodeResponseFromJson(String str) => GetReferenceCodeResponse.fromJson(json.decode(str));

String getReferenceCodeResponseToJson(GetReferenceCodeResponse data) => json.encode(data.toJson());

class GetReferenceCodeResponse {
  GetReferenceCodeResponse({
    this.body,
    this.statusCode,
    this.statusCodeValue,
  });

  final Body body;
  final String statusCode;
  final int statusCodeValue;

  factory GetReferenceCodeResponse.fromJson(Map<String, dynamic> json) => GetReferenceCodeResponse(
    body: json["body"] == null ? null : Body.fromJson(json["body"]),
    statusCode: json["statusCode"] == null ? null : json["statusCode"],
    statusCodeValue: json["statusCodeValue"] == null ? null : json["statusCodeValue"],
  );

  Map<String, dynamic> toJson() => {
    "body": body == null ? null : body.toJson(),
    "statusCode": statusCode == null ? null : statusCode,
    "statusCodeValue": statusCodeValue == null ? null : statusCodeValue,
  };
}

class Body {
  Body();

  factory Body.fromJson(Map<String, dynamic> json) => Body(
  );

  Map<String, dynamic> toJson() => {
  };
}
