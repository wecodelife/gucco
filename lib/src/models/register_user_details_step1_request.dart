// To parse this JSON data, do
//
//     final registerUserDetailsStep1Request = registerUserDetailsStep1RequestFromJson(jsonString);

import 'dart:convert';

RegisterUserDetailsStep1Request registerUserDetailsStep1RequestFromJson(String str) => RegisterUserDetailsStep1Request.fromJson(json.decode(str));

String registerUserDetailsStep1RequestToJson(RegisterUserDetailsStep1Request data) => json.encode(data.toJson());

class RegisterUserDetailsStep1Request {
  RegisterUserDetailsStep1Request({
    this.contactno,
    this.email,
    this.firstname,
    this.lastname,
    this.salutation,
  });

  final String contactno;
  final String email;
  final String firstname;
  final String lastname;
  final String salutation;

  factory RegisterUserDetailsStep1Request.fromJson(Map<String, dynamic> json) => RegisterUserDetailsStep1Request(
    contactno: json["contactno"] == null ? null : json["contactno"],
    email: json["email"] == null ? null : json["email"],
    firstname: json["firstname"] == null ? null : json["firstname"],
    lastname: json["lastname"] == null ? null : json["lastname"],
    salutation: json["salutation"] == null ? null : json["salutation"],
  );

  Map<String, dynamic> toJson() => {
    "contactno": contactno == null ? null : contactno,
    "email": email == null ? null : email,
    "firstname": firstname == null ? null : firstname,
    "lastname": lastname == null ? null : lastname,
    "salutation": salutation == null ? null : salutation,
  };
}
