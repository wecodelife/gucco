// To parse this JSON data, do
//
//     final keyCollectionSelfResponse = keyCollectionSelfResponseFromJson(jsonString);

import 'dart:convert';

KeyCollectionSelfResponse keyCollectionSelfResponseFromJson(String str) => KeyCollectionSelfResponse.fromJson(json.decode(str));

String keyCollectionSelfResponseToJson(KeyCollectionSelfResponse data) => json.encode(data.toJson());

class KeyCollectionSelfResponse {
  KeyCollectionSelfResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final Data data;

  factory KeyCollectionSelfResponse.fromJson(Map<String, dynamic> json) => KeyCollectionSelfResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.id,
    this.appointmentdate,
    this.appointmentfromtime,
    this.appointmenttotime,
    this.self,
    this.appointmentstatus,
    this.noofpeople,
    this.vehicleplateno,
    this.additionalinfo,
    this.comments,
    this.fullname,
    this.contactno,
    this.emailaddress,
    this.createdOn,
  });

  final String id;
  final String appointmentdate;
  final String appointmentfromtime;
  final String appointmenttotime;
  final bool self;
  final String appointmentstatus;
  final int noofpeople;
  final String vehicleplateno;
  final String additionalinfo;
  final dynamic comments;
  final String fullname;
  final String contactno;
  final String emailaddress;
  final DateTime createdOn;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    id: json["id"] == null ? null : json["id"],
    appointmentdate: json["appointmentdate"] == null ? null : json["appointmentdate"],
    appointmentfromtime: json["appointmentfromtime"] == null ? null : json["appointmentfromtime"],
    appointmenttotime: json["appointmenttotime"] == null ? null : json["appointmenttotime"],
    self: json["self"] == null ? null : json["self"],
    appointmentstatus: json["appointmentstatus"] == null ? null : json["appointmentstatus"],
    noofpeople: json["noofpeople"] == null ? null : json["noofpeople"],
    vehicleplateno: json["vehicleplateno"] == null ? null : json["vehicleplateno"],
    additionalinfo: json["additionalinfo"] == null ? null : json["additionalinfo"],
    comments: json["comments"],
    fullname: json["fullname"] == null ? null : json["fullname"],
    contactno: json["contactno"] == null ? null : json["contactno"],
    emailaddress: json["emailaddress"] == null ? null : json["emailaddress"],
    createdOn: json["createdOn"] == null ? null : DateTime.parse(json["createdOn"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "appointmentdate": appointmentdate == null ? null : appointmentdate,
    "appointmentfromtime": appointmentfromtime == null ? null : appointmentfromtime,
    "appointmenttotime": appointmenttotime == null ? null : appointmenttotime,
    "self": self == null ? null : self,
    "appointmentstatus": appointmentstatus == null ? null : appointmentstatus,
    "noofpeople": noofpeople == null ? null : noofpeople,
    "vehicleplateno": vehicleplateno == null ? null : vehicleplateno,
    "additionalinfo": additionalinfo == null ? null : additionalinfo,
    "comments": comments,
    "fullname": fullname == null ? null : fullname,
    "contactno": contactno == null ? null : contactno,
    "emailaddress": emailaddress == null ? null : emailaddress,
    "createdOn": createdOn == null ? null : createdOn.toIso8601String(),
  };
}
