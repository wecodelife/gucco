// To parse this JSON data, do
//
//     final detachPreferenceResponse = detachPreferenceResponseFromJson(jsonString);

import 'dart:convert';

DetachPreferenceResponse detachPreferenceResponseFromJson(String str) => DetachPreferenceResponse.fromJson(json.decode(str));

String detachPreferenceResponseToJson(DetachPreferenceResponse data) => json.encode(data.toJson());

class DetachPreferenceResponse {
  DetachPreferenceResponse({
    this.body,
    this.statusCode,
    this.statusCodeValue,
  });

  final Body body;
  final String statusCode;
  final int statusCodeValue;

  factory DetachPreferenceResponse.fromJson(Map<String, dynamic> json) => DetachPreferenceResponse(
    body: json["body"] == null ? null : Body.fromJson(json["body"]),
    statusCode: json["statusCode"] == null ? null : json["statusCode"],
    statusCodeValue: json["statusCodeValue"] == null ? null : json["statusCodeValue"],
  );

  Map<String, dynamic> toJson() => {
    "body": body == null ? null : body.toJson(),
    "statusCode": statusCode == null ? null : statusCode,
    "statusCodeValue": statusCodeValue == null ? null : statusCodeValue,
  };
}

class Body {
  Body();

  factory Body.fromJson(Map<String, dynamic> json) => Body(
  );

  Map<String, dynamic> toJson() => {
  };
}
