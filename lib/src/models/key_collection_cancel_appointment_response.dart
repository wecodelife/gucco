// To parse this JSON data, do
//
//     final keyCollectionCancelAppointment = keyCollectionCancelAppointmentFromJson(jsonString);

import 'dart:convert';

KeyCollectionCancelAppointment keyCollectionCancelAppointmentFromJson(String str) => KeyCollectionCancelAppointment.fromJson(json.decode(str));

String keyCollectionCancelAppointmentToJson(KeyCollectionCancelAppointment data) => json.encode(data.toJson());

class KeyCollectionCancelAppointment {
  KeyCollectionCancelAppointment({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final dynamic data;

  factory KeyCollectionCancelAppointment.fromJson(Map<String, dynamic> json) => KeyCollectionCancelAppointment(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"],
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data,
  };
}
