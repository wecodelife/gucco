// To parse this JSON data, do
//
//     final paymentStatusResponse = paymentStatusResponseFromJson(jsonString);

import 'dart:convert';

PaymentStatusResponse paymentStatusResponseFromJson(String str) =>
    PaymentStatusResponse.fromJson(json.decode(str));

String paymentStatusResponseToJson(PaymentStatusResponse data) =>
    json.encode(data.toJson());

class PaymentStatusResponse {
  PaymentStatusResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final Data data;

  factory PaymentStatusResponse.fromJson(Map<String, dynamic> json) =>
      PaymentStatusResponse(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.condoinfoId,
    this.floorinfoId,
    this.blockinfoId,
    this.unitdisplayname,
    this.blockname,
    this.floorname,
    this.unitno,
    this.area,
    this.occupancy,
    this.unittype,
    this.status,
    this.keycollectionpaymentstatus,
    this.keycollected,
    this.keycollectionmoduleenabled,
  });

  final String id;
  final String condoinfoId;
  final String floorinfoId;
  final String blockinfoId;
  final String unitdisplayname;
  final String blockname;
  final String floorname;
  final String unitno;
  final double area;
  final int occupancy;
  final dynamic unittype;
  final String status;
  final bool keycollectionpaymentstatus;
  final bool keycollected;
  final bool keycollectionmoduleenabled;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        condoinfoId: json["condoinfo_id"] == null ? null : json["condoinfo_id"],
        floorinfoId: json["floorinfo_id"] == null ? null : json["floorinfo_id"],
        blockinfoId: json["blockinfo_id"] == null ? null : json["blockinfo_id"],
        unitdisplayname:
            json["unitdisplayname"] == null ? null : json["unitdisplayname"],
        blockname: json["blockname"] == null ? null : json["blockname"],
        floorname: json["floorname"] == null ? null : json["floorname"],
        unitno: json["unitno"] == null ? null : json["unitno"],
        area: json["area"] == null ? null : json["area"],
        occupancy: json["occupancy"] == null ? null : json["occupancy"],
        unittype: json["unittype"],
        status: json["status"] == null ? null : json["status"],
        keycollectionpaymentstatus: json["keycollectionpaymentstatus"] == null
            ? null
            : json["keycollectionpaymentstatus"],
        keycollected:
            json["keycollected"] == null ? null : json["keycollected"],
        keycollectionmoduleenabled: json["keycollectionmoduleenabled"] == null
            ? null
            : json["keycollectionmoduleenabled"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "condoinfo_id": condoinfoId == null ? null : condoinfoId,
        "floorinfo_id": floorinfoId == null ? null : floorinfoId,
        "blockinfo_id": blockinfoId == null ? null : blockinfoId,
        "unitdisplayname": unitdisplayname == null ? null : unitdisplayname,
        "blockname": blockname == null ? null : blockname,
        "floorname": floorname == null ? null : floorname,
        "unitno": unitno == null ? null : unitno,
        "area": area == null ? null : area,
        "occupancy": occupancy == null ? null : occupancy,
        "unittype": unittype,
        "status": status == null ? null : status,
        "keycollectionpaymentstatus": keycollectionpaymentstatus == null
            ? null
            : keycollectionpaymentstatus,
        "keycollected": keycollected == null ? null : keycollected,
        "keycollectionmoduleenabled": keycollectionmoduleenabled == null
            ? null
            : keycollectionmoduleenabled,
      };
}
