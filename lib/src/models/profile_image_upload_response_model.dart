// To parse this JSON data, do
//
//     final uploadImageRequestBody = uploadImageRequestBodyFromJson(jsonString);

import 'dart:convert';

UploadImageRequestBody uploadImageRequestBodyFromJson(String str) =>
    UploadImageRequestBody.fromJson(json.decode(str));

String uploadImageRequestBodyToJson(UploadImageRequestBody data) =>
    json.encode(data.toJson());

class UploadImageRequestBody {
  UploadImageRequestBody({
    this.file,
    this.category,
  });

  final String file;
  final String category;

  factory UploadImageRequestBody.fromJson(Map<String, dynamic> json) =>
      UploadImageRequestBody(
        file: json["File"] == null ? null : json["File"],
        category: json["Category"] == null ? null : json["Category"],
      );

  Map<String, dynamic> toJson() => {
        "File": file == null ? null : file,
        "Category": category == null ? null : category,
      };
}
