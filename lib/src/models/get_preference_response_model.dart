// To parse this JSON data, do
//
//     final getPreferenceResponse = getPreferenceResponseFromJson(jsonString);

import 'dart:convert';

GetPreferenceResponse getPreferenceResponseFromJson(String str) => GetPreferenceResponse.fromJson(json.decode(str));

String getPreferenceResponseToJson(GetPreferenceResponse data) => json.encode(data.toJson());

class GetPreferenceResponse {
  GetPreferenceResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final List<Datum> data;

  factory GetPreferenceResponse.fromJson(Map<String, dynamic> json) => GetPreferenceResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.name,
    this.displayname,
    this.description,
    this.status,
    this.id,
  });

  final String name;
  final String displayname;
  final String description;
  final dynamic status;
  final dynamic id;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    name: json["name"] == null ? null : json["name"],
    displayname: json["displayname"] == null ? null : json["displayname"],
    description: json["description"] == null ? null : json["description"],
    status: json["status"],
    id: json["id"],
  );

  Map<String, dynamic> toJson() => {
    "name": name == null ? null : name,
    "displayname": displayname == null ? null : displayname,
    "description": description == null ? null : description,
    "status": status,
    "id": id,
  };
}
