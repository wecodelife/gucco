// To parse this JSON data, do
//
//     final getBlockIdResponse = getBlockIdResponseFromJson(jsonString);

import 'dart:convert';

GetBlockIdResponse getBlockIdResponseFromJson(String str) => GetBlockIdResponse.fromJson(json.decode(str));

String getBlockIdResponseToJson(GetBlockIdResponse data) => json.encode(data.toJson());

class GetBlockIdResponse {
  GetBlockIdResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final List<Datum> data;

  factory GetBlockIdResponse.fromJson(Map<String, dynamic> json) => GetBlockIdResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.createdOn,
    this.createdBy,
    this.updatedOn,
    this.updatedBy,
    this.deleted,
    this.active,
    this.status,
    this.blockname,
    this.area,
    this.latitude,
    this.longitude,
    this.floorcount,
    this.unitcount,
  });

  final String id;
  final DateTime createdOn;
  final String createdBy;
  final DateTime updatedOn;
  final String updatedBy;
  final bool deleted;
  final bool active;
  final String status;
  final String blockname;
  final double area;
  final double latitude;
  final double longitude;
  final int floorcount;
  final int unitcount;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    createdOn: json["createdOn"] == null ? null : DateTime.parse(json["createdOn"]),
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    updatedOn: json["updatedOn"] == null ? null : DateTime.parse(json["updatedOn"]),
    updatedBy: json["updatedBy"] == null ? null : json["updatedBy"],
    deleted: json["deleted"] == null ? null : json["deleted"],
    active: json["active"] == null ? null : json["active"],
    status: json["status"] == null ? null : json["status"],
    blockname: json["blockname"] == null ? null : json["blockname"],
    area: json["area"] == null ? null : json["area"],
    latitude: json["latitude"] == null ? null : json["latitude"],
    longitude: json["longitude"] == null ? null : json["longitude"],
    floorcount: json["floorcount"] == null ? null : json["floorcount"],
    unitcount: json["unitcount"] == null ? null : json["unitcount"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "createdOn": createdOn == null ? null : createdOn.toIso8601String(),
    "createdBy": createdBy == null ? null : createdBy,
    "updatedOn": updatedOn == null ? null : updatedOn.toIso8601String(),
    "updatedBy": updatedBy == null ? null : updatedBy,
    "deleted": deleted == null ? null : deleted,
    "active": active == null ? null : active,
    "status": status == null ? null : status,
    "blockname": blockname == null ? null : blockname,
    "area": area == null ? null : area,
    "latitude": latitude == null ? null : latitude,
    "longitude": longitude == null ? null : longitude,
    "floorcount": floorcount == null ? null : floorcount,
    "unitcount": unitcount == null ? null : unitcount,
  };
}
