// To parse this JSON data, do
//
//     final addUnitLoginMartinModernResponse = addUnitLoginMartinModernResponseFromJson(jsonString);

import 'dart:convert';

AddUnitLoginMartinModernResponse addUnitLoginMartinModernResponseFromJson(String str) => AddUnitLoginMartinModernResponse.fromJson(json.decode(str));

String addUnitLoginMartinModernResponseToJson(AddUnitLoginMartinModernResponse data) => json.encode(data.toJson());

class AddUnitLoginMartinModernResponse {
  AddUnitLoginMartinModernResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final String data;

  factory AddUnitLoginMartinModernResponse.fromJson(Map<String, dynamic> json) => AddUnitLoginMartinModernResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : json["data"],
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : data,
  };
}
