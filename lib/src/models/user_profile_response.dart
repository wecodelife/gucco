// To parse this JSON data, do
//
//     final profileResponseModel = profileResponseModelFromJson(jsonString);

import 'dart:convert';

ProfileResponseModel profileResponseModelFromJson(String str) =>
    ProfileResponseModel.fromJson(json.decode(str));

String profileResponseModelToJson(ProfileResponseModel data) =>
    json.encode(data.toJson());

class ProfileResponseModel {
  ProfileResponseModel({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final Data data;

  factory ProfileResponseModel.fromJson(Map<String, dynamic> json) =>
      ProfileResponseModel(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.firstname,
    this.lastname,
    this.zone,
    this.unit,
    this.block,
    this.postalcode,
    this.status,
    this.profileimage,
    this.birthday,
    this.tandcstatus1,
    this.tandcstatus2,
    this.username,
    this.email,
    this.mobileno,
    this.passwordlastchangedon,
  });

  final String id;
  final String firstname;
  final String lastname;
  final String zone;
  final String unit;
  final String block;
  final String postalcode;
  final String status;
  final dynamic profileimage;
  final String birthday;
  final bool tandcstatus1;
  final bool tandcstatus2;
  final String username;
  final String email;
  final dynamic mobileno;
  final String passwordlastchangedon;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
      id: json["id"] == null ? null : json["id"],
      firstname: json["firstname"] == null ? null : json["firstname"],
      lastname: json["lastname"] == null ? null : json["lastname"],
      zone: json["zone"] == null ? null : json["zone"],
      unit: json["unit"] == null ? null : json["unit"],
      block: json["block"] == null ? null : json["block"],
      postalcode: json["postalcode"] == null ? null : json["postalcode"],
      status: json["status"] == null ? null : json["status"],
      profileimage: json["profileimage"],
      birthday: json["birthday"] == null ? null : json["birthday"],
      tandcstatus1: json["tandcstatus1"] == null ? null : json["tandcstatus1"],
      tandcstatus2: json["tandcstatus2"] == null ? null : json["tandcstatus2"],
      username: json["username"] == null ? null : json["username"],
      email: json["email"] == null ? null : json["email"],
      mobileno: json["mobileno"],
      passwordlastchangedon: json["passwordlastchangedon"] == null
          ? null
          : json["passwordlastchangedon"]);

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "firstname": firstname == null ? null : firstname,
        "lastname": lastname == null ? null : lastname,
        "zone": zone == null ? null : zone,
        "unit": unit == null ? null : unit,
        "block": block == null ? null : block,
        "postalcode": postalcode == null ? null : postalcode,
        "status": status == null ? null : status,
        "profileimage": profileimage,
        "birthday": birthday == null ? null : birthday,
        "tandcstatus1": tandcstatus1 == null ? null : tandcstatus1,
        "tandcstatus2": tandcstatus2 == null ? null : tandcstatus2,
        "username": username == null ? null : username,
        "email": email == null ? null : email,
        "mobileno": mobileno,
        "passwordlastchangedon":
            passwordlastchangedon == null ? null : passwordlastchangedon
      };
}
