// To parse this JSON data, do
//
//     final timeSlotForFacilitiesResponse = timeSlotForFacilitiesResponseFromJson(jsonString);

import 'dart:convert';

TimeSlotForFacilitiesResponse timeSlotForFacilitiesResponseFromJson(
        String str) =>
    TimeSlotForFacilitiesResponse.fromJson(json.decode(str));

String timeSlotForFacilitiesResponseToJson(
        TimeSlotForFacilitiesResponse data) =>
    json.encode(data.toJson());

class TimeSlotForFacilitiesResponse {
  TimeSlotForFacilitiesResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final List<Datum> data;

  factory TimeSlotForFacilitiesResponse.fromJson(Map<String, dynamic> json) =>
      TimeSlotForFacilitiesResponse(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.id,
    this.starttime,
    this.endtime,
    this.rateperbooking,
  });

  final String id;
  final String starttime;
  final dynamic endtime;
  final double rateperbooking;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"] == null ? null : json["id"],
        starttime: json["starttime"] == null ? null : json["starttime"],
        endtime: json["endtime"],
        rateperbooking:
            json["rateperbooking"] == null ? null : json["rateperbooking"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "starttime": starttime == null ? null : starttime,
        "endtime": endtime,
        "rateperbooking": rateperbooking == null ? null : rateperbooking,
      };
}
