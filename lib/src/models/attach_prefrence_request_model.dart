// To parse this JSON data, do
//
//     final addPreferenceRequest = addPreferenceRequestFromJson(jsonString);

import 'dart:convert';

AddPreferenceRequest addPreferenceRequestFromJson(String str) => AddPreferenceRequest.fromJson(json.decode(str));

String addPreferenceRequestToJson(AddPreferenceRequest data) => json.encode(data.toJson());

class AddPreferenceRequest {
  AddPreferenceRequest({
    this.description,
    this.displayname,
    this.name,
  });

  final String description;
  final String displayname;
  final String name;

  factory AddPreferenceRequest.fromJson(Map<String, dynamic> json) => AddPreferenceRequest(
    description: json["description"] == null ? null : json["description"],
    displayname: json["displayname"] == null ? null : json["displayname"],
    name: json["name"] == null ? null : json["name"],
  );

  Map<String, dynamic> toJson() => {
    "description": description == null ? null : description,
    "displayname": displayname == null ? null : displayname,
    "name": name == null ? null : name,
  };
}
