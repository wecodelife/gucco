// To parse this JSON data, do
//
//     final changePasswordForUserResponseModel = changePasswordForUserResponseModelFromJson(jsonString);

import 'dart:convert';

ChangePasswordForUserResponseModel changePasswordForUserResponseModelFromJson(
        String str) =>
    ChangePasswordForUserResponseModel.fromJson(json.decode(str));

String changePasswordForUserResponseModelToJson(
        ChangePasswordForUserResponseModel data) =>
    json.encode(data.toJson());

class ChangePasswordForUserResponseModel {
  ChangePasswordForUserResponseModel({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final Data data;

  factory ChangePasswordForUserResponseModel.fromJson(
          Map<String, dynamic> json) =>
      ChangePasswordForUserResponseModel(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.id,
    this.username,
    this.logintype,
    this.email,
    this.mobileno,
    this.emailverified,
    this.mobileverified,
    this.usertype,
    this.status,
    this.lastlogin,
    this.lastaccessed,
    this.lastpasswordchanged,
    this.admin,
    this.owner,
    this.tenant,
    this.manager,
    this.managingagent,
    this.cso,
  });

  final String id;
  final String username;
  final String logintype;
  final String email;
  final String mobileno;
  final bool emailverified;
  final bool mobileverified;
  final dynamic usertype;
  final String status;
  final dynamic lastlogin;
  final dynamic lastaccessed;
  final DateTime lastpasswordchanged;
  final bool admin;
  final bool owner;
  final bool tenant;
  final bool manager;
  final bool managingagent;
  final bool cso;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        id: json["id"] == null ? null : json["id"],
        username: json["username"] == null ? null : json["username"],
        logintype: json["logintype"] == null ? null : json["logintype"],
        email: json["email"] == null ? null : json["email"],
        mobileno: json["mobileno"] == null ? null : json["mobileno"],
        emailverified:
            json["emailverified"] == null ? null : json["emailverified"],
        mobileverified:
            json["mobileverified"] == null ? null : json["mobileverified"],
        usertype: json["usertype"],
        status: json["status"] == null ? null : json["status"],
        lastlogin: json["lastlogin"],
        lastaccessed: json["lastaccessed"],
        lastpasswordchanged: json["lastpasswordchanged"] == null
            ? null
            : DateTime.parse(json["lastpasswordchanged"]),
        admin: json["admin"] == null ? null : json["admin"],
        owner: json["owner"] == null ? null : json["owner"],
        tenant: json["tenant"] == null ? null : json["tenant"],
        manager: json["manager"] == null ? null : json["manager"],
        managingagent:
            json["managingagent"] == null ? null : json["managingagent"],
        cso: json["cso"] == null ? null : json["cso"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "username": username == null ? null : username,
        "logintype": logintype == null ? null : logintype,
        "email": email == null ? null : email,
        "mobileno": mobileno == null ? null : mobileno,
        "emailverified": emailverified == null ? null : emailverified,
        "mobileverified": mobileverified == null ? null : mobileverified,
        "usertype": usertype,
        "status": status == null ? null : status,
        "lastlogin": lastlogin,
        "lastaccessed": lastaccessed,
        "lastpasswordchanged": lastpasswordchanged == null
            ? null
            : lastpasswordchanged.toIso8601String(),
        "admin": admin == null ? null : admin,
        "owner": owner == null ? null : owner,
        "tenant": tenant == null ? null : tenant,
        "manager": manager == null ? null : manager,
        "managingagent": managingagent == null ? null : managingagent,
        "cso": cso == null ? null : cso,
      };
}
