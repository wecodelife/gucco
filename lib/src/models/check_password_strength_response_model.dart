// To parse this JSON data, do
//
//     final checkPasswordStrengthResponseModel = checkPasswordStrengthResponseModelFromJson(jsonString);

import 'dart:convert';

CheckPasswordStrengthResponseModel checkPasswordStrengthResponseModelFromJson(
        String str) =>
    CheckPasswordStrengthResponseModel.fromJson(json.decode(str));

String checkPasswordStrengthResponseModelToJson(
        CheckPasswordStrengthResponseModel data) =>
    json.encode(data.toJson());

class CheckPasswordStrengthResponseModel {
  CheckPasswordStrengthResponseModel({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final Data data;

  factory CheckPasswordStrengthResponseModel.fromJson(
          Map<String, dynamic> json) =>
      CheckPasswordStrengthResponseModel(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null ? null : Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null ? null : data.toJson(),
      };
}

class Data {
  Data({
    this.errorMessage,
    this.strength,
    this.status,
  });

  final String errorMessage;
  final int strength;
  final bool status;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        errorMessage:
            json["errorMessage"] == null ? null : json["errorMessage"],
        strength: json["strength"] == null ? null : json["strength"],
        status: json["status"] == null ? null : json["status"],
      );

  Map<String, dynamic> toJson() => {
        "errorMessage": errorMessage == null ? null : errorMessage,
        "strength": strength == null ? null : strength,
        "status": status == null ? null : status,
      };
}
