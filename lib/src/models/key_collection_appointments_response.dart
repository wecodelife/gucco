// To parse this JSON data, do
//
//     final keyCollectionAppointmentsResponse = keyCollectionAppointmentsResponseFromJson(jsonString);

import 'dart:convert';

KeyCollectionAppointmentsResponse keyCollectionAppointmentsResponseFromJson(String str) => KeyCollectionAppointmentsResponse.fromJson(json.decode(str));

String keyCollectionAppointmentsResponseToJson(KeyCollectionAppointmentsResponse data) => json.encode(data.toJson());

class KeyCollectionAppointmentsResponse {
  KeyCollectionAppointmentsResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final List<Datum> data;

  factory KeyCollectionAppointmentsResponse.fromJson(Map<String, dynamic> json) => KeyCollectionAppointmentsResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.appointmentdate,
    this.appointmentfromtime,
    this.appointmenttotime,
    this.self,
    this.appointmentstatus,
    this.noofpeople,
    this.vehicleplateno,
    this.additionalinfo,
    this.comments,
    this.fullname,
    this.contactno,
    this.emailaddress,
    this.createdOn,
  });

  final String id;
  final String appointmentdate;
  final String appointmentfromtime;
  final String appointmenttotime;
  final bool self;
  final String appointmentstatus;
  final int noofpeople;
  final dynamic vehicleplateno;
  final dynamic additionalinfo;
  final dynamic comments;
  final String fullname;
  final String contactno;
  final String emailaddress;
  final DateTime createdOn;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    appointmentdate: json["appointmentdate"] == null ? null : json["appointmentdate"],
    appointmentfromtime: json["appointmentfromtime"] == null ? null : json["appointmentfromtime"],
    appointmenttotime: json["appointmenttotime"] == null ? null : json["appointmenttotime"],
    self: json["self"] == null ? null : json["self"],
    appointmentstatus: json["appointmentstatus"] == null ? null : json["appointmentstatus"],
    noofpeople: json["noofpeople"] == null ? null : json["noofpeople"],
    vehicleplateno: json["vehicleplateno"],
    additionalinfo: json["additionalinfo"],
    comments: json["comments"],
    fullname: json["fullname"] == null ? null : json["fullname"],
    contactno: json["contactno"] == null ? null : json["contactno"],
    emailaddress: json["emailaddress"] == null ? null : json["emailaddress"],
    createdOn: json["createdOn"] == null ? null : DateTime.parse(json["createdOn"]),
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "appointmentdate": appointmentdate == null ? null : appointmentdate,
    "appointmentfromtime": appointmentfromtime == null ? null : appointmentfromtime,
    "appointmenttotime": appointmenttotime == null ? null : appointmenttotime,
    "self": self == null ? null : self,
    "appointmentstatus": appointmentstatus == null ? null : appointmentstatus,
    "noofpeople": noofpeople == null ? null : noofpeople,
    "vehicleplateno": vehicleplateno,
    "additionalinfo": additionalinfo,
    "comments": comments,
    "fullname": fullname == null ? null : fullname,
    "contactno": contactno == null ? null : contactno,
    "emailaddress": emailaddress == null ? null : emailaddress,
    "createdOn": createdOn == null ? null : createdOn.toIso8601String(),
  };
}
