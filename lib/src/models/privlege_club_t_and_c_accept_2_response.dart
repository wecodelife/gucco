// To parse this JSON data, do
//
//     final privilegeClubTAndCAccept2Response = privilegeClubTAndCAccept2ResponseFromJson(jsonString);

import 'dart:convert';

PrivilegeClubTAndCAccept2Response privilegeClubTAndCAccept2ResponseFromJson(String str) => PrivilegeClubTAndCAccept2Response.fromJson(json.decode(str));

String privilegeClubTAndCAccept2ResponseToJson(PrivilegeClubTAndCAccept2Response data) => json.encode(data.toJson());

class PrivilegeClubTAndCAccept2Response {
  PrivilegeClubTAndCAccept2Response({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final Data data;

  factory PrivilegeClubTAndCAccept2Response.fromJson(Map<String, dynamic> json) => PrivilegeClubTAndCAccept2Response(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : Data.fromJson(json["data"]),
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : data.toJson(),
  };
}

class Data {
  Data({
    this.username,
    this.logintype,
    this.email,
    this.mobileno,
    this.emailverified,
    this.mobileverified,
    this.usertype,
    this.status,
    this.lastlogin,
    this.lastaccessed,
    this.lastpasswordchanged,
    this.id,
  });

  final String username;
  final String logintype;
  final String email;
  final dynamic mobileno;
  final bool emailverified;
  final bool mobileverified;
  final dynamic usertype;
  final dynamic status;
  final dynamic lastlogin;
  final dynamic lastaccessed;
  final dynamic lastpasswordchanged;
  final String id;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
    username: json["username"] == null ? null : json["username"],
    logintype: json["logintype"] == null ? null : json["logintype"],
    email: json["email"] == null ? null : json["email"],
    mobileno: json["mobileno"],
    emailverified: json["emailverified"] == null ? null : json["emailverified"],
    mobileverified: json["mobileverified"] == null ? null : json["mobileverified"],
    usertype: json["usertype"],
    status: json["status"],
    lastlogin: json["lastlogin"],
    lastaccessed: json["lastaccessed"],
    lastpasswordchanged: json["lastpasswordchanged"],
    id: json["id"] == null ? null : json["id"],
  );

  Map<String, dynamic> toJson() => {
    "username": username == null ? null : username,
    "logintype": logintype == null ? null : logintype,
    "email": email == null ? null : email,
    "mobileno": mobileno,
    "emailverified": emailverified == null ? null : emailverified,
    "mobileverified": mobileverified == null ? null : mobileverified,
    "usertype": usertype,
    "status": status,
    "lastlogin": lastlogin,
    "lastaccessed": lastaccessed,
    "lastpasswordchanged": lastpasswordchanged,
    "id": id == null ? null : id,
  };
}
