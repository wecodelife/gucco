// To parse this JSON data, do
//
//     final addUserProfileRequest = addUserProfileRequestFromJson(jsonString);

import 'dart:convert';

AddUserProfileRequest addUserProfileRequestFromJson(String str) => AddUserProfileRequest.fromJson(json.decode(str));

String addUserProfileRequestToJson(AddUserProfileRequest data) => json.encode(data.toJson());

class AddUserProfileRequest {
  AddUserProfileRequest({
    this.block,
    this.firstname,
    this.lastname,
    this.postalcode,
    this.salutation,
    this.unit,
    this.zone,
  });

  final String block;
  final String firstname;
  final String lastname;
  final String postalcode;
  final String salutation;
  final String unit;
  final String zone;

  factory AddUserProfileRequest.fromJson(Map<String, dynamic> json) => AddUserProfileRequest(
    block: json["block"] == null ? null : json["block"],
    firstname: json["firstname"] == null ? null : json["firstname"],
    lastname: json["lastname"] == null ? null : json["lastname"],
    postalcode: json["postalcode"] == null ? null : json["postalcode"],
    salutation: json["salutation"] == null ? null : json["salutation"],
    unit: json["unit"] == null ? null : json["unit"],
    zone: json["zone"] == null ? null : json["zone"],
  );

  Map<String, dynamic> toJson() => {
    "block": block == null ? null : block,
    "firstname": firstname == null ? null : firstname,
    "lastname": lastname == null ? null : lastname,
    "postalcode": postalcode == null ? null : postalcode,
    "salutation": salutation == null ? null : salutation,
    "unit": unit == null ? null : unit,
    "zone": zone == null ? null : zone,
  };
}
