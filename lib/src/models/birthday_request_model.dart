// To parse this JSON data, do
//
//     final birthdayRequest = birthdayRequestFromJson(jsonString);

import 'dart:convert';

BirthdayRequest birthdayRequestFromJson(String str) => BirthdayRequest.fromJson(json.decode(str));

String birthdayRequestToJson(BirthdayRequest data) => json.encode(data.toJson());

class BirthdayRequest {
  BirthdayRequest({
    this.month,
    this.year,
  });

  int month;
  int year;

  factory BirthdayRequest.fromJson(Map<String, dynamic> json) => BirthdayRequest(
    month: json["month"] == null ? null : json["month"],
    year: json["year"] == null ? null : json["year"],
  );

  Map<String, dynamic> toJson() => {
    "month": month == null ? null : month,
    "year": year == null ? null : year,
  };
}
