// To parse this JSON data, do
//
//     final keyCollectionTimeSlotsResponse = keyCollectionTimeSlotsResponseFromJson(jsonString);

import 'dart:convert';

KeyCollectionTimeSlotsResponse keyCollectionTimeSlotsResponseFromJson(String str) => KeyCollectionTimeSlotsResponse.fromJson(json.decode(str));

String keyCollectionTimeSlotsResponseToJson(KeyCollectionTimeSlotsResponse data) => json.encode(data.toJson());

class KeyCollectionTimeSlotsResponse {
  KeyCollectionTimeSlotsResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final List<Datum> data;

  factory KeyCollectionTimeSlotsResponse.fromJson(Map<String, dynamic> json) => KeyCollectionTimeSlotsResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"] == null ? null : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data == null ? null : List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  Datum({
    this.id,
    this.createdOn,
    this.createdBy,
    this.updatedOn,
    this.updatedBy,
    this.deleted,
    this.active,
    this.status,
    this.seqno,
    this.ondate,
    this.starttime,
    this.endtime,
    this.durationinmins,
    this.appointmentsperslot,
    this.officialholiday,
    this.appointmentallowed,
    this.slottype,
  });

  final String id;
  final DateTime createdOn;
  final String createdBy;
  final DateTime updatedOn;
  final String updatedBy;
  final bool deleted;
  final bool active;
  final String status;
  final int seqno;
  final DateTime ondate;
  final String starttime;
  final String endtime;
  final int durationinmins;
  final int appointmentsperslot;
  final bool officialholiday;
  final bool appointmentallowed;
  final String slottype;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"] == null ? null : json["id"],
    createdOn: json["createdOn"] == null ? null : DateTime.parse(json["createdOn"]),
    createdBy: json["createdBy"] == null ? null : json["createdBy"],
    updatedOn: json["updatedOn"] == null ? null : DateTime.parse(json["updatedOn"]),
    updatedBy: json["updatedBy"] == null ? null : json["updatedBy"],
    deleted: json["deleted"] == null ? null : json["deleted"],
    active: json["active"] == null ? null : json["active"],
    status: json["status"] == null ? null : json["status"],
    seqno: json["seqno"] == null ? null : json["seqno"],
    ondate: json["ondate"] == null ? null : DateTime.parse(json["ondate"]),
    starttime: json["starttime"] == null ? null : json["starttime"],
    endtime: json["endtime"] == null ? null : json["endtime"],
    durationinmins: json["durationinmins"] == null ? null : json["durationinmins"],
    appointmentsperslot: json["appointmentsperslot"] == null ? null : json["appointmentsperslot"],
    officialholiday: json["officialholiday"] == null ? null : json["officialholiday"],
    appointmentallowed: json["appointmentallowed"] == null ? null : json["appointmentallowed"],
    slottype: json["slottype"] == null ? null : json["slottype"],
  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "createdOn": createdOn == null ? null : createdOn.toIso8601String(),
    "createdBy": createdBy == null ? null : createdBy,
    "updatedOn": updatedOn == null ? null : updatedOn.toIso8601String(),
    "updatedBy": updatedBy == null ? null : updatedBy,
    "deleted": deleted == null ? null : deleted,
    "active": active == null ? null : active,
    "status": status == null ? null : status,
    "seqno": seqno == null ? null : seqno,
    "ondate": ondate == null ? null : "${ondate.year.toString().padLeft(4, '0')}-${ondate.month.toString().padLeft(2, '0')}-${ondate.day.toString().padLeft(2, '0')}",
    "starttime": starttime == null ? null : starttime,
    "endtime": endtime == null ? null : endtime,
    "durationinmins": durationinmins == null ? null : durationinmins,
    "appointmentsperslot": appointmentsperslot == null ? null : appointmentsperslot,
    "officialholiday": officialholiday == null ? null : officialholiday,
    "appointmentallowed": appointmentallowed == null ? null : appointmentallowed,
    "slottype": slottype == null ? null : slottype,
  };
}
