// To parse this JSON data, do
//
//     final registerPrivilegeClubOtpResponse = registerPrivilegeClubOtpResponseFromJson(jsonString);

import 'dart:convert';

RegisterPrivilegeClubOtpResponse registerPrivilegeClubOtpResponseFromJson(String str) => RegisterPrivilegeClubOtpResponse.fromJson(json.decode(str));

String registerPrivilegeClubOtpResponseToJson(RegisterPrivilegeClubOtpResponse data) => json.encode(data.toJson());

class RegisterPrivilegeClubOtpResponse {
  RegisterPrivilegeClubOtpResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final dynamic data;

  factory RegisterPrivilegeClubOtpResponse.fromJson(Map<String, dynamic> json) => RegisterPrivilegeClubOtpResponse(
    statuscode: json["statuscode"] == null ? null : json["statuscode"],
    statusmessage: json["statusmessage"],
    errorcode: json["errorcode"] == null ? null : json["errorcode"],
    errormessage: json["errormessage"] == null ? null : json["errormessage"],
    data: json["data"],
  );

  Map<String, dynamic> toJson() => {
    "statuscode": statuscode == null ? null : statuscode,
    "statusmessage": statusmessage,
    "errorcode": errorcode == null ? null : errorcode,
    "errormessage": errormessage == null ? null : errormessage,
    "data": data,
  };
}
