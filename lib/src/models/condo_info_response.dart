// To parse this JSON data, do
//
//     final condoInfoResponse = condoInfoResponseFromJson(jsonString);

import 'dart:convert';

CondoInfoResponse condoInfoResponseFromJson(String str) =>
    CondoInfoResponse.fromJson(json.decode(str));

String condoInfoResponseToJson(CondoInfoResponse data) =>
    json.encode(data.toJson());

class CondoInfoResponse {
  CondoInfoResponse({
    this.statuscode,
    this.statusmessage,
    this.errorcode,
    this.errormessage,
    this.data,
  });

  final int statuscode;
  final dynamic statusmessage;
  final int errorcode;
  final String errormessage;
  final List<Datum> data;

  factory CondoInfoResponse.fromJson(Map<String, dynamic> json) =>
      CondoInfoResponse(
        statuscode: json["statuscode"] == null ? null : json["statuscode"],
        statusmessage: json["statusmessage"],
        errorcode: json["errorcode"] == null ? null : json["errorcode"],
        errormessage:
            json["errormessage"] == null ? null : json["errormessage"],
        data: json["data"] == null
            ? null
            : List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "statuscode": statuscode == null ? null : statuscode,
        "statusmessage": statusmessage,
        "errorcode": errorcode == null ? null : errorcode,
        "errormessage": errormessage == null ? null : errormessage,
        "data": data == null
            ? null
            : List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Datum {
  Datum({
    this.webprofileimage,
    this.mobileprofileimage,
    this.webimages,
    this.mobileimages,
    this.id,
    this.propertycode,
    this.propertyname,
    this.propertytype,
    this.area,
    this.country,
    this.district,
    this.postalcode,
    this.address,
    this.latitude,
    this.longitude,
    this.accountingemail,
    this.billingemail,
    this.propertyemail,
    this.propertycontact,
    this.blockcount,
    this.floorcount,
    this.unitcount,
    this.allocatedunits,
    this.tenantallocatedunits,
  });

  final Images webprofileimage;
  final dynamic mobileprofileimage;
  final List<Images> webimages;
  final List<Images> mobileimages;
  final String id;
  final String propertycode;
  final String propertyname;
  final String propertytype;
  final double area;
  final String country;
  final String district;
  final String postalcode;
  final String address;
  final double latitude;
  final double longitude;
  final String accountingemail;
  final String billingemail;
  final String propertyemail;
  final String propertycontact;
  final int blockcount;
  final int floorcount;
  final int unitcount;
  final int allocatedunits;
  final int tenantallocatedunits;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        webprofileimage: json["webprofileimage"] == null
            ? null
            : Images.fromJson(json["webprofileimage"]),
        mobileprofileimage: json["mobileprofileimage"],
        webimages: json["webimages"] == null
            ? null
            : List<Images>.from(
                json["webimages"].map((x) => Images.fromJson(x))),
        mobileimages: json["mobileimages"] == null
            ? null
            : List<Images>.from(
                json["mobileimages"].map((x) => Images.fromJson(x))),
        id: json["id"] == null ? null : json["id"],
        propertycode:
            json["propertycode"] == null ? null : json["propertycode"],
        propertyname:
            json["propertyname"] == null ? null : json["propertyname"],
        propertytype:
            json["propertytype"] == null ? null : json["propertytype"],
        area: json["area"] == null ? null : json["area"],
        country: json["country"] == null ? null : json["country"],
        district: json["district"] == null ? null : json["district"],
        postalcode: json["postalcode"] == null ? null : json["postalcode"],
        address: json["address"] == null ? null : json["address"],
        latitude: json["latitude"] == null ? null : json["latitude"],
        longitude: json["longitude"] == null ? null : json["longitude"],
        accountingemail:
            json["accountingemail"] == null ? null : json["accountingemail"],
        billingemail:
            json["billingemail"] == null ? null : json["billingemail"],
        propertyemail:
            json["propertyemail"] == null ? null : json["propertyemail"],
        propertycontact:
            json["propertycontact"] == null ? null : json["propertycontact"],
        blockcount: json["blockcount"] == null ? null : json["blockcount"],
        floorcount: json["floorcount"] == null ? null : json["floorcount"],
        unitcount: json["unitcount"] == null ? null : json["unitcount"],
        allocatedunits:
            json["allocatedunits"] == null ? null : json["allocatedunits"],
        tenantallocatedunits: json["tenantallocatedunits"] == null
            ? null
            : json["tenantallocatedunits"],
      );

  Map<String, dynamic> toJson() => {
        "webprofileimage":
            webprofileimage == null ? null : webprofileimage.toJson(),
        "mobileprofileimage": mobileprofileimage,
        "webimages": webimages == null
            ? null
            : List<dynamic>.from(webimages.map((x) => x.toJson())),
        "mobileimages": mobileimages == null
            ? null
            : List<dynamic>.from(mobileimages.map((x) => x.toJson())),
        "id": id == null ? null : id,
        "propertycode": propertycode == null ? null : propertycode,
        "propertyname": propertyname == null ? null : propertyname,
        "propertytype": propertytype == null ? null : propertytype,
        "area": area == null ? null : area,
        "country": country == null ? null : country,
        "district": district == null ? null : district,
        "postalcode": postalcode == null ? null : postalcode,
        "address": address == null ? null : address,
        "latitude": latitude == null ? null : latitude,
        "longitude": longitude == null ? null : longitude,
        "accountingemail": accountingemail == null ? null : accountingemail,
        "billingemail": billingemail == null ? null : billingemail,
        "propertyemail": propertyemail == null ? null : propertyemail,
        "propertycontact": propertycontact == null ? null : propertycontact,
        "blockcount": blockcount == null ? null : blockcount,
        "floorcount": floorcount == null ? null : floorcount,
        "unitcount": unitcount == null ? null : unitcount,
        "allocatedunits": allocatedunits == null ? null : allocatedunits,
        "tenantallocatedunits":
            tenantallocatedunits == null ? null : tenantallocatedunits,
      };
}

class Images {
  Images({
    this.id,
    this.name,
    this.filetype,
    this.category,
    this.refid,
    this.description,
  });

  final String id;
  final dynamic name;
  final String filetype;
  final String category;
  final String refid;
  final dynamic description;

  factory Images.fromJson(Map<String, dynamic> json) => Images(
        id: json["id"] == null ? null : json["id"],
        name: json["name"],
        filetype: json["filetype"] == null ? null : json["filetype"],
        category: json["category"] == null ? null : json["category"],
        refid: json["refid"] == null ? null : json["refid"],
        description: json["description"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "name": name,
        "filetype": filetype == null ? null : filetype,
        "category": category == null ? null : category,
        "refid": refid == null ? null : refid,
        "description": description,
      };
}
