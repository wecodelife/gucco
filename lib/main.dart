import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:guoco_priv_app/src/app.dart';
import 'package:guoco_priv_app/src/utils/constants.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as path_provider;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  // SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
  final appDocDir = await path_provider.getApplicationDocumentsDirectory();
  Hive.init(appDocDir.path);

  await Hive.openBox(Constants.BOX_NAME);
  runApp(MyApp());
}

// class MyApp extends StatelessWidget {
//   // Create the initialization Future outside of `build`:
//   final Future<FirebaseApp> _initialization = Firebase.initializeApp();
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       home: FutureBuilder(
//         // Initialize FlutterFire:
//         future: _initialization,
//         builder: (context, snapshot) {
//           // Check for errors
//           if (snapshot.hasError) {
//             return Scaffold(
//               backgroundColor: Colors.red,
//             );
//           }
//
//           // Once complete, show your application
//           if (snapshot.connectionState == ConnectionState.done) {
//             return Scaffold(
//               backgroundColor: Colors.green,
//             );
//           }
//
//           // Otherwise, show something whilst waiting for initialization to complete
//           return Scaffold(
//             backgroundColor: Colors.yellow,
//           );
//         },
//       ),
//     );
//   }
